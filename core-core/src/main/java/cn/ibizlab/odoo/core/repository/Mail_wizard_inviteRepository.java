package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;

/**
 * 实体 [邀请向导] 存储对象
 */
public interface Mail_wizard_inviteRepository extends Repository<Mail_wizard_invite> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context);

    Mail_wizard_invite convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite domain , Mail_wizard_invite po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite convert2Domain( Mail_wizard_invite po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite domain) ;

}
