package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;

/**
 * 实体 [库存追溯确认] 存储对象
 */
public interface Stock_track_confirmationRepository extends Repository<Stock_track_confirmation> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context);

    Stock_track_confirmation convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation domain , Stock_track_confirmation po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation convert2Domain( Stock_track_confirmation po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation domain) ;

}
