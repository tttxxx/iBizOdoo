package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;


/**
 * 实体[Account_invoice_tax] 服务对象接口
 */
public interface IAccount_invoice_taxService{

    boolean update(Account_invoice_tax et) ;
    void updateBatch(List<Account_invoice_tax> list) ;
    Account_invoice_tax get(Integer key) ;
    boolean create(Account_invoice_tax et) ;
    void createBatch(List<Account_invoice_tax> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context) ;

}



