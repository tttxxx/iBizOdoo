package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_product_configuratorService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_product_configuratorOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_product_configuratorClientModel;

/**
 * 实体[销售产品配置器] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_product_configuratorServiceImpl implements ISale_product_configuratorService {

    @Autowired
    sale_product_configuratorOdooClient sale_product_configuratorOdooClient;


    @Override
    public boolean create(Sale_product_configurator et) {
        sale_product_configuratorClientModel clientModel = convert2Model(et,null);
		sale_product_configuratorOdooClient.create(clientModel);
        Sale_product_configurator rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_product_configurator> list){
    }

    @Override
    public boolean remove(Integer id) {
        sale_product_configuratorClientModel clientModel = new sale_product_configuratorClientModel();
        clientModel.setId(id);
		sale_product_configuratorOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Sale_product_configurator get(Integer id) {
        sale_product_configuratorClientModel clientModel = new sale_product_configuratorClientModel();
        clientModel.setId(id);
		sale_product_configuratorOdooClient.get(clientModel);
        Sale_product_configurator et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_product_configurator();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Sale_product_configurator et) {
        sale_product_configuratorClientModel clientModel = convert2Model(et,null);
		sale_product_configuratorOdooClient.update(clientModel);
        Sale_product_configurator rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_product_configurator> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context) {
        List<Sale_product_configurator> list = new ArrayList<Sale_product_configurator>();
        Page<sale_product_configuratorClientModel> clientModelList = sale_product_configuratorOdooClient.search(context);
        for(sale_product_configuratorClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_product_configurator>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_product_configuratorClientModel convert2Model(Sale_product_configurator domain , sale_product_configuratorClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_product_configuratorClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_template_id_textdirtyflag"))
                model.setProduct_template_id_text(domain.getProductTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("pricelist_id_textdirtyflag"))
                model.setPricelist_id_text(domain.getPricelistIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("product_template_iddirtyflag"))
                model.setProduct_template_id(domain.getProductTemplateId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_product_configurator convert2Domain( sale_product_configuratorClientModel model ,Sale_product_configurator domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_product_configurator();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_template_id_textDirtyFlag())
            domain.setProductTemplateIdText(model.getProduct_template_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPricelist_id_textDirtyFlag())
            domain.setPricelistIdText(model.getPricelist_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getProduct_template_idDirtyFlag())
            domain.setProductTemplateId(model.getProduct_template_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



