package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_custom_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_attribute_custom_valueOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_attribute_custom_valueClientModel;

/**
 * 实体[产品属性自定义值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attribute_custom_valueServiceImpl implements IProduct_attribute_custom_valueService {

    @Autowired
    product_attribute_custom_valueOdooClient product_attribute_custom_valueOdooClient;


    @Override
    public boolean create(Product_attribute_custom_value et) {
        product_attribute_custom_valueClientModel clientModel = convert2Model(et,null);
		product_attribute_custom_valueOdooClient.create(clientModel);
        Product_attribute_custom_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute_custom_value> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_attribute_custom_valueClientModel clientModel = new product_attribute_custom_valueClientModel();
        clientModel.setId(id);
		product_attribute_custom_valueOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_attribute_custom_value et) {
        product_attribute_custom_valueClientModel clientModel = convert2Model(et,null);
		product_attribute_custom_valueOdooClient.update(clientModel);
        Product_attribute_custom_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_attribute_custom_value> list){
    }

    @Override
    public Product_attribute_custom_value get(Integer id) {
        product_attribute_custom_valueClientModel clientModel = new product_attribute_custom_valueClientModel();
        clientModel.setId(id);
		product_attribute_custom_valueOdooClient.get(clientModel);
        Product_attribute_custom_value et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_attribute_custom_value();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute_custom_value> searchDefault(Product_attribute_custom_valueSearchContext context) {
        List<Product_attribute_custom_value> list = new ArrayList<Product_attribute_custom_value>();
        Page<product_attribute_custom_valueClientModel> clientModelList = product_attribute_custom_valueOdooClient.search(context);
        for(product_attribute_custom_valueClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_attribute_custom_value>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_attribute_custom_valueClientModel convert2Model(Product_attribute_custom_value domain , product_attribute_custom_valueClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_attribute_custom_valueClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("custom_valuedirtyflag"))
                model.setCustom_value(domain.getCustomValue());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("sale_order_line_id_textdirtyflag"))
                model.setSale_order_line_id_text(domain.getSaleOrderLineIdText());
            if((Boolean) domain.getExtensionparams().get("attribute_value_id_textdirtyflag"))
                model.setAttribute_value_id_text(domain.getAttributeValueIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("sale_order_line_iddirtyflag"))
                model.setSale_order_line_id(domain.getSaleOrderLineId());
            if((Boolean) domain.getExtensionparams().get("attribute_value_iddirtyflag"))
                model.setAttribute_value_id(domain.getAttributeValueId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_attribute_custom_value convert2Domain( product_attribute_custom_valueClientModel model ,Product_attribute_custom_value domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_attribute_custom_value();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCustom_valueDirtyFlag())
            domain.setCustomValue(model.getCustom_value());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSale_order_line_id_textDirtyFlag())
            domain.setSaleOrderLineIdText(model.getSale_order_line_id_text());
        if(model.getAttribute_value_id_textDirtyFlag())
            domain.setAttributeValueIdText(model.getAttribute_value_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getSale_order_line_idDirtyFlag())
            domain.setSaleOrderLineId(model.getSale_order_line_id());
        if(model.getAttribute_value_idDirtyFlag())
            domain.setAttributeValueId(model.getAttribute_value_id());
        return domain ;
    }

}

    



