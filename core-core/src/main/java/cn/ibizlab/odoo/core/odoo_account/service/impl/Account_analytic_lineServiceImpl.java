package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_analytic_lineClientModel;

/**
 * 实体[分析行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_lineServiceImpl implements IAccount_analytic_lineService {

    @Autowired
    account_analytic_lineOdooClient account_analytic_lineOdooClient;


    @Override
    public boolean create(Account_analytic_line et) {
        account_analytic_lineClientModel clientModel = convert2Model(et,null);
		account_analytic_lineOdooClient.create(clientModel);
        Account_analytic_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_line> list){
    }

    @Override
    public Account_analytic_line get(Integer id) {
        account_analytic_lineClientModel clientModel = new account_analytic_lineClientModel();
        clientModel.setId(id);
		account_analytic_lineOdooClient.get(clientModel);
        Account_analytic_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_analytic_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_analytic_lineClientModel clientModel = new account_analytic_lineClientModel();
        clientModel.setId(id);
		account_analytic_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_analytic_line et) {
        account_analytic_lineClientModel clientModel = convert2Model(et,null);
		account_analytic_lineOdooClient.update(clientModel);
        Account_analytic_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_analytic_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context) {
        List<Account_analytic_line> list = new ArrayList<Account_analytic_line>();
        Page<account_analytic_lineClientModel> clientModelList = account_analytic_lineOdooClient.search(context);
        for(account_analytic_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_analytic_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_analytic_lineClientModel convert2Model(Account_analytic_line domain , account_analytic_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_analytic_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("unit_amountdirtyflag"))
                model.setUnit_amount(domain.getUnitAmount());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("so_line_textdirtyflag"))
                model.setSo_line_text(domain.getSoLineText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("general_account_id_textdirtyflag"))
                model.setGeneral_account_id_text(domain.getGeneralAccountIdText());
            if((Boolean) domain.getExtensionparams().get("group_id_textdirtyflag"))
                model.setGroup_id_text(domain.getGroupIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("so_linedirtyflag"))
                model.setSo_line(domain.getSoLine());
            if((Boolean) domain.getExtensionparams().get("general_account_iddirtyflag"))
                model.setGeneral_account_id(domain.getGeneralAccountId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_analytic_line convert2Domain( account_analytic_lineClientModel model ,Account_analytic_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_analytic_line();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getUnit_amountDirtyFlag())
            domain.setUnitAmount(model.getUnit_amount());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getSo_line_textDirtyFlag())
            domain.setSoLineText(model.getSo_line_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getGeneral_account_id_textDirtyFlag())
            domain.setGeneralAccountIdText(model.getGeneral_account_id_text());
        if(model.getGroup_id_textDirtyFlag())
            domain.setGroupIdText(model.getGroup_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getSo_lineDirtyFlag())
            domain.setSoLine(model.getSo_line());
        if(model.getGeneral_account_idDirtyFlag())
            domain.setGeneralAccountId(model.getGeneral_account_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        return domain ;
    }

}

    



