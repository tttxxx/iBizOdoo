package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_common_journal_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_journal_reportSearchContext;

/**
 * 实体 [通用日记账报告] 存储对象
 */
public interface Account_common_journal_reportRepository extends Repository<Account_common_journal_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_common_journal_report> searchDefault(Account_common_journal_reportSearchContext context);

    Account_common_journal_report convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report domain , Account_common_journal_report po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report convert2Domain( Account_common_journal_report po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report domain) ;

}
