package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_employee] 对象
 */
public interface hr_employee {

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getAdditional_note();

    public void setAdditional_note(String additional_note);

    public Integer getAddress_home_id();

    public void setAddress_home_id(Integer address_home_id);

    public String getAddress_home_id_text();

    public void setAddress_home_id_text(String address_home_id_text);

    public Integer getAddress_id();

    public void setAddress_id(Integer address_id);

    public String getAddress_id_text();

    public void setAddress_id_text(String address_id_text);

    public String getAttendance_ids();

    public void setAttendance_ids(String attendance_ids);

    public String getAttendance_state();

    public void setAttendance_state(String attendance_state);

    public String getBadge_ids();

    public void setBadge_ids(String badge_ids);

    public Integer getBank_account_id();

    public void setBank_account_id(Integer bank_account_id);

    public String getBarcode();

    public void setBarcode(String barcode);

    public Timestamp getBirthday();

    public void setBirthday(Timestamp birthday);

    public String getCategory_ids();

    public void setCategory_ids(String category_ids);

    public String getCertificate();

    public void setCertificate(String certificate);

    public Integer getChildren();

    public void setChildren(Integer children);

    public String getChild_ids();

    public void setChild_ids(String child_ids);

    public Integer getCoach_id();

    public void setCoach_id(Integer coach_id);

    public String getCoach_id_text();

    public void setCoach_id_text(String coach_id_text);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Integer getContracts_count();

    public void setContracts_count(Integer contracts_count);

    public Integer getContract_id();

    public void setContract_id(Integer contract_id);

    public String getContract_ids();

    public void setContract_ids(String contract_ids);

    public String getContract_id_text();

    public void setContract_id_text(String contract_id_text);

    public Integer getCountry_id();

    public void setCountry_id(Integer country_id);

    public String getCountry_id_text();

    public void setCountry_id_text(String country_id_text);

    public Integer getCountry_of_birth();

    public void setCountry_of_birth(Integer country_of_birth);

    public String getCountry_of_birth_text();

    public void setCountry_of_birth_text(String country_of_birth_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrent_leave_id();

    public void setCurrent_leave_id(Integer current_leave_id);

    public String getCurrent_leave_id_text();

    public void setCurrent_leave_id_text(String current_leave_id_text);

    public String getCurrent_leave_state();

    public void setCurrent_leave_state(String current_leave_state);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDirect_badge_ids();

    public void setDirect_badge_ids(String direct_badge_ids);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmergency_contact();

    public void setEmergency_contact(String emergency_contact);

    public String getEmergency_phone();

    public void setEmergency_phone(String emergency_phone);

    public Integer getExpense_manager_id();

    public void setExpense_manager_id(Integer expense_manager_id);

    public String getExpense_manager_id_text();

    public void setExpense_manager_id_text(String expense_manager_id_text);

    public String getGender();

    public void setGender(String gender);

    public String getGoal_ids();

    public void setGoal_ids(String goal_ids);

    public String getGoogle_drive_link();

    public void setGoogle_drive_link(String google_drive_link);

    public String getHas_badges();

    public void setHas_badges(String has_badges);

    public Integer getId();

    public void setId(Integer id);

    public String getIdentification_id();

    public void setIdentification_id(String identification_id);

    public byte[] getImage();

    public void setImage(byte[] image);

    public byte[] getImage_medium();

    public void setImage_medium(byte[] image_medium);

    public byte[] getImage_small();

    public void setImage_small(byte[] image_small);

    public String getIs_absent_totay();

    public void setIs_absent_totay(String is_absent_totay);

    public String getIs_address_home_a_company();

    public void setIs_address_home_a_company(String is_address_home_a_company);

    public Integer getJob_id();

    public void setJob_id(Integer job_id);

    public String getJob_id_text();

    public void setJob_id_text(String job_id_text);

    public String getJob_title();

    public void setJob_title(String job_title);

    public Integer getKm_home_work();

    public void setKm_home_work(Integer km_home_work);

    public Integer getLast_attendance_id();

    public void setLast_attendance_id(Integer last_attendance_id);

    public Double getLeaves_count();

    public void setLeaves_count(Double leaves_count);

    public Timestamp getLeave_date_from();

    public void setLeave_date_from(Timestamp leave_date_from);

    public Timestamp getLeave_date_to();

    public void setLeave_date_to(Timestamp leave_date_to);

    public String getManager();

    public void setManager(String manager);

    public String getManual_attendance();

    public void setManual_attendance(String manual_attendance);

    public String getMarital();

    public void setMarital(String marital);

    public Timestamp getMedic_exam();

    public void setMedic_exam(Timestamp medic_exam);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getMobile_phone();

    public void setMobile_phone(String mobile_phone);

    public String getName();

    public void setName(String name);

    public String getNewly_hired_employee();

    public void setNewly_hired_employee(String newly_hired_employee);

    public String getNotes();

    public void setNotes(String notes);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public String getPassport_id();

    public void setPassport_id(String passport_id);

    public String getPermit_no();

    public void setPermit_no(String permit_no);

    public String getPin();

    public void setPin(String pin);

    public String getPlace_of_birth();

    public void setPlace_of_birth(String place_of_birth);

    public Double getRemaining_leaves();

    public void setRemaining_leaves(Double remaining_leaves);

    public Integer getResource_calendar_id();

    public void setResource_calendar_id(Integer resource_calendar_id);

    public String getResource_calendar_id_text();

    public void setResource_calendar_id_text(String resource_calendar_id_text);

    public Integer getResource_id();

    public void setResource_id(Integer resource_id);

    public String getResource_id_text();

    public void setResource_id_text(String resource_id_text);

    public String getShow_leaves();

    public void setShow_leaves(String show_leaves);

    public String getSinid();

    public void setSinid(String sinid);

    public Timestamp getSpouse_birthdate();

    public void setSpouse_birthdate(Timestamp spouse_birthdate);

    public String getSpouse_complete_name();

    public void setSpouse_complete_name(String spouse_complete_name);

    public String getSsnid();

    public void setSsnid(String ssnid);

    public String getStudy_field();

    public void setStudy_field(String study_field);

    public String getStudy_school();

    public void setStudy_school(String study_school);

    public String getTz();

    public void setTz(String tz);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getVehicle();

    public void setVehicle(String vehicle);

    public Timestamp getVisa_expire();

    public void setVisa_expire(Timestamp visa_expire);

    public String getVisa_no();

    public void setVisa_no(String visa_no);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public String getWork_email();

    public void setWork_email(String work_email);

    public String getWork_location();

    public void setWork_location(String work_location);

    public String getWork_phone();

    public void setWork_phone(String work_phone);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
