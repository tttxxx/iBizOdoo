package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;

/**
 * 实体 [在银行对账单导入创建日记账] 存储对象
 */
public interface Account_bank_statement_import_journal_creationRepository extends Repository<Account_bank_statement_import_journal_creation> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement_import_journal_creation> searchDefault(Account_bank_statement_import_journal_creationSearchContext context);

    Account_bank_statement_import_journal_creation convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation domain , Account_bank_statement_import_journal_creation po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation convert2Domain( Account_bank_statement_import_journal_creation po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation domain) ;

}
