package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.maintenance_stage;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
public interface maintenance_stageRepository{


    public maintenance_stage createPO() ;
        public void get(String id);

        public void create(maintenance_stage maintenance_stage);

        public void remove(String id);

        public void update(maintenance_stage maintenance_stage);

        public List<maintenance_stage> search();

        public void removeBatch(String id);

        public void updateBatch(maintenance_stage maintenance_stage);

        public void createBatch(maintenance_stage maintenance_stage);


}
