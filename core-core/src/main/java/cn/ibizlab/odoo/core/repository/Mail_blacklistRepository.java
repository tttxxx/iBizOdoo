package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;

/**
 * 实体 [邮件黑名单] 存储对象
 */
public interface Mail_blacklistRepository extends Repository<Mail_blacklist> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_blacklist> searchDefault(Mail_blacklistSearchContext context);

    Mail_blacklist convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist domain , Mail_blacklist po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist convert2Domain( Mail_blacklist po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist domain) ;

}
