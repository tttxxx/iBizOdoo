package cn.ibizlab.odoo.core.odoo_web_editor.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[web_editor_converter_test] 对象
 */
public class web_editor_converter_testClientModel implements Serializable{

    /**
     * Binary
     */
    public byte[] binary;

    @JsonIgnore
    public boolean binaryDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * Datetime
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp datetime;

    @JsonIgnore
    public boolean datetimeDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * Html
     */
    public String html;

    @JsonIgnore
    public boolean htmlDirtyFlag;
    
    /**
     * Char
     */
    public String ibizchar;

    @JsonIgnore
    public boolean ibizcharDirtyFlag;
    
    /**
     * 浮点数
     */
    public Double ibizfloat;

    @JsonIgnore
    public boolean ibizfloatDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Integer
     */
    public Integer integer;

    @JsonIgnore
    public boolean integerDirtyFlag;
    
    /**
     * Many2One
     */
    public Integer many2one;

    @JsonIgnore
    public boolean many2oneDirtyFlag;
    
    /**
     * Many2One
     */
    public String many2one_text;

    @JsonIgnore
    public boolean many2one_textDirtyFlag;
    
    /**
     * Numeric
     */
    public Double numeric;

    @JsonIgnore
    public boolean numericDirtyFlag;
    
    /**
     * Selection
     */
    public String selection;

    @JsonIgnore
    public boolean selectionDirtyFlag;
    
    /**
     * Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:
     */
    public String selection_str;

    @JsonIgnore
    public boolean selection_strDirtyFlag;
    
    /**
     * Text
     */
    public String text;

    @JsonIgnore
    public boolean textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [Binary]
     */
    @JsonProperty("binary")
    public byte[] getBinary(){
        return this.binary ;
    }

    /**
     * 设置 [Binary]
     */
    @JsonProperty("binary")
    public void setBinary(byte[]  binary){
        this.binary = binary ;
        this.binaryDirtyFlag = true ;
    }

     /**
     * 获取 [Binary]脏标记
     */
    @JsonIgnore
    public boolean getBinaryDirtyFlag(){
        return this.binaryDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [Datetime]
     */
    @JsonProperty("datetime")
    public Timestamp getDatetime(){
        return this.datetime ;
    }

    /**
     * 设置 [Datetime]
     */
    @JsonProperty("datetime")
    public void setDatetime(Timestamp  datetime){
        this.datetime = datetime ;
        this.datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [Datetime]脏标记
     */
    @JsonIgnore
    public boolean getDatetimeDirtyFlag(){
        return this.datetimeDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [Html]
     */
    @JsonProperty("html")
    public String getHtml(){
        return this.html ;
    }

    /**
     * 设置 [Html]
     */
    @JsonProperty("html")
    public void setHtml(String  html){
        this.html = html ;
        this.htmlDirtyFlag = true ;
    }

     /**
     * 获取 [Html]脏标记
     */
    @JsonIgnore
    public boolean getHtmlDirtyFlag(){
        return this.htmlDirtyFlag ;
    }   

    /**
     * 获取 [Char]
     */
    @JsonProperty("ibizchar")
    public String getIbizchar(){
        return this.ibizchar ;
    }

    /**
     * 设置 [Char]
     */
    @JsonProperty("ibizchar")
    public void setIbizchar(String  ibizchar){
        this.ibizchar = ibizchar ;
        this.ibizcharDirtyFlag = true ;
    }

     /**
     * 获取 [Char]脏标记
     */
    @JsonIgnore
    public boolean getIbizcharDirtyFlag(){
        return this.ibizcharDirtyFlag ;
    }   

    /**
     * 获取 [浮点数]
     */
    @JsonProperty("ibizfloat")
    public Double getIbizfloat(){
        return this.ibizfloat ;
    }

    /**
     * 设置 [浮点数]
     */
    @JsonProperty("ibizfloat")
    public void setIbizfloat(Double  ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.ibizfloatDirtyFlag = true ;
    }

     /**
     * 获取 [浮点数]脏标记
     */
    @JsonIgnore
    public boolean getIbizfloatDirtyFlag(){
        return this.ibizfloatDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Integer]
     */
    @JsonProperty("integer")
    public Integer getInteger(){
        return this.integer ;
    }

    /**
     * 设置 [Integer]
     */
    @JsonProperty("integer")
    public void setInteger(Integer  integer){
        this.integer = integer ;
        this.integerDirtyFlag = true ;
    }

     /**
     * 获取 [Integer]脏标记
     */
    @JsonIgnore
    public boolean getIntegerDirtyFlag(){
        return this.integerDirtyFlag ;
    }   

    /**
     * 获取 [Many2One]
     */
    @JsonProperty("many2one")
    public Integer getMany2one(){
        return this.many2one ;
    }

    /**
     * 设置 [Many2One]
     */
    @JsonProperty("many2one")
    public void setMany2one(Integer  many2one){
        this.many2one = many2one ;
        this.many2oneDirtyFlag = true ;
    }

     /**
     * 获取 [Many2One]脏标记
     */
    @JsonIgnore
    public boolean getMany2oneDirtyFlag(){
        return this.many2oneDirtyFlag ;
    }   

    /**
     * 获取 [Many2One]
     */
    @JsonProperty("many2one_text")
    public String getMany2one_text(){
        return this.many2one_text ;
    }

    /**
     * 设置 [Many2One]
     */
    @JsonProperty("many2one_text")
    public void setMany2one_text(String  many2one_text){
        this.many2one_text = many2one_text ;
        this.many2one_textDirtyFlag = true ;
    }

     /**
     * 获取 [Many2One]脏标记
     */
    @JsonIgnore
    public boolean getMany2one_textDirtyFlag(){
        return this.many2one_textDirtyFlag ;
    }   

    /**
     * 获取 [Numeric]
     */
    @JsonProperty("numeric")
    public Double getNumeric(){
        return this.numeric ;
    }

    /**
     * 设置 [Numeric]
     */
    @JsonProperty("numeric")
    public void setNumeric(Double  numeric){
        this.numeric = numeric ;
        this.numericDirtyFlag = true ;
    }

     /**
     * 获取 [Numeric]脏标记
     */
    @JsonIgnore
    public boolean getNumericDirtyFlag(){
        return this.numericDirtyFlag ;
    }   

    /**
     * 获取 [Selection]
     */
    @JsonProperty("selection")
    public String getSelection(){
        return this.selection ;
    }

    /**
     * 设置 [Selection]
     */
    @JsonProperty("selection")
    public void setSelection(String  selection){
        this.selection = selection ;
        this.selectionDirtyFlag = true ;
    }

     /**
     * 获取 [Selection]脏标记
     */
    @JsonIgnore
    public boolean getSelectionDirtyFlag(){
        return this.selectionDirtyFlag ;
    }   

    /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    @JsonProperty("selection_str")
    public String getSelection_str(){
        return this.selection_str ;
    }

    /**
     * 设置 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    @JsonProperty("selection_str")
    public void setSelection_str(String  selection_str){
        this.selection_str = selection_str ;
        this.selection_strDirtyFlag = true ;
    }

     /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]脏标记
     */
    @JsonIgnore
    public boolean getSelection_strDirtyFlag(){
        return this.selection_strDirtyFlag ;
    }   

    /**
     * 获取 [Text]
     */
    @JsonProperty("text")
    public String getText(){
        return this.text ;
    }

    /**
     * 设置 [Text]
     */
    @JsonProperty("text")
    public void setText(String  text){
        this.text = text ;
        this.textDirtyFlag = true ;
    }

     /**
     * 获取 [Text]脏标记
     */
    @JsonIgnore
    public boolean getTextDirtyFlag(){
        return this.textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("binary") instanceof Boolean)&& map.get("binary")!=null){
			//暂时忽略
			//this.setBinary(((String)map.get("binary")).getBytes("UTF-8"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("datetime") instanceof Boolean)&& map.get("datetime")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("datetime"));
   			this.setDatetime(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("html") instanceof Boolean)&& map.get("html")!=null){
			this.setHtml((String)map.get("html"));
		}
		if(!(map.get("char") instanceof Boolean)&& map.get("char")!=null){
			this.setIbizchar((String)map.get("char"));
		}
		if(!(map.get("float") instanceof Boolean)&& map.get("float")!=null){
			this.setIbizfloat((Double)map.get("float"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("integer") instanceof Boolean)&& map.get("integer")!=null){
			this.setInteger((Integer)map.get("integer"));
		}
		if(!(map.get("many2one") instanceof Boolean)&& map.get("many2one")!=null){
			Object[] objs = (Object[])map.get("many2one");
			if(objs.length > 0){
				this.setMany2one((Integer)objs[0]);
			}
		}
		if(!(map.get("many2one") instanceof Boolean)&& map.get("many2one")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("many2one");
			if(objs.length > 1){
				this.setMany2one_text((String)objs[1]);
			}
		}
		if(!(map.get("numeric") instanceof Boolean)&& map.get("numeric")!=null){
			this.setNumeric((Double)map.get("numeric"));
		}
		if(!(map.get("selection") instanceof Boolean)&& map.get("selection")!=null){
			this.setSelection((String)map.get("selection"));
		}
		if(!(map.get("selection_str") instanceof Boolean)&& map.get("selection_str")!=null){
			this.setSelection_str((String)map.get("selection_str"));
		}
		if(!(map.get("text") instanceof Boolean)&& map.get("text")!=null){
			this.setText((String)map.get("text"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getBinary()!=null&&this.getBinaryDirtyFlag()){
			//暂不支持binary类型binary
		}else if(this.getBinaryDirtyFlag()){
			map.put("binary",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDatetime()!=null&&this.getDatetimeDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDatetime());
			map.put("datetime",datetimeStr);
		}else if(this.getDatetimeDirtyFlag()){
			map.put("datetime",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getHtml()!=null&&this.getHtmlDirtyFlag()){
			map.put("html",this.getHtml());
		}else if(this.getHtmlDirtyFlag()){
			map.put("html",false);
		}
		if(this.getIbizchar()!=null&&this.getIbizcharDirtyFlag()){
			map.put("char",this.getIbizchar());
		}else if(this.getIbizcharDirtyFlag()){
			map.put("char",false);
		}
		if(this.getIbizfloat()!=null&&this.getIbizfloatDirtyFlag()){
			map.put("float",this.getIbizfloat());
		}else if(this.getIbizfloatDirtyFlag()){
			map.put("float",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInteger()!=null&&this.getIntegerDirtyFlag()){
			map.put("integer",this.getInteger());
		}else if(this.getIntegerDirtyFlag()){
			map.put("integer",false);
		}
		if(this.getMany2one()!=null&&this.getMany2oneDirtyFlag()){
			map.put("many2one",this.getMany2one());
		}else if(this.getMany2oneDirtyFlag()){
			map.put("many2one",false);
		}
		if(this.getMany2one_text()!=null&&this.getMany2one_textDirtyFlag()){
			//忽略文本外键many2one_text
		}else if(this.getMany2one_textDirtyFlag()){
			map.put("many2one",false);
		}
		if(this.getNumeric()!=null&&this.getNumericDirtyFlag()){
			map.put("numeric",this.getNumeric());
		}else if(this.getNumericDirtyFlag()){
			map.put("numeric",false);
		}
		if(this.getSelection()!=null&&this.getSelectionDirtyFlag()){
			map.put("selection",this.getSelection());
		}else if(this.getSelectionDirtyFlag()){
			map.put("selection",false);
		}
		if(this.getSelection_str()!=null&&this.getSelection_strDirtyFlag()){
			map.put("selection_str",this.getSelection_str());
		}else if(this.getSelection_strDirtyFlag()){
			map.put("selection_str",false);
		}
		if(this.getText()!=null&&this.getTextDirtyFlag()){
			map.put("text",this.getText());
		}else if(this.getTextDirtyFlag()){
			map.put("text",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
