package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;


/**
 * 实体[Res_users] 服务对象接口
 */
public interface IRes_usersService{

    boolean update(Res_users et) ;
    void updateBatch(List<Res_users> list) ;
    boolean create(Res_users et) ;
    void createBatch(List<Res_users> list) ;
    Res_users get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Res_users> searchDefault(Res_usersSearchContext context) ;

}



