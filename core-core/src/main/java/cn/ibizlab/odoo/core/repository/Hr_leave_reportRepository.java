package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;

/**
 * 实体 [请假摘要/报告] 存储对象
 */
public interface Hr_leave_reportRepository extends Repository<Hr_leave_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_leave_report> searchDefault(Hr_leave_reportSearchContext context);

    Hr_leave_report convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report domain , Hr_leave_report po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report convert2Domain( Hr_leave_report po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report domain) ;

}
