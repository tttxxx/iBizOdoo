package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;


/**
 * 实体[Product_packaging] 服务对象接口
 */
public interface IProduct_packagingService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Product_packaging et) ;
    void createBatch(List<Product_packaging> list) ;
    Product_packaging get(Integer key) ;
    boolean update(Product_packaging et) ;
    void updateBatch(List<Product_packaging> list) ;
    Page<Product_packaging> searchDefault(Product_packagingSearchContext context) ;

}



