package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [website_page] 对象
 */
public interface website_page {

    public String getActive();

    public void setActive(String active);

    public String getArch();

    public void setArch(String arch);

    public String getArch_base();

    public void setArch_base(String arch_base);

    public String getArch_db();

    public void setArch_db(String arch_db);

    public String getArch_fs();

    public void setArch_fs(String arch_fs);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getCustomize_show();

    public void setCustomize_show(String customize_show);

    public Timestamp getDate_publish();

    public void setDate_publish(Timestamp date_publish);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getField_parent();

    public void setField_parent(String field_parent);

    public Integer getFirst_page_id();

    public void setFirst_page_id(Integer first_page_id);

    public String getFirst_page_id_text();

    public void setFirst_page_id_text(String first_page_id_text);

    public String getGroups_id();

    public void setGroups_id(String groups_id);

    public String getHeader_color();

    public void setHeader_color(String header_color);

    public String getHeader_overlay();

    public void setHeader_overlay(String header_overlay);

    public Integer getId();

    public void setId(Integer id);

    public String getInherit_children_ids();

    public void setInherit_children_ids(String inherit_children_ids);

    public String getIs_homepage();

    public void setIs_homepage(String is_homepage);

    public String getIs_published();

    public void setIs_published(String is_published);

    public String getIs_seo_optimized();

    public void setIs_seo_optimized(String is_seo_optimized);

    public String getIs_visible();

    public void setIs_visible(String is_visible);

    public String getKey();

    public void setKey(String key);

    public String getMenu_ids();

    public void setMenu_ids(String menu_ids);

    public String getMode();

    public void setMode(String mode);

    public String getModel();

    public void setModel(String model);

    public String getModel_ids();

    public void setModel_ids(String model_ids);

    public String getName();

    public void setName(String name);

    public String getPage_ids();

    public void setPage_ids(String page_ids);

    public Integer getPriority();

    public void setPriority(Integer priority);

    public String getType();

    public void setType(String type);

    public String getUrl();

    public void setUrl(String url);

    public String getWebsite_indexed();

    public void setWebsite_indexed(String website_indexed);

    public String getWebsite_meta_description();

    public void setWebsite_meta_description(String website_meta_description);

    public String getWebsite_meta_keywords();

    public void setWebsite_meta_keywords(String website_meta_keywords);

    public String getWebsite_meta_og_img();

    public void setWebsite_meta_og_img(String website_meta_og_img);

    public String getWebsite_meta_title();

    public void setWebsite_meta_title(String website_meta_title);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public String getXml_id();

    public void setXml_id(String xml_id);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
