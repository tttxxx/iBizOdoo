package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;


/**
 * 实体[Product_price_history] 服务对象接口
 */
public interface IProduct_price_historyService{

    boolean create(Product_price_history et) ;
    void createBatch(List<Product_price_history> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_price_history get(Integer key) ;
    boolean update(Product_price_history et) ;
    void updateBatch(List<Product_price_history> list) ;
    Page<Product_price_history> searchDefault(Product_price_historySearchContext context) ;

}



