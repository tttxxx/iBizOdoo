package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;

/**
 * 实体 [银行] 存储对象
 */
public interface Res_bankRepository extends Repository<Res_bank> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_bank> searchDefault(Res_bankSearchContext context);

    Res_bank convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_bank domain , Res_bank po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_bank convert2Domain( Res_bank po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_bank domain) ;

}
