package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_levelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_package_levelOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_package_levelClientModel;

/**
 * 实体[库存包装层级] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_package_levelServiceImpl implements IStock_package_levelService {

    @Autowired
    stock_package_levelOdooClient stock_package_levelOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_package_levelClientModel clientModel = new stock_package_levelClientModel();
        clientModel.setId(id);
		stock_package_levelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_package_level get(Integer id) {
        stock_package_levelClientModel clientModel = new stock_package_levelClientModel();
        clientModel.setId(id);
		stock_package_levelOdooClient.get(clientModel);
        Stock_package_level et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_package_level();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_package_level et) {
        stock_package_levelClientModel clientModel = convert2Model(et,null);
		stock_package_levelOdooClient.create(clientModel);
        Stock_package_level rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_package_level> list){
    }

    @Override
    public boolean update(Stock_package_level et) {
        stock_package_levelClientModel clientModel = convert2Model(et,null);
		stock_package_levelOdooClient.update(clientModel);
        Stock_package_level rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_package_level> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_package_level> searchDefault(Stock_package_levelSearchContext context) {
        List<Stock_package_level> list = new ArrayList<Stock_package_level>();
        Page<stock_package_levelClientModel> clientModelList = stock_package_levelOdooClient.search(context);
        for(stock_package_levelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_package_level>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_package_levelClientModel convert2Model(Stock_package_level domain , stock_package_levelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_package_levelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("move_idsdirtyflag"))
                model.setMove_ids(domain.getMoveIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("show_lots_textdirtyflag"))
                model.setShow_lots_text(domain.getShowLotsText());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("is_donedirtyflag"))
                model.setIs_done(domain.getIsDone());
            if((Boolean) domain.getExtensionparams().get("show_lots_m2odirtyflag"))
                model.setShow_lots_m2o(domain.getShowLotsM2o());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("is_fresh_packagedirtyflag"))
                model.setIs_fresh_package(domain.getIsFreshPackage());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("picking_id_textdirtyflag"))
                model.setPicking_id_text(domain.getPickingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("package_id_textdirtyflag"))
                model.setPackage_id_text(domain.getPackageIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("picking_source_locationdirtyflag"))
                model.setPicking_source_location(domain.getPickingSourceLocation());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("picking_iddirtyflag"))
                model.setPicking_id(domain.getPickingId());
            if((Boolean) domain.getExtensionparams().get("package_iddirtyflag"))
                model.setPackage_id(domain.getPackageId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_package_level convert2Domain( stock_package_levelClientModel model ,Stock_package_level domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_package_level();
        }

        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getMove_idsDirtyFlag())
            domain.setMoveIds(model.getMove_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getShow_lots_textDirtyFlag())
            domain.setShowLotsText(model.getShow_lots_text());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIs_doneDirtyFlag())
            domain.setIsDone(model.getIs_done());
        if(model.getShow_lots_m2oDirtyFlag())
            domain.setShowLotsM2o(model.getShow_lots_m2o());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIs_fresh_packageDirtyFlag())
            domain.setIsFreshPackage(model.getIs_fresh_package());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPicking_id_textDirtyFlag())
            domain.setPickingIdText(model.getPicking_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPackage_id_textDirtyFlag())
            domain.setPackageIdText(model.getPackage_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPicking_source_locationDirtyFlag())
            domain.setPickingSourceLocation(model.getPicking_source_location());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPicking_idDirtyFlag())
            domain.setPickingId(model.getPicking_id());
        if(model.getPackage_idDirtyFlag())
            domain.setPackageId(model.getPackage_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        return domain ;
    }

}

    



