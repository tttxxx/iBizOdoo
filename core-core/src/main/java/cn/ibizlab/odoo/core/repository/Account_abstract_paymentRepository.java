package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;

/**
 * 实体 [包含在允许登记付款的模块之间共享的逻辑] 存储对象
 */
public interface Account_abstract_paymentRepository extends Repository<Account_abstract_payment> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_abstract_payment> searchDefault(Account_abstract_paymentSearchContext context);

    Account_abstract_payment convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment domain , Account_abstract_payment po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment convert2Domain( Account_abstract_payment po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment domain) ;

}
