package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_input_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_user_input_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_user_input_lineClientModel;

/**
 * 实体[调查用户输入明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_user_input_lineServiceImpl implements ISurvey_user_input_lineService {

    @Autowired
    survey_user_input_lineOdooClient survey_user_input_lineOdooClient;


    @Override
    public boolean update(Survey_user_input_line et) {
        survey_user_input_lineClientModel clientModel = convert2Model(et,null);
		survey_user_input_lineOdooClient.update(clientModel);
        Survey_user_input_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_user_input_line> list){
    }

    @Override
    public Survey_user_input_line get(Integer id) {
        survey_user_input_lineClientModel clientModel = new survey_user_input_lineClientModel();
        clientModel.setId(id);
		survey_user_input_lineOdooClient.get(clientModel);
        Survey_user_input_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_user_input_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Survey_user_input_line et) {
        survey_user_input_lineClientModel clientModel = convert2Model(et,null);
		survey_user_input_lineOdooClient.create(clientModel);
        Survey_user_input_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_user_input_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        survey_user_input_lineClientModel clientModel = new survey_user_input_lineClientModel();
        clientModel.setId(id);
		survey_user_input_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context) {
        List<Survey_user_input_line> list = new ArrayList<Survey_user_input_line>();
        Page<survey_user_input_lineClientModel> clientModelList = survey_user_input_lineOdooClient.search(context);
        for(survey_user_input_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_user_input_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_user_input_lineClientModel convert2Model(Survey_user_input_line domain , survey_user_input_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_user_input_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("value_datedirtyflag"))
                model.setValue_date(domain.getValueDate());
            if((Boolean) domain.getExtensionparams().get("value_free_textdirtyflag"))
                model.setValue_free_text(domain.getValueFreeText());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("date_createdirtyflag"))
                model.setDate_create(domain.getDateCreate());
            if((Boolean) domain.getExtensionparams().get("skippeddirtyflag"))
                model.setSkipped(domain.getSkipped());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("quizz_markdirtyflag"))
                model.setQuizz_mark(domain.getQuizzMark());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("answer_typedirtyflag"))
                model.setAnswer_type(domain.getAnswerType());
            if((Boolean) domain.getExtensionparams().get("value_textdirtyflag"))
                model.setValue_text(domain.getValueText());
            if((Boolean) domain.getExtensionparams().get("value_numberdirtyflag"))
                model.setValue_number(domain.getValueNumber());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("page_iddirtyflag"))
                model.setPage_id(domain.getPageId());
            if((Boolean) domain.getExtensionparams().get("user_input_iddirtyflag"))
                model.setUser_input_id(domain.getUserInputId());
            if((Boolean) domain.getExtensionparams().get("value_suggesteddirtyflag"))
                model.setValue_suggested(domain.getValueSuggested());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("value_suggested_rowdirtyflag"))
                model.setValue_suggested_row(domain.getValueSuggestedRow());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("question_iddirtyflag"))
                model.setQuestion_id(domain.getQuestionId());
            if((Boolean) domain.getExtensionparams().get("survey_iddirtyflag"))
                model.setSurvey_id(domain.getSurveyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_user_input_line convert2Domain( survey_user_input_lineClientModel model ,Survey_user_input_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_user_input_line();
        }

        if(model.getValue_dateDirtyFlag())
            domain.setValueDate(model.getValue_date());
        if(model.getValue_free_textDirtyFlag())
            domain.setValueFreeText(model.getValue_free_text());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDate_createDirtyFlag())
            domain.setDateCreate(model.getDate_create());
        if(model.getSkippedDirtyFlag())
            domain.setSkipped(model.getSkipped());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getQuizz_markDirtyFlag())
            domain.setQuizzMark(model.getQuizz_mark());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAnswer_typeDirtyFlag())
            domain.setAnswerType(model.getAnswer_type());
        if(model.getValue_textDirtyFlag())
            domain.setValueText(model.getValue_text());
        if(model.getValue_numberDirtyFlag())
            domain.setValueNumber(model.getValue_number());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPage_idDirtyFlag())
            domain.setPageId(model.getPage_id());
        if(model.getUser_input_idDirtyFlag())
            domain.setUserInputId(model.getUser_input_id());
        if(model.getValue_suggestedDirtyFlag())
            domain.setValueSuggested(model.getValue_suggested());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getValue_suggested_rowDirtyFlag())
            domain.setValueSuggestedRow(model.getValue_suggested_row());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getQuestion_idDirtyFlag())
            domain.setQuestionId(model.getQuestion_id());
        if(model.getSurvey_idDirtyFlag())
            domain.setSurveyId(model.getSurvey_id());
        return domain ;
    }

}

    



