package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iutm_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_mixin] 服务对象接口
 */
public interface Iutm_mixinClientService{

    public Iutm_mixin createModel() ;

    public void createBatch(List<Iutm_mixin> utm_mixins);

    public void removeBatch(List<Iutm_mixin> utm_mixins);

    public void remove(Iutm_mixin utm_mixin);

    public void updateBatch(List<Iutm_mixin> utm_mixins);

    public void create(Iutm_mixin utm_mixin);

    public void get(Iutm_mixin utm_mixin);

    public Page<Iutm_mixin> search(SearchContext context);

    public void update(Iutm_mixin utm_mixin);

    public Page<Iutm_mixin> select(SearchContext context);

}
