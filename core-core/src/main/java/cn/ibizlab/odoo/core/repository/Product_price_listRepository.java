package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;

/**
 * 实体 [基于价格列表版本的单位产品价格] 存储对象
 */
public interface Product_price_listRepository extends Repository<Product_price_list> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_price_list> searchDefault(Product_price_listSearchContext context);

    Product_price_list convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list domain , Product_price_list po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list convert2Domain( Product_price_list po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list domain) ;

}
