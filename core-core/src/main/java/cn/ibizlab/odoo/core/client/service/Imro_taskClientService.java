package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_task;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_task] 服务对象接口
 */
public interface Imro_taskClientService{

    public Imro_task createModel() ;

    public void updateBatch(List<Imro_task> mro_tasks);

    public void createBatch(List<Imro_task> mro_tasks);

    public Page<Imro_task> search(SearchContext context);

    public void get(Imro_task mro_task);

    public void create(Imro_task mro_task);

    public void update(Imro_task mro_task);

    public void remove(Imro_task mro_task);

    public void removeBatch(List<Imro_task> mro_tasks);

    public Page<Imro_task> select(SearchContext context);

}
