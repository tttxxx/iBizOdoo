package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_tokenService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_payment.client.payment_tokenOdooClient;
import cn.ibizlab.odoo.core.odoo_payment.clientmodel.payment_tokenClientModel;

/**
 * 实体[付款令牌] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_tokenServiceImpl implements IPayment_tokenService {

    @Autowired
    payment_tokenOdooClient payment_tokenOdooClient;


    @Override
    public boolean create(Payment_token et) {
        payment_tokenClientModel clientModel = convert2Model(et,null);
		payment_tokenOdooClient.create(clientModel);
        Payment_token rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_token> list){
    }

    @Override
    public boolean update(Payment_token et) {
        payment_tokenClientModel clientModel = convert2Model(et,null);
		payment_tokenOdooClient.update(clientModel);
        Payment_token rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Payment_token> list){
    }

    @Override
    public boolean remove(Integer id) {
        payment_tokenClientModel clientModel = new payment_tokenClientModel();
        clientModel.setId(id);
		payment_tokenOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Payment_token get(Integer id) {
        payment_tokenClientModel clientModel = new payment_tokenClientModel();
        clientModel.setId(id);
		payment_tokenOdooClient.get(clientModel);
        Payment_token et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Payment_token();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_token> searchDefault(Payment_tokenSearchContext context) {
        List<Payment_token> list = new ArrayList<Payment_token>();
        Page<payment_tokenClientModel> clientModelList = payment_tokenOdooClient.search(context);
        for(payment_tokenClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Payment_token>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public payment_tokenClientModel convert2Model(Payment_token domain , payment_tokenClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new payment_tokenClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("acquirer_refdirtyflag"))
                model.setAcquirer_ref(domain.getAcquirerRef());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("verifieddirtyflag"))
                model.setVerified(domain.getVerified());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("short_namedirtyflag"))
                model.setShort_name(domain.getShortName());
            if((Boolean) domain.getExtensionparams().get("payment_idsdirtyflag"))
                model.setPayment_ids(domain.getPaymentIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("acquirer_id_textdirtyflag"))
                model.setAcquirer_id_text(domain.getAcquirerIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("acquirer_iddirtyflag"))
                model.setAcquirer_id(domain.getAcquirerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Payment_token convert2Domain( payment_tokenClientModel model ,Payment_token domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Payment_token();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAcquirer_refDirtyFlag())
            domain.setAcquirerRef(model.getAcquirer_ref());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getVerifiedDirtyFlag())
            domain.setVerified(model.getVerified());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getShort_nameDirtyFlag())
            domain.setShortName(model.getShort_name());
        if(model.getPayment_idsDirtyFlag())
            domain.setPaymentIds(model.getPayment_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAcquirer_id_textDirtyFlag())
            domain.setAcquirerIdText(model.getAcquirer_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAcquirer_idDirtyFlag())
            domain.setAcquirerId(model.getAcquirer_id());
        return domain ;
    }

}

    



