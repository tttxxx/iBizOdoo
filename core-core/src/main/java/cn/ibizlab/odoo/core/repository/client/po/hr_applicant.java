package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_applicant] 对象
 */
public interface hr_applicant {

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getAttachment_ids();

    public void setAttachment_ids(String attachment_ids);

    public Integer getAttachment_number();

    public void setAttachment_number(Integer attachment_number);

    public Timestamp getAvailability();

    public void setAvailability(Timestamp availability);

    public Integer getCampaign_id();

    public void setCampaign_id(Integer campaign_id);

    public String getCampaign_id_text();

    public void setCampaign_id_text(String campaign_id_text);

    public String getCateg_ids();

    public void setCateg_ids(String categ_ids);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_closed();

    public void setDate_closed(Timestamp date_closed);

    public Timestamp getDate_last_stage_update();

    public void setDate_last_stage_update(Timestamp date_last_stage_update);

    public Timestamp getDate_open();

    public void setDate_open(Timestamp date_open);

    public Double getDay_close();

    public void setDay_close(Double day_close);

    public Double getDay_open();

    public void setDay_open(Double day_open);

    public Double getDelay_close();

    public void setDelay_close(Double delay_close);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail_cc();

    public void setEmail_cc(String email_cc);

    public String getEmail_from();

    public void setEmail_from(String email_from);

    public String getEmployee_name();

    public void setEmployee_name(String employee_name);

    public Integer getEmp_id();

    public void setEmp_id(Integer emp_id);

    public String getEmp_id_text();

    public void setEmp_id_text(String emp_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Integer getJob_id();

    public void setJob_id(Integer job_id);

    public String getJob_id_text();

    public void setJob_id_text(String job_id_text);

    public String getKanban_state();

    public void setKanban_state(String kanban_state);

    public Integer getLast_stage_id();

    public void setLast_stage_id(Integer last_stage_id);

    public String getLast_stage_id_text();

    public void setLast_stage_id_text(String last_stage_id_text);

    public String getLegend_blocked();

    public void setLegend_blocked(String legend_blocked);

    public String getLegend_done();

    public void setLegend_done(String legend_done);

    public String getLegend_normal();

    public void setLegend_normal(String legend_normal);

    public Integer getMedium_id();

    public void setMedium_id(Integer medium_id);

    public String getMedium_id_text();

    public void setMedium_id_text(String medium_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getPartner_mobile();

    public void setPartner_mobile(String partner_mobile);

    public String getPartner_name();

    public void setPartner_name(String partner_name);

    public String getPartner_phone();

    public void setPartner_phone(String partner_phone);

    public String getPriority();

    public void setPriority(String priority);

    public Double getProbability();

    public void setProbability(Double probability);

    public String getReference();

    public void setReference(String reference);

    public Double getSalary_expected();

    public void setSalary_expected(Double salary_expected);

    public String getSalary_expected_extra();

    public void setSalary_expected_extra(String salary_expected_extra);

    public Double getSalary_proposed();

    public void setSalary_proposed(Double salary_proposed);

    public String getSalary_proposed_extra();

    public void setSalary_proposed_extra(String salary_proposed_extra);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public Integer getStage_id();

    public void setStage_id(Integer stage_id);

    public String getStage_id_text();

    public void setStage_id_text(String stage_id_text);

    public Integer getType_id();

    public void setType_id(Integer type_id);

    public String getType_id_text();

    public void setType_id_text(String type_id_text);

    public String getUser_email();

    public void setUser_email(String user_email);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
