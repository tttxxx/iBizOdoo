package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;

/**
 * 实体 [采购订单行] 存储对象
 */
public interface Purchase_order_lineRepository extends Repository<Purchase_order_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context);

    Purchase_order_line convert2PO(cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line domain , Purchase_order_line po) ;

    cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line convert2Domain( Purchase_order_line po ,cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line domain) ;

}
