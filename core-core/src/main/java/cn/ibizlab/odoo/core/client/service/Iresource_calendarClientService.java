package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iresource_calendar;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_calendar] 服务对象接口
 */
public interface Iresource_calendarClientService{

    public Iresource_calendar createModel() ;

    public void update(Iresource_calendar resource_calendar);

    public void removeBatch(List<Iresource_calendar> resource_calendars);

    public void remove(Iresource_calendar resource_calendar);

    public void get(Iresource_calendar resource_calendar);

    public void createBatch(List<Iresource_calendar> resource_calendars);

    public void updateBatch(List<Iresource_calendar> resource_calendars);

    public void create(Iresource_calendar resource_calendar);

    public Page<Iresource_calendar> search(SearchContext context);

    public Page<Iresource_calendar> select(SearchContext context);

}
