package cn.ibizlab.odoo.core.odoo_asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_assetService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_asset.client.asset_assetOdooClient;
import cn.ibizlab.odoo.core.odoo_asset.clientmodel.asset_assetClientModel;

/**
 * 实体[Asset] 服务对象接口实现
 */
@Slf4j
@Service
public class Asset_assetServiceImpl implements IAsset_assetService {

    @Autowired
    asset_assetOdooClient asset_assetOdooClient;


    @Override
    public boolean update(Asset_asset et) {
        asset_assetClientModel clientModel = convert2Model(et,null);
		asset_assetOdooClient.update(clientModel);
        Asset_asset rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Asset_asset> list){
    }

    @Override
    public Asset_asset get(Integer id) {
        asset_assetClientModel clientModel = new asset_assetClientModel();
        clientModel.setId(id);
		asset_assetOdooClient.get(clientModel);
        Asset_asset et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Asset_asset();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        asset_assetClientModel clientModel = new asset_assetClientModel();
        clientModel.setId(id);
		asset_assetOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Asset_asset et) {
        asset_assetClientModel clientModel = convert2Model(et,null);
		asset_assetOdooClient.create(clientModel);
        Asset_asset rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Asset_asset> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Asset_asset> searchDefault(Asset_assetSearchContext context) {
        List<Asset_asset> list = new ArrayList<Asset_asset>();
        Page<asset_assetClientModel> clientModelList = asset_assetOdooClient.search(context);
        for(asset_assetClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Asset_asset>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public asset_assetClientModel convert2Model(Asset_asset domain , asset_assetClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new asset_assetClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("positiondirtyflag"))
                model.setPosition(domain.getPosition());
            if((Boolean) domain.getExtensionparams().get("meter_idsdirtyflag"))
                model.setMeter_ids(domain.getMeterIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("maintenance_datedirtyflag"))
                model.setMaintenance_date(domain.getMaintenanceDate());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("property_stock_assetdirtyflag"))
                model.setProperty_stock_asset(domain.getPropertyStockAsset());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("asset_numberdirtyflag"))
                model.setAsset_number(domain.getAssetNumber());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("serialdirtyflag"))
                model.setSerial(domain.getSerial());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("purchase_datedirtyflag"))
                model.setPurchase_date(domain.getPurchaseDate());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("category_idsdirtyflag"))
                model.setCategory_ids(domain.getCategoryIds());
            if((Boolean) domain.getExtensionparams().get("warranty_start_datedirtyflag"))
                model.setWarranty_start_date(domain.getWarrantyStartDate());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("warranty_end_datedirtyflag"))
                model.setWarranty_end_date(domain.getWarrantyEndDate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("criticalitydirtyflag"))
                model.setCriticality(domain.getCriticality());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("mro_countdirtyflag"))
                model.setMro_count(domain.getMroCount());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("warehouse_state_id_textdirtyflag"))
                model.setWarehouse_state_id_text(domain.getWarehouseStateIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("maintenance_state_colordirtyflag"))
                model.setMaintenance_state_color(domain.getMaintenanceStateColor());
            if((Boolean) domain.getExtensionparams().get("accounting_state_id_textdirtyflag"))
                model.setAccounting_state_id_text(domain.getAccountingStateIdText());
            if((Boolean) domain.getExtensionparams().get("manufacture_state_id_textdirtyflag"))
                model.setManufacture_state_id_text(domain.getManufactureStateIdText());
            if((Boolean) domain.getExtensionparams().get("finance_state_id_textdirtyflag"))
                model.setFinance_state_id_text(domain.getFinanceStateIdText());
            if((Boolean) domain.getExtensionparams().get("maintenance_state_id_textdirtyflag"))
                model.setMaintenance_state_id_text(domain.getMaintenanceStateIdText());
            if((Boolean) domain.getExtensionparams().get("manufacturer_id_textdirtyflag"))
                model.setManufacturer_id_text(domain.getManufacturerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("vendor_id_textdirtyflag"))
                model.setVendor_id_text(domain.getVendorIdText());
            if((Boolean) domain.getExtensionparams().get("manufacture_state_iddirtyflag"))
                model.setManufacture_state_id(domain.getManufactureStateId());
            if((Boolean) domain.getExtensionparams().get("accounting_state_iddirtyflag"))
                model.setAccounting_state_id(domain.getAccountingStateId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("vendor_iddirtyflag"))
                model.setVendor_id(domain.getVendorId());
            if((Boolean) domain.getExtensionparams().get("warehouse_state_iddirtyflag"))
                model.setWarehouse_state_id(domain.getWarehouseStateId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("finance_state_iddirtyflag"))
                model.setFinance_state_id(domain.getFinanceStateId());
            if((Boolean) domain.getExtensionparams().get("maintenance_state_iddirtyflag"))
                model.setMaintenance_state_id(domain.getMaintenanceStateId());
            if((Boolean) domain.getExtensionparams().get("manufacturer_iddirtyflag"))
                model.setManufacturer_id(domain.getManufacturerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Asset_asset convert2Domain( asset_assetClientModel model ,Asset_asset domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Asset_asset();
        }

        if(model.getPositionDirtyFlag())
            domain.setPosition(model.getPosition());
        if(model.getMeter_idsDirtyFlag())
            domain.setMeterIds(model.getMeter_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getMaintenance_dateDirtyFlag())
            domain.setMaintenanceDate(model.getMaintenance_date());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getProperty_stock_assetDirtyFlag())
            domain.setPropertyStockAsset(model.getProperty_stock_asset());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getAsset_numberDirtyFlag())
            domain.setAssetNumber(model.getAsset_number());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getSerialDirtyFlag())
            domain.setSerial(model.getSerial());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getPurchase_dateDirtyFlag())
            domain.setPurchaseDate(model.getPurchase_date());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCategory_idsDirtyFlag())
            domain.setCategoryIds(model.getCategory_ids());
        if(model.getWarranty_start_dateDirtyFlag())
            domain.setWarrantyStartDate(model.getWarranty_start_date());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWarranty_end_dateDirtyFlag())
            domain.setWarrantyEndDate(model.getWarranty_end_date());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCriticalityDirtyFlag())
            domain.setCriticality(model.getCriticality());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMro_countDirtyFlag())
            domain.setMroCount(model.getMro_count());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getWarehouse_state_id_textDirtyFlag())
            domain.setWarehouseStateIdText(model.getWarehouse_state_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMaintenance_state_colorDirtyFlag())
            domain.setMaintenanceStateColor(model.getMaintenance_state_color());
        if(model.getAccounting_state_id_textDirtyFlag())
            domain.setAccountingStateIdText(model.getAccounting_state_id_text());
        if(model.getManufacture_state_id_textDirtyFlag())
            domain.setManufactureStateIdText(model.getManufacture_state_id_text());
        if(model.getFinance_state_id_textDirtyFlag())
            domain.setFinanceStateIdText(model.getFinance_state_id_text());
        if(model.getMaintenance_state_id_textDirtyFlag())
            domain.setMaintenanceStateIdText(model.getMaintenance_state_id_text());
        if(model.getManufacturer_id_textDirtyFlag())
            domain.setManufacturerIdText(model.getManufacturer_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getVendor_id_textDirtyFlag())
            domain.setVendorIdText(model.getVendor_id_text());
        if(model.getManufacture_state_idDirtyFlag())
            domain.setManufactureStateId(model.getManufacture_state_id());
        if(model.getAccounting_state_idDirtyFlag())
            domain.setAccountingStateId(model.getAccounting_state_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getVendor_idDirtyFlag())
            domain.setVendorId(model.getVendor_id());
        if(model.getWarehouse_state_idDirtyFlag())
            domain.setWarehouseStateId(model.getWarehouse_state_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getFinance_state_idDirtyFlag())
            domain.setFinanceStateId(model.getFinance_state_id());
        if(model.getMaintenance_state_idDirtyFlag())
            domain.setMaintenanceStateId(model.getMaintenance_state_id());
        if(model.getManufacturer_idDirtyFlag())
            domain.setManufacturerId(model.getManufacturer_id());
        return domain ;
    }

}

    



