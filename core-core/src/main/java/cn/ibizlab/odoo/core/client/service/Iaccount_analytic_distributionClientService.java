package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_distribution;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
public interface Iaccount_analytic_distributionClientService{

    public Iaccount_analytic_distribution createModel() ;

    public void updateBatch(List<Iaccount_analytic_distribution> account_analytic_distributions);

    public void createBatch(List<Iaccount_analytic_distribution> account_analytic_distributions);

    public void create(Iaccount_analytic_distribution account_analytic_distribution);

    public void update(Iaccount_analytic_distribution account_analytic_distribution);

    public void removeBatch(List<Iaccount_analytic_distribution> account_analytic_distributions);

    public Page<Iaccount_analytic_distribution> search(SearchContext context);

    public void get(Iaccount_analytic_distribution account_analytic_distribution);

    public void remove(Iaccount_analytic_distribution account_analytic_distribution);

    public Page<Iaccount_analytic_distribution> select(SearchContext context);

}
