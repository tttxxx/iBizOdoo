package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;

/**
 * 实体 [付款] 存储模型
 */
public interface Account_payment{

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 操作编号
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作编号]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 付款类型
     */
    String getPayment_type();

    void setPayment_type(String payment_type);

    /**
     * 获取 [付款类型]脏标记
     */
    boolean getPayment_typeDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 有发票
     */
    String getHas_invoices();

    void setHas_invoices(String has_invoices);

    /**
     * 获取 [有发票]脏标记
     */
    boolean getHas_invoicesDirtyFlag();

    /**
     * 付款日期
     */
    Timestamp getPayment_date();

    void setPayment_date(Timestamp payment_date);

    /**
     * 获取 [付款日期]脏标记
     */
    boolean getPayment_dateDirtyFlag();

    /**
     * 多
     */
    String getMulti();

    void setMulti(String multi);

    /**
     * 获取 [多]脏标记
     */
    boolean getMultiDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 凭证已核销
     */
    String getMove_reconciled();

    void setMove_reconciled(String move_reconciled);

    /**
     * 获取 [凭证已核销]脏标记
     */
    boolean getMove_reconciledDirtyFlag();

    /**
     * 目标账户
     */
    Integer getDestination_account_id();

    void setDestination_account_id(Integer destination_account_id);

    /**
     * 获取 [目标账户]脏标记
     */
    boolean getDestination_account_idDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 付款差异
     */
    Double getPayment_difference();

    void setPayment_difference(Double payment_difference);

    /**
     * 获取 [付款差异]脏标记
     */
    boolean getPayment_differenceDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 需要一个动作消息的编码
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 付款差异处理
     */
    String getPayment_difference_handling();

    void setPayment_difference_handling(String payment_difference_handling);

    /**
     * 获取 [付款差异处理]脏标记
     */
    boolean getPayment_difference_handlingDirtyFlag();

    /**
     * 已核销的发票
     */
    String getReconciled_invoice_ids();

    void setReconciled_invoice_ids(String reconciled_invoice_ids);

    /**
     * 获取 [已核销的发票]脏标记
     */
    boolean getReconciled_invoice_idsDirtyFlag();

    /**
     * 付款参考
     */
    String getPayment_reference();

    void setPayment_reference(String payment_reference);

    /**
     * 获取 [付款参考]脏标记
     */
    boolean getPayment_referenceDirtyFlag();

    /**
     * 日记账项目标签
     */
    String getWriteoff_label();

    void setWriteoff_label(String writeoff_label);

    /**
     * 获取 [日记账项目标签]脏标记
     */
    boolean getWriteoff_labelDirtyFlag();

    /**
     * 分录行
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [分录行]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 付款金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [付款金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 显示合作伙伴银行账户
     */
    String getShow_partner_bank_account();

    void setShow_partner_bank_account(String show_partner_bank_account);

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    boolean getShow_partner_bank_accountDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 业务伙伴类型
     */
    String getPartner_type();

    void setPartner_type(String partner_type);

    /**
     * 获取 [业务伙伴类型]脏标记
     */
    boolean getPartner_typeDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 备忘
     */
    String getCommunication();

    void setCommunication(String communication);

    /**
     * 获取 [备忘]脏标记
     */
    boolean getCommunicationDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 隐藏付款方式
     */
    String getHide_payment_method();

    void setHide_payment_method(String hide_payment_method);

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    boolean getHide_payment_methodDirtyFlag();

    /**
     * 日记账分录名称
     */
    String getMove_name();

    void setMove_name(String move_name);

    /**
     * 获取 [日记账分录名称]脏标记
     */
    boolean getMove_nameDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 转账到
     */
    String getDestination_journal_id_text();

    void setDestination_journal_id_text(String destination_journal_id_text);

    /**
     * 获取 [转账到]脏标记
     */
    boolean getDestination_journal_id_textDirtyFlag();

    /**
     * 代码
     */
    String getPayment_method_code();

    void setPayment_method_code(String payment_method_code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getPayment_method_codeDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 付款日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 差异科目
     */
    String getWriteoff_account_id_text();

    void setWriteoff_account_id_text(String writeoff_account_id_text);

    /**
     * 获取 [差异科目]脏标记
     */
    boolean getWriteoff_account_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 付款方法类型
     */
    String getPayment_method_id_text();

    void setPayment_method_id_text(String payment_method_id_text);

    /**
     * 获取 [付款方法类型]脏标记
     */
    boolean getPayment_method_id_textDirtyFlag();

    /**
     * 保存的付款令牌
     */
    String getPayment_token_id_text();

    void setPayment_token_id_text(String payment_token_id_text);

    /**
     * 获取 [保存的付款令牌]脏标记
     */
    boolean getPayment_token_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 保存的付款令牌
     */
    Integer getPayment_token_id();

    void setPayment_token_id(Integer payment_token_id);

    /**
     * 获取 [保存的付款令牌]脏标记
     */
    boolean getPayment_token_idDirtyFlag();

    /**
     * 付款交易
     */
    Integer getPayment_transaction_id();

    void setPayment_transaction_id(Integer payment_transaction_id);

    /**
     * 获取 [付款交易]脏标记
     */
    boolean getPayment_transaction_idDirtyFlag();

    /**
     * 差异科目
     */
    Integer getWriteoff_account_id();

    void setWriteoff_account_id(Integer writeoff_account_id);

    /**
     * 获取 [差异科目]脏标记
     */
    boolean getWriteoff_account_idDirtyFlag();

    /**
     * 收款银行账号
     */
    Integer getPartner_bank_account_id();

    void setPartner_bank_account_id(Integer partner_bank_account_id);

    /**
     * 获取 [收款银行账号]脏标记
     */
    boolean getPartner_bank_account_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 付款方法类型
     */
    Integer getPayment_method_id();

    void setPayment_method_id(Integer payment_method_id);

    /**
     * 获取 [付款方法类型]脏标记
     */
    boolean getPayment_method_idDirtyFlag();

    /**
     * 转账到
     */
    Integer getDestination_journal_id();

    void setDestination_journal_id(Integer destination_journal_id);

    /**
     * 获取 [转账到]脏标记
     */
    boolean getDestination_journal_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 付款日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

}
