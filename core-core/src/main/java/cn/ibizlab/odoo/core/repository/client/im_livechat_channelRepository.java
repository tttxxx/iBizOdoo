package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.im_livechat_channel;

/**
 * 实体[im_livechat_channel] 服务对象接口
 */
public interface im_livechat_channelRepository{


    public im_livechat_channel createPO() ;
        public void createBatch(im_livechat_channel im_livechat_channel);

        public void remove(String id);

        public List<im_livechat_channel> search();

        public void create(im_livechat_channel im_livechat_channel);

        public void removeBatch(String id);

        public void update(im_livechat_channel im_livechat_channel);

        public void updateBatch(im_livechat_channel im_livechat_channel);

        public void get(String id);


}
