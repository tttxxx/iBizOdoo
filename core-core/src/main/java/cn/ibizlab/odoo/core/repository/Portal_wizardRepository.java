package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;

/**
 * 实体 [授予门户访问] 存储对象
 */
public interface Portal_wizardRepository extends Repository<Portal_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context);

    Portal_wizard convert2PO(cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard domain , Portal_wizard po) ;

    cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard convert2Domain( Portal_wizard po ,cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard domain) ;

}
