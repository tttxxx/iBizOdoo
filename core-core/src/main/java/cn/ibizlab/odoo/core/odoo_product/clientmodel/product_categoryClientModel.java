package cn.ibizlab.odoo.core.odoo_product.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[product_category] 对象
 */
public class product_categoryClientModel implements Serializable{

    /**
     * 下级类别
     */
    public String child_id;

    @JsonIgnore
    public boolean child_idDirtyFlag;
    
    /**
     * 完整名称
     */
    public String complete_name;

    @JsonIgnore
    public boolean complete_nameDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 上级类别
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级类别
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 父级路径
     */
    public String parent_path;

    @JsonIgnore
    public boolean parent_pathDirtyFlag;
    
    /**
     * # 产品
     */
    public Integer product_count;

    @JsonIgnore
    public boolean product_countDirtyFlag;
    
    /**
     * 价格差异科目
     */
    public Integer property_account_creditor_price_difference_categ;

    @JsonIgnore
    public boolean property_account_creditor_price_difference_categDirtyFlag;
    
    /**
     * 费用科目
     */
    public Integer property_account_expense_categ_id;

    @JsonIgnore
    public boolean property_account_expense_categ_idDirtyFlag;
    
    /**
     * 收入科目
     */
    public Integer property_account_income_categ_id;

    @JsonIgnore
    public boolean property_account_income_categ_idDirtyFlag;
    
    /**
     * 成本方法
     */
    public String property_cost_method;

    @JsonIgnore
    public boolean property_cost_methodDirtyFlag;
    
    /**
     * 库存进货科目
     */
    public Integer property_stock_account_input_categ_id;

    @JsonIgnore
    public boolean property_stock_account_input_categ_idDirtyFlag;
    
    /**
     * 库存出货科目
     */
    public Integer property_stock_account_output_categ_id;

    @JsonIgnore
    public boolean property_stock_account_output_categ_idDirtyFlag;
    
    /**
     * 库存日记账
     */
    public Integer property_stock_journal;

    @JsonIgnore
    public boolean property_stock_journalDirtyFlag;
    
    /**
     * 库存计价科目
     */
    public Integer property_stock_valuation_account_id;

    @JsonIgnore
    public boolean property_stock_valuation_account_idDirtyFlag;
    
    /**
     * 库存计价
     */
    public String property_valuation;

    @JsonIgnore
    public boolean property_valuationDirtyFlag;
    
    /**
     * 强制下架策略
     */
    public Integer removal_strategy_id;

    @JsonIgnore
    public boolean removal_strategy_idDirtyFlag;
    
    /**
     * 强制下架策略
     */
    public String removal_strategy_id_text;

    @JsonIgnore
    public boolean removal_strategy_id_textDirtyFlag;
    
    /**
     * 路线
     */
    public String route_ids;

    @JsonIgnore
    public boolean route_idsDirtyFlag;
    
    /**
     * 路线合计
     */
    public String total_route_ids;

    @JsonIgnore
    public boolean total_route_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下级类别]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return this.child_id ;
    }

    /**
     * 设置 [下级类别]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

     /**
     * 获取 [下级类别]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return this.child_idDirtyFlag ;
    }   

    /**
     * 获取 [完整名称]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return this.complete_name ;
    }

    /**
     * 设置 [完整名称]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

     /**
     * 获取 [完整名称]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return this.complete_nameDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [父级路径]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return this.parent_path ;
    }

    /**
     * 设置 [父级路径]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

     /**
     * 获取 [父级路径]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return this.parent_pathDirtyFlag ;
    }   

    /**
     * 获取 [# 产品]
     */
    @JsonProperty("product_count")
    public Integer getProduct_count(){
        return this.product_count ;
    }

    /**
     * 设置 [# 产品]
     */
    @JsonProperty("product_count")
    public void setProduct_count(Integer  product_count){
        this.product_count = product_count ;
        this.product_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_countDirtyFlag(){
        return this.product_countDirtyFlag ;
    }   

    /**
     * 获取 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public Integer getProperty_account_creditor_price_difference_categ(){
        return this.property_account_creditor_price_difference_categ ;
    }

    /**
     * 设置 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public void setProperty_account_creditor_price_difference_categ(Integer  property_account_creditor_price_difference_categ){
        this.property_account_creditor_price_difference_categ = property_account_creditor_price_difference_categ ;
        this.property_account_creditor_price_difference_categDirtyFlag = true ;
    }

     /**
     * 获取 [价格差异科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_creditor_price_difference_categDirtyFlag(){
        return this.property_account_creditor_price_difference_categDirtyFlag ;
    }   

    /**
     * 获取 [费用科目]
     */
    @JsonProperty("property_account_expense_categ_id")
    public Integer getProperty_account_expense_categ_id(){
        return this.property_account_expense_categ_id ;
    }

    /**
     * 设置 [费用科目]
     */
    @JsonProperty("property_account_expense_categ_id")
    public void setProperty_account_expense_categ_id(Integer  property_account_expense_categ_id){
        this.property_account_expense_categ_id = property_account_expense_categ_id ;
        this.property_account_expense_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [费用科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_idDirtyFlag(){
        return this.property_account_expense_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [收入科目]
     */
    @JsonProperty("property_account_income_categ_id")
    public Integer getProperty_account_income_categ_id(){
        return this.property_account_income_categ_id ;
    }

    /**
     * 设置 [收入科目]
     */
    @JsonProperty("property_account_income_categ_id")
    public void setProperty_account_income_categ_id(Integer  property_account_income_categ_id){
        this.property_account_income_categ_id = property_account_income_categ_id ;
        this.property_account_income_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [收入科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_idDirtyFlag(){
        return this.property_account_income_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public String getProperty_cost_method(){
        return this.property_cost_method ;
    }

    /**
     * 设置 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public void setProperty_cost_method(String  property_cost_method){
        this.property_cost_method = property_cost_method ;
        this.property_cost_methodDirtyFlag = true ;
    }

     /**
     * 获取 [成本方法]脏标记
     */
    @JsonIgnore
    public boolean getProperty_cost_methodDirtyFlag(){
        return this.property_cost_methodDirtyFlag ;
    }   

    /**
     * 获取 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return this.property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存进货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return this.property_stock_account_input_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return this.property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return this.property_stock_account_output_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存日记账]
     */
    @JsonProperty("property_stock_journal")
    public Integer getProperty_stock_journal(){
        return this.property_stock_journal ;
    }

    /**
     * 设置 [库存日记账]
     */
    @JsonProperty("property_stock_journal")
    public void setProperty_stock_journal(Integer  property_stock_journal){
        this.property_stock_journal = property_stock_journal ;
        this.property_stock_journalDirtyFlag = true ;
    }

     /**
     * 获取 [库存日记账]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_journalDirtyFlag(){
        return this.property_stock_journalDirtyFlag ;
    }   

    /**
     * 获取 [库存计价科目]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return this.property_stock_valuation_account_id ;
    }

    /**
     * 设置 [库存计价科目]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return this.property_stock_valuation_account_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价]
     */
    @JsonProperty("property_valuation")
    public String getProperty_valuation(){
        return this.property_valuation ;
    }

    /**
     * 设置 [库存计价]
     */
    @JsonProperty("property_valuation")
    public void setProperty_valuation(String  property_valuation){
        this.property_valuation = property_valuation ;
        this.property_valuationDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价]脏标记
     */
    @JsonIgnore
    public boolean getProperty_valuationDirtyFlag(){
        return this.property_valuationDirtyFlag ;
    }   

    /**
     * 获取 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public Integer getRemoval_strategy_id(){
        return this.removal_strategy_id ;
    }

    /**
     * 设置 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public void setRemoval_strategy_id(Integer  removal_strategy_id){
        this.removal_strategy_id = removal_strategy_id ;
        this.removal_strategy_idDirtyFlag = true ;
    }

     /**
     * 获取 [强制下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_idDirtyFlag(){
        return this.removal_strategy_idDirtyFlag ;
    }   

    /**
     * 获取 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public String getRemoval_strategy_id_text(){
        return this.removal_strategy_id_text ;
    }

    /**
     * 设置 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public void setRemoval_strategy_id_text(String  removal_strategy_id_text){
        this.removal_strategy_id_text = removal_strategy_id_text ;
        this.removal_strategy_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [强制下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_id_textDirtyFlag(){
        return this.removal_strategy_id_textDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return this.route_ids ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return this.route_idsDirtyFlag ;
    }   

    /**
     * 获取 [路线合计]
     */
    @JsonProperty("total_route_ids")
    public String getTotal_route_ids(){
        return this.total_route_ids ;
    }

    /**
     * 设置 [路线合计]
     */
    @JsonProperty("total_route_ids")
    public void setTotal_route_ids(String  total_route_ids){
        this.total_route_ids = total_route_ids ;
        this.total_route_idsDirtyFlag = true ;
    }

     /**
     * 获取 [路线合计]脏标记
     */
    @JsonIgnore
    public boolean getTotal_route_idsDirtyFlag(){
        return this.total_route_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("child_id") instanceof Boolean)&& map.get("child_id")!=null){
			Object[] objs = (Object[])map.get("child_id");
			if(objs.length > 0){
				Integer[] child_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_id(Arrays.toString(child_id).replace(" ",""));
			}
		}
		if(!(map.get("complete_name") instanceof Boolean)&& map.get("complete_name")!=null){
			this.setComplete_name((String)map.get("complete_name"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("parent_path") instanceof Boolean)&& map.get("parent_path")!=null){
			this.setParent_path((String)map.get("parent_path"));
		}
		if(!(map.get("product_count") instanceof Boolean)&& map.get("product_count")!=null){
			this.setProduct_count((Integer)map.get("product_count"));
		}
		if(!(map.get("property_account_creditor_price_difference_categ") instanceof Boolean)&& map.get("property_account_creditor_price_difference_categ")!=null){
			Object[] objs = (Object[])map.get("property_account_creditor_price_difference_categ");
			if(objs.length > 0){
				this.setProperty_account_creditor_price_difference_categ((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_expense_categ_id") instanceof Boolean)&& map.get("property_account_expense_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_account_expense_categ_id");
			if(objs.length > 0){
				this.setProperty_account_expense_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_income_categ_id") instanceof Boolean)&& map.get("property_account_income_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_account_income_categ_id");
			if(objs.length > 0){
				this.setProperty_account_income_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_cost_method") instanceof Boolean)&& map.get("property_cost_method")!=null){
			this.setProperty_cost_method((String)map.get("property_cost_method"));
		}
		if(!(map.get("property_stock_account_input_categ_id") instanceof Boolean)&& map.get("property_stock_account_input_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_account_input_categ_id");
			if(objs.length > 0){
				this.setProperty_stock_account_input_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_account_output_categ_id") instanceof Boolean)&& map.get("property_stock_account_output_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_account_output_categ_id");
			if(objs.length > 0){
				this.setProperty_stock_account_output_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_journal") instanceof Boolean)&& map.get("property_stock_journal")!=null){
			Object[] objs = (Object[])map.get("property_stock_journal");
			if(objs.length > 0){
				this.setProperty_stock_journal((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_valuation_account_id") instanceof Boolean)&& map.get("property_stock_valuation_account_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_valuation_account_id");
			if(objs.length > 0){
				this.setProperty_stock_valuation_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_valuation") instanceof Boolean)&& map.get("property_valuation")!=null){
			this.setProperty_valuation((String)map.get("property_valuation"));
		}
		if(!(map.get("removal_strategy_id") instanceof Boolean)&& map.get("removal_strategy_id")!=null){
			Object[] objs = (Object[])map.get("removal_strategy_id");
			if(objs.length > 0){
				this.setRemoval_strategy_id((Integer)objs[0]);
			}
		}
		if(!(map.get("removal_strategy_id") instanceof Boolean)&& map.get("removal_strategy_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("removal_strategy_id");
			if(objs.length > 1){
				this.setRemoval_strategy_id_text((String)objs[1]);
			}
		}
		if(!(map.get("route_ids") instanceof Boolean)&& map.get("route_ids")!=null){
			Object[] objs = (Object[])map.get("route_ids");
			if(objs.length > 0){
				Integer[] route_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRoute_ids(Arrays.toString(route_ids).replace(" ",""));
			}
		}
		if(!(map.get("total_route_ids") instanceof Boolean)&& map.get("total_route_ids")!=null){
			Object[] objs = (Object[])map.get("total_route_ids");
			if(objs.length > 0){
				Integer[] total_route_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTotal_route_ids(Arrays.toString(total_route_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChild_id()!=null&&this.getChild_idDirtyFlag()){
			map.put("child_id",this.getChild_id());
		}else if(this.getChild_idDirtyFlag()){
			map.put("child_id",false);
		}
		if(this.getComplete_name()!=null&&this.getComplete_nameDirtyFlag()){
			map.put("complete_name",this.getComplete_name());
		}else if(this.getComplete_nameDirtyFlag()){
			map.put("complete_name",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_path()!=null&&this.getParent_pathDirtyFlag()){
			map.put("parent_path",this.getParent_path());
		}else if(this.getParent_pathDirtyFlag()){
			map.put("parent_path",false);
		}
		if(this.getProduct_count()!=null&&this.getProduct_countDirtyFlag()){
			map.put("product_count",this.getProduct_count());
		}else if(this.getProduct_countDirtyFlag()){
			map.put("product_count",false);
		}
		if(this.getProperty_account_creditor_price_difference_categ()!=null&&this.getProperty_account_creditor_price_difference_categDirtyFlag()){
			map.put("property_account_creditor_price_difference_categ",this.getProperty_account_creditor_price_difference_categ());
		}else if(this.getProperty_account_creditor_price_difference_categDirtyFlag()){
			map.put("property_account_creditor_price_difference_categ",false);
		}
		if(this.getProperty_account_expense_categ_id()!=null&&this.getProperty_account_expense_categ_idDirtyFlag()){
			map.put("property_account_expense_categ_id",this.getProperty_account_expense_categ_id());
		}else if(this.getProperty_account_expense_categ_idDirtyFlag()){
			map.put("property_account_expense_categ_id",false);
		}
		if(this.getProperty_account_income_categ_id()!=null&&this.getProperty_account_income_categ_idDirtyFlag()){
			map.put("property_account_income_categ_id",this.getProperty_account_income_categ_id());
		}else if(this.getProperty_account_income_categ_idDirtyFlag()){
			map.put("property_account_income_categ_id",false);
		}
		if(this.getProperty_cost_method()!=null&&this.getProperty_cost_methodDirtyFlag()){
			map.put("property_cost_method",this.getProperty_cost_method());
		}else if(this.getProperty_cost_methodDirtyFlag()){
			map.put("property_cost_method",false);
		}
		if(this.getProperty_stock_account_input_categ_id()!=null&&this.getProperty_stock_account_input_categ_idDirtyFlag()){
			map.put("property_stock_account_input_categ_id",this.getProperty_stock_account_input_categ_id());
		}else if(this.getProperty_stock_account_input_categ_idDirtyFlag()){
			map.put("property_stock_account_input_categ_id",false);
		}
		if(this.getProperty_stock_account_output_categ_id()!=null&&this.getProperty_stock_account_output_categ_idDirtyFlag()){
			map.put("property_stock_account_output_categ_id",this.getProperty_stock_account_output_categ_id());
		}else if(this.getProperty_stock_account_output_categ_idDirtyFlag()){
			map.put("property_stock_account_output_categ_id",false);
		}
		if(this.getProperty_stock_journal()!=null&&this.getProperty_stock_journalDirtyFlag()){
			map.put("property_stock_journal",this.getProperty_stock_journal());
		}else if(this.getProperty_stock_journalDirtyFlag()){
			map.put("property_stock_journal",false);
		}
		if(this.getProperty_stock_valuation_account_id()!=null&&this.getProperty_stock_valuation_account_idDirtyFlag()){
			map.put("property_stock_valuation_account_id",this.getProperty_stock_valuation_account_id());
		}else if(this.getProperty_stock_valuation_account_idDirtyFlag()){
			map.put("property_stock_valuation_account_id",false);
		}
		if(this.getProperty_valuation()!=null&&this.getProperty_valuationDirtyFlag()){
			map.put("property_valuation",this.getProperty_valuation());
		}else if(this.getProperty_valuationDirtyFlag()){
			map.put("property_valuation",false);
		}
		if(this.getRemoval_strategy_id()!=null&&this.getRemoval_strategy_idDirtyFlag()){
			map.put("removal_strategy_id",this.getRemoval_strategy_id());
		}else if(this.getRemoval_strategy_idDirtyFlag()){
			map.put("removal_strategy_id",false);
		}
		if(this.getRemoval_strategy_id_text()!=null&&this.getRemoval_strategy_id_textDirtyFlag()){
			//忽略文本外键removal_strategy_id_text
		}else if(this.getRemoval_strategy_id_textDirtyFlag()){
			map.put("removal_strategy_id",false);
		}
		if(this.getRoute_ids()!=null&&this.getRoute_idsDirtyFlag()){
			map.put("route_ids",this.getRoute_ids());
		}else if(this.getRoute_idsDirtyFlag()){
			map.put("route_ids",false);
		}
		if(this.getTotal_route_ids()!=null&&this.getTotal_route_idsDirtyFlag()){
			map.put("total_route_ids",this.getTotal_route_ids());
		}else if(this.getTotal_route_idsDirtyFlag()){
			map.put("total_route_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
