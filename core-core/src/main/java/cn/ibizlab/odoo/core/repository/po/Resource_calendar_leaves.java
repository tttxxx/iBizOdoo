package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;

/**
 * 实体 [休假详情] 存储模型
 */
public interface Resource_calendar_leaves{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 原因
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [原因]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 时间类型
     */
    String getTime_type();

    void setTime_type(String time_type);

    /**
     * 获取 [时间类型]脏标记
     */
    boolean getTime_typeDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_to();

    void setDate_to(Timestamp date_to);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_toDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_from();

    void setDate_from(Timestamp date_from);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_fromDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 工作时间
     */
    String getCalendar_id_text();

    void setCalendar_id_text(String calendar_id_text);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getCalendar_id_textDirtyFlag();

    /**
     * 休假申请
     */
    String getHoliday_id_text();

    void setHoliday_id_text(String holiday_id_text);

    /**
     * 获取 [休假申请]脏标记
     */
    boolean getHoliday_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 资源
     */
    String getResource_id_text();

    void setResource_id_text(String resource_id_text);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 休假申请
     */
    Integer getHoliday_id();

    void setHoliday_id(Integer holiday_id);

    /**
     * 获取 [休假申请]脏标记
     */
    boolean getHoliday_idDirtyFlag();

    /**
     * 工作时间
     */
    Integer getCalendar_id();

    void setCalendar_id(Integer calendar_id);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getCalendar_idDirtyFlag();

    /**
     * 资源
     */
    Integer getResource_id();

    void setResource_id(Integer resource_id);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_idDirtyFlag();

}
