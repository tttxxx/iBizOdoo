package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meter_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_pm_meter_lineClientModel;

/**
 * 实体[History of Asset Meter Reading] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meter_lineServiceImpl implements IMro_pm_meter_lineService {

    @Autowired
    mro_pm_meter_lineOdooClient mro_pm_meter_lineOdooClient;


    @Override
    public boolean update(Mro_pm_meter_line et) {
        mro_pm_meter_lineClientModel clientModel = convert2Model(et,null);
		mro_pm_meter_lineOdooClient.update(clientModel);
        Mro_pm_meter_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_pm_meter_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_pm_meter_lineClientModel clientModel = new mro_pm_meter_lineClientModel();
        clientModel.setId(id);
		mro_pm_meter_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mro_pm_meter_line get(Integer id) {
        mro_pm_meter_lineClientModel clientModel = new mro_pm_meter_lineClientModel();
        clientModel.setId(id);
		mro_pm_meter_lineOdooClient.get(clientModel);
        Mro_pm_meter_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_pm_meter_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mro_pm_meter_line et) {
        mro_pm_meter_lineClientModel clientModel = convert2Model(et,null);
		mro_pm_meter_lineOdooClient.create(clientModel);
        Mro_pm_meter_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter_line> searchDefault(Mro_pm_meter_lineSearchContext context) {
        List<Mro_pm_meter_line> list = new ArrayList<Mro_pm_meter_line>();
        Page<mro_pm_meter_lineClientModel> clientModelList = mro_pm_meter_lineOdooClient.search(context);
        for(mro_pm_meter_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_pm_meter_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_pm_meter_lineClientModel convert2Model(Mro_pm_meter_line domain , mro_pm_meter_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_pm_meter_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("total_valuedirtyflag"))
                model.setTotal_value(domain.getTotalValue());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("meter_iddirtyflag"))
                model.setMeter_id(domain.getMeterId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_pm_meter_line convert2Domain( mro_pm_meter_lineClientModel model ,Mro_pm_meter_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_pm_meter_line();
        }

        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTotal_valueDirtyFlag())
            domain.setTotalValue(model.getTotal_value());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMeter_idDirtyFlag())
            domain.setMeterId(model.getMeter_id());
        return domain ;
    }

}

    



