package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Idigest_tip;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[digest_tip] 服务对象接口
 */
public interface Idigest_tipClientService{

    public Idigest_tip createModel() ;

    public void updateBatch(List<Idigest_tip> digest_tips);

    public Page<Idigest_tip> search(SearchContext context);

    public void get(Idigest_tip digest_tip);

    public void remove(Idigest_tip digest_tip);

    public void createBatch(List<Idigest_tip> digest_tips);

    public void update(Idigest_tip digest_tip);

    public void create(Idigest_tip digest_tip);

    public void removeBatch(List<Idigest_tip> digest_tips);

    public Page<Idigest_tip> select(SearchContext context);

}
