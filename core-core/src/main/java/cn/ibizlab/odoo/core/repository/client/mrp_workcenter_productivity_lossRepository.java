package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_workcenter_productivity_loss;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
public interface mrp_workcenter_productivity_lossRepository{


    public mrp_workcenter_productivity_loss createPO() ;
        public List<mrp_workcenter_productivity_loss> search();

        public void update(mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void create(mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void remove(String id);

        public void removeBatch(String id);

        public void createBatch(mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void updateBatch(mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

        public void get(String id);


}
