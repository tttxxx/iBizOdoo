package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;

/**
 * 实体 [已过帐移动] 存储对象
 */
public interface Stock_overprocessed_transferRepository extends Repository<Stock_overprocessed_transfer> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_overprocessed_transfer> searchDefault(Stock_overprocessed_transferSearchContext context);

    Stock_overprocessed_transfer convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer domain , Stock_overprocessed_transfer po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer convert2Domain( Stock_overprocessed_transfer po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer domain) ;

}
