package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_recruitment_source;

/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
public interface hr_recruitment_sourceRepository{


    public hr_recruitment_source createPO() ;
        public void createBatch(hr_recruitment_source hr_recruitment_source);

        public void get(String id);

        public List<hr_recruitment_source> search();

        public void update(hr_recruitment_source hr_recruitment_source);

        public void remove(String id);

        public void updateBatch(hr_recruitment_source hr_recruitment_source);

        public void removeBatch(String id);

        public void create(hr_recruitment_source hr_recruitment_source);


}
