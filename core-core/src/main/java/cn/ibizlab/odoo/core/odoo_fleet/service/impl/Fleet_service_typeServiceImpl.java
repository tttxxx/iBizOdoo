package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_service_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_service_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_service_typeClientModel;

/**
 * 实体[车辆服务类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_service_typeServiceImpl implements IFleet_service_typeService {

    @Autowired
    fleet_service_typeOdooClient fleet_service_typeOdooClient;


    @Override
    public boolean update(Fleet_service_type et) {
        fleet_service_typeClientModel clientModel = convert2Model(et,null);
		fleet_service_typeOdooClient.update(clientModel);
        Fleet_service_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_service_type> list){
    }

    @Override
    public Fleet_service_type get(Integer id) {
        fleet_service_typeClientModel clientModel = new fleet_service_typeClientModel();
        clientModel.setId(id);
		fleet_service_typeOdooClient.get(clientModel);
        Fleet_service_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_service_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Fleet_service_type et) {
        fleet_service_typeClientModel clientModel = convert2Model(et,null);
		fleet_service_typeOdooClient.create(clientModel);
        Fleet_service_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_service_type> list){
    }

    @Override
    public boolean remove(Integer id) {
        fleet_service_typeClientModel clientModel = new fleet_service_typeClientModel();
        clientModel.setId(id);
		fleet_service_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context) {
        List<Fleet_service_type> list = new ArrayList<Fleet_service_type>();
        Page<fleet_service_typeClientModel> clientModelList = fleet_service_typeOdooClient.search(context);
        for(fleet_service_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_service_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_service_typeClientModel convert2Model(Fleet_service_type domain , fleet_service_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_service_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("categorydirtyflag"))
                model.setCategory(domain.getCategory());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_service_type convert2Domain( fleet_service_typeClientModel model ,Fleet_service_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_service_type();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCategoryDirtyFlag())
            domain.setCategory(model.getCategory());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



