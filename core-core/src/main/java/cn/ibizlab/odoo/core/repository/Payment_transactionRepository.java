package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;

/**
 * 实体 [付款交易] 存储对象
 */
public interface Payment_transactionRepository extends Repository<Payment_transaction> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context);

    Payment_transaction convert2PO(cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction domain , Payment_transaction po) ;

    cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction convert2Domain( Payment_transaction po ,cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction domain) ;

}
