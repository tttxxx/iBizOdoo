package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_template_attribute_valueOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_template_attribute_valueClientModel;

/**
 * 实体[产品属性值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_template_attribute_valueServiceImpl implements IProduct_template_attribute_valueService {

    @Autowired
    product_template_attribute_valueOdooClient product_template_attribute_valueOdooClient;


    @Override
    public boolean update(Product_template_attribute_value et) {
        product_template_attribute_valueClientModel clientModel = convert2Model(et,null);
		product_template_attribute_valueOdooClient.update(clientModel);
        Product_template_attribute_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_template_attribute_value> list){
    }

    @Override
    public boolean create(Product_template_attribute_value et) {
        product_template_attribute_valueClientModel clientModel = convert2Model(et,null);
		product_template_attribute_valueOdooClient.create(clientModel);
        Product_template_attribute_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template_attribute_value> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_template_attribute_valueClientModel clientModel = new product_template_attribute_valueClientModel();
        clientModel.setId(id);
		product_template_attribute_valueOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Product_template_attribute_value get(Integer id) {
        product_template_attribute_valueClientModel clientModel = new product_template_attribute_valueClientModel();
        clientModel.setId(id);
		product_template_attribute_valueOdooClient.get(clientModel);
        Product_template_attribute_value et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_template_attribute_value();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template_attribute_value> searchDefault(Product_template_attribute_valueSearchContext context) {
        List<Product_template_attribute_value> list = new ArrayList<Product_template_attribute_value>();
        Page<product_template_attribute_valueClientModel> clientModelList = product_template_attribute_valueOdooClient.search(context);
        for(product_template_attribute_valueClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_template_attribute_value>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_template_attribute_valueClientModel convert2Model(Product_template_attribute_value domain , product_template_attribute_valueClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_template_attribute_valueClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("exclude_fordirtyflag"))
                model.setExclude_for(domain.getExcludeFor());
            if((Boolean) domain.getExtensionparams().get("price_extradirtyflag"))
                model.setPrice_extra(domain.getPriceExtra());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("attribute_iddirtyflag"))
                model.setAttribute_id(domain.getAttributeId());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("html_colordirtyflag"))
                model.setHtml_color(domain.getHtmlColor());
            if((Boolean) domain.getExtensionparams().get("is_customdirtyflag"))
                model.setIs_custom(domain.getIsCustom());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_attribute_value_iddirtyflag"))
                model.setProduct_attribute_value_id(domain.getProductAttributeValueId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_template_attribute_value convert2Domain( product_template_attribute_valueClientModel model ,Product_template_attribute_value domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_template_attribute_value();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getExclude_forDirtyFlag())
            domain.setExcludeFor(model.getExclude_for());
        if(model.getPrice_extraDirtyFlag())
            domain.setPriceExtra(model.getPrice_extra());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAttribute_idDirtyFlag())
            domain.setAttributeId(model.getAttribute_id());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getHtml_colorDirtyFlag())
            domain.setHtmlColor(model.getHtml_color());
        if(model.getIs_customDirtyFlag())
            domain.setIsCustom(model.getIs_custom());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_attribute_value_idDirtyFlag())
            domain.setProductAttributeValueId(model.getProduct_attribute_value_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



