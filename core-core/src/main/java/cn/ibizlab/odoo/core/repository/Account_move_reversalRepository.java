package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;

/**
 * 实体 [会计凭证逆转] 存储对象
 */
public interface Account_move_reversalRepository extends Repository<Account_move_reversal> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context);

    Account_move_reversal convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal domain , Account_move_reversal po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal convert2Domain( Account_move_reversal po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal domain) ;

}
