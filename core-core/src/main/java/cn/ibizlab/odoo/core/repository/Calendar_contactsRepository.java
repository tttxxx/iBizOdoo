package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;

/**
 * 实体 [日历联系人] 存储对象
 */
public interface Calendar_contactsRepository extends Repository<Calendar_contacts> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context);

    Calendar_contacts convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts domain , Calendar_contacts po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts convert2Domain( Calendar_contacts po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts domain) ;

}
