package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_order_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_order_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_purchase.clientmodel.purchase_order_lineClientModel;

/**
 * 实体[采购订单行] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_order_lineServiceImpl implements IPurchase_order_lineService {

    @Autowired
    purchase_order_lineOdooClient purchase_order_lineOdooClient;


    @Override
    public boolean create(Purchase_order_line et) {
        purchase_order_lineClientModel clientModel = convert2Model(et,null);
		purchase_order_lineOdooClient.create(clientModel);
        Purchase_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_order_line> list){
    }

    @Override
    public boolean update(Purchase_order_line et) {
        purchase_order_lineClientModel clientModel = convert2Model(et,null);
		purchase_order_lineOdooClient.update(clientModel);
        Purchase_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Purchase_order_line> list){
    }

    @Override
    public Purchase_order_line get(Integer id) {
        purchase_order_lineClientModel clientModel = new purchase_order_lineClientModel();
        clientModel.setId(id);
		purchase_order_lineOdooClient.get(clientModel);
        Purchase_order_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Purchase_order_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        purchase_order_lineClientModel clientModel = new purchase_order_lineClientModel();
        clientModel.setId(id);
		purchase_order_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context) {
        List<Purchase_order_line> list = new ArrayList<Purchase_order_line>();
        Page<purchase_order_lineClientModel> clientModelList = purchase_order_lineOdooClient.search(context);
        for(purchase_order_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Purchase_order_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public purchase_order_lineClientModel convert2Model(Purchase_order_line domain , purchase_order_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new purchase_order_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("qty_receiveddirtyflag"))
                model.setQty_received(domain.getQtyReceived());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("price_taxdirtyflag"))
                model.setPrice_tax(domain.getPriceTax());
            if((Boolean) domain.getExtensionparams().get("move_idsdirtyflag"))
                model.setMove_ids(domain.getMoveIds());
            if((Boolean) domain.getExtensionparams().get("price_subtotaldirtyflag"))
                model.setPrice_subtotal(domain.getPriceSubtotal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("taxes_iddirtyflag"))
                model.setTaxes_id(domain.getTaxesId());
            if((Boolean) domain.getExtensionparams().get("invoice_linesdirtyflag"))
                model.setInvoice_lines(domain.getInvoiceLines());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_planneddirtyflag"))
                model.setDate_planned(domain.getDatePlanned());
            if((Boolean) domain.getExtensionparams().get("qty_invoiceddirtyflag"))
                model.setQty_invoiced(domain.getQtyInvoiced());
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("move_dest_idsdirtyflag"))
                model.setMove_dest_ids(domain.getMoveDestIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("orderpoint_id_textdirtyflag"))
                model.setOrderpoint_id_text(domain.getOrderpointIdText());
            if((Boolean) domain.getExtensionparams().get("sale_order_id_textdirtyflag"))
                model.setSale_order_id_text(domain.getSaleOrderIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_typedirtyflag"))
                model.setProduct_type(domain.getProductType());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("account_analytic_id_textdirtyflag"))
                model.setAccount_analytic_id_text(domain.getAccountAnalyticIdText());
            if((Boolean) domain.getExtensionparams().get("product_imagedirtyflag"))
                model.setProduct_image(domain.getProductImage());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("order_id_textdirtyflag"))
                model.setOrder_id_text(domain.getOrderIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("sale_line_id_textdirtyflag"))
                model.setSale_line_id_text(domain.getSaleLineIdText());
            if((Boolean) domain.getExtensionparams().get("date_orderdirtyflag"))
                model.setDate_order(domain.getDateOrder());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("account_analytic_iddirtyflag"))
                model.setAccount_analytic_id(domain.getAccountAnalyticId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("sale_order_iddirtyflag"))
                model.setSale_order_id(domain.getSaleOrderId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("sale_line_iddirtyflag"))
                model.setSale_line_id(domain.getSaleLineId());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            if((Boolean) domain.getExtensionparams().get("orderpoint_iddirtyflag"))
                model.setOrderpoint_id(domain.getOrderpointId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Purchase_order_line convert2Domain( purchase_order_lineClientModel model ,Purchase_order_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Purchase_order_line();
        }

        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getQty_receivedDirtyFlag())
            domain.setQtyReceived(model.getQty_received());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getPrice_taxDirtyFlag())
            domain.setPriceTax(model.getPrice_tax());
        if(model.getMove_idsDirtyFlag())
            domain.setMoveIds(model.getMove_ids());
        if(model.getPrice_subtotalDirtyFlag())
            domain.setPriceSubtotal(model.getPrice_subtotal());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getTaxes_idDirtyFlag())
            domain.setTaxesId(model.getTaxes_id());
        if(model.getInvoice_linesDirtyFlag())
            domain.setInvoiceLines(model.getInvoice_lines());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_plannedDirtyFlag())
            domain.setDatePlanned(model.getDate_planned());
        if(model.getQty_invoicedDirtyFlag())
            domain.setQtyInvoiced(model.getQty_invoiced());
        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getMove_dest_idsDirtyFlag())
            domain.setMoveDestIds(model.getMove_dest_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOrderpoint_id_textDirtyFlag())
            domain.setOrderpointIdText(model.getOrderpoint_id_text());
        if(model.getSale_order_id_textDirtyFlag())
            domain.setSaleOrderIdText(model.getSale_order_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_typeDirtyFlag())
            domain.setProductType(model.getProduct_type());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getAccount_analytic_id_textDirtyFlag())
            domain.setAccountAnalyticIdText(model.getAccount_analytic_id_text());
        if(model.getProduct_imageDirtyFlag())
            domain.setProductImage(model.getProduct_image());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getOrder_id_textDirtyFlag())
            domain.setOrderIdText(model.getOrder_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getSale_line_id_textDirtyFlag())
            domain.setSaleLineIdText(model.getSale_line_id_text());
        if(model.getDate_orderDirtyFlag())
            domain.setDateOrder(model.getDate_order());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getAccount_analytic_idDirtyFlag())
            domain.setAccountAnalyticId(model.getAccount_analytic_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getSale_order_idDirtyFlag())
            domain.setSaleOrderId(model.getSale_order_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getSale_line_idDirtyFlag())
            domain.setSaleLineId(model.getSale_line_id());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        if(model.getOrderpoint_idDirtyFlag())
            domain.setOrderpointId(model.getOrderpoint_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



