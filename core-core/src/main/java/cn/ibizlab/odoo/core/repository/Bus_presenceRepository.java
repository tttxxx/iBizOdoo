package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;

/**
 * 实体 [用户上线] 存储对象
 */
public interface Bus_presenceRepository extends Repository<Bus_presence> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Bus_presence> searchDefault(Bus_presenceSearchContext context);

    Bus_presence convert2PO(cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence domain , Bus_presence po) ;

    cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence convert2Domain( Bus_presence po ,cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence domain) ;

}
