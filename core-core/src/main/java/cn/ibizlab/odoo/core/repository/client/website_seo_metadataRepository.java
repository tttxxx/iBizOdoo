package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_seo_metadata;

/**
 * 实体[website_seo_metadata] 服务对象接口
 */
public interface website_seo_metadataRepository{


    public website_seo_metadata createPO() ;
        public List<website_seo_metadata> search();

        public void createBatch(website_seo_metadata website_seo_metadata);

        public void create(website_seo_metadata website_seo_metadata);

        public void get(String id);

        public void updateBatch(website_seo_metadata website_seo_metadata);

        public void removeBatch(String id);

        public void update(website_seo_metadata website_seo_metadata);

        public void remove(String id);


}
