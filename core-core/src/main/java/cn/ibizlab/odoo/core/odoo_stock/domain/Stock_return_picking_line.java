package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [退料明细行] 对象
 */
@Data
public class Stock_return_picking_line extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 数量
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 退款 (更新 SO/PO)
     */
    @DEField(name = "to_refund")
    @JSONField(name = "to_refund")
    @JsonProperty("to_refund")
    private String toRefund;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 移动
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 单位
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    private Integer uomId;

    /**
     * 移动
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 向导
     */
    @DEField(name = "wizard_id")
    @JSONField(name = "wizard_id")
    @JsonProperty("wizard_id")
    private Integer wizardId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoowizard")
    @JsonProperty("odoowizard")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking odooWizard;




    /**
     * 设置 [数量]
     */
    public void setQuantity(Double quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }
    /**
     * 设置 [退款 (更新 SO/PO)]
     */
    public void setToRefund(String toRefund){
        this.toRefund = toRefund ;
        this.modify("to_refund",toRefund);
    }
    /**
     * 设置 [移动]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [向导]
     */
    public void setWizardId(Integer wizardId){
        this.wizardId = wizardId ;
        this.modify("wizard_id",wizardId);
    }

}


