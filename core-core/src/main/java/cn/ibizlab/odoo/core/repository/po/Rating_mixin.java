package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;

/**
 * 实体 [混合评级] 存储模型
 */
public interface Rating_mixin{

    /**
     * 最新反馈评级
     */
    String getRating_last_feedback();

    void setRating_last_feedback(String rating_last_feedback);

    /**
     * 获取 [最新反馈评级]脏标记
     */
    boolean getRating_last_feedbackDirtyFlag();

    /**
     * 评级数
     */
    Integer getRating_count();

    void setRating_count(Integer rating_count);

    /**
     * 获取 [评级数]脏标记
     */
    boolean getRating_countDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 最新图像评级
     */
    byte[] getRating_last_image();

    void setRating_last_image(byte[] rating_last_image);

    /**
     * 获取 [最新图像评级]脏标记
     */
    boolean getRating_last_imageDirtyFlag();

    /**
     * 最新值评级
     */
    Double getRating_last_value();

    void setRating_last_value(Double rating_last_value);

    /**
     * 获取 [最新值评级]脏标记
     */
    boolean getRating_last_valueDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

}
