package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_activity_type] 对象
 */
public class mail_activity_typeClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 类别
     */
    public String category;

    @JsonIgnore
    public boolean categoryDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 排版类型
     */
    public String decoration_type;

    @JsonIgnore
    public boolean decoration_typeDirtyFlag;
    
    /**
     * 设置默认下一个活动
     */
    public Integer default_next_type_id;

    @JsonIgnore
    public boolean default_next_type_idDirtyFlag;
    
    /**
     * 设置默认下一个活动
     */
    public String default_next_type_id_text;

    @JsonIgnore
    public boolean default_next_type_id_textDirtyFlag;
    
    /**
     * 之后
     */
    public Integer delay_count;

    @JsonIgnore
    public boolean delay_countDirtyFlag;
    
    /**
     * 延迟类型
     */
    public String delay_from;

    @JsonIgnore
    public boolean delay_fromDirtyFlag;
    
    /**
     * 延迟单位
     */
    public String delay_unit;

    @JsonIgnore
    public boolean delay_unitDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 自动安排下一个活动
     */
    public String force_next;

    @JsonIgnore
    public boolean force_nextDirtyFlag;
    
    /**
     * 图标
     */
    public String icon;

    @JsonIgnore
    public boolean iconDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 初始模型
     */
    public Integer initial_res_model_id;

    @JsonIgnore
    public boolean initial_res_model_idDirtyFlag;
    
    /**
     * 邮件模板
     */
    public String mail_template_ids;

    @JsonIgnore
    public boolean mail_template_idsDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 推荐的下一活动
     */
    public String next_type_ids;

    @JsonIgnore
    public boolean next_type_idsDirtyFlag;
    
    /**
     * 预先活动
     */
    public String previous_type_ids;

    @JsonIgnore
    public boolean previous_type_idsDirtyFlag;
    
    /**
     * 模型已更改
     */
    public String res_model_change;

    @JsonIgnore
    public boolean res_model_changeDirtyFlag;
    
    /**
     * 模型
     */
    public Integer res_model_id;

    @JsonIgnore
    public boolean res_model_idDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 摘要
     */
    public String summary;

    @JsonIgnore
    public boolean summaryDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("category")
    public String getCategory(){
        return this.category ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category")
    public void setCategory(String  category){
        this.category = category ;
        this.categoryDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategoryDirtyFlag(){
        return this.categoryDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [排版类型]
     */
    @JsonProperty("decoration_type")
    public String getDecoration_type(){
        return this.decoration_type ;
    }

    /**
     * 设置 [排版类型]
     */
    @JsonProperty("decoration_type")
    public void setDecoration_type(String  decoration_type){
        this.decoration_type = decoration_type ;
        this.decoration_typeDirtyFlag = true ;
    }

     /**
     * 获取 [排版类型]脏标记
     */
    @JsonIgnore
    public boolean getDecoration_typeDirtyFlag(){
        return this.decoration_typeDirtyFlag ;
    }   

    /**
     * 获取 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id")
    public Integer getDefault_next_type_id(){
        return this.default_next_type_id ;
    }

    /**
     * 设置 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id")
    public void setDefault_next_type_id(Integer  default_next_type_id){
        this.default_next_type_id = default_next_type_id ;
        this.default_next_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [设置默认下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_idDirtyFlag(){
        return this.default_next_type_idDirtyFlag ;
    }   

    /**
     * 获取 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id_text")
    public String getDefault_next_type_id_text(){
        return this.default_next_type_id_text ;
    }

    /**
     * 设置 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id_text")
    public void setDefault_next_type_id_text(String  default_next_type_id_text){
        this.default_next_type_id_text = default_next_type_id_text ;
        this.default_next_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [设置默认下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_id_textDirtyFlag(){
        return this.default_next_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [之后]
     */
    @JsonProperty("delay_count")
    public Integer getDelay_count(){
        return this.delay_count ;
    }

    /**
     * 设置 [之后]
     */
    @JsonProperty("delay_count")
    public void setDelay_count(Integer  delay_count){
        this.delay_count = delay_count ;
        this.delay_countDirtyFlag = true ;
    }

     /**
     * 获取 [之后]脏标记
     */
    @JsonIgnore
    public boolean getDelay_countDirtyFlag(){
        return this.delay_countDirtyFlag ;
    }   

    /**
     * 获取 [延迟类型]
     */
    @JsonProperty("delay_from")
    public String getDelay_from(){
        return this.delay_from ;
    }

    /**
     * 设置 [延迟类型]
     */
    @JsonProperty("delay_from")
    public void setDelay_from(String  delay_from){
        this.delay_from = delay_from ;
        this.delay_fromDirtyFlag = true ;
    }

     /**
     * 获取 [延迟类型]脏标记
     */
    @JsonIgnore
    public boolean getDelay_fromDirtyFlag(){
        return this.delay_fromDirtyFlag ;
    }   

    /**
     * 获取 [延迟单位]
     */
    @JsonProperty("delay_unit")
    public String getDelay_unit(){
        return this.delay_unit ;
    }

    /**
     * 设置 [延迟单位]
     */
    @JsonProperty("delay_unit")
    public void setDelay_unit(String  delay_unit){
        this.delay_unit = delay_unit ;
        this.delay_unitDirtyFlag = true ;
    }

     /**
     * 获取 [延迟单位]脏标记
     */
    @JsonIgnore
    public boolean getDelay_unitDirtyFlag(){
        return this.delay_unitDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return this.force_next ;
    }

    /**
     * 设置 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

     /**
     * 获取 [自动安排下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return this.force_nextDirtyFlag ;
    }   

    /**
     * 获取 [图标]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return this.icon ;
    }

    /**
     * 设置 [图标]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

     /**
     * 获取 [图标]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return this.iconDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [初始模型]
     */
    @JsonProperty("initial_res_model_id")
    public Integer getInitial_res_model_id(){
        return this.initial_res_model_id ;
    }

    /**
     * 设置 [初始模型]
     */
    @JsonProperty("initial_res_model_id")
    public void setInitial_res_model_id(Integer  initial_res_model_id){
        this.initial_res_model_id = initial_res_model_id ;
        this.initial_res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [初始模型]脏标记
     */
    @JsonIgnore
    public boolean getInitial_res_model_idDirtyFlag(){
        return this.initial_res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return this.mail_template_ids ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return this.mail_template_idsDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [推荐的下一活动]
     */
    @JsonProperty("next_type_ids")
    public String getNext_type_ids(){
        return this.next_type_ids ;
    }

    /**
     * 设置 [推荐的下一活动]
     */
    @JsonProperty("next_type_ids")
    public void setNext_type_ids(String  next_type_ids){
        this.next_type_ids = next_type_ids ;
        this.next_type_idsDirtyFlag = true ;
    }

     /**
     * 获取 [推荐的下一活动]脏标记
     */
    @JsonIgnore
    public boolean getNext_type_idsDirtyFlag(){
        return this.next_type_idsDirtyFlag ;
    }   

    /**
     * 获取 [预先活动]
     */
    @JsonProperty("previous_type_ids")
    public String getPrevious_type_ids(){
        return this.previous_type_ids ;
    }

    /**
     * 设置 [预先活动]
     */
    @JsonProperty("previous_type_ids")
    public void setPrevious_type_ids(String  previous_type_ids){
        this.previous_type_ids = previous_type_ids ;
        this.previous_type_idsDirtyFlag = true ;
    }

     /**
     * 获取 [预先活动]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_type_idsDirtyFlag(){
        return this.previous_type_idsDirtyFlag ;
    }   

    /**
     * 获取 [模型已更改]
     */
    @JsonProperty("res_model_change")
    public String getRes_model_change(){
        return this.res_model_change ;
    }

    /**
     * 设置 [模型已更改]
     */
    @JsonProperty("res_model_change")
    public void setRes_model_change(String  res_model_change){
        this.res_model_change = res_model_change ;
        this.res_model_changeDirtyFlag = true ;
    }

     /**
     * 获取 [模型已更改]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_changeDirtyFlag(){
        return this.res_model_changeDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [摘要]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return this.summary ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

     /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return this.summaryDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("category") instanceof Boolean)&& map.get("category")!=null){
			this.setCategory((String)map.get("category"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("decoration_type") instanceof Boolean)&& map.get("decoration_type")!=null){
			this.setDecoration_type((String)map.get("decoration_type"));
		}
		if(!(map.get("default_next_type_id") instanceof Boolean)&& map.get("default_next_type_id")!=null){
			Object[] objs = (Object[])map.get("default_next_type_id");
			if(objs.length > 0){
				this.setDefault_next_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("default_next_type_id") instanceof Boolean)&& map.get("default_next_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("default_next_type_id");
			if(objs.length > 1){
				this.setDefault_next_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("delay_count") instanceof Boolean)&& map.get("delay_count")!=null){
			this.setDelay_count((Integer)map.get("delay_count"));
		}
		if(!(map.get("delay_from") instanceof Boolean)&& map.get("delay_from")!=null){
			this.setDelay_from((String)map.get("delay_from"));
		}
		if(!(map.get("delay_unit") instanceof Boolean)&& map.get("delay_unit")!=null){
			this.setDelay_unit((String)map.get("delay_unit"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("force_next") instanceof Boolean){
			this.setForce_next(((Boolean)map.get("force_next"))? "true" : "false");
		}
		if(!(map.get("icon") instanceof Boolean)&& map.get("icon")!=null){
			this.setIcon((String)map.get("icon"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("initial_res_model_id") instanceof Boolean)&& map.get("initial_res_model_id")!=null){
			Object[] objs = (Object[])map.get("initial_res_model_id");
			if(objs.length > 0){
				this.setInitial_res_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_template_ids") instanceof Boolean)&& map.get("mail_template_ids")!=null){
			Object[] objs = (Object[])map.get("mail_template_ids");
			if(objs.length > 0){
				Integer[] mail_template_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMail_template_ids(Arrays.toString(mail_template_ids).replace(" ",""));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("next_type_ids") instanceof Boolean)&& map.get("next_type_ids")!=null){
			Object[] objs = (Object[])map.get("next_type_ids");
			if(objs.length > 0){
				Integer[] next_type_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setNext_type_ids(Arrays.toString(next_type_ids).replace(" ",""));
			}
		}
		if(!(map.get("previous_type_ids") instanceof Boolean)&& map.get("previous_type_ids")!=null){
			Object[] objs = (Object[])map.get("previous_type_ids");
			if(objs.length > 0){
				Integer[] previous_type_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPrevious_type_ids(Arrays.toString(previous_type_ids).replace(" ",""));
			}
		}
		if(map.get("res_model_change") instanceof Boolean){
			this.setRes_model_change(((Boolean)map.get("res_model_change"))? "true" : "false");
		}
		if(!(map.get("res_model_id") instanceof Boolean)&& map.get("res_model_id")!=null){
			Object[] objs = (Object[])map.get("res_model_id");
			if(objs.length > 0){
				this.setRes_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("summary") instanceof Boolean)&& map.get("summary")!=null){
			this.setSummary((String)map.get("summary"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getCategory()!=null&&this.getCategoryDirtyFlag()){
			map.put("category",this.getCategory());
		}else if(this.getCategoryDirtyFlag()){
			map.put("category",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDecoration_type()!=null&&this.getDecoration_typeDirtyFlag()){
			map.put("decoration_type",this.getDecoration_type());
		}else if(this.getDecoration_typeDirtyFlag()){
			map.put("decoration_type",false);
		}
		if(this.getDefault_next_type_id()!=null&&this.getDefault_next_type_idDirtyFlag()){
			map.put("default_next_type_id",this.getDefault_next_type_id());
		}else if(this.getDefault_next_type_idDirtyFlag()){
			map.put("default_next_type_id",false);
		}
		if(this.getDefault_next_type_id_text()!=null&&this.getDefault_next_type_id_textDirtyFlag()){
			//忽略文本外键default_next_type_id_text
		}else if(this.getDefault_next_type_id_textDirtyFlag()){
			map.put("default_next_type_id",false);
		}
		if(this.getDelay_count()!=null&&this.getDelay_countDirtyFlag()){
			map.put("delay_count",this.getDelay_count());
		}else if(this.getDelay_countDirtyFlag()){
			map.put("delay_count",false);
		}
		if(this.getDelay_from()!=null&&this.getDelay_fromDirtyFlag()){
			map.put("delay_from",this.getDelay_from());
		}else if(this.getDelay_fromDirtyFlag()){
			map.put("delay_from",false);
		}
		if(this.getDelay_unit()!=null&&this.getDelay_unitDirtyFlag()){
			map.put("delay_unit",this.getDelay_unit());
		}else if(this.getDelay_unitDirtyFlag()){
			map.put("delay_unit",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getForce_next()!=null&&this.getForce_nextDirtyFlag()){
			map.put("force_next",Boolean.parseBoolean(this.getForce_next()));		
		}		if(this.getIcon()!=null&&this.getIconDirtyFlag()){
			map.put("icon",this.getIcon());
		}else if(this.getIconDirtyFlag()){
			map.put("icon",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInitial_res_model_id()!=null&&this.getInitial_res_model_idDirtyFlag()){
			map.put("initial_res_model_id",this.getInitial_res_model_id());
		}else if(this.getInitial_res_model_idDirtyFlag()){
			map.put("initial_res_model_id",false);
		}
		if(this.getMail_template_ids()!=null&&this.getMail_template_idsDirtyFlag()){
			map.put("mail_template_ids",this.getMail_template_ids());
		}else if(this.getMail_template_idsDirtyFlag()){
			map.put("mail_template_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNext_type_ids()!=null&&this.getNext_type_idsDirtyFlag()){
			map.put("next_type_ids",this.getNext_type_ids());
		}else if(this.getNext_type_idsDirtyFlag()){
			map.put("next_type_ids",false);
		}
		if(this.getPrevious_type_ids()!=null&&this.getPrevious_type_idsDirtyFlag()){
			map.put("previous_type_ids",this.getPrevious_type_ids());
		}else if(this.getPrevious_type_idsDirtyFlag()){
			map.put("previous_type_ids",false);
		}
		if(this.getRes_model_change()!=null&&this.getRes_model_changeDirtyFlag()){
			map.put("res_model_change",Boolean.parseBoolean(this.getRes_model_change()));		
		}		if(this.getRes_model_id()!=null&&this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",this.getRes_model_id());
		}else if(this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getSummary()!=null&&this.getSummaryDirtyFlag()){
			map.put("summary",this.getSummary());
		}else if(this.getSummaryDirtyFlag()){
			map.put("summary",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
