package cn.ibizlab.odoo.core.odoo_im_livechat.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [即时聊天] 对象
 */
@Data
public class Im_livechat_channel extends EntityClient implements Serializable {

    /**
     * 操作员
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 规则
     */
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    private String ruleIds;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * Web页
     */
    @JSONField(name = "web_page")
    @JsonProperty("web_page")
    private String webPage;

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 脚本（外部）
     */
    @JSONField(name = "script_external")
    @JsonProperty("script_external")
    private String scriptExternal;

    /**
     * 对话数
     */
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;

    /**
     * 按钮的文本
     */
    @DEField(name = "button_text")
    @JSONField(name = "button_text")
    @JsonProperty("button_text")
    private String buttonText;

    /**
     * 网站说明
     */
    @DEField(name = "website_description")
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 会话
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 普通
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * % 高兴
     */
    @JSONField(name = "rating_percentage_satisfaction")
    @JsonProperty("rating_percentage_satisfaction")
    private Integer ratingPercentageSatisfaction;

    /**
     * 聊天输入为空时显示
     */
    @DEField(name = "input_placeholder")
    @JSONField(name = "input_placeholder")
    @JsonProperty("input_placeholder")
    private String inputPlaceholder;

    /**
     * 缩略图
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 您是否在频道中？
     */
    @JSONField(name = "are_you_inside")
    @JsonProperty("are_you_inside")
    private String areYouInside;

    /**
     * 欢迎信息
     */
    @DEField(name = "default_message")
    @JSONField(name = "default_message")
    @JsonProperty("default_message")
    private String defaultMessage;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [按钮的文本]
     */
    public void setButtonText(String buttonText){
        this.buttonText = buttonText ;
        this.modify("button_text",buttonText);
    }
    /**
     * 设置 [网站说明]
     */
    public void setWebsiteDescription(String websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [聊天输入为空时显示]
     */
    public void setInputPlaceholder(String inputPlaceholder){
        this.inputPlaceholder = inputPlaceholder ;
        this.modify("input_placeholder",inputPlaceholder);
    }
    /**
     * 设置 [欢迎信息]
     */
    public void setDefaultMessage(String defaultMessage){
        this.defaultMessage = defaultMessage ;
        this.modify("default_message",defaultMessage);
    }

}


