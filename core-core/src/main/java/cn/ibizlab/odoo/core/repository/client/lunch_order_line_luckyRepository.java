package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_order_line_lucky;

/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
public interface lunch_order_line_luckyRepository{


    public lunch_order_line_lucky createPO() ;
        public List<lunch_order_line_lucky> search();

        public void get(String id);

        public void createBatch(lunch_order_line_lucky lunch_order_line_lucky);

        public void updateBatch(lunch_order_line_lucky lunch_order_line_lucky);

        public void create(lunch_order_line_lucky lunch_order_line_lucky);

        public void update(lunch_order_line_lucky lunch_order_line_lucky);

        public void remove(String id);

        public void removeBatch(String id);


}
