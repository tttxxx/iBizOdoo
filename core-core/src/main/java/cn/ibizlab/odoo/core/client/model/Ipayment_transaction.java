package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [payment_transaction] 对象
 */
public interface Ipayment_transaction {

    /**
     * 获取 [收单方]
     */
    public void setAcquirer_id(Integer acquirer_id);
    
    /**
     * 设置 [收单方]
     */
    public Integer getAcquirer_id();

    /**
     * 获取 [收单方]脏标记
     */
    public boolean getAcquirer_idDirtyFlag();
    /**
     * 获取 [收单方]
     */
    public void setAcquirer_id_text(String acquirer_id_text);
    
    /**
     * 设置 [收单方]
     */
    public String getAcquirer_id_text();

    /**
     * 获取 [收单方]脏标记
     */
    public boolean getAcquirer_id_textDirtyFlag();
    /**
     * 获取 [收单方参考]
     */
    public void setAcquirer_reference(String acquirer_reference);
    
    /**
     * 设置 [收单方参考]
     */
    public String getAcquirer_reference();

    /**
     * 获取 [收单方参考]脏标记
     */
    public boolean getAcquirer_referenceDirtyFlag();
    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [回调哈希函数]
     */
    public void setCallback_hash(String callback_hash);
    
    /**
     * 设置 [回调哈希函数]
     */
    public String getCallback_hash();

    /**
     * 获取 [回调哈希函数]脏标记
     */
    public boolean getCallback_hashDirtyFlag();
    /**
     * 获取 [回调方法]
     */
    public void setCallback_method(String callback_method);
    
    /**
     * 设置 [回调方法]
     */
    public String getCallback_method();

    /**
     * 获取 [回调方法]脏标记
     */
    public boolean getCallback_methodDirtyFlag();
    /**
     * 获取 [回调文档模型]
     */
    public void setCallback_model_id(Integer callback_model_id);
    
    /**
     * 设置 [回调文档模型]
     */
    public Integer getCallback_model_id();

    /**
     * 获取 [回调文档模型]脏标记
     */
    public boolean getCallback_model_idDirtyFlag();
    /**
     * 获取 [回调文档 ID]
     */
    public void setCallback_res_id(Integer callback_res_id);
    
    /**
     * 设置 [回调文档 ID]
     */
    public Integer getCallback_res_id();

    /**
     * 获取 [回调文档 ID]脏标记
     */
    public boolean getCallback_res_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [验证日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [验证日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [验证日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [费用]
     */
    public void setFees(Double fees);
    
    /**
     * 设置 [费用]
     */
    public Double getFees();

    /**
     * 获取 [费用]脏标记
     */
    public boolean getFeesDirtyFlag();
    /**
     * 获取 [3D Secure HTML]
     */
    public void setHtml_3ds(String html_3ds);
    
    /**
     * 设置 [3D Secure HTML]
     */
    public String getHtml_3ds();

    /**
     * 获取 [3D Secure HTML]脏标记
     */
    public boolean getHtml_3dsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_ids();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [# 发票]
     */
    public void setInvoice_ids_nbr(Integer invoice_ids_nbr);
    
    /**
     * 设置 [# 发票]
     */
    public Integer getInvoice_ids_nbr();

    /**
     * 获取 [# 发票]脏标记
     */
    public boolean getInvoice_ids_nbrDirtyFlag();
    /**
     * 获取 [付款是否已过账处理]
     */
    public void setIs_processed(String is_processed);
    
    /**
     * 设置 [付款是否已过账处理]
     */
    public String getIs_processed();

    /**
     * 获取 [付款是否已过账处理]脏标记
     */
    public boolean getIs_processedDirtyFlag();
    /**
     * 获取 [地址]
     */
    public void setPartner_address(String partner_address);
    
    /**
     * 设置 [地址]
     */
    public String getPartner_address();

    /**
     * 获取 [地址]脏标记
     */
    public boolean getPartner_addressDirtyFlag();
    /**
     * 获取 [城市]
     */
    public void setPartner_city(String partner_city);
    
    /**
     * 设置 [城市]
     */
    public String getPartner_city();

    /**
     * 获取 [城市]脏标记
     */
    public boolean getPartner_cityDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setPartner_country_id(Integer partner_country_id);
    
    /**
     * 设置 [国家]
     */
    public Integer getPartner_country_id();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getPartner_country_idDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setPartner_country_id_text(String partner_country_id_text);
    
    /**
     * 设置 [国家]
     */
    public String getPartner_country_id_text();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getPartner_country_id_textDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setPartner_email(String partner_email);
    
    /**
     * 设置 [EMail]
     */
    public String getPartner_email();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getPartner_emailDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [语言]
     */
    public void setPartner_lang(String partner_lang);
    
    /**
     * 设置 [语言]
     */
    public String getPartner_lang();

    /**
     * 获取 [语言]脏标记
     */
    public boolean getPartner_langDirtyFlag();
    /**
     * 获取 [合作伙伴名称]
     */
    public void setPartner_name(String partner_name);
    
    /**
     * 设置 [合作伙伴名称]
     */
    public String getPartner_name();

    /**
     * 获取 [合作伙伴名称]脏标记
     */
    public boolean getPartner_nameDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPartner_phone(String partner_phone);
    
    /**
     * 设置 [电话]
     */
    public String getPartner_phone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPartner_phoneDirtyFlag();
    /**
     * 获取 [邮政编码]
     */
    public void setPartner_zip(String partner_zip);
    
    /**
     * 设置 [邮政编码]
     */
    public String getPartner_zip();

    /**
     * 获取 [邮政编码]脏标记
     */
    public boolean getPartner_zipDirtyFlag();
    /**
     * 获取 [付款]
     */
    public void setPayment_id(Integer payment_id);
    
    /**
     * 设置 [付款]
     */
    public Integer getPayment_id();

    /**
     * 获取 [付款]脏标记
     */
    public boolean getPayment_idDirtyFlag();
    /**
     * 获取 [付款]
     */
    public void setPayment_id_text(String payment_id_text);
    
    /**
     * 设置 [付款]
     */
    public String getPayment_id_text();

    /**
     * 获取 [付款]脏标记
     */
    public boolean getPayment_id_textDirtyFlag();
    /**
     * 获取 [付款令牌]
     */
    public void setPayment_token_id(Integer payment_token_id);
    
    /**
     * 设置 [付款令牌]
     */
    public Integer getPayment_token_id();

    /**
     * 获取 [付款令牌]脏标记
     */
    public boolean getPayment_token_idDirtyFlag();
    /**
     * 获取 [付款令牌]
     */
    public void setPayment_token_id_text(String payment_token_id_text);
    
    /**
     * 设置 [付款令牌]
     */
    public String getPayment_token_id_text();

    /**
     * 获取 [付款令牌]脏标记
     */
    public boolean getPayment_token_id_textDirtyFlag();
    /**
     * 获取 [服务商]
     */
    public void setProvider(String provider);
    
    /**
     * 设置 [服务商]
     */
    public String getProvider();

    /**
     * 获取 [服务商]脏标记
     */
    public boolean getProviderDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [参考]
     */
    public String getReference();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [付款后返回网址]
     */
    public void setReturn_url(String return_url);
    
    /**
     * 设置 [付款后返回网址]
     */
    public String getReturn_url();

    /**
     * 获取 [付款后返回网址]脏标记
     */
    public boolean getReturn_urlDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_ids(String sale_order_ids);
    
    /**
     * 设置 [销售订单]
     */
    public String getSale_order_ids();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_idsDirtyFlag();
    /**
     * 获取 [# 销售订单]
     */
    public void setSale_order_ids_nbr(Integer sale_order_ids_nbr);
    
    /**
     * 设置 [# 销售订单]
     */
    public Integer getSale_order_ids_nbr();

    /**
     * 获取 [# 销售订单]脏标记
     */
    public boolean getSale_order_ids_nbrDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setState_message(String state_message);
    
    /**
     * 设置 [消息]
     */
    public String getState_message();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getState_messageDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
