package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_mail_compose_message;

/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
public interface survey_mail_compose_messageRepository{


    public survey_mail_compose_message createPO() ;
        public void remove(String id);

        public void createBatch(survey_mail_compose_message survey_mail_compose_message);

        public void update(survey_mail_compose_message survey_mail_compose_message);

        public void get(String id);

        public void removeBatch(String id);

        public void updateBatch(survey_mail_compose_message survey_mail_compose_message);

        public void create(survey_mail_compose_message survey_mail_compose_message);

        public List<survey_mail_compose_message> search();


}
