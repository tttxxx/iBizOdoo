package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;

/**
 * 实体 [银行] 存储模型
 */
public interface Res_bank{

    /**
     * 街道 2
     */
    String getStreet2();

    void setStreet2(String street2);

    /**
     * 获取 [街道 2]脏标记
     */
    boolean getStreet2DirtyFlag();

    /**
     * 电话
     */
    String getPhone();

    void setPhone(String phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPhoneDirtyFlag();

    /**
     * 城市
     */
    String getCity();

    void setCity(String city);

    /**
     * 获取 [城市]脏标记
     */
    boolean getCityDirtyFlag();

    /**
     * 街道
     */
    String getStreet();

    void setStreet(String street);

    /**
     * 获取 [街道]脏标记
     */
    boolean getStreetDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 银行识别代码
     */
    String getBic();

    void setBic(String bic);

    /**
     * 获取 [银行识别代码]脏标记
     */
    boolean getBicDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 邮政编码
     */
    String getZip();

    void setZip(String zip);

    /**
     * 获取 [邮政编码]脏标记
     */
    boolean getZipDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 国家/地区
     */
    String getCountry_text();

    void setCountry_text(String country_text);

    /**
     * 获取 [国家/地区]脏标记
     */
    boolean getCountry_textDirtyFlag();

    /**
     * 状态
     */
    String getState_text();

    void setState_text(String state_text);

    /**
     * 获取 [状态]脏标记
     */
    boolean getState_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 国家/地区
     */
    Integer getCountry();

    void setCountry(Integer country);

    /**
     * 获取 [国家/地区]脏标记
     */
    boolean getCountryDirtyFlag();

    /**
     * 状态
     */
    Integer getState();

    void setState(Integer state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
