package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;

/**
 * 实体 [调查用户输入] 存储模型
 */
public interface Survey_user_input{

    /**
     * 答案
     */
    String getUser_input_line_ids();

    void setUser_input_line_ids(String user_input_line_ids);

    /**
     * 获取 [答案]脏标记
     */
    boolean getUser_input_line_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 测试成绩
     */
    Double getQuizz_score();

    void setQuizz_score(Double quizz_score);

    /**
     * 获取 [测试成绩]脏标记
     */
    boolean getQuizz_scoreDirtyFlag();

    /**
     * 标识令牌
     */
    String getToken();

    void setToken(String token);

    /**
     * 获取 [标识令牌]脏标记
     */
    boolean getTokenDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 测试问卷
     */
    String getTest_entry();

    void setTest_entry(String test_entry);

    /**
     * 获取 [测试问卷]脏标记
     */
    boolean getTest_entryDirtyFlag();

    /**
     * 截止日期
     */
    Timestamp getDeadline();

    void setDeadline(Timestamp deadline);

    /**
     * 获取 [截止日期]脏标记
     */
    boolean getDeadlineDirtyFlag();

    /**
     * 回复类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [回复类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getDate_create();

    void setDate_create(Timestamp date_create);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getDate_createDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 空白调查的公开链接
     */
    String getPrint_url();

    void setPrint_url(String print_url);

    /**
     * 获取 [空白调查的公开链接]脏标记
     */
    boolean getPrint_urlDirtyFlag();

    /**
     * 调查结果的公开链接
     */
    String getResult_url();

    void setResult_url(String result_url);

    /**
     * 获取 [调查结果的公开链接]脏标记
     */
    boolean getResult_urlDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 问卷
     */
    Integer getSurvey_id();

    void setSurvey_id(Integer survey_id);

    /**
     * 获取 [问卷]脏标记
     */
    boolean getSurvey_idDirtyFlag();

    /**
     * 最后显示页面
     */
    Integer getLast_displayed_page_id();

    void setLast_displayed_page_id(Integer last_displayed_page_id);

    /**
     * 获取 [最后显示页面]脏标记
     */
    boolean getLast_displayed_page_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
