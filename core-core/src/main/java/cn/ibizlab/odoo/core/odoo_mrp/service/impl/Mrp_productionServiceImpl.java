package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_productionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_productionOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_productionClientModel;

/**
 * 实体[Production Order] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_productionServiceImpl implements IMrp_productionService {

    @Autowired
    mrp_productionOdooClient mrp_productionOdooClient;


    @Override
    public boolean create(Mrp_production et) {
        mrp_productionClientModel clientModel = convert2Model(et,null);
		mrp_productionOdooClient.create(clientModel);
        Mrp_production rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_production> list){
    }

    @Override
    public boolean update(Mrp_production et) {
        mrp_productionClientModel clientModel = convert2Model(et,null);
		mrp_productionOdooClient.update(clientModel);
        Mrp_production rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_production> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_productionClientModel clientModel = new mrp_productionClientModel();
        clientModel.setId(id);
		mrp_productionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_production get(Integer id) {
        mrp_productionClientModel clientModel = new mrp_productionClientModel();
        clientModel.setId(id);
		mrp_productionOdooClient.get(clientModel);
        Mrp_production et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_production();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_production> searchDefault(Mrp_productionSearchContext context) {
        List<Mrp_production> list = new ArrayList<Mrp_production>();
        Page<mrp_productionClientModel> clientModelList = mrp_productionOdooClient.search(context);
        for(mrp_productionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_production>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_productionClientModel convert2Model(Mrp_production domain , mrp_productionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_productionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("workorder_done_countdirtyflag"))
                model.setWorkorder_done_count(domain.getWorkorderDoneCount());
            if((Boolean) domain.getExtensionparams().get("finished_move_line_idsdirtyflag"))
                model.setFinished_move_line_ids(domain.getFinishedMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("qty_produceddirtyflag"))
                model.setQty_produced(domain.getQtyProduced());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("unreserve_visibledirtyflag"))
                model.setUnreserve_visible(domain.getUnreserveVisible());
            if((Boolean) domain.getExtensionparams().get("has_movesdirtyflag"))
                model.setHas_moves(domain.getHasMoves());
            if((Boolean) domain.getExtensionparams().get("is_lockeddirtyflag"))
                model.setIs_locked(domain.getIsLocked());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("move_finished_idsdirtyflag"))
                model.setMove_finished_ids(domain.getMoveFinishedIds());
            if((Boolean) domain.getExtensionparams().get("scrap_countdirtyflag"))
                model.setScrap_count(domain.getScrapCount());
            if((Boolean) domain.getExtensionparams().get("scrap_idsdirtyflag"))
                model.setScrap_ids(domain.getScrapIds());
            if((Boolean) domain.getExtensionparams().get("show_final_lotsdirtyflag"))
                model.setShow_final_lots(domain.getShowFinalLots());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("move_dest_idsdirtyflag"))
                model.setMove_dest_ids(domain.getMoveDestIds());
            if((Boolean) domain.getExtensionparams().get("procurement_group_iddirtyflag"))
                model.setProcurement_group_id(domain.getProcurementGroupId());
            if((Boolean) domain.getExtensionparams().get("delivery_countdirtyflag"))
                model.setDelivery_count(domain.getDeliveryCount());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("workorder_countdirtyflag"))
                model.setWorkorder_count(domain.getWorkorderCount());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("date_planned_finisheddirtyflag"))
                model.setDate_planned_finished(domain.getDatePlannedFinished());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("date_planned_startdirtyflag"))
                model.setDate_planned_start(domain.getDatePlannedStart());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("check_to_donedirtyflag"))
                model.setCheck_to_done(domain.getCheckToDone());
            if((Boolean) domain.getExtensionparams().get("post_visibledirtyflag"))
                model.setPost_visible(domain.getPostVisible());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("picking_idsdirtyflag"))
                model.setPicking_ids(domain.getPickingIds());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("consumed_less_than_planneddirtyflag"))
                model.setConsumed_less_than_planned(domain.getConsumedLessThanPlanned());
            if((Boolean) domain.getExtensionparams().get("move_raw_idsdirtyflag"))
                model.setMove_raw_ids(domain.getMoveRawIds());
            if((Boolean) domain.getExtensionparams().get("availabilitydirtyflag"))
                model.setAvailability(domain.getAvailability());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("workorder_idsdirtyflag"))
                model.setWorkorder_ids(domain.getWorkorderIds());
            if((Boolean) domain.getExtensionparams().get("date_finisheddirtyflag"))
                model.setDate_finished(domain.getDateFinished());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("propagatedirtyflag"))
                model.setPropagate(domain.getPropagate());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("location_src_id_textdirtyflag"))
                model.setLocation_src_id_text(domain.getLocationSrcIdText());
            if((Boolean) domain.getExtensionparams().get("production_location_iddirtyflag"))
                model.setProduction_location_id(domain.getProductionLocationId());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("routing_id_textdirtyflag"))
                model.setRouting_id_text(domain.getRoutingIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("routing_iddirtyflag"))
                model.setRouting_id(domain.getRoutingId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("bom_iddirtyflag"))
                model.setBom_id(domain.getBomId());
            if((Boolean) domain.getExtensionparams().get("location_src_iddirtyflag"))
                model.setLocation_src_id(domain.getLocationSrcId());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_production convert2Domain( mrp_productionClientModel model ,Mrp_production domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_production();
        }

        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getWorkorder_done_countDirtyFlag())
            domain.setWorkorderDoneCount(model.getWorkorder_done_count());
        if(model.getFinished_move_line_idsDirtyFlag())
            domain.setFinishedMoveLineIds(model.getFinished_move_line_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getQty_producedDirtyFlag())
            domain.setQtyProduced(model.getQty_produced());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getUnreserve_visibleDirtyFlag())
            domain.setUnreserveVisible(model.getUnreserve_visible());
        if(model.getHas_movesDirtyFlag())
            domain.setHasMoves(model.getHas_moves());
        if(model.getIs_lockedDirtyFlag())
            domain.setIsLocked(model.getIs_locked());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getMove_finished_idsDirtyFlag())
            domain.setMoveFinishedIds(model.getMove_finished_ids());
        if(model.getScrap_countDirtyFlag())
            domain.setScrapCount(model.getScrap_count());
        if(model.getScrap_idsDirtyFlag())
            domain.setScrapIds(model.getScrap_ids());
        if(model.getShow_final_lotsDirtyFlag())
            domain.setShowFinalLots(model.getShow_final_lots());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMove_dest_idsDirtyFlag())
            domain.setMoveDestIds(model.getMove_dest_ids());
        if(model.getProcurement_group_idDirtyFlag())
            domain.setProcurementGroupId(model.getProcurement_group_id());
        if(model.getDelivery_countDirtyFlag())
            domain.setDeliveryCount(model.getDelivery_count());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getWorkorder_countDirtyFlag())
            domain.setWorkorderCount(model.getWorkorder_count());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getDate_planned_finishedDirtyFlag())
            domain.setDatePlannedFinished(model.getDate_planned_finished());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDate_planned_startDirtyFlag())
            domain.setDatePlannedStart(model.getDate_planned_start());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getCheck_to_doneDirtyFlag())
            domain.setCheckToDone(model.getCheck_to_done());
        if(model.getPost_visibleDirtyFlag())
            domain.setPostVisible(model.getPost_visible());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPicking_idsDirtyFlag())
            domain.setPickingIds(model.getPicking_ids());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getConsumed_less_than_plannedDirtyFlag())
            domain.setConsumedLessThanPlanned(model.getConsumed_less_than_planned());
        if(model.getMove_raw_idsDirtyFlag())
            domain.setMoveRawIds(model.getMove_raw_ids());
        if(model.getAvailabilityDirtyFlag())
            domain.setAvailability(model.getAvailability());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getWorkorder_idsDirtyFlag())
            domain.setWorkorderIds(model.getWorkorder_ids());
        if(model.getDate_finishedDirtyFlag())
            domain.setDateFinished(model.getDate_finished());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getPropagateDirtyFlag())
            domain.setPropagate(model.getPropagate());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getLocation_src_id_textDirtyFlag())
            domain.setLocationSrcIdText(model.getLocation_src_id_text());
        if(model.getProduction_location_idDirtyFlag())
            domain.setProductionLocationId(model.getProduction_location_id());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRouting_id_textDirtyFlag())
            domain.setRoutingIdText(model.getRouting_id_text());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getRouting_idDirtyFlag())
            domain.setRoutingId(model.getRouting_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getBom_idDirtyFlag())
            domain.setBomId(model.getBom_id());
        if(model.getLocation_src_idDirtyFlag())
            domain.setLocationSrcId(model.getLocation_src_id());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        return domain ;
    }

}

    



