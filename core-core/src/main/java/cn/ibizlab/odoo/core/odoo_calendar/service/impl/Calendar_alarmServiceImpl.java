package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_alarmOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_alarmClientModel;

/**
 * 实体[活动提醒] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_alarmServiceImpl implements ICalendar_alarmService {

    @Autowired
    calendar_alarmOdooClient calendar_alarmOdooClient;


    @Override
    public Calendar_alarm get(Integer id) {
        calendar_alarmClientModel clientModel = new calendar_alarmClientModel();
        clientModel.setId(id);
		calendar_alarmOdooClient.get(clientModel);
        Calendar_alarm et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_alarm();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        calendar_alarmClientModel clientModel = new calendar_alarmClientModel();
        clientModel.setId(id);
		calendar_alarmOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Calendar_alarm et) {
        calendar_alarmClientModel clientModel = convert2Model(et,null);
		calendar_alarmOdooClient.create(clientModel);
        Calendar_alarm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_alarm> list){
    }

    @Override
    public boolean update(Calendar_alarm et) {
        calendar_alarmClientModel clientModel = convert2Model(et,null);
		calendar_alarmOdooClient.update(clientModel);
        Calendar_alarm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_alarm> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context) {
        List<Calendar_alarm> list = new ArrayList<Calendar_alarm>();
        Page<calendar_alarmClientModel> clientModelList = calendar_alarmOdooClient.search(context);
        for(calendar_alarmClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_alarm>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_alarmClientModel convert2Model(Calendar_alarm domain , calendar_alarmClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_alarmClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("intervaldirtyflag"))
                model.setInterval(domain.getInterval());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("duration_minutesdirtyflag"))
                model.setDuration_minutes(domain.getDurationMinutes());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_alarm convert2Domain( calendar_alarmClientModel model ,Calendar_alarm domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_alarm();
        }

        if(model.getIntervalDirtyFlag())
            domain.setInterval(model.getInterval());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDuration_minutesDirtyFlag())
            domain.setDurationMinutes(model.getDuration_minutes());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



