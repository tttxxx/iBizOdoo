package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;

/**
 * 实体 [税组] 存储对象
 */
public interface Account_tax_groupRepository extends Repository<Account_tax_group> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context);

    Account_tax_group convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group domain , Account_tax_group po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group convert2Domain( Account_tax_group po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group domain) ;

}
