package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_applicant;

/**
 * 实体[hr_applicant] 服务对象接口
 */
public interface hr_applicantRepository{


    public hr_applicant createPO() ;
        public void get(String id);

        public void createBatch(hr_applicant hr_applicant);

        public void create(hr_applicant hr_applicant);

        public void remove(String id);

        public List<hr_applicant> search();

        public void updateBatch(hr_applicant hr_applicant);

        public void update(hr_applicant hr_applicant);

        public void removeBatch(String id);


}
