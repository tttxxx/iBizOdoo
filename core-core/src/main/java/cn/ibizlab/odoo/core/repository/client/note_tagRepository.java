package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.note_tag;

/**
 * 实体[note_tag] 服务对象接口
 */
public interface note_tagRepository{


    public note_tag createPO() ;
        public void removeBatch(String id);

        public List<note_tag> search();

        public void get(String id);

        public void create(note_tag note_tag);

        public void remove(String id);

        public void createBatch(note_tag note_tag);

        public void updateBatch(note_tag note_tag);

        public void update(note_tag note_tag);


}
