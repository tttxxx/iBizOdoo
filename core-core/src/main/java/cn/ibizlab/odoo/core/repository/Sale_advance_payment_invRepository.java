package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;

/**
 * 实体 [销售预付款发票] 存储对象
 */
public interface Sale_advance_payment_invRepository extends Repository<Sale_advance_payment_inv> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context);

    Sale_advance_payment_inv convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv domain , Sale_advance_payment_inv po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv convert2Domain( Sale_advance_payment_inv po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv domain) ;

}
