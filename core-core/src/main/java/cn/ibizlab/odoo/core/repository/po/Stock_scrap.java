package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;

/**
 * 实体 [报废] 存储模型
 */
public interface Stock_scrap{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 预计日期
     */
    Timestamp getDate_expected();

    void setDate_expected(Timestamp date_expected);

    /**
     * 获取 [预计日期]脏标记
     */
    boolean getDate_expectedDirtyFlag();

    /**
     * 数量
     */
    Double getScrap_qty();

    void setScrap_qty(Double scrap_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getScrap_qtyDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 编号
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [编号]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 工单
     */
    String getWorkorder_id_text();

    void setWorkorder_id_text(String workorder_id_text);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_id_textDirtyFlag();

    /**
     * 报废位置
     */
    String getScrap_location_id_text();

    void setScrap_location_id_text(String scrap_location_id_text);

    /**
     * 获取 [报废位置]脏标记
     */
    boolean getScrap_location_id_textDirtyFlag();

    /**
     * 批次
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [批次]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 报废移动
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [报废移动]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 分拣
     */
    String getPicking_id_text();

    void setPicking_id_text(String picking_id_text);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_id_textDirtyFlag();

    /**
     * 制造订单
     */
    String getProduction_id_text();

    void setProduction_id_text(String production_id_text);

    /**
     * 获取 [制造订单]脏标记
     */
    boolean getProduction_id_textDirtyFlag();

    /**
     * 位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 追踪
     */
    String getTracking();

    void setTracking(String tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getTrackingDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_id_text();

    void setOwner_id_text(String owner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 批次
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [批次]脏标记
     */
    boolean getLot_idDirtyFlag();

    /**
     * 制造订单
     */
    Integer getProduction_id();

    void setProduction_id(Integer production_id);

    /**
     * 获取 [制造订单]脏标记
     */
    boolean getProduction_idDirtyFlag();

    /**
     * 分拣
     */
    Integer getPicking_id();

    void setPicking_id(Integer picking_id);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_idDirtyFlag();

    /**
     * 报废位置
     */
    Integer getScrap_location_id();

    void setScrap_location_id(Integer scrap_location_id);

    /**
     * 获取 [报废位置]脏标记
     */
    boolean getScrap_location_idDirtyFlag();

    /**
     * 报废移动
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [报废移动]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 工单
     */
    Integer getWorkorder_id();

    void setWorkorder_id(Integer workorder_id);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_idDirtyFlag();

    /**
     * 单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getOwner_id();

    void setOwner_id(Integer owner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_idDirtyFlag();

}
