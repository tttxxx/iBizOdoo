package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_utm.client.utm_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_utm.clientmodel.utm_mixinClientModel;

/**
 * 实体[UTM Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_mixinServiceImpl implements IUtm_mixinService {

    @Autowired
    utm_mixinOdooClient utm_mixinOdooClient;


    @Override
    public boolean update(Utm_mixin et) {
        utm_mixinClientModel clientModel = convert2Model(et,null);
		utm_mixinOdooClient.update(clientModel);
        Utm_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Utm_mixin> list){
    }

    @Override
    public Utm_mixin get(Integer id) {
        utm_mixinClientModel clientModel = new utm_mixinClientModel();
        clientModel.setId(id);
		utm_mixinOdooClient.get(clientModel);
        Utm_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Utm_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Utm_mixin et) {
        utm_mixinClientModel clientModel = convert2Model(et,null);
		utm_mixinOdooClient.create(clientModel);
        Utm_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_mixin> list){
    }

    @Override
    public boolean remove(Integer id) {
        utm_mixinClientModel clientModel = new utm_mixinClientModel();
        clientModel.setId(id);
		utm_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context) {
        List<Utm_mixin> list = new ArrayList<Utm_mixin>();
        Page<utm_mixinClientModel> clientModelList = utm_mixinOdooClient.search(context);
        for(utm_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Utm_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public utm_mixinClientModel convert2Model(Utm_mixin domain , utm_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new utm_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Utm_mixin convert2Domain( utm_mixinClientModel model ,Utm_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Utm_mixin();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        return domain ;
    }

}

    



