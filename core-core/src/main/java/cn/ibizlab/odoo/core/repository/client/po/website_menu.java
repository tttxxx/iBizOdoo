package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [website_menu] 对象
 */
public interface website_menu {

    public String getChild_id();

    public void setChild_id(String child_id);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_visible();

    public void setIs_visible(String is_visible);

    public String getName();

    public void setName(String name);

    public String getNew_window();

    public void setNew_window(String new_window);

    public Integer getPage_id();

    public void setPage_id(Integer page_id);

    public String getPage_id_text();

    public void setPage_id_text(String page_id_text);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public String getParent_path();

    public void setParent_path(String parent_path);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getUrl();

    public void setUrl(String url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
