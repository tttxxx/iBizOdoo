package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_packagingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_packagingOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_packagingClientModel;

/**
 * 实体[产品包装] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_packagingServiceImpl implements IProduct_packagingService {

    @Autowired
    product_packagingOdooClient product_packagingOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_packagingClientModel clientModel = new product_packagingClientModel();
        clientModel.setId(id);
		product_packagingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Product_packaging et) {
        product_packagingClientModel clientModel = convert2Model(et,null);
		product_packagingOdooClient.create(clientModel);
        Product_packaging rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_packaging> list){
    }

    @Override
    public Product_packaging get(Integer id) {
        product_packagingClientModel clientModel = new product_packagingClientModel();
        clientModel.setId(id);
		product_packagingOdooClient.get(clientModel);
        Product_packaging et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_packaging();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Product_packaging et) {
        product_packagingClientModel clientModel = convert2Model(et,null);
		product_packagingOdooClient.update(clientModel);
        Product_packaging rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_packaging> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_packaging> searchDefault(Product_packagingSearchContext context) {
        List<Product_packaging> list = new ArrayList<Product_packaging>();
        Page<product_packagingClientModel> clientModelList = product_packagingOdooClient.search(context);
        for(product_packagingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_packaging>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_packagingClientModel convert2Model(Product_packaging domain , product_packagingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_packagingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("qtydirtyflag"))
                model.setQty(domain.getQty());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_packaging convert2Domain( product_packagingClientModel model ,Product_packaging domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_packaging();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getQtyDirtyFlag())
            domain.setQty(model.getQty());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



