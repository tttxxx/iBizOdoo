package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;

/**
 * 实体 [工作中心生产力日志] 存储模型
 */
public interface Mrp_workcenter_productivity{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 时长
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [时长]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 工作中心
     */
    String getWorkcenter_id_text();

    void setWorkcenter_id_text(String workcenter_id_text);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_id_textDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 有效性类别
     */
    String getLoss_type();

    void setLoss_type(String loss_type);

    /**
     * 获取 [有效性类别]脏标记
     */
    boolean getLoss_typeDirtyFlag();

    /**
     * 工单
     */
    String getWorkorder_id_text();

    void setWorkorder_id_text(String workorder_id_text);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_id_textDirtyFlag();

    /**
     * 损失原因
     */
    String getLoss_id_text();

    void setLoss_id_text(String loss_id_text);

    /**
     * 获取 [损失原因]脏标记
     */
    boolean getLoss_id_textDirtyFlag();

    /**
     * 制造订单
     */
    Integer getProduction_id();

    void setProduction_id(Integer production_id);

    /**
     * 获取 [制造订单]脏标记
     */
    boolean getProduction_idDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 工单
     */
    Integer getWorkorder_id();

    void setWorkorder_id(Integer workorder_id);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_idDirtyFlag();

    /**
     * 工作中心
     */
    Integer getWorkcenter_id();

    void setWorkcenter_id(Integer workcenter_id);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_idDirtyFlag();

    /**
     * 损失原因
     */
    Integer getLoss_id();

    void setLoss_id(Integer loss_id);

    /**
     * 获取 [损失原因]脏标记
     */
    boolean getLoss_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
