package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_account] 服务对象接口
 */
public interface Iaccount_analytic_accountClientService{

    public Iaccount_analytic_account createModel() ;

    public void updateBatch(List<Iaccount_analytic_account> account_analytic_accounts);

    public Page<Iaccount_analytic_account> search(SearchContext context);

    public void create(Iaccount_analytic_account account_analytic_account);

    public void remove(Iaccount_analytic_account account_analytic_account);

    public void removeBatch(List<Iaccount_analytic_account> account_analytic_accounts);

    public void createBatch(List<Iaccount_analytic_account> account_analytic_accounts);

    public void get(Iaccount_analytic_account account_analytic_account);

    public void update(Iaccount_analytic_account account_analytic_account);

    public Page<Iaccount_analytic_account> select(SearchContext context);

}
