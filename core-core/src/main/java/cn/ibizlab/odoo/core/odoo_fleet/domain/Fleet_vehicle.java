package cn.ibizlab.odoo.core.odoo_fleet.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [车辆] 对象
 */
@Data
public class Fleet_vehicle extends EntityClient implements Serializable {

    /**
     * 燃油类型
     */
    @DEField(name = "fuel_type")
    @JSONField(name = "fuel_type")
    @JsonProperty("fuel_type")
    private String fuelType;

    /**
     * 标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 动力
     */
    @JSONField(name = "power")
    @JsonProperty("power")
    private Integer power;

    /**
     * 需要激活
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 残余价值
     */
    @DEField(name = "residual_value")
    @JSONField(name = "residual_value")
    @JsonProperty("residual_value")
    private Double residualValue;

    /**
     * 最新里程表
     */
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 加油记录统计
     */
    @JSONField(name = "fuel_logs_count")
    @JsonProperty("fuel_logs_count")
    private Integer fuelLogsCount;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 型号年份
     */
    @DEField(name = "model_year")
    @JSONField(name = "model_year")
    @JsonProperty("model_year")
    private String modelYear;

    /**
     * 目录值（包括增值税）
     */
    @DEField(name = "car_value")
    @JSONField(name = "car_value")
    @JsonProperty("car_value")
    private Double carValue;

    /**
     * 车辆牌照
     */
    @DEField(name = "license_plate")
    @JSONField(name = "license_plate")
    @JsonProperty("license_plate")
    private String licensePlate;

    /**
     * 有逾期合同
     */
    @JSONField(name = "contract_renewal_overdue")
    @JsonProperty("contract_renewal_overdue")
    private String contractRenewalOverdue;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 注册日期
     */
    @DEField(name = "acquisition_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "acquisition_date" , format="yyyy-MM-dd")
    @JsonProperty("acquisition_date")
    private Timestamp acquisitionDate;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 车船税
     */
    @DEField(name = "horsepower_tax")
    @JSONField(name = "horsepower_tax")
    @JsonProperty("horsepower_tax")
    private Double horsepowerTax;

    /**
     * 地点
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 马力
     */
    @JSONField(name = "horsepower")
    @JsonProperty("horsepower")
    private Integer horsepower;

    /**
     * 车架号
     */
    @DEField(name = "vin_sn")
    @JSONField(name = "vin_sn")
    @JsonProperty("vin_sn")
    private String vinSn;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 里程表
     */
    @JSONField(name = "odometer_count")
    @JsonProperty("odometer_count")
    private Integer odometerCount;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 需马上续签合同的名称
     */
    @JSONField(name = "contract_renewal_name")
    @JsonProperty("contract_renewal_name")
    private String contractRenewalName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 座位数
     */
    @JSONField(name = "seats")
    @JsonProperty("seats")
    private Integer seats;

    /**
     * 燃油记录
     */
    @JSONField(name = "log_fuel")
    @JsonProperty("log_fuel")
    private String logFuel;

    /**
     * 费用
     */
    @JSONField(name = "cost_count")
    @JsonProperty("cost_count")
    private Integer costCount;

    /**
     * 指派记录
     */
    @JSONField(name = "log_drivers")
    @JsonProperty("log_drivers")
    private String logDrivers;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 有合同待续签
     */
    @JSONField(name = "contract_renewal_due_soon")
    @JsonProperty("contract_renewal_due_soon")
    private String contractRenewalDueSoon;

    /**
     * 里程表单位
     */
    @DEField(name = "odometer_unit")
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    private String odometerUnit;

    /**
     * 二氧化碳排放量
     */
    @JSONField(name = "co2")
    @JsonProperty("co2")
    private Double co2;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 合同
     */
    @JSONField(name = "log_contracts")
    @JsonProperty("log_contracts")
    private String logContracts;

    /**
     * 首次合同日期
     */
    @DEField(name = "first_contract_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "first_contract_date" , format="yyyy-MM-dd")
    @JsonProperty("first_contract_date")
    private Timestamp firstContractDate;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 颜色
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private String color;

    /**
     * 车门数量
     */
    @JSONField(name = "doors")
    @JsonProperty("doors")
    private Integer doors;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 服务记录
     */
    @JSONField(name = "log_services")
    @JsonProperty("log_services")
    private String logServices;

    /**
     * 合同统计
     */
    @JSONField(name = "contract_count")
    @JsonProperty("contract_count")
    private Integer contractCount;

    /**
     * 变速器
     */
    @JSONField(name = "transmission")
    @JsonProperty("transmission")
    private String transmission;

    /**
     * 服务
     */
    @JSONField(name = "service_count")
    @JsonProperty("service_count")
    private Integer serviceCount;

    /**
     * 截止或者逾期减一的合同总计
     */
    @JSONField(name = "contract_renewal_total")
    @JsonProperty("contract_renewal_total")
    private String contractRenewalTotal;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 驾驶员
     */
    @JSONField(name = "driver_id_text")
    @JsonProperty("driver_id_text")
    private String driverIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 车辆标志图片(普通大小)
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 徽标
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 型号
     */
    @JSONField(name = "model_id_text")
    @JsonProperty("model_id_text")
    private String modelIdText;

    /**
     * 车辆标志图片(小图片)
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 品牌
     */
    @JSONField(name = "brand_id_text")
    @JsonProperty("brand_id_text")
    private String brandIdText;

    /**
     * 状态
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 型号
     */
    @DEField(name = "model_id")
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 品牌
     */
    @DEField(name = "brand_id")
    @JSONField(name = "brand_id")
    @JsonProperty("brand_id")
    private Integer brandId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 状态
     */
    @DEField(name = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 驾驶员
     */
    @DEField(name = "driver_id")
    @JSONField(name = "driver_id")
    @JsonProperty("driver_id")
    private Integer driverId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoobrand")
    @JsonProperty("odoobrand")
    private cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand odooBrand;

    /**
     * 
     */
    @JSONField(name = "odoomodel")
    @JsonProperty("odoomodel")
    private cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model odooModel;

    /**
     * 
     */
    @JSONField(name = "odoostate")
    @JsonProperty("odoostate")
    private cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state odooState;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoodriver")
    @JsonProperty("odoodriver")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooDriver;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [燃油类型]
     */
    public void setFuelType(String fuelType){
        this.fuelType = fuelType ;
        this.modify("fuel_type",fuelType);
    }
    /**
     * 设置 [动力]
     */
    public void setPower(Integer power){
        this.power = power ;
        this.modify("power",power);
    }
    /**
     * 设置 [残余价值]
     */
    public void setResidualValue(Double residualValue){
        this.residualValue = residualValue ;
        this.modify("residual_value",residualValue);
    }
    /**
     * 设置 [型号年份]
     */
    public void setModelYear(String modelYear){
        this.modelYear = modelYear ;
        this.modify("model_year",modelYear);
    }
    /**
     * 设置 [目录值（包括增值税）]
     */
    public void setCarValue(Double carValue){
        this.carValue = carValue ;
        this.modify("car_value",carValue);
    }
    /**
     * 设置 [车辆牌照]
     */
    public void setLicensePlate(String licensePlate){
        this.licensePlate = licensePlate ;
        this.modify("license_plate",licensePlate);
    }
    /**
     * 设置 [注册日期]
     */
    public void setAcquisitionDate(Timestamp acquisitionDate){
        this.acquisitionDate = acquisitionDate ;
        this.modify("acquisition_date",acquisitionDate);
    }
    /**
     * 设置 [车船税]
     */
    public void setHorsepowerTax(Double horsepowerTax){
        this.horsepowerTax = horsepowerTax ;
        this.modify("horsepower_tax",horsepowerTax);
    }
    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [马力]
     */
    public void setHorsepower(Integer horsepower){
        this.horsepower = horsepower ;
        this.modify("horsepower",horsepower);
    }
    /**
     * 设置 [车架号]
     */
    public void setVinSn(String vinSn){
        this.vinSn = vinSn ;
        this.modify("vin_sn",vinSn);
    }
    /**
     * 设置 [座位数]
     */
    public void setSeats(Integer seats){
        this.seats = seats ;
        this.modify("seats",seats);
    }
    /**
     * 设置 [里程表单位]
     */
    public void setOdometerUnit(String odometerUnit){
        this.odometerUnit = odometerUnit ;
        this.modify("odometer_unit",odometerUnit);
    }
    /**
     * 设置 [二氧化碳排放量]
     */
    public void setCo2(Double co2){
        this.co2 = co2 ;
        this.modify("co2",co2);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [首次合同日期]
     */
    public void setFirstContractDate(Timestamp firstContractDate){
        this.firstContractDate = firstContractDate ;
        this.modify("first_contract_date",firstContractDate);
    }
    /**
     * 设置 [颜色]
     */
    public void setColor(String color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [车门数量]
     */
    public void setDoors(Integer doors){
        this.doors = doors ;
        this.modify("doors",doors);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [变速器]
     */
    public void setTransmission(String transmission){
        this.transmission = transmission ;
        this.modify("transmission",transmission);
    }
    /**
     * 设置 [型号]
     */
    public void setModelId(Integer modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }
    /**
     * 设置 [品牌]
     */
    public void setBrandId(Integer brandId){
        this.brandId = brandId ;
        this.modify("brand_id",brandId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [状态]
     */
    public void setStateId(Integer stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }
    /**
     * 设置 [驾驶员]
     */
    public void setDriverId(Integer driverId){
        this.driverId = driverId ;
        this.modify("driver_id",driverId);
    }

}


