package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_degreeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_recruitment_degreeOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_recruitment_degreeClientModel;

/**
 * 实体[申请人学历] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_recruitment_degreeServiceImpl implements IHr_recruitment_degreeService {

    @Autowired
    hr_recruitment_degreeOdooClient hr_recruitment_degreeOdooClient;


    @Override
    public boolean update(Hr_recruitment_degree et) {
        hr_recruitment_degreeClientModel clientModel = convert2Model(et,null);
		hr_recruitment_degreeOdooClient.update(clientModel);
        Hr_recruitment_degree rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_recruitment_degree> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_recruitment_degreeClientModel clientModel = new hr_recruitment_degreeClientModel();
        clientModel.setId(id);
		hr_recruitment_degreeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Hr_recruitment_degree get(Integer id) {
        hr_recruitment_degreeClientModel clientModel = new hr_recruitment_degreeClientModel();
        clientModel.setId(id);
		hr_recruitment_degreeOdooClient.get(clientModel);
        Hr_recruitment_degree et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_recruitment_degree();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Hr_recruitment_degree et) {
        hr_recruitment_degreeClientModel clientModel = convert2Model(et,null);
		hr_recruitment_degreeOdooClient.create(clientModel);
        Hr_recruitment_degree rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_recruitment_degree> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context) {
        List<Hr_recruitment_degree> list = new ArrayList<Hr_recruitment_degree>();
        Page<hr_recruitment_degreeClientModel> clientModelList = hr_recruitment_degreeOdooClient.search(context);
        for(hr_recruitment_degreeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_recruitment_degree>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_recruitment_degreeClientModel convert2Model(Hr_recruitment_degree domain , hr_recruitment_degreeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_recruitment_degreeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_recruitment_degree convert2Domain( hr_recruitment_degreeClientModel model ,Hr_recruitment_degree domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_recruitment_degree();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



