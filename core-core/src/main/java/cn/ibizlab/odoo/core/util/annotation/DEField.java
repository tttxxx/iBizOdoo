package cn.ibizlab.odoo.core.util.annotation;

import cn.ibizlab.odoo.core.util.enums.DEFieldDefaultValueType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD})
public @interface DEField
{
	/**
	 * 是否为数据主键
	 * @return
	 */
	boolean isKeyField() default false;
	/**
	 * 填充模式
	 * @return
	 */
	String defaultValue() default "";

	/**
	 * 预置属性类型
	 * @return
	 */
	DEFieldDefaultValueType defaultValueType() default DEFieldDefaultValueType.NONE;
}

