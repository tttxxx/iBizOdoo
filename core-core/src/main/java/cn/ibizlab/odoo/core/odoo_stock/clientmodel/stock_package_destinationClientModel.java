package cn.ibizlab.odoo.core.odoo_stock.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_package_destination] 对象
 */
public class stock_package_destinationClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 筛选的位置
     */
    public String filtered_location;

    @JsonIgnore
    public boolean filtered_locationDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 目的位置
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 目的位置
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 凭证明细
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 分拣
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 分拣
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [筛选的位置]
     */
    @JsonProperty("filtered_location")
    public String getFiltered_location(){
        return this.filtered_location ;
    }

    /**
     * 设置 [筛选的位置]
     */
    @JsonProperty("filtered_location")
    public void setFiltered_location(String  filtered_location){
        this.filtered_location = filtered_location ;
        this.filtered_locationDirtyFlag = true ;
    }

     /**
     * 获取 [筛选的位置]脏标记
     */
    @JsonIgnore
    public boolean getFiltered_locationDirtyFlag(){
        return this.filtered_locationDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [凭证明细]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("filtered_location") instanceof Boolean)&& map.get("filtered_location")!=null){
			Object[] objs = (Object[])map.get("filtered_location");
			if(objs.length > 0){
				Integer[] filtered_location = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setFiltered_location(Arrays.toString(filtered_location).replace(" ",""));
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 0){
				this.setLocation_dest_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 1){
				this.setLocation_dest_id_text((String)objs[1]);
			}
		}
		if(!(map.get("move_line_ids") instanceof Boolean)&& map.get("move_line_ids")!=null){
			Object[] objs = (Object[])map.get("move_line_ids");
			if(objs.length > 0){
				Integer[] move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_ids(Arrays.toString(move_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 0){
				this.setPicking_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 1){
				this.setPicking_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFiltered_location()!=null&&this.getFiltered_locationDirtyFlag()){
			map.put("filtered_location",this.getFiltered_location());
		}else if(this.getFiltered_locationDirtyFlag()){
			map.put("filtered_location",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLocation_dest_id()!=null&&this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",this.getLocation_dest_id());
		}else if(this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_dest_id_text()!=null&&this.getLocation_dest_id_textDirtyFlag()){
			//忽略文本外键location_dest_id_text
		}else if(this.getLocation_dest_id_textDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getMove_line_ids()!=null&&this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",this.getMove_line_ids());
		}else if(this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",false);
		}
		if(this.getPicking_id()!=null&&this.getPicking_idDirtyFlag()){
			map.put("picking_id",this.getPicking_id());
		}else if(this.getPicking_idDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getPicking_id_text()!=null&&this.getPicking_id_textDirtyFlag()){
			//忽略文本外键picking_id_text
		}else if(this.getPicking_id_textDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
