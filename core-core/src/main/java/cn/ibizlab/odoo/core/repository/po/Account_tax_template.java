package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_templateSearchContext;

/**
 * 实体 [税率模板] 存储模型
 */
public interface Account_tax_template{

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 显示在发票上
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [显示在发票上]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 影响后续税收
     */
    String getInclude_base_amount();

    void setInclude_base_amount(String include_base_amount);

    /**
     * 获取 [影响后续税收]脏标记
     */
    boolean getInclude_base_amountDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 应有税金
     */
    String getTax_exigibility();

    void setTax_exigibility(String tax_exigibility);

    /**
     * 获取 [应有税金]脏标记
     */
    boolean getTax_exigibilityDirtyFlag();

    /**
     * 税率名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [税率名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 包含在价格中
     */
    String getPrice_include();

    void setPrice_include(String price_include);

    /**
     * 获取 [包含在价格中]脏标记
     */
    boolean getPrice_includeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 科目标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [科目标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 税率计算
     */
    String getAmount_type();

    void setAmount_type(String amount_type);

    /**
     * 获取 [税率计算]脏标记
     */
    boolean getAmount_typeDirtyFlag();

    /**
     * 税范围
     */
    String getType_tax_use();

    void setType_tax_use(String type_tax_use);

    /**
     * 获取 [税范围]脏标记
     */
    boolean getType_tax_useDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 分析成本
     */
    String getAnalytic();

    void setAnalytic(String analytic);

    /**
     * 获取 [分析成本]脏标记
     */
    boolean getAnalyticDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 子级税
     */
    String getChildren_tax_ids();

    void setChildren_tax_ids(String children_tax_ids);

    /**
     * 获取 [子级税]脏标记
     */
    boolean getChildren_tax_idsDirtyFlag();

    /**
     * 税应收科目
     */
    String getCash_basis_account_id_text();

    void setCash_basis_account_id_text(String cash_basis_account_id_text);

    /**
     * 获取 [税应收科目]脏标记
     */
    boolean getCash_basis_account_id_textDirtyFlag();

    /**
     * 表模板
     */
    String getChart_template_id_text();

    void setChart_template_id_text(String chart_template_id_text);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 基本税应收科目
     */
    String getCash_basis_base_account_id_text();

    void setCash_basis_base_account_id_text(String cash_basis_base_account_id_text);

    /**
     * 获取 [基本税应收科目]脏标记
     */
    boolean getCash_basis_base_account_id_textDirtyFlag();

    /**
     * 税组
     */
    String getTax_group_id_text();

    void setTax_group_id_text(String tax_group_id_text);

    /**
     * 获取 [税组]脏标记
     */
    boolean getTax_group_id_textDirtyFlag();

    /**
     * 税率科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 退款的税金科目
     */
    String getRefund_account_id_text();

    void setRefund_account_id_text(String refund_account_id_text);

    /**
     * 获取 [退款的税金科目]脏标记
     */
    boolean getRefund_account_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 表模板
     */
    Integer getChart_template_id();

    void setChart_template_id(Integer chart_template_id);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_idDirtyFlag();

    /**
     * 退款的税金科目
     */
    Integer getRefund_account_id();

    void setRefund_account_id(Integer refund_account_id);

    /**
     * 获取 [退款的税金科目]脏标记
     */
    boolean getRefund_account_idDirtyFlag();

    /**
     * 税应收科目
     */
    Integer getCash_basis_account_id();

    void setCash_basis_account_id(Integer cash_basis_account_id);

    /**
     * 获取 [税应收科目]脏标记
     */
    boolean getCash_basis_account_idDirtyFlag();

    /**
     * 税率科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 基本税应收科目
     */
    Integer getCash_basis_base_account_id();

    void setCash_basis_base_account_id(Integer cash_basis_base_account_id);

    /**
     * 获取 [基本税应收科目]脏标记
     */
    boolean getCash_basis_base_account_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 税组
     */
    Integer getTax_group_id();

    void setTax_group_id(Integer tax_group_id);

    /**
     * 获取 [税组]脏标记
     */
    boolean getTax_group_idDirtyFlag();

}
