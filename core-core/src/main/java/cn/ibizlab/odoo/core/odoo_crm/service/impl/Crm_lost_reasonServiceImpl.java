package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lost_reasonService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_lost_reasonOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_lost_reasonClientModel;

/**
 * 实体[丢单原因] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lost_reasonServiceImpl implements ICrm_lost_reasonService {

    @Autowired
    crm_lost_reasonOdooClient crm_lost_reasonOdooClient;


    @Override
    public Crm_lost_reason get(Integer id) {
        crm_lost_reasonClientModel clientModel = new crm_lost_reasonClientModel();
        clientModel.setId(id);
		crm_lost_reasonOdooClient.get(clientModel);
        Crm_lost_reason et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_lost_reason();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        crm_lost_reasonClientModel clientModel = new crm_lost_reasonClientModel();
        clientModel.setId(id);
		crm_lost_reasonOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Crm_lost_reason et) {
        crm_lost_reasonClientModel clientModel = convert2Model(et,null);
		crm_lost_reasonOdooClient.update(clientModel);
        Crm_lost_reason rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_lost_reason> list){
    }

    @Override
    public boolean create(Crm_lost_reason et) {
        crm_lost_reasonClientModel clientModel = convert2Model(et,null);
		crm_lost_reasonOdooClient.create(clientModel);
        Crm_lost_reason rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lost_reason> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context) {
        List<Crm_lost_reason> list = new ArrayList<Crm_lost_reason>();
        Page<crm_lost_reasonClientModel> clientModelList = crm_lost_reasonOdooClient.search(context);
        for(crm_lost_reasonClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_lost_reason>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_lost_reasonClientModel convert2Model(Crm_lost_reason domain , crm_lost_reasonClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_lost_reasonClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_lost_reason convert2Domain( crm_lost_reasonClientModel model ,Crm_lost_reason domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_lost_reason();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



