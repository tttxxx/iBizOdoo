package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_tax] 对象
 */
public class account_taxClientModel implements Serializable{

    /**
     * 税率科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 税率科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 税率计算
     */
    public String amount_type;

    @JsonIgnore
    public boolean amount_typeDirtyFlag;
    
    /**
     * 包含在分析成本
     */
    public String analytic;

    @JsonIgnore
    public boolean analyticDirtyFlag;
    
    /**
     * 税应收科目
     */
    public Integer cash_basis_account_id;

    @JsonIgnore
    public boolean cash_basis_account_idDirtyFlag;
    
    /**
     * 税应收科目
     */
    public String cash_basis_account_id_text;

    @JsonIgnore
    public boolean cash_basis_account_id_textDirtyFlag;
    
    /**
     * 基本税应收科目
     */
    public Integer cash_basis_base_account_id;

    @JsonIgnore
    public boolean cash_basis_base_account_idDirtyFlag;
    
    /**
     * 基本税应收科目
     */
    public String cash_basis_base_account_id_text;

    @JsonIgnore
    public boolean cash_basis_base_account_id_textDirtyFlag;
    
    /**
     * 子级税
     */
    public String children_tax_ids;

    @JsonIgnore
    public boolean children_tax_idsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 发票上的标签
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 隐藏现金收付制选项
     */
    public String hide_tax_exigibility;

    @JsonIgnore
    public boolean hide_tax_exigibilityDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 影响后续税收的基础
     */
    public String include_base_amount;

    @JsonIgnore
    public boolean include_base_amountDirtyFlag;
    
    /**
     * 税率名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 包含在价格中
     */
    public String price_include;

    @JsonIgnore
    public boolean price_includeDirtyFlag;
    
    /**
     * 退款单的税率科目
     */
    public Integer refund_account_id;

    @JsonIgnore
    public boolean refund_account_idDirtyFlag;
    
    /**
     * 退款单的税率科目
     */
    public String refund_account_id_text;

    @JsonIgnore
    public boolean refund_account_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 应有税金
     */
    public String tax_exigibility;

    @JsonIgnore
    public boolean tax_exigibilityDirtyFlag;
    
    /**
     * 税组
     */
    public Integer tax_group_id;

    @JsonIgnore
    public boolean tax_group_idDirtyFlag;
    
    /**
     * 税组
     */
    public String tax_group_id_text;

    @JsonIgnore
    public boolean tax_group_id_textDirtyFlag;
    
    /**
     * 税范围
     */
    public String type_tax_use;

    @JsonIgnore
    public boolean type_tax_useDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [税率计算]
     */
    @JsonProperty("amount_type")
    public String getAmount_type(){
        return this.amount_type ;
    }

    /**
     * 设置 [税率计算]
     */
    @JsonProperty("amount_type")
    public void setAmount_type(String  amount_type){
        this.amount_type = amount_type ;
        this.amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [税率计算]脏标记
     */
    @JsonIgnore
    public boolean getAmount_typeDirtyFlag(){
        return this.amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [包含在分析成本]
     */
    @JsonProperty("analytic")
    public String getAnalytic(){
        return this.analytic ;
    }

    /**
     * 设置 [包含在分析成本]
     */
    @JsonProperty("analytic")
    public void setAnalytic(String  analytic){
        this.analytic = analytic ;
        this.analyticDirtyFlag = true ;
    }

     /**
     * 获取 [包含在分析成本]脏标记
     */
    @JsonIgnore
    public boolean getAnalyticDirtyFlag(){
        return this.analyticDirtyFlag ;
    }   

    /**
     * 获取 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id")
    public Integer getCash_basis_account_id(){
        return this.cash_basis_account_id ;
    }

    /**
     * 设置 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id")
    public void setCash_basis_account_id(Integer  cash_basis_account_id){
        this.cash_basis_account_id = cash_basis_account_id ;
        this.cash_basis_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_idDirtyFlag(){
        return this.cash_basis_account_idDirtyFlag ;
    }   

    /**
     * 获取 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id_text")
    public String getCash_basis_account_id_text(){
        return this.cash_basis_account_id_text ;
    }

    /**
     * 设置 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id_text")
    public void setCash_basis_account_id_text(String  cash_basis_account_id_text){
        this.cash_basis_account_id_text = cash_basis_account_id_text ;
        this.cash_basis_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_id_textDirtyFlag(){
        return this.cash_basis_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id")
    public Integer getCash_basis_base_account_id(){
        return this.cash_basis_base_account_id ;
    }

    /**
     * 设置 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id")
    public void setCash_basis_base_account_id(Integer  cash_basis_base_account_id){
        this.cash_basis_base_account_id = cash_basis_base_account_id ;
        this.cash_basis_base_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [基本税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_idDirtyFlag(){
        return this.cash_basis_base_account_idDirtyFlag ;
    }   

    /**
     * 获取 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public String getCash_basis_base_account_id_text(){
        return this.cash_basis_base_account_id_text ;
    }

    /**
     * 设置 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public void setCash_basis_base_account_id_text(String  cash_basis_base_account_id_text){
        this.cash_basis_base_account_id_text = cash_basis_base_account_id_text ;
        this.cash_basis_base_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [基本税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_id_textDirtyFlag(){
        return this.cash_basis_base_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [子级税]
     */
    @JsonProperty("children_tax_ids")
    public String getChildren_tax_ids(){
        return this.children_tax_ids ;
    }

    /**
     * 设置 [子级税]
     */
    @JsonProperty("children_tax_ids")
    public void setChildren_tax_ids(String  children_tax_ids){
        this.children_tax_ids = children_tax_ids ;
        this.children_tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [子级税]脏标记
     */
    @JsonIgnore
    public boolean getChildren_tax_idsDirtyFlag(){
        return this.children_tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [发票上的标签]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [发票上的标签]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [发票上的标签]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [隐藏现金收付制选项]
     */
    @JsonProperty("hide_tax_exigibility")
    public String getHide_tax_exigibility(){
        return this.hide_tax_exigibility ;
    }

    /**
     * 设置 [隐藏现金收付制选项]
     */
    @JsonProperty("hide_tax_exigibility")
    public void setHide_tax_exigibility(String  hide_tax_exigibility){
        this.hide_tax_exigibility = hide_tax_exigibility ;
        this.hide_tax_exigibilityDirtyFlag = true ;
    }

     /**
     * 获取 [隐藏现金收付制选项]脏标记
     */
    @JsonIgnore
    public boolean getHide_tax_exigibilityDirtyFlag(){
        return this.hide_tax_exigibilityDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [影响后续税收的基础]
     */
    @JsonProperty("include_base_amount")
    public String getInclude_base_amount(){
        return this.include_base_amount ;
    }

    /**
     * 设置 [影响后续税收的基础]
     */
    @JsonProperty("include_base_amount")
    public void setInclude_base_amount(String  include_base_amount){
        this.include_base_amount = include_base_amount ;
        this.include_base_amountDirtyFlag = true ;
    }

     /**
     * 获取 [影响后续税收的基础]脏标记
     */
    @JsonIgnore
    public boolean getInclude_base_amountDirtyFlag(){
        return this.include_base_amountDirtyFlag ;
    }   

    /**
     * 获取 [税率名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [税率名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [税率名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [包含在价格中]
     */
    @JsonProperty("price_include")
    public String getPrice_include(){
        return this.price_include ;
    }

    /**
     * 设置 [包含在价格中]
     */
    @JsonProperty("price_include")
    public void setPrice_include(String  price_include){
        this.price_include = price_include ;
        this.price_includeDirtyFlag = true ;
    }

     /**
     * 获取 [包含在价格中]脏标记
     */
    @JsonIgnore
    public boolean getPrice_includeDirtyFlag(){
        return this.price_includeDirtyFlag ;
    }   

    /**
     * 获取 [退款单的税率科目]
     */
    @JsonProperty("refund_account_id")
    public Integer getRefund_account_id(){
        return this.refund_account_id ;
    }

    /**
     * 设置 [退款单的税率科目]
     */
    @JsonProperty("refund_account_id")
    public void setRefund_account_id(Integer  refund_account_id){
        this.refund_account_id = refund_account_id ;
        this.refund_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [退款单的税率科目]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_idDirtyFlag(){
        return this.refund_account_idDirtyFlag ;
    }   

    /**
     * 获取 [退款单的税率科目]
     */
    @JsonProperty("refund_account_id_text")
    public String getRefund_account_id_text(){
        return this.refund_account_id_text ;
    }

    /**
     * 设置 [退款单的税率科目]
     */
    @JsonProperty("refund_account_id_text")
    public void setRefund_account_id_text(String  refund_account_id_text){
        this.refund_account_id_text = refund_account_id_text ;
        this.refund_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [退款单的税率科目]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_id_textDirtyFlag(){
        return this.refund_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [应有税金]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return this.tax_exigibility ;
    }

    /**
     * 设置 [应有税金]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

     /**
     * 获取 [应有税金]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return this.tax_exigibilityDirtyFlag ;
    }   

    /**
     * 获取 [税组]
     */
    @JsonProperty("tax_group_id")
    public Integer getTax_group_id(){
        return this.tax_group_id ;
    }

    /**
     * 设置 [税组]
     */
    @JsonProperty("tax_group_id")
    public void setTax_group_id(Integer  tax_group_id){
        this.tax_group_id = tax_group_id ;
        this.tax_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [税组]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_idDirtyFlag(){
        return this.tax_group_idDirtyFlag ;
    }   

    /**
     * 获取 [税组]
     */
    @JsonProperty("tax_group_id_text")
    public String getTax_group_id_text(){
        return this.tax_group_id_text ;
    }

    /**
     * 设置 [税组]
     */
    @JsonProperty("tax_group_id_text")
    public void setTax_group_id_text(String  tax_group_id_text){
        this.tax_group_id_text = tax_group_id_text ;
        this.tax_group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税组]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_id_textDirtyFlag(){
        return this.tax_group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税范围]
     */
    @JsonProperty("type_tax_use")
    public String getType_tax_use(){
        return this.type_tax_use ;
    }

    /**
     * 设置 [税范围]
     */
    @JsonProperty("type_tax_use")
    public void setType_tax_use(String  type_tax_use){
        this.type_tax_use = type_tax_use ;
        this.type_tax_useDirtyFlag = true ;
    }

     /**
     * 获取 [税范围]脏标记
     */
    @JsonIgnore
    public boolean getType_tax_useDirtyFlag(){
        return this.type_tax_useDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 0){
				this.setAccount_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 1){
				this.setAccount_id_text((String)objs[1]);
			}
		}
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("amount_type") instanceof Boolean)&& map.get("amount_type")!=null){
			this.setAmount_type((String)map.get("amount_type"));
		}
		if(map.get("analytic") instanceof Boolean){
			this.setAnalytic(((Boolean)map.get("analytic"))? "true" : "false");
		}
		if(!(map.get("cash_basis_account_id") instanceof Boolean)&& map.get("cash_basis_account_id")!=null){
			Object[] objs = (Object[])map.get("cash_basis_account_id");
			if(objs.length > 0){
				this.setCash_basis_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cash_basis_account_id") instanceof Boolean)&& map.get("cash_basis_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("cash_basis_account_id");
			if(objs.length > 1){
				this.setCash_basis_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("cash_basis_base_account_id") instanceof Boolean)&& map.get("cash_basis_base_account_id")!=null){
			Object[] objs = (Object[])map.get("cash_basis_base_account_id");
			if(objs.length > 0){
				this.setCash_basis_base_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cash_basis_base_account_id") instanceof Boolean)&& map.get("cash_basis_base_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("cash_basis_base_account_id");
			if(objs.length > 1){
				this.setCash_basis_base_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("children_tax_ids") instanceof Boolean)&& map.get("children_tax_ids")!=null){
			Object[] objs = (Object[])map.get("children_tax_ids");
			if(objs.length > 0){
				Integer[] children_tax_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChildren_tax_ids(Arrays.toString(children_tax_ids).replace(" ",""));
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("hide_tax_exigibility") instanceof Boolean){
			this.setHide_tax_exigibility(((Boolean)map.get("hide_tax_exigibility"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("include_base_amount") instanceof Boolean){
			this.setInclude_base_amount(((Boolean)map.get("include_base_amount"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(map.get("price_include") instanceof Boolean){
			this.setPrice_include(((Boolean)map.get("price_include"))? "true" : "false");
		}
		if(!(map.get("refund_account_id") instanceof Boolean)&& map.get("refund_account_id")!=null){
			Object[] objs = (Object[])map.get("refund_account_id");
			if(objs.length > 0){
				this.setRefund_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("refund_account_id") instanceof Boolean)&& map.get("refund_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("refund_account_id");
			if(objs.length > 1){
				this.setRefund_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("tag_ids") instanceof Boolean)&& map.get("tag_ids")!=null){
			Object[] objs = (Object[])map.get("tag_ids");
			if(objs.length > 0){
				Integer[] tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTag_ids(Arrays.toString(tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("tax_exigibility") instanceof Boolean)&& map.get("tax_exigibility")!=null){
			this.setTax_exigibility((String)map.get("tax_exigibility"));
		}
		if(!(map.get("tax_group_id") instanceof Boolean)&& map.get("tax_group_id")!=null){
			Object[] objs = (Object[])map.get("tax_group_id");
			if(objs.length > 0){
				this.setTax_group_id((Integer)objs[0]);
			}
		}
		if(!(map.get("tax_group_id") instanceof Boolean)&& map.get("tax_group_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("tax_group_id");
			if(objs.length > 1){
				this.setTax_group_id_text((String)objs[1]);
			}
		}
		if(!(map.get("type_tax_use") instanceof Boolean)&& map.get("type_tax_use")!=null){
			this.setType_tax_use((String)map.get("type_tax_use"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_id()!=null&&this.getAccount_idDirtyFlag()){
			map.put("account_id",this.getAccount_id());
		}else if(this.getAccount_idDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAccount_id_text()!=null&&this.getAccount_id_textDirtyFlag()){
			//忽略文本外键account_id_text
		}else if(this.getAccount_id_textDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getAmount_type()!=null&&this.getAmount_typeDirtyFlag()){
			map.put("amount_type",this.getAmount_type());
		}else if(this.getAmount_typeDirtyFlag()){
			map.put("amount_type",false);
		}
		if(this.getAnalytic()!=null&&this.getAnalyticDirtyFlag()){
			map.put("analytic",Boolean.parseBoolean(this.getAnalytic()));		
		}		if(this.getCash_basis_account_id()!=null&&this.getCash_basis_account_idDirtyFlag()){
			map.put("cash_basis_account_id",this.getCash_basis_account_id());
		}else if(this.getCash_basis_account_idDirtyFlag()){
			map.put("cash_basis_account_id",false);
		}
		if(this.getCash_basis_account_id_text()!=null&&this.getCash_basis_account_id_textDirtyFlag()){
			//忽略文本外键cash_basis_account_id_text
		}else if(this.getCash_basis_account_id_textDirtyFlag()){
			map.put("cash_basis_account_id",false);
		}
		if(this.getCash_basis_base_account_id()!=null&&this.getCash_basis_base_account_idDirtyFlag()){
			map.put("cash_basis_base_account_id",this.getCash_basis_base_account_id());
		}else if(this.getCash_basis_base_account_idDirtyFlag()){
			map.put("cash_basis_base_account_id",false);
		}
		if(this.getCash_basis_base_account_id_text()!=null&&this.getCash_basis_base_account_id_textDirtyFlag()){
			//忽略文本外键cash_basis_base_account_id_text
		}else if(this.getCash_basis_base_account_id_textDirtyFlag()){
			map.put("cash_basis_base_account_id",false);
		}
		if(this.getChildren_tax_ids()!=null&&this.getChildren_tax_idsDirtyFlag()){
			map.put("children_tax_ids",this.getChildren_tax_ids());
		}else if(this.getChildren_tax_idsDirtyFlag()){
			map.put("children_tax_ids",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getHide_tax_exigibility()!=null&&this.getHide_tax_exigibilityDirtyFlag()){
			map.put("hide_tax_exigibility",Boolean.parseBoolean(this.getHide_tax_exigibility()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInclude_base_amount()!=null&&this.getInclude_base_amountDirtyFlag()){
			map.put("include_base_amount",Boolean.parseBoolean(this.getInclude_base_amount()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPrice_include()!=null&&this.getPrice_includeDirtyFlag()){
			map.put("price_include",Boolean.parseBoolean(this.getPrice_include()));		
		}		if(this.getRefund_account_id()!=null&&this.getRefund_account_idDirtyFlag()){
			map.put("refund_account_id",this.getRefund_account_id());
		}else if(this.getRefund_account_idDirtyFlag()){
			map.put("refund_account_id",false);
		}
		if(this.getRefund_account_id_text()!=null&&this.getRefund_account_id_textDirtyFlag()){
			//忽略文本外键refund_account_id_text
		}else if(this.getRefund_account_id_textDirtyFlag()){
			map.put("refund_account_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTag_ids()!=null&&this.getTag_idsDirtyFlag()){
			map.put("tag_ids",this.getTag_ids());
		}else if(this.getTag_idsDirtyFlag()){
			map.put("tag_ids",false);
		}
		if(this.getTax_exigibility()!=null&&this.getTax_exigibilityDirtyFlag()){
			map.put("tax_exigibility",this.getTax_exigibility());
		}else if(this.getTax_exigibilityDirtyFlag()){
			map.put("tax_exigibility",false);
		}
		if(this.getTax_group_id()!=null&&this.getTax_group_idDirtyFlag()){
			map.put("tax_group_id",this.getTax_group_id());
		}else if(this.getTax_group_idDirtyFlag()){
			map.put("tax_group_id",false);
		}
		if(this.getTax_group_id_text()!=null&&this.getTax_group_id_textDirtyFlag()){
			//忽略文本外键tax_group_id_text
		}else if(this.getTax_group_id_textDirtyFlag()){
			map.put("tax_group_id",false);
		}
		if(this.getType_tax_use()!=null&&this.getType_tax_useDirtyFlag()){
			map.put("type_tax_use",this.getType_tax_use());
		}else if(this.getType_tax_useDirtyFlag()){
			map.put("type_tax_use",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
