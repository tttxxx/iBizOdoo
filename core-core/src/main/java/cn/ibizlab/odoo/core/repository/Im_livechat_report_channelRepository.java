package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Im_livechat_report_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;

/**
 * 实体 [实时聊天支持频道报告] 存储对象
 */
public interface Im_livechat_report_channelRepository extends Repository<Im_livechat_report_channel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Im_livechat_report_channel> searchDefault(Im_livechat_report_channelSearchContext context);

    Im_livechat_report_channel convert2PO(cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_channel domain , Im_livechat_report_channel po) ;

    cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_channel convert2Domain( Im_livechat_report_channel po ,cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_channel domain) ;

}
