package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workorderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workorderOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_workorderClientModel;

/**
 * 实体[工单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workorderServiceImpl implements IMrp_workorderService {

    @Autowired
    mrp_workorderOdooClient mrp_workorderOdooClient;


    @Override
    public boolean update(Mrp_workorder et) {
        mrp_workorderClientModel clientModel = convert2Model(et,null);
		mrp_workorderOdooClient.update(clientModel);
        Mrp_workorder rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_workorder> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_workorderClientModel clientModel = new mrp_workorderClientModel();
        clientModel.setId(id);
		mrp_workorderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_workorder get(Integer id) {
        mrp_workorderClientModel clientModel = new mrp_workorderClientModel();
        clientModel.setId(id);
		mrp_workorderOdooClient.get(clientModel);
        Mrp_workorder et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_workorder();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mrp_workorder et) {
        mrp_workorderClientModel clientModel = convert2Model(et,null);
		mrp_workorderOdooClient.create(clientModel);
        Mrp_workorder rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workorder> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context) {
        List<Mrp_workorder> list = new ArrayList<Mrp_workorder>();
        Page<mrp_workorderClientModel> clientModelList = mrp_workorderOdooClient.search(context);
        for(mrp_workorderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_workorder>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_workorderClientModel convert2Model(Mrp_workorder domain , mrp_workorderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_workorderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("time_idsdirtyflag"))
                model.setTime_ids(domain.getTimeIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("working_user_idsdirtyflag"))
                model.setWorking_user_ids(domain.getWorkingUserIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("qty_produceddirtyflag"))
                model.setQty_produced(domain.getQtyProduced());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("scrap_countdirtyflag"))
                model.setScrap_count(domain.getScrapCount());
            if((Boolean) domain.getExtensionparams().get("date_planned_startdirtyflag"))
                model.setDate_planned_start(domain.getDatePlannedStart());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("is_user_workingdirtyflag"))
                model.setIs_user_working(domain.getIsUserWorking());
            if((Boolean) domain.getExtensionparams().get("move_raw_idsdirtyflag"))
                model.setMove_raw_ids(domain.getMoveRawIds());
            if((Boolean) domain.getExtensionparams().get("last_working_user_iddirtyflag"))
                model.setLast_working_user_id(domain.getLastWorkingUserId());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("is_first_wodirtyflag"))
                model.setIs_first_wo(domain.getIsFirstWo());
            if((Boolean) domain.getExtensionparams().get("is_produceddirtyflag"))
                model.setIs_produced(domain.getIsProduced());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("duration_unitdirtyflag"))
                model.setDuration_unit(domain.getDurationUnit());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("capacitydirtyflag"))
                model.setCapacity(domain.getCapacity());
            if((Boolean) domain.getExtensionparams().get("active_move_line_idsdirtyflag"))
                model.setActive_move_line_ids(domain.getActiveMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("date_planned_finisheddirtyflag"))
                model.setDate_planned_finished(domain.getDatePlannedFinished());
            if((Boolean) domain.getExtensionparams().get("date_finisheddirtyflag"))
                model.setDate_finished(domain.getDateFinished());
            if((Boolean) domain.getExtensionparams().get("qty_remainingdirtyflag"))
                model.setQty_remaining(domain.getQtyRemaining());
            if((Boolean) domain.getExtensionparams().get("scrap_idsdirtyflag"))
                model.setScrap_ids(domain.getScrapIds());
            if((Boolean) domain.getExtensionparams().get("duration_percentdirtyflag"))
                model.setDuration_percent(domain.getDurationPercent());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("duration_expecteddirtyflag"))
                model.setDuration_expected(domain.getDurationExpected());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("qty_producingdirtyflag"))
                model.setQty_producing(domain.getQtyProducing());
            if((Boolean) domain.getExtensionparams().get("production_datedirtyflag"))
                model.setProduction_date(domain.getProductionDate());
            if((Boolean) domain.getExtensionparams().get("worksheetdirtyflag"))
                model.setWorksheet(domain.getWorksheet());
            if((Boolean) domain.getExtensionparams().get("workcenter_id_textdirtyflag"))
                model.setWorkcenter_id_text(domain.getWorkcenterIdText());
            if((Boolean) domain.getExtensionparams().get("final_lot_id_textdirtyflag"))
                model.setFinal_lot_id_text(domain.getFinalLotIdText());
            if((Boolean) domain.getExtensionparams().get("working_statedirtyflag"))
                model.setWorking_state(domain.getWorkingState());
            if((Boolean) domain.getExtensionparams().get("qty_productiondirtyflag"))
                model.setQty_production(domain.getQtyProduction());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("production_availabilitydirtyflag"))
                model.setProduction_availability(domain.getProductionAvailability());
            if((Boolean) domain.getExtensionparams().get("production_statedirtyflag"))
                model.setProduction_state(domain.getProductionState());
            if((Boolean) domain.getExtensionparams().get("next_work_order_id_textdirtyflag"))
                model.setNext_work_order_id_text(domain.getNextWorkOrderIdText());
            if((Boolean) domain.getExtensionparams().get("operation_id_textdirtyflag"))
                model.setOperation_id_text(domain.getOperationIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("production_id_textdirtyflag"))
                model.setProduction_id_text(domain.getProductionIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_trackingdirtyflag"))
                model.setProduct_tracking(domain.getProductTracking());
            if((Boolean) domain.getExtensionparams().get("final_lot_iddirtyflag"))
                model.setFinal_lot_id(domain.getFinalLotId());
            if((Boolean) domain.getExtensionparams().get("workcenter_iddirtyflag"))
                model.setWorkcenter_id(domain.getWorkcenterId());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("operation_iddirtyflag"))
                model.setOperation_id(domain.getOperationId());
            if((Boolean) domain.getExtensionparams().get("next_work_order_iddirtyflag"))
                model.setNext_work_order_id(domain.getNextWorkOrderId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_workorder convert2Domain( mrp_workorderClientModel model ,Mrp_workorder domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_workorder();
        }

        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getTime_idsDirtyFlag())
            domain.setTimeIds(model.getTime_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getWorking_user_idsDirtyFlag())
            domain.setWorkingUserIds(model.getWorking_user_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQty_producedDirtyFlag())
            domain.setQtyProduced(model.getQty_produced());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getScrap_countDirtyFlag())
            domain.setScrapCount(model.getScrap_count());
        if(model.getDate_planned_startDirtyFlag())
            domain.setDatePlannedStart(model.getDate_planned_start());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getIs_user_workingDirtyFlag())
            domain.setIsUserWorking(model.getIs_user_working());
        if(model.getMove_raw_idsDirtyFlag())
            domain.setMoveRawIds(model.getMove_raw_ids());
        if(model.getLast_working_user_idDirtyFlag())
            domain.setLastWorkingUserId(model.getLast_working_user_id());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIs_first_woDirtyFlag())
            domain.setIsFirstWo(model.getIs_first_wo());
        if(model.getIs_producedDirtyFlag())
            domain.setIsProduced(model.getIs_produced());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getDuration_unitDirtyFlag())
            domain.setDurationUnit(model.getDuration_unit());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getCapacityDirtyFlag())
            domain.setCapacity(model.getCapacity());
        if(model.getActive_move_line_idsDirtyFlag())
            domain.setActiveMoveLineIds(model.getActive_move_line_ids());
        if(model.getDate_planned_finishedDirtyFlag())
            domain.setDatePlannedFinished(model.getDate_planned_finished());
        if(model.getDate_finishedDirtyFlag())
            domain.setDateFinished(model.getDate_finished());
        if(model.getQty_remainingDirtyFlag())
            domain.setQtyRemaining(model.getQty_remaining());
        if(model.getScrap_idsDirtyFlag())
            domain.setScrapIds(model.getScrap_ids());
        if(model.getDuration_percentDirtyFlag())
            domain.setDurationPercent(model.getDuration_percent());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDuration_expectedDirtyFlag())
            domain.setDurationExpected(model.getDuration_expected());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getQty_producingDirtyFlag())
            domain.setQtyProducing(model.getQty_producing());
        if(model.getProduction_dateDirtyFlag())
            domain.setProductionDate(model.getProduction_date());
        if(model.getWorksheetDirtyFlag())
            domain.setWorksheet(model.getWorksheet());
        if(model.getWorkcenter_id_textDirtyFlag())
            domain.setWorkcenterIdText(model.getWorkcenter_id_text());
        if(model.getFinal_lot_id_textDirtyFlag())
            domain.setFinalLotIdText(model.getFinal_lot_id_text());
        if(model.getWorking_stateDirtyFlag())
            domain.setWorkingState(model.getWorking_state());
        if(model.getQty_productionDirtyFlag())
            domain.setQtyProduction(model.getQty_production());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduction_availabilityDirtyFlag())
            domain.setProductionAvailability(model.getProduction_availability());
        if(model.getProduction_stateDirtyFlag())
            domain.setProductionState(model.getProduction_state());
        if(model.getNext_work_order_id_textDirtyFlag())
            domain.setNextWorkOrderIdText(model.getNext_work_order_id_text());
        if(model.getOperation_id_textDirtyFlag())
            domain.setOperationIdText(model.getOperation_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduction_id_textDirtyFlag())
            domain.setProductionIdText(model.getProduction_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_trackingDirtyFlag())
            domain.setProductTracking(model.getProduct_tracking());
        if(model.getFinal_lot_idDirtyFlag())
            domain.setFinalLotId(model.getFinal_lot_id());
        if(model.getWorkcenter_idDirtyFlag())
            domain.setWorkcenterId(model.getWorkcenter_id());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getOperation_idDirtyFlag())
            domain.setOperationId(model.getOperation_id());
        if(model.getNext_work_order_idDirtyFlag())
            domain.setNextWorkOrderId(model.getNext_work_order_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



