package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_alertService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_alertOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_alertClientModel;

/**
 * 实体[午餐提醒] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_alertServiceImpl implements ILunch_alertService {

    @Autowired
    lunch_alertOdooClient lunch_alertOdooClient;


    @Override
    public boolean remove(Integer id) {
        lunch_alertClientModel clientModel = new lunch_alertClientModel();
        clientModel.setId(id);
		lunch_alertOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Lunch_alert et) {
        lunch_alertClientModel clientModel = convert2Model(et,null);
		lunch_alertOdooClient.update(clientModel);
        Lunch_alert rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_alert> list){
    }

    @Override
    public Lunch_alert get(Integer id) {
        lunch_alertClientModel clientModel = new lunch_alertClientModel();
        clientModel.setId(id);
		lunch_alertOdooClient.get(clientModel);
        Lunch_alert et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_alert();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Lunch_alert et) {
        lunch_alertClientModel clientModel = convert2Model(et,null);
		lunch_alertOdooClient.create(clientModel);
        Lunch_alert rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_alert> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context) {
        List<Lunch_alert> list = new ArrayList<Lunch_alert>();
        Page<lunch_alertClientModel> clientModelList = lunch_alertOdooClient.search(context);
        for(lunch_alertClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_alert>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_alertClientModel convert2Model(Lunch_alert domain , lunch_alertClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_alertClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("specific_daydirtyflag"))
                model.setSpecific_day(domain.getSpecificDay());
            if((Boolean) domain.getExtensionparams().get("displaydirtyflag"))
                model.setDisplay(domain.getDisplay());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("thursdaydirtyflag"))
                model.setThursday(domain.getThursday());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("wednesdaydirtyflag"))
                model.setWednesday(domain.getWednesday());
            if((Boolean) domain.getExtensionparams().get("tuesdaydirtyflag"))
                model.setTuesday(domain.getTuesday());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("messagedirtyflag"))
                model.setMessage(domain.getMessage());
            if((Boolean) domain.getExtensionparams().get("alert_typedirtyflag"))
                model.setAlert_type(domain.getAlertType());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("saturdaydirtyflag"))
                model.setSaturday(domain.getSaturday());
            if((Boolean) domain.getExtensionparams().get("start_hourdirtyflag"))
                model.setStart_hour(domain.getStartHour());
            if((Boolean) domain.getExtensionparams().get("mondaydirtyflag"))
                model.setMonday(domain.getMonday());
            if((Boolean) domain.getExtensionparams().get("fridaydirtyflag"))
                model.setFriday(domain.getFriday());
            if((Boolean) domain.getExtensionparams().get("end_hourdirtyflag"))
                model.setEnd_hour(domain.getEndHour());
            if((Boolean) domain.getExtensionparams().get("sundaydirtyflag"))
                model.setSunday(domain.getSunday());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_alert convert2Domain( lunch_alertClientModel model ,Lunch_alert domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_alert();
        }

        if(model.getSpecific_dayDirtyFlag())
            domain.setSpecificDay(model.getSpecific_day());
        if(model.getDisplayDirtyFlag())
            domain.setDisplay(model.getDisplay());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getThursdayDirtyFlag())
            domain.setThursday(model.getThursday());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWednesdayDirtyFlag())
            domain.setWednesday(model.getWednesday());
        if(model.getTuesdayDirtyFlag())
            domain.setTuesday(model.getTuesday());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessageDirtyFlag())
            domain.setMessage(model.getMessage());
        if(model.getAlert_typeDirtyFlag())
            domain.setAlertType(model.getAlert_type());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getSaturdayDirtyFlag())
            domain.setSaturday(model.getSaturday());
        if(model.getStart_hourDirtyFlag())
            domain.setStartHour(model.getStart_hour());
        if(model.getMondayDirtyFlag())
            domain.setMonday(model.getMonday());
        if(model.getFridayDirtyFlag())
            domain.setFriday(model.getFriday());
        if(model.getEnd_hourDirtyFlag())
            domain.setEndHour(model.getEnd_hour());
        if(model.getSundayDirtyFlag())
            domain.setSunday(model.getSunday());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



