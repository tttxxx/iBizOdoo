package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;

/**
 * 实体 [门户分享] 存储对象
 */
public interface Portal_shareRepository extends Repository<Portal_share> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Portal_share> searchDefault(Portal_shareSearchContext context);

    Portal_share convert2PO(cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share domain , Portal_share po) ;

    cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share convert2Domain( Portal_share po ,cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share domain) ;

}
