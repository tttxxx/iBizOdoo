package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;

/**
 * 实体 [IAP 账户] 存储对象
 */
public interface Iap_accountRepository extends Repository<Iap_account> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Iap_account> searchDefault(Iap_accountSearchContext context);

    Iap_account convert2PO(cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account domain , Iap_account po) ;

    cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account convert2Domain( Iap_account po ,cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account domain) ;

}
