package cn.ibizlab.odoo.core.odoo_bus.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_busService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_bus.client.bus_busOdooClient;
import cn.ibizlab.odoo.core.odoo_bus.clientmodel.bus_busClientModel;

/**
 * 实体[通讯总线] 服务对象接口实现
 */
@Slf4j
@Service
public class Bus_busServiceImpl implements IBus_busService {

    @Autowired
    bus_busOdooClient bus_busOdooClient;


    @Override
    public Bus_bus get(Integer id) {
        bus_busClientModel clientModel = new bus_busClientModel();
        clientModel.setId(id);
		bus_busOdooClient.get(clientModel);
        Bus_bus et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Bus_bus();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Bus_bus et) {
        bus_busClientModel clientModel = convert2Model(et,null);
		bus_busOdooClient.update(clientModel);
        Bus_bus rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Bus_bus> list){
    }

    @Override
    public boolean remove(Integer id) {
        bus_busClientModel clientModel = new bus_busClientModel();
        clientModel.setId(id);
		bus_busOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Bus_bus et) {
        bus_busClientModel clientModel = convert2Model(et,null);
		bus_busOdooClient.create(clientModel);
        Bus_bus rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Bus_bus> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Bus_bus> searchDefault(Bus_busSearchContext context) {
        List<Bus_bus> list = new ArrayList<Bus_bus>();
        Page<bus_busClientModel> clientModelList = bus_busOdooClient.search(context);
        for(bus_busClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Bus_bus>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public bus_busClientModel convert2Model(Bus_bus domain , bus_busClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new bus_busClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("messagedirtyflag"))
                model.setMessage(domain.getMessage());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("channeldirtyflag"))
                model.setChannel(domain.getChannel());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Bus_bus convert2Domain( bus_busClientModel model ,Bus_bus domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Bus_bus();
        }

        if(model.getMessageDirtyFlag())
            domain.setMessage(model.getMessage());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getChannelDirtyFlag())
            domain.setChannel(model.getChannel());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



