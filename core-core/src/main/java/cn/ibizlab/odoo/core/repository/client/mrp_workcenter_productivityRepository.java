package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_workcenter_productivity;

/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
public interface mrp_workcenter_productivityRepository{


    public mrp_workcenter_productivity createPO() ;
        public void updateBatch(mrp_workcenter_productivity mrp_workcenter_productivity);

        public void remove(String id);

        public void update(mrp_workcenter_productivity mrp_workcenter_productivity);

        public List<mrp_workcenter_productivity> search();

        public void create(mrp_workcenter_productivity mrp_workcenter_productivity);

        public void createBatch(mrp_workcenter_productivity mrp_workcenter_productivity);

        public void removeBatch(String id);

        public void get(String id);


}
