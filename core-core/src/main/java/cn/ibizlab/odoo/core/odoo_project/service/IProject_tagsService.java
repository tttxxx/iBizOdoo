package cn.ibizlab.odoo.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;


/**
 * 实体[Project_tags] 服务对象接口
 */
public interface IProject_tagsService{

    Project_tags get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Project_tags et) ;
    void createBatch(List<Project_tags> list) ;
    boolean update(Project_tags et) ;
    void updateBatch(List<Project_tags> list) ;
    Page<Project_tags> searchDefault(Project_tagsSearchContext context) ;

}



