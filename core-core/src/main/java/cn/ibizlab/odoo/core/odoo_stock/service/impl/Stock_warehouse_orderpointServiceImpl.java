package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouse_orderpointService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_warehouse_orderpointOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_warehouse_orderpointClientModel;

/**
 * 实体[最小库存规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warehouse_orderpointServiceImpl implements IStock_warehouse_orderpointService {

    @Autowired
    stock_warehouse_orderpointOdooClient stock_warehouse_orderpointOdooClient;


    @Override
    public boolean update(Stock_warehouse_orderpoint et) {
        stock_warehouse_orderpointClientModel clientModel = convert2Model(et,null);
		stock_warehouse_orderpointOdooClient.update(clientModel);
        Stock_warehouse_orderpoint rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_warehouse_orderpoint> list){
    }

    @Override
    public Stock_warehouse_orderpoint get(Integer id) {
        stock_warehouse_orderpointClientModel clientModel = new stock_warehouse_orderpointClientModel();
        clientModel.setId(id);
		stock_warehouse_orderpointOdooClient.get(clientModel);
        Stock_warehouse_orderpoint et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_warehouse_orderpoint();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_warehouse_orderpointClientModel clientModel = new stock_warehouse_orderpointClientModel();
        clientModel.setId(id);
		stock_warehouse_orderpointOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Stock_warehouse_orderpoint et) {
        stock_warehouse_orderpointClientModel clientModel = convert2Model(et,null);
		stock_warehouse_orderpointOdooClient.create(clientModel);
        Stock_warehouse_orderpoint rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warehouse_orderpoint> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context) {
        List<Stock_warehouse_orderpoint> list = new ArrayList<Stock_warehouse_orderpoint>();
        Page<stock_warehouse_orderpointClientModel> clientModelList = stock_warehouse_orderpointOdooClient.search(context);
        for(stock_warehouse_orderpointClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_warehouse_orderpoint>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_warehouse_orderpointClientModel convert2Model(Stock_warehouse_orderpoint domain , stock_warehouse_orderpointClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_warehouse_orderpointClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("lead_daysdirtyflag"))
                model.setLead_days(domain.getLeadDays());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("lead_typedirtyflag"))
                model.setLead_type(domain.getLeadType());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("product_max_qtydirtyflag"))
                model.setProduct_max_qty(domain.getProductMaxQty());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("qty_multipledirtyflag"))
                model.setQty_multiple(domain.getQtyMultiple());
            if((Boolean) domain.getExtensionparams().get("product_min_qtydirtyflag"))
                model.setProduct_min_qty(domain.getProductMinQty());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_warehouse_orderpoint convert2Domain( stock_warehouse_orderpointClientModel model ,Stock_warehouse_orderpoint domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_warehouse_orderpoint();
        }

        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getLead_daysDirtyFlag())
            domain.setLeadDays(model.getLead_days());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLead_typeDirtyFlag())
            domain.setLeadType(model.getLead_type());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getProduct_max_qtyDirtyFlag())
            domain.setProductMaxQty(model.getProduct_max_qty());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQty_multipleDirtyFlag())
            domain.setQtyMultiple(model.getQty_multiple());
        if(model.getProduct_min_qtyDirtyFlag())
            domain.setProductMinQty(model.getProduct_min_qty());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



