package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currencyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_currencyOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_currencyClientModel;

/**
 * 实体[币种] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_currencyServiceImpl implements IRes_currencyService {

    @Autowired
    res_currencyOdooClient res_currencyOdooClient;


    @Override
    public Res_currency get(Integer id) {
        res_currencyClientModel clientModel = new res_currencyClientModel();
        clientModel.setId(id);
		res_currencyOdooClient.get(clientModel);
        Res_currency et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_currency();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Res_currency et) {
        res_currencyClientModel clientModel = convert2Model(et,null);
		res_currencyOdooClient.update(clientModel);
        Res_currency rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_currency> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_currencyClientModel clientModel = new res_currencyClientModel();
        clientModel.setId(id);
		res_currencyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_currency et) {
        res_currencyClientModel clientModel = convert2Model(et,null);
		res_currencyOdooClient.create(clientModel);
        Res_currency rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_currency> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_currency> searchDefault(Res_currencySearchContext context) {
        List<Res_currency> list = new ArrayList<Res_currency>();
        Page<res_currencyClientModel> clientModelList = res_currencyOdooClient.search(context);
        for(res_currencyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_currency>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_currencyClientModel convert2Model(Res_currency domain , res_currencyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_currencyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("decimal_placesdirtyflag"))
                model.setDecimal_places(domain.getDecimalPlaces());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("currency_unit_labeldirtyflag"))
                model.setCurrency_unit_label(domain.getCurrencyUnitLabel());
            if((Boolean) domain.getExtensionparams().get("roundingdirtyflag"))
                model.setRounding(domain.getRounding());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("positiondirtyflag"))
                model.setPosition(domain.getPosition());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("ratedirtyflag"))
                model.setRate(domain.getRate());
            if((Boolean) domain.getExtensionparams().get("rate_idsdirtyflag"))
                model.setRate_ids(domain.getRateIds());
            if((Boolean) domain.getExtensionparams().get("symboldirtyflag"))
                model.setSymbol(domain.getSymbol());
            if((Boolean) domain.getExtensionparams().get("currency_subunit_labeldirtyflag"))
                model.setCurrency_subunit_label(domain.getCurrencySubunitLabel());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_currency convert2Domain( res_currencyClientModel model ,Res_currency domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_currency();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDecimal_placesDirtyFlag())
            domain.setDecimalPlaces(model.getDecimal_places());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCurrency_unit_labelDirtyFlag())
            domain.setCurrencyUnitLabel(model.getCurrency_unit_label());
        if(model.getRoundingDirtyFlag())
            domain.setRounding(model.getRounding());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getPositionDirtyFlag())
            domain.setPosition(model.getPosition());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getRateDirtyFlag())
            domain.setRate(model.getRate());
        if(model.getRate_idsDirtyFlag())
            domain.setRateIds(model.getRate_ids());
        if(model.getSymbolDirtyFlag())
            domain.setSymbol(model.getSymbol());
        if(model.getCurrency_subunit_labelDirtyFlag())
            domain.setCurrencySubunitLabel(model.getCurrency_subunit_label());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



