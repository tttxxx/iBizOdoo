package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;

/**
 * 实体 [调查问题] 存储对象
 */
public interface Survey_questionRepository extends Repository<Survey_question> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_question> searchDefault(Survey_questionSearchContext context);

    Survey_question convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question domain , Survey_question po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question convert2Domain( Survey_question po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question domain) ;

}
