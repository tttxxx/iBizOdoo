package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_orderClientModel;

/**
 * 实体[Maintenance Order] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_orderServiceImpl implements IMro_orderService {

    @Autowired
    mro_orderOdooClient mro_orderOdooClient;


    @Override
    public boolean create(Mro_order et) {
        mro_orderClientModel clientModel = convert2Model(et,null);
		mro_orderOdooClient.create(clientModel);
        Mro_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_order> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_orderClientModel clientModel = new mro_orderClientModel();
        clientModel.setId(id);
		mro_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mro_order get(Integer id) {
        mro_orderClientModel clientModel = new mro_orderClientModel();
        clientModel.setId(id);
		mro_orderOdooClient.get(clientModel);
        Mro_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mro_order et) {
        mro_orderClientModel clientModel = convert2Model(et,null);
		mro_orderOdooClient.update(clientModel);
        Mro_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_order> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_order> searchDefault(Mro_orderSearchContext context) {
        List<Mro_order> list = new ArrayList<Mro_order>();
        Page<mro_orderClientModel> clientModelList = mro_orderOdooClient.search(context);
        for(mro_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_orderClientModel convert2Model(Mro_order domain , mro_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("maintenance_typedirtyflag"))
                model.setMaintenance_type(domain.getMaintenanceType());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("documentation_descriptiondirtyflag"))
                model.setDocumentation_description(domain.getDocumentationDescription());
            if((Boolean) domain.getExtensionparams().get("parts_moved_linesdirtyflag"))
                model.setParts_moved_lines(domain.getPartsMovedLines());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("category_idsdirtyflag"))
                model.setCategory_ids(domain.getCategoryIds());
            if((Boolean) domain.getExtensionparams().get("date_scheduleddirtyflag"))
                model.setDate_scheduled(domain.getDateScheduled());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("labor_descriptiondirtyflag"))
                model.setLabor_description(domain.getLaborDescription());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("parts_ready_linesdirtyflag"))
                model.setParts_ready_lines(domain.getPartsReadyLines());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("parts_move_linesdirtyflag"))
                model.setParts_move_lines(domain.getPartsMoveLines());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("operations_descriptiondirtyflag"))
                model.setOperations_description(domain.getOperationsDescription());
            if((Boolean) domain.getExtensionparams().get("problem_descriptiondirtyflag"))
                model.setProblem_description(domain.getProblemDescription());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("date_planneddirtyflag"))
                model.setDate_planned(domain.getDatePlanned());
            if((Boolean) domain.getExtensionparams().get("procurement_group_iddirtyflag"))
                model.setProcurement_group_id(domain.getProcurementGroupId());
            if((Boolean) domain.getExtensionparams().get("date_executiondirtyflag"))
                model.setDate_execution(domain.getDateExecution());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("parts_linesdirtyflag"))
                model.setParts_lines(domain.getPartsLines());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("tools_descriptiondirtyflag"))
                model.setTools_description(domain.getToolsDescription());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("wo_id_textdirtyflag"))
                model.setWo_id_text(domain.getWoIdText());
            if((Boolean) domain.getExtensionparams().get("asset_id_textdirtyflag"))
                model.setAsset_id_text(domain.getAssetIdText());
            if((Boolean) domain.getExtensionparams().get("task_id_textdirtyflag"))
                model.setTask_id_text(domain.getTaskIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("request_id_textdirtyflag"))
                model.setRequest_id_text(domain.getRequestIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("request_iddirtyflag"))
                model.setRequest_id(domain.getRequestId());
            if((Boolean) domain.getExtensionparams().get("task_iddirtyflag"))
                model.setTask_id(domain.getTaskId());
            if((Boolean) domain.getExtensionparams().get("wo_iddirtyflag"))
                model.setWo_id(domain.getWoId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("asset_iddirtyflag"))
                model.setAsset_id(domain.getAssetId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_order convert2Domain( mro_orderClientModel model ,Mro_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_order();
        }

        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMaintenance_typeDirtyFlag())
            domain.setMaintenanceType(model.getMaintenance_type());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getDocumentation_descriptionDirtyFlag())
            domain.setDocumentationDescription(model.getDocumentation_description());
        if(model.getParts_moved_linesDirtyFlag())
            domain.setPartsMovedLines(model.getParts_moved_lines());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getCategory_idsDirtyFlag())
            domain.setCategoryIds(model.getCategory_ids());
        if(model.getDate_scheduledDirtyFlag())
            domain.setDateScheduled(model.getDate_scheduled());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLabor_descriptionDirtyFlag())
            domain.setLaborDescription(model.getLabor_description());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getParts_ready_linesDirtyFlag())
            domain.setPartsReadyLines(model.getParts_ready_lines());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getParts_move_linesDirtyFlag())
            domain.setPartsMoveLines(model.getParts_move_lines());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getOperations_descriptionDirtyFlag())
            domain.setOperationsDescription(model.getOperations_description());
        if(model.getProblem_descriptionDirtyFlag())
            domain.setProblemDescription(model.getProblem_description());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getDate_plannedDirtyFlag())
            domain.setDatePlanned(model.getDate_planned());
        if(model.getProcurement_group_idDirtyFlag())
            domain.setProcurementGroupId(model.getProcurement_group_id());
        if(model.getDate_executionDirtyFlag())
            domain.setDateExecution(model.getDate_execution());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getParts_linesDirtyFlag())
            domain.setPartsLines(model.getParts_lines());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getTools_descriptionDirtyFlag())
            domain.setToolsDescription(model.getTools_description());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWo_id_textDirtyFlag())
            domain.setWoIdText(model.getWo_id_text());
        if(model.getAsset_id_textDirtyFlag())
            domain.setAssetIdText(model.getAsset_id_text());
        if(model.getTask_id_textDirtyFlag())
            domain.setTaskIdText(model.getTask_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getRequest_id_textDirtyFlag())
            domain.setRequestIdText(model.getRequest_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getRequest_idDirtyFlag())
            domain.setRequestId(model.getRequest_id());
        if(model.getTask_idDirtyFlag())
            domain.setTaskId(model.getTask_id());
        if(model.getWo_idDirtyFlag())
            domain.setWoId(model.getWo_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getAsset_idDirtyFlag())
            domain.setAssetId(model.getAsset_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



