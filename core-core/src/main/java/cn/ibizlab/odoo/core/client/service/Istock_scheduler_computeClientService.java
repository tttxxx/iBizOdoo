package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_scheduler_compute;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
public interface Istock_scheduler_computeClientService{

    public Istock_scheduler_compute createModel() ;

    public void updateBatch(List<Istock_scheduler_compute> stock_scheduler_computes);

    public void removeBatch(List<Istock_scheduler_compute> stock_scheduler_computes);

    public void createBatch(List<Istock_scheduler_compute> stock_scheduler_computes);

    public void create(Istock_scheduler_compute stock_scheduler_compute);

    public void get(Istock_scheduler_compute stock_scheduler_compute);

    public Page<Istock_scheduler_compute> search(SearchContext context);

    public void remove(Istock_scheduler_compute stock_scheduler_compute);

    public void update(Istock_scheduler_compute stock_scheduler_compute);

    public Page<Istock_scheduler_compute> select(SearchContext context);

}
