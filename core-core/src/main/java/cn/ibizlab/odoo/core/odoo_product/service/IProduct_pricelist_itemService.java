package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;


/**
 * 实体[Product_pricelist_item] 服务对象接口
 */
public interface IProduct_pricelist_itemService{

    Product_pricelist_item get(Integer key) ;
    boolean create(Product_pricelist_item et) ;
    void createBatch(List<Product_pricelist_item> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Product_pricelist_item et) ;
    void updateBatch(List<Product_pricelist_item> list) ;
    Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context) ;

}



