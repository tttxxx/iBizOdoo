package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [税科目调整] 对象
 */
@Data
public class Account_fiscal_position extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 税科目调整
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * VAT必须
     */
    @DEField(name = "vat_required")
    @JSONField(name = "vat_required")
    @JsonProperty("vat_required")
    private String vatRequired;

    /**
     * 科目映射
     */
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    private String accountIds;

    /**
     * 状态数
     */
    @JSONField(name = "states_count")
    @JsonProperty("states_count")
    private Integer statesCount;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 联邦政府
     */
    @JSONField(name = "state_ids")
    @JsonProperty("state_ids")
    private String stateIds;

    /**
     * 自动检测
     */
    @DEField(name = "auto_apply")
    @JSONField(name = "auto_apply")
    @JsonProperty("auto_apply")
    private String autoApply;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 备注
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 邮编范围到
     */
    @DEField(name = "zip_to")
    @JSONField(name = "zip_to")
    @JsonProperty("zip_to")
    private Integer zipTo;

    /**
     * 税映射
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 邮编范围从
     */
    @DEField(name = "zip_from")
    @JSONField(name = "zip_from")
    @JsonProperty("zip_from")
    private Integer zipFrom;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 国家群组
     */
    @JSONField(name = "country_group_id_text")
    @JsonProperty("country_group_id_text")
    private String countryGroupIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 国家
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 国家
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 国家群组
     */
    @DEField(name = "country_group_id")
    @JSONField(name = "country_group_id")
    @JsonProperty("country_group_id")
    private Integer countryGroupId;


    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocountrygroup")
    @JsonProperty("odoocountrygroup")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group odooCountryGroup;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [税科目调整]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [VAT必须]
     */
    public void setVatRequired(String vatRequired){
        this.vatRequired = vatRequired ;
        this.modify("vat_required",vatRequired);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [自动检测]
     */
    public void setAutoApply(String autoApply){
        this.autoApply = autoApply ;
        this.modify("auto_apply",autoApply);
    }
    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [邮编范围到]
     */
    public void setZipTo(Integer zipTo){
        this.zipTo = zipTo ;
        this.modify("zip_to",zipTo);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [邮编范围从]
     */
    public void setZipFrom(Integer zipFrom){
        this.zipFrom = zipFrom ;
        this.modify("zip_from",zipFrom);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [国家]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [国家群组]
     */
    public void setCountryGroupId(Integer countryGroupId){
        this.countryGroupId = countryGroupId ;
        this.modify("country_group_id",countryGroupId);
    }

}


