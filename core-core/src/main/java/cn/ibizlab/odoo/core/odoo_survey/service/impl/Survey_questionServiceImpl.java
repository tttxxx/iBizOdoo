package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_questionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_questionOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_questionClientModel;

/**
 * 实体[调查问题] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_questionServiceImpl implements ISurvey_questionService {

    @Autowired
    survey_questionOdooClient survey_questionOdooClient;


    @Override
    public boolean create(Survey_question et) {
        survey_questionClientModel clientModel = convert2Model(et,null);
		survey_questionOdooClient.create(clientModel);
        Survey_question rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_question> list){
    }

    @Override
    public boolean update(Survey_question et) {
        survey_questionClientModel clientModel = convert2Model(et,null);
		survey_questionOdooClient.update(clientModel);
        Survey_question rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_question> list){
    }

    @Override
    public boolean remove(Integer id) {
        survey_questionClientModel clientModel = new survey_questionClientModel();
        clientModel.setId(id);
		survey_questionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Survey_question get(Integer id) {
        survey_questionClientModel clientModel = new survey_questionClientModel();
        clientModel.setId(id);
		survey_questionOdooClient.get(clientModel);
        Survey_question et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_question();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_question> searchDefault(Survey_questionSearchContext context) {
        List<Survey_question> list = new ArrayList<Survey_question>();
        Page<survey_questionClientModel> clientModelList = survey_questionOdooClient.search(context);
        for(survey_questionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_question>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_questionClientModel convert2Model(Survey_question domain , survey_questionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_questionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("matrix_subtypedirtyflag"))
                model.setMatrix_subtype(domain.getMatrixSubtype());
            if((Boolean) domain.getExtensionparams().get("validation_error_msgdirtyflag"))
                model.setValidation_error_msg(domain.getValidationErrorMsg());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("comments_alloweddirtyflag"))
                model.setComments_allowed(domain.getCommentsAllowed());
            if((Boolean) domain.getExtensionparams().get("validation_max_datedirtyflag"))
                model.setValidation_max_date(domain.getValidationMaxDate());
            if((Boolean) domain.getExtensionparams().get("labels_ids_2dirtyflag"))
                model.setLabels_ids_2(domain.getLabelsIds2());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("validation_max_float_valuedirtyflag"))
                model.setValidation_max_float_value(domain.getValidationMaxFloatValue());
            if((Boolean) domain.getExtensionparams().get("questiondirtyflag"))
                model.setQuestion(domain.getQuestion());
            if((Boolean) domain.getExtensionparams().get("labels_idsdirtyflag"))
                model.setLabels_ids(domain.getLabelsIds());
            if((Boolean) domain.getExtensionparams().get("validation_min_float_valuedirtyflag"))
                model.setValidation_min_float_value(domain.getValidationMinFloatValue());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("constr_mandatorydirtyflag"))
                model.setConstr_mandatory(domain.getConstrMandatory());
            if((Boolean) domain.getExtensionparams().get("user_input_line_idsdirtyflag"))
                model.setUser_input_line_ids(domain.getUserInputLineIds());
            if((Boolean) domain.getExtensionparams().get("comments_messagedirtyflag"))
                model.setComments_message(domain.getCommentsMessage());
            if((Boolean) domain.getExtensionparams().get("comment_count_as_answerdirtyflag"))
                model.setComment_count_as_answer(domain.getCommentCountAsAnswer());
            if((Boolean) domain.getExtensionparams().get("validation_requireddirtyflag"))
                model.setValidation_required(domain.getValidationRequired());
            if((Boolean) domain.getExtensionparams().get("validation_emaildirtyflag"))
                model.setValidation_email(domain.getValidationEmail());
            if((Boolean) domain.getExtensionparams().get("validation_min_datedirtyflag"))
                model.setValidation_min_date(domain.getValidationMinDate());
            if((Boolean) domain.getExtensionparams().get("constr_error_msgdirtyflag"))
                model.setConstr_error_msg(domain.getConstrErrorMsg());
            if((Boolean) domain.getExtensionparams().get("validation_length_maxdirtyflag"))
                model.setValidation_length_max(domain.getValidationLengthMax());
            if((Boolean) domain.getExtensionparams().get("column_nbdirtyflag"))
                model.setColumn_nb(domain.getColumnNb());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("display_modedirtyflag"))
                model.setDisplay_mode(domain.getDisplayMode());
            if((Boolean) domain.getExtensionparams().get("validation_length_mindirtyflag"))
                model.setValidation_length_min(domain.getValidationLengthMin());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("survey_iddirtyflag"))
                model.setSurvey_id(domain.getSurveyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("page_iddirtyflag"))
                model.setPage_id(domain.getPageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_question convert2Domain( survey_questionClientModel model ,Survey_question domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_question();
        }

        if(model.getMatrix_subtypeDirtyFlag())
            domain.setMatrixSubtype(model.getMatrix_subtype());
        if(model.getValidation_error_msgDirtyFlag())
            domain.setValidationErrorMsg(model.getValidation_error_msg());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getComments_allowedDirtyFlag())
            domain.setCommentsAllowed(model.getComments_allowed());
        if(model.getValidation_max_dateDirtyFlag())
            domain.setValidationMaxDate(model.getValidation_max_date());
        if(model.getLabels_ids_2DirtyFlag())
            domain.setLabelsIds2(model.getLabels_ids_2());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getValidation_max_float_valueDirtyFlag())
            domain.setValidationMaxFloatValue(model.getValidation_max_float_value());
        if(model.getQuestionDirtyFlag())
            domain.setQuestion(model.getQuestion());
        if(model.getLabels_idsDirtyFlag())
            domain.setLabelsIds(model.getLabels_ids());
        if(model.getValidation_min_float_valueDirtyFlag())
            domain.setValidationMinFloatValue(model.getValidation_min_float_value());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getConstr_mandatoryDirtyFlag())
            domain.setConstrMandatory(model.getConstr_mandatory());
        if(model.getUser_input_line_idsDirtyFlag())
            domain.setUserInputLineIds(model.getUser_input_line_ids());
        if(model.getComments_messageDirtyFlag())
            domain.setCommentsMessage(model.getComments_message());
        if(model.getComment_count_as_answerDirtyFlag())
            domain.setCommentCountAsAnswer(model.getComment_count_as_answer());
        if(model.getValidation_requiredDirtyFlag())
            domain.setValidationRequired(model.getValidation_required());
        if(model.getValidation_emailDirtyFlag())
            domain.setValidationEmail(model.getValidation_email());
        if(model.getValidation_min_dateDirtyFlag())
            domain.setValidationMinDate(model.getValidation_min_date());
        if(model.getConstr_error_msgDirtyFlag())
            domain.setConstrErrorMsg(model.getConstr_error_msg());
        if(model.getValidation_length_maxDirtyFlag())
            domain.setValidationLengthMax(model.getValidation_length_max());
        if(model.getColumn_nbDirtyFlag())
            domain.setColumnNb(model.getColumn_nb());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDisplay_modeDirtyFlag())
            domain.setDisplayMode(model.getDisplay_mode());
        if(model.getValidation_length_minDirtyFlag())
            domain.setValidationLengthMin(model.getValidation_length_min());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSurvey_idDirtyFlag())
            domain.setSurveyId(model.getSurvey_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPage_idDirtyFlag())
            domain.setPageId(model.getPage_id());
        return domain ;
    }

}

    



