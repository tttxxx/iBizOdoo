package cn.ibizlab.odoo.core.odoo_crm.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[crm_lead] 对象
 */
public class crm_leadClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 城市
     */
    public String city;

    @JsonIgnore
    public boolean cityDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 币种
     */
    public Integer company_currency;

    @JsonIgnore
    public boolean company_currencyDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 联系人姓名
     */
    public String contact_name;

    @JsonIgnore
    public boolean contact_nameDirtyFlag;
    
    /**
     * 国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 最近行动
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_action_last;

    @JsonIgnore
    public boolean date_action_lastDirtyFlag;
    
    /**
     * 关闭日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_closed;

    @JsonIgnore
    public boolean date_closedDirtyFlag;
    
    /**
     * 转换日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_conversion;

    @JsonIgnore
    public boolean date_conversionDirtyFlag;
    
    /**
     * 预期结束
     */
    public Timestamp date_deadline;

    @JsonIgnore
    public boolean date_deadlineDirtyFlag;
    
    /**
     * 最后阶段更新
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_last_stage_update;

    @JsonIgnore
    public boolean date_last_stage_updateDirtyFlag;
    
    /**
     * 分配日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_open;

    @JsonIgnore
    public boolean date_openDirtyFlag;
    
    /**
     * 关闭日期
     */
    public Double day_close;

    @JsonIgnore
    public boolean day_closeDirtyFlag;
    
    /**
     * 分配天数
     */
    public Double day_open;

    @JsonIgnore
    public boolean day_openDirtyFlag;
    
    /**
     * 便签
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 全局抄送
     */
    public String email_cc;

    @JsonIgnore
    public boolean email_ccDirtyFlag;
    
    /**
     * EMail
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * 按比例分摊收入
     */
    public Double expected_revenue;

    @JsonIgnore
    public boolean expected_revenueDirtyFlag;
    
    /**
     * 工作岗位
     */
    public String ibizfunction;

    @JsonIgnore
    public boolean ibizfunctionDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 黑名单
     */
    public String is_blacklisted;

    @JsonIgnore
    public boolean is_blacklistedDirtyFlag;
    
    /**
     * 看板状态
     */
    public String kanban_state;

    @JsonIgnore
    public boolean kanban_stateDirtyFlag;
    
    /**
     * 失去原因
     */
    public Integer lost_reason;

    @JsonIgnore
    public boolean lost_reasonDirtyFlag;
    
    /**
     * 失去原因
     */
    public String lost_reason_text;

    @JsonIgnore
    public boolean lost_reason_textDirtyFlag;
    
    /**
     * 媒介
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒介
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * #会议
     */
    public Integer meeting_count;

    @JsonIgnore
    public boolean meeting_countDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 退回
     */
    public Integer message_bounce;

    @JsonIgnore
    public boolean message_bounceDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 手机
     */
    public String mobile;

    @JsonIgnore
    public boolean mobileDirtyFlag;
    
    /**
     * 商机
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 订单
     */
    public String order_ids;

    @JsonIgnore
    public boolean order_idsDirtyFlag;
    
    /**
     * 业务伙伴联系EMail
     */
    public String partner_address_email;

    @JsonIgnore
    public boolean partner_address_emailDirtyFlag;
    
    /**
     * Partner Contact Mobile
     */
    public String partner_address_mobile;

    @JsonIgnore
    public boolean partner_address_mobileDirtyFlag;
    
    /**
     * 业务伙伴联系姓名
     */
    public String partner_address_name;

    @JsonIgnore
    public boolean partner_address_nameDirtyFlag;
    
    /**
     * 合作伙伴联系电话
     */
    public String partner_address_phone;

    @JsonIgnore
    public boolean partner_address_phoneDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 合作伙伴黑名单
     */
    public String partner_is_blacklisted;

    @JsonIgnore
    public boolean partner_is_blacklistedDirtyFlag;
    
    /**
     * 客户名称
     */
    public String partner_name;

    @JsonIgnore
    public boolean partner_nameDirtyFlag;
    
    /**
     * 电话
     */
    public String phone;

    @JsonIgnore
    public boolean phoneDirtyFlag;
    
    /**
     * 预期收益
     */
    public Double planned_revenue;

    @JsonIgnore
    public boolean planned_revenueDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 概率
     */
    public Double probability;

    @JsonIgnore
    public boolean probabilityDirtyFlag;
    
    /**
     * 引荐于
     */
    public String referred;

    @JsonIgnore
    public boolean referredDirtyFlag;
    
    /**
     * 销售订单的总数
     */
    public Double sale_amount_total;

    @JsonIgnore
    public boolean sale_amount_totalDirtyFlag;
    
    /**
     * 报价单的数量
     */
    public Integer sale_number;

    @JsonIgnore
    public boolean sale_numberDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 阶段
     */
    public Integer stage_id;

    @JsonIgnore
    public boolean stage_idDirtyFlag;
    
    /**
     * 阶段
     */
    public String stage_id_text;

    @JsonIgnore
    public boolean stage_id_textDirtyFlag;
    
    /**
     * 省份
     */
    public Integer state_id;

    @JsonIgnore
    public boolean state_idDirtyFlag;
    
    /**
     * 省份
     */
    public String state_id_text;

    @JsonIgnore
    public boolean state_id_textDirtyFlag;
    
    /**
     * 街道
     */
    public String street;

    @JsonIgnore
    public boolean streetDirtyFlag;
    
    /**
     * 街道 2
     */
    public String street2;

    @JsonIgnore
    public boolean street2DirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 称谓
     */
    public Integer title;

    @JsonIgnore
    public boolean titleDirtyFlag;
    
    /**
     * 称谓
     */
    public String title_text;

    @JsonIgnore
    public boolean title_textDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 用户EMail
     */
    public String user_email;

    @JsonIgnore
    public boolean user_emailDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 用户 登录
     */
    public String user_login;

    @JsonIgnore
    public boolean user_loginDirtyFlag;
    
    /**
     * 网站
     */
    public String website;

    @JsonIgnore
    public boolean websiteDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮政编码
     */
    public String zip;

    @JsonIgnore
    public boolean zipDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

     /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("company_currency")
    public Integer getCompany_currency(){
        return this.company_currency ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("company_currency")
    public void setCompany_currency(Integer  company_currency){
        this.company_currency = company_currency ;
        this.company_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currencyDirtyFlag(){
        return this.company_currencyDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [联系人姓名]
     */
    @JsonProperty("contact_name")
    public String getContact_name(){
        return this.contact_name ;
    }

    /**
     * 设置 [联系人姓名]
     */
    @JsonProperty("contact_name")
    public void setContact_name(String  contact_name){
        this.contact_name = contact_name ;
        this.contact_nameDirtyFlag = true ;
    }

     /**
     * 获取 [联系人姓名]脏标记
     */
    @JsonIgnore
    public boolean getContact_nameDirtyFlag(){
        return this.contact_nameDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最近行动]
     */
    @JsonProperty("date_action_last")
    public Timestamp getDate_action_last(){
        return this.date_action_last ;
    }

    /**
     * 设置 [最近行动]
     */
    @JsonProperty("date_action_last")
    public void setDate_action_last(Timestamp  date_action_last){
        this.date_action_last = date_action_last ;
        this.date_action_lastDirtyFlag = true ;
    }

     /**
     * 获取 [最近行动]脏标记
     */
    @JsonIgnore
    public boolean getDate_action_lastDirtyFlag(){
        return this.date_action_lastDirtyFlag ;
    }   

    /**
     * 获取 [关闭日期]
     */
    @JsonProperty("date_closed")
    public Timestamp getDate_closed(){
        return this.date_closed ;
    }

    /**
     * 设置 [关闭日期]
     */
    @JsonProperty("date_closed")
    public void setDate_closed(Timestamp  date_closed){
        this.date_closed = date_closed ;
        this.date_closedDirtyFlag = true ;
    }

     /**
     * 获取 [关闭日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_closedDirtyFlag(){
        return this.date_closedDirtyFlag ;
    }   

    /**
     * 获取 [转换日期]
     */
    @JsonProperty("date_conversion")
    public Timestamp getDate_conversion(){
        return this.date_conversion ;
    }

    /**
     * 设置 [转换日期]
     */
    @JsonProperty("date_conversion")
    public void setDate_conversion(Timestamp  date_conversion){
        this.date_conversion = date_conversion ;
        this.date_conversionDirtyFlag = true ;
    }

     /**
     * 获取 [转换日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_conversionDirtyFlag(){
        return this.date_conversionDirtyFlag ;
    }   

    /**
     * 获取 [预期结束]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return this.date_deadline ;
    }

    /**
     * 设置 [预期结束]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [预期结束]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return this.date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [最后阶段更新]
     */
    @JsonProperty("date_last_stage_update")
    public Timestamp getDate_last_stage_update(){
        return this.date_last_stage_update ;
    }

    /**
     * 设置 [最后阶段更新]
     */
    @JsonProperty("date_last_stage_update")
    public void setDate_last_stage_update(Timestamp  date_last_stage_update){
        this.date_last_stage_update = date_last_stage_update ;
        this.date_last_stage_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后阶段更新]脏标记
     */
    @JsonIgnore
    public boolean getDate_last_stage_updateDirtyFlag(){
        return this.date_last_stage_updateDirtyFlag ;
    }   

    /**
     * 获取 [分配日期]
     */
    @JsonProperty("date_open")
    public Timestamp getDate_open(){
        return this.date_open ;
    }

    /**
     * 设置 [分配日期]
     */
    @JsonProperty("date_open")
    public void setDate_open(Timestamp  date_open){
        this.date_open = date_open ;
        this.date_openDirtyFlag = true ;
    }

     /**
     * 获取 [分配日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_openDirtyFlag(){
        return this.date_openDirtyFlag ;
    }   

    /**
     * 获取 [关闭日期]
     */
    @JsonProperty("day_close")
    public Double getDay_close(){
        return this.day_close ;
    }

    /**
     * 设置 [关闭日期]
     */
    @JsonProperty("day_close")
    public void setDay_close(Double  day_close){
        this.day_close = day_close ;
        this.day_closeDirtyFlag = true ;
    }

     /**
     * 获取 [关闭日期]脏标记
     */
    @JsonIgnore
    public boolean getDay_closeDirtyFlag(){
        return this.day_closeDirtyFlag ;
    }   

    /**
     * 获取 [分配天数]
     */
    @JsonProperty("day_open")
    public Double getDay_open(){
        return this.day_open ;
    }

    /**
     * 设置 [分配天数]
     */
    @JsonProperty("day_open")
    public void setDay_open(Double  day_open){
        this.day_open = day_open ;
        this.day_openDirtyFlag = true ;
    }

     /**
     * 获取 [分配天数]脏标记
     */
    @JsonIgnore
    public boolean getDay_openDirtyFlag(){
        return this.day_openDirtyFlag ;
    }   

    /**
     * 获取 [便签]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [便签]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [便签]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [全局抄送]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return this.email_cc ;
    }

    /**
     * 设置 [全局抄送]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

     /**
     * 获取 [全局抄送]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return this.email_ccDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [按比例分摊收入]
     */
    @JsonProperty("expected_revenue")
    public Double getExpected_revenue(){
        return this.expected_revenue ;
    }

    /**
     * 设置 [按比例分摊收入]
     */
    @JsonProperty("expected_revenue")
    public void setExpected_revenue(Double  expected_revenue){
        this.expected_revenue = expected_revenue ;
        this.expected_revenueDirtyFlag = true ;
    }

     /**
     * 获取 [按比例分摊收入]脏标记
     */
    @JsonIgnore
    public boolean getExpected_revenueDirtyFlag(){
        return this.expected_revenueDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return this.ibizfunction ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return this.ibizfunctionDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

     /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }   

    /**
     * 获取 [看板状态]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return this.kanban_state ;
    }

    /**
     * 设置 [看板状态]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

     /**
     * 获取 [看板状态]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return this.kanban_stateDirtyFlag ;
    }   

    /**
     * 获取 [失去原因]
     */
    @JsonProperty("lost_reason")
    public Integer getLost_reason(){
        return this.lost_reason ;
    }

    /**
     * 设置 [失去原因]
     */
    @JsonProperty("lost_reason")
    public void setLost_reason(Integer  lost_reason){
        this.lost_reason = lost_reason ;
        this.lost_reasonDirtyFlag = true ;
    }

     /**
     * 获取 [失去原因]脏标记
     */
    @JsonIgnore
    public boolean getLost_reasonDirtyFlag(){
        return this.lost_reasonDirtyFlag ;
    }   

    /**
     * 获取 [失去原因]
     */
    @JsonProperty("lost_reason_text")
    public String getLost_reason_text(){
        return this.lost_reason_text ;
    }

    /**
     * 设置 [失去原因]
     */
    @JsonProperty("lost_reason_text")
    public void setLost_reason_text(String  lost_reason_text){
        this.lost_reason_text = lost_reason_text ;
        this.lost_reason_textDirtyFlag = true ;
    }

     /**
     * 获取 [失去原因]脏标记
     */
    @JsonIgnore
    public boolean getLost_reason_textDirtyFlag(){
        return this.lost_reason_textDirtyFlag ;
    }   

    /**
     * 获取 [媒介]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒介]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒介]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒介]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒介]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒介]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [#会议]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return this.meeting_count ;
    }

    /**
     * 设置 [#会议]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

     /**
     * 获取 [#会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return this.meeting_countDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [退回]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return this.message_bounce ;
    }

    /**
     * 设置 [退回]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

     /**
     * 获取 [退回]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return this.message_bounceDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [手机]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return this.mobile ;
    }

    /**
     * 设置 [手机]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

     /**
     * 获取 [手机]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return this.mobileDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [订单]
     */
    @JsonProperty("order_ids")
    public String getOrder_ids(){
        return this.order_ids ;
    }

    /**
     * 设置 [订单]
     */
    @JsonProperty("order_ids")
    public void setOrder_ids(String  order_ids){
        this.order_ids = order_ids ;
        this.order_idsDirtyFlag = true ;
    }

     /**
     * 获取 [订单]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idsDirtyFlag(){
        return this.order_idsDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴联系EMail]
     */
    @JsonProperty("partner_address_email")
    public String getPartner_address_email(){
        return this.partner_address_email ;
    }

    /**
     * 设置 [业务伙伴联系EMail]
     */
    @JsonProperty("partner_address_email")
    public void setPartner_address_email(String  partner_address_email){
        this.partner_address_email = partner_address_email ;
        this.partner_address_emailDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴联系EMail]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_emailDirtyFlag(){
        return this.partner_address_emailDirtyFlag ;
    }   

    /**
     * 获取 [Partner Contact Mobile]
     */
    @JsonProperty("partner_address_mobile")
    public String getPartner_address_mobile(){
        return this.partner_address_mobile ;
    }

    /**
     * 设置 [Partner Contact Mobile]
     */
    @JsonProperty("partner_address_mobile")
    public void setPartner_address_mobile(String  partner_address_mobile){
        this.partner_address_mobile = partner_address_mobile ;
        this.partner_address_mobileDirtyFlag = true ;
    }

     /**
     * 获取 [Partner Contact Mobile]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_mobileDirtyFlag(){
        return this.partner_address_mobileDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴联系姓名]
     */
    @JsonProperty("partner_address_name")
    public String getPartner_address_name(){
        return this.partner_address_name ;
    }

    /**
     * 设置 [业务伙伴联系姓名]
     */
    @JsonProperty("partner_address_name")
    public void setPartner_address_name(String  partner_address_name){
        this.partner_address_name = partner_address_name ;
        this.partner_address_nameDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴联系姓名]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_nameDirtyFlag(){
        return this.partner_address_nameDirtyFlag ;
    }   

    /**
     * 获取 [合作伙伴联系电话]
     */
    @JsonProperty("partner_address_phone")
    public String getPartner_address_phone(){
        return this.partner_address_phone ;
    }

    /**
     * 设置 [合作伙伴联系电话]
     */
    @JsonProperty("partner_address_phone")
    public void setPartner_address_phone(String  partner_address_phone){
        this.partner_address_phone = partner_address_phone ;
        this.partner_address_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [合作伙伴联系电话]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_phoneDirtyFlag(){
        return this.partner_address_phoneDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [合作伙伴黑名单]
     */
    @JsonProperty("partner_is_blacklisted")
    public String getPartner_is_blacklisted(){
        return this.partner_is_blacklisted ;
    }

    /**
     * 设置 [合作伙伴黑名单]
     */
    @JsonProperty("partner_is_blacklisted")
    public void setPartner_is_blacklisted(String  partner_is_blacklisted){
        this.partner_is_blacklisted = partner_is_blacklisted ;
        this.partner_is_blacklistedDirtyFlag = true ;
    }

     /**
     * 获取 [合作伙伴黑名单]脏标记
     */
    @JsonIgnore
    public boolean getPartner_is_blacklistedDirtyFlag(){
        return this.partner_is_blacklistedDirtyFlag ;
    }   

    /**
     * 获取 [客户名称]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return this.partner_name ;
    }

    /**
     * 设置 [客户名称]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

     /**
     * 获取 [客户名称]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return this.partner_nameDirtyFlag ;
    }   

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

     /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }   

    /**
     * 获取 [预期收益]
     */
    @JsonProperty("planned_revenue")
    public Double getPlanned_revenue(){
        return this.planned_revenue ;
    }

    /**
     * 设置 [预期收益]
     */
    @JsonProperty("planned_revenue")
    public void setPlanned_revenue(Double  planned_revenue){
        this.planned_revenue = planned_revenue ;
        this.planned_revenueDirtyFlag = true ;
    }

     /**
     * 获取 [预期收益]脏标记
     */
    @JsonIgnore
    public boolean getPlanned_revenueDirtyFlag(){
        return this.planned_revenueDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [概率]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return this.probability ;
    }

    /**
     * 设置 [概率]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

     /**
     * 获取 [概率]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return this.probabilityDirtyFlag ;
    }   

    /**
     * 获取 [引荐于]
     */
    @JsonProperty("referred")
    public String getReferred(){
        return this.referred ;
    }

    /**
     * 设置 [引荐于]
     */
    @JsonProperty("referred")
    public void setReferred(String  referred){
        this.referred = referred ;
        this.referredDirtyFlag = true ;
    }

     /**
     * 获取 [引荐于]脏标记
     */
    @JsonIgnore
    public boolean getReferredDirtyFlag(){
        return this.referredDirtyFlag ;
    }   

    /**
     * 获取 [销售订单的总数]
     */
    @JsonProperty("sale_amount_total")
    public Double getSale_amount_total(){
        return this.sale_amount_total ;
    }

    /**
     * 设置 [销售订单的总数]
     */
    @JsonProperty("sale_amount_total")
    public void setSale_amount_total(Double  sale_amount_total){
        this.sale_amount_total = sale_amount_total ;
        this.sale_amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单的总数]脏标记
     */
    @JsonIgnore
    public boolean getSale_amount_totalDirtyFlag(){
        return this.sale_amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [报价单的数量]
     */
    @JsonProperty("sale_number")
    public Integer getSale_number(){
        return this.sale_number ;
    }

    /**
     * 设置 [报价单的数量]
     */
    @JsonProperty("sale_number")
    public void setSale_number(Integer  sale_number){
        this.sale_number = sale_number ;
        this.sale_numberDirtyFlag = true ;
    }

     /**
     * 获取 [报价单的数量]脏标记
     */
    @JsonIgnore
    public boolean getSale_numberDirtyFlag(){
        return this.sale_numberDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }   

    /**
     * 获取 [省份]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [省份]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省份]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }   

    /**
     * 获取 [省份]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return this.state_id_text ;
    }

    /**
     * 设置 [省份]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省份]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return this.state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

     /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }   

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

     /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return this.title ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

     /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return this.titleDirtyFlag ;
    }   

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title_text")
    public String getTitle_text(){
        return this.title_text ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title_text")
    public void setTitle_text(String  title_text){
        this.title_text = title_text ;
        this.title_textDirtyFlag = true ;
    }

     /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitle_textDirtyFlag(){
        return this.title_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [用户EMail]
     */
    @JsonProperty("user_email")
    public String getUser_email(){
        return this.user_email ;
    }

    /**
     * 设置 [用户EMail]
     */
    @JsonProperty("user_email")
    public void setUser_email(String  user_email){
        this.user_email = user_email ;
        this.user_emailDirtyFlag = true ;
    }

     /**
     * 获取 [用户EMail]脏标记
     */
    @JsonIgnore
    public boolean getUser_emailDirtyFlag(){
        return this.user_emailDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [用户 登录]
     */
    @JsonProperty("user_login")
    public String getUser_login(){
        return this.user_login ;
    }

    /**
     * 设置 [用户 登录]
     */
    @JsonProperty("user_login")
    public void setUser_login(String  user_login){
        this.user_login = user_login ;
        this.user_loginDirtyFlag = true ;
    }

     /**
     * 获取 [用户 登录]脏标记
     */
    @JsonIgnore
    public boolean getUser_loginDirtyFlag(){
        return this.user_loginDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return this.website ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return this.websiteDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

     /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 1){
				this.setCampaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("city") instanceof Boolean)&& map.get("city")!=null){
			this.setCity((String)map.get("city"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_currency") instanceof Boolean)&& map.get("company_currency")!=null){
			Object[] objs = (Object[])map.get("company_currency");
			if(objs.length > 0){
				this.setCompany_currency((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("contact_name") instanceof Boolean)&& map.get("contact_name")!=null){
			this.setContact_name((String)map.get("contact_name"));
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date_action_last") instanceof Boolean)&& map.get("date_action_last")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_action_last"));
   			this.setDate_action_last(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_closed") instanceof Boolean)&& map.get("date_closed")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_closed"));
   			this.setDate_closed(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_conversion") instanceof Boolean)&& map.get("date_conversion")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_conversion"));
   			this.setDate_conversion(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_deadline") instanceof Boolean)&& map.get("date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_deadline"));
   			this.setDate_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_last_stage_update") instanceof Boolean)&& map.get("date_last_stage_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_last_stage_update"));
   			this.setDate_last_stage_update(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_open") instanceof Boolean)&& map.get("date_open")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_open"));
   			this.setDate_open(new Timestamp(parse.getTime()));
		}
		if(!(map.get("day_close") instanceof Boolean)&& map.get("day_close")!=null){
			this.setDay_close((Double)map.get("day_close"));
		}
		if(!(map.get("day_open") instanceof Boolean)&& map.get("day_open")!=null){
			this.setDay_open((Double)map.get("day_open"));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_cc") instanceof Boolean)&& map.get("email_cc")!=null){
			this.setEmail_cc((String)map.get("email_cc"));
		}
		if(!(map.get("email_from") instanceof Boolean)&& map.get("email_from")!=null){
			this.setEmail_from((String)map.get("email_from"));
		}
		if(!(map.get("expected_revenue") instanceof Boolean)&& map.get("expected_revenue")!=null){
			this.setExpected_revenue((Double)map.get("expected_revenue"));
		}
		if(!(map.get("function") instanceof Boolean)&& map.get("function")!=null){
			this.setIbizfunction((String)map.get("function"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_blacklisted") instanceof Boolean){
			this.setIs_blacklisted(((Boolean)map.get("is_blacklisted"))? "true" : "false");
		}
		if(!(map.get("kanban_state") instanceof Boolean)&& map.get("kanban_state")!=null){
			this.setKanban_state((String)map.get("kanban_state"));
		}
		if(!(map.get("lost_reason") instanceof Boolean)&& map.get("lost_reason")!=null){
			Object[] objs = (Object[])map.get("lost_reason");
			if(objs.length > 0){
				this.setLost_reason((Integer)objs[0]);
			}
		}
		if(!(map.get("lost_reason") instanceof Boolean)&& map.get("lost_reason")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lost_reason");
			if(objs.length > 1){
				this.setLost_reason_text((String)objs[1]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("meeting_count") instanceof Boolean)&& map.get("meeting_count")!=null){
			this.setMeeting_count((Integer)map.get("meeting_count"));
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_bounce") instanceof Boolean)&& map.get("message_bounce")!=null){
			this.setMessage_bounce((Integer)map.get("message_bounce"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("mobile") instanceof Boolean)&& map.get("mobile")!=null){
			this.setMobile((String)map.get("mobile"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("order_ids") instanceof Boolean)&& map.get("order_ids")!=null){
			Object[] objs = (Object[])map.get("order_ids");
			if(objs.length > 0){
				Integer[] order_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOrder_ids(Arrays.toString(order_ids).replace(" ",""));
			}
		}
		if(!(map.get("partner_address_email") instanceof Boolean)&& map.get("partner_address_email")!=null){
			this.setPartner_address_email((String)map.get("partner_address_email"));
		}
		if(!(map.get("partner_address_mobile") instanceof Boolean)&& map.get("partner_address_mobile")!=null){
			this.setPartner_address_mobile((String)map.get("partner_address_mobile"));
		}
		if(!(map.get("partner_address_name") instanceof Boolean)&& map.get("partner_address_name")!=null){
			this.setPartner_address_name((String)map.get("partner_address_name"));
		}
		if(!(map.get("partner_address_phone") instanceof Boolean)&& map.get("partner_address_phone")!=null){
			this.setPartner_address_phone((String)map.get("partner_address_phone"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(map.get("partner_is_blacklisted") instanceof Boolean){
			this.setPartner_is_blacklisted(((Boolean)map.get("partner_is_blacklisted"))? "true" : "false");
		}
		if(!(map.get("partner_name") instanceof Boolean)&& map.get("partner_name")!=null){
			this.setPartner_name((String)map.get("partner_name"));
		}
		if(!(map.get("phone") instanceof Boolean)&& map.get("phone")!=null){
			this.setPhone((String)map.get("phone"));
		}
		if(!(map.get("planned_revenue") instanceof Boolean)&& map.get("planned_revenue")!=null){
			this.setPlanned_revenue((Double)map.get("planned_revenue"));
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((String)map.get("priority"));
		}
		if(!(map.get("probability") instanceof Boolean)&& map.get("probability")!=null){
			this.setProbability((Double)map.get("probability"));
		}
		if(!(map.get("referred") instanceof Boolean)&& map.get("referred")!=null){
			this.setReferred((String)map.get("referred"));
		}
		if(!(map.get("sale_amount_total") instanceof Boolean)&& map.get("sale_amount_total")!=null){
			this.setSale_amount_total((Double)map.get("sale_amount_total"));
		}
		if(!(map.get("sale_number") instanceof Boolean)&& map.get("sale_number")!=null){
			this.setSale_number((Integer)map.get("sale_number"));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 1){
				this.setSource_id_text((String)objs[1]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 0){
				this.setStage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 1){
				this.setStage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state_id") instanceof Boolean)&& map.get("state_id")!=null){
			Object[] objs = (Object[])map.get("state_id");
			if(objs.length > 0){
				this.setState_id((Integer)objs[0]);
			}
		}
		if(!(map.get("state_id") instanceof Boolean)&& map.get("state_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("state_id");
			if(objs.length > 1){
				this.setState_id_text((String)objs[1]);
			}
		}
		if(!(map.get("street") instanceof Boolean)&& map.get("street")!=null){
			this.setStreet((String)map.get("street"));
		}
		if(!(map.get("street2") instanceof Boolean)&& map.get("street2")!=null){
			this.setStreet2((String)map.get("street2"));
		}
		if(!(map.get("tag_ids") instanceof Boolean)&& map.get("tag_ids")!=null){
			Object[] objs = (Object[])map.get("tag_ids");
			if(objs.length > 0){
				Integer[] tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTag_ids(Arrays.toString(tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("title") instanceof Boolean)&& map.get("title")!=null){
			Object[] objs = (Object[])map.get("title");
			if(objs.length > 0){
				this.setTitle((Integer)objs[0]);
			}
		}
		if(!(map.get("title") instanceof Boolean)&& map.get("title")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("title");
			if(objs.length > 1){
				this.setTitle_text((String)objs[1]);
			}
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("user_email") instanceof Boolean)&& map.get("user_email")!=null){
			this.setUser_email((String)map.get("user_email"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("user_login") instanceof Boolean)&& map.get("user_login")!=null){
			this.setUser_login((String)map.get("user_login"));
		}
		if(!(map.get("website") instanceof Boolean)&& map.get("website")!=null){
			this.setWebsite((String)map.get("website"));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("zip") instanceof Boolean)&& map.get("zip")!=null){
			this.setZip((String)map.get("zip"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCampaign_id_text()!=null&&this.getCampaign_id_textDirtyFlag()){
			//忽略文本外键campaign_id_text
		}else if(this.getCampaign_id_textDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCity()!=null&&this.getCityDirtyFlag()){
			map.put("city",this.getCity());
		}else if(this.getCityDirtyFlag()){
			map.put("city",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_currency()!=null&&this.getCompany_currencyDirtyFlag()){
			map.put("company_currency",this.getCompany_currency());
		}else if(this.getCompany_currencyDirtyFlag()){
			map.put("company_currency",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getContact_name()!=null&&this.getContact_nameDirtyFlag()){
			map.put("contact_name",this.getContact_name());
		}else if(this.getContact_nameDirtyFlag()){
			map.put("contact_name",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate_action_last()!=null&&this.getDate_action_lastDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_action_last());
			map.put("date_action_last",datetimeStr);
		}else if(this.getDate_action_lastDirtyFlag()){
			map.put("date_action_last",false);
		}
		if(this.getDate_closed()!=null&&this.getDate_closedDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_closed());
			map.put("date_closed",datetimeStr);
		}else if(this.getDate_closedDirtyFlag()){
			map.put("date_closed",false);
		}
		if(this.getDate_conversion()!=null&&this.getDate_conversionDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_conversion());
			map.put("date_conversion",datetimeStr);
		}else if(this.getDate_conversionDirtyFlag()){
			map.put("date_conversion",false);
		}
		if(this.getDate_deadline()!=null&&this.getDate_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_deadline());
			map.put("date_deadline",datetimeStr);
		}else if(this.getDate_deadlineDirtyFlag()){
			map.put("date_deadline",false);
		}
		if(this.getDate_last_stage_update()!=null&&this.getDate_last_stage_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_last_stage_update());
			map.put("date_last_stage_update",datetimeStr);
		}else if(this.getDate_last_stage_updateDirtyFlag()){
			map.put("date_last_stage_update",false);
		}
		if(this.getDate_open()!=null&&this.getDate_openDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_open());
			map.put("date_open",datetimeStr);
		}else if(this.getDate_openDirtyFlag()){
			map.put("date_open",false);
		}
		if(this.getDay_close()!=null&&this.getDay_closeDirtyFlag()){
			map.put("day_close",this.getDay_close());
		}else if(this.getDay_closeDirtyFlag()){
			map.put("day_close",false);
		}
		if(this.getDay_open()!=null&&this.getDay_openDirtyFlag()){
			map.put("day_open",this.getDay_open());
		}else if(this.getDay_openDirtyFlag()){
			map.put("day_open",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_cc()!=null&&this.getEmail_ccDirtyFlag()){
			map.put("email_cc",this.getEmail_cc());
		}else if(this.getEmail_ccDirtyFlag()){
			map.put("email_cc",false);
		}
		if(this.getEmail_from()!=null&&this.getEmail_fromDirtyFlag()){
			map.put("email_from",this.getEmail_from());
		}else if(this.getEmail_fromDirtyFlag()){
			map.put("email_from",false);
		}
		if(this.getExpected_revenue()!=null&&this.getExpected_revenueDirtyFlag()){
			map.put("expected_revenue",this.getExpected_revenue());
		}else if(this.getExpected_revenueDirtyFlag()){
			map.put("expected_revenue",false);
		}
		if(this.getIbizfunction()!=null&&this.getIbizfunctionDirtyFlag()){
			map.put("function",this.getIbizfunction());
		}else if(this.getIbizfunctionDirtyFlag()){
			map.put("function",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_blacklisted()!=null&&this.getIs_blacklistedDirtyFlag()){
			map.put("is_blacklisted",Boolean.parseBoolean(this.getIs_blacklisted()));		
		}		if(this.getKanban_state()!=null&&this.getKanban_stateDirtyFlag()){
			map.put("kanban_state",this.getKanban_state());
		}else if(this.getKanban_stateDirtyFlag()){
			map.put("kanban_state",false);
		}
		if(this.getLost_reason()!=null&&this.getLost_reasonDirtyFlag()){
			map.put("lost_reason",this.getLost_reason());
		}else if(this.getLost_reasonDirtyFlag()){
			map.put("lost_reason",false);
		}
		if(this.getLost_reason_text()!=null&&this.getLost_reason_textDirtyFlag()){
			//忽略文本外键lost_reason_text
		}else if(this.getLost_reason_textDirtyFlag()){
			map.put("lost_reason",false);
		}
		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMeeting_count()!=null&&this.getMeeting_countDirtyFlag()){
			map.put("meeting_count",this.getMeeting_count());
		}else if(this.getMeeting_countDirtyFlag()){
			map.put("meeting_count",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_bounce()!=null&&this.getMessage_bounceDirtyFlag()){
			map.put("message_bounce",this.getMessage_bounce());
		}else if(this.getMessage_bounceDirtyFlag()){
			map.put("message_bounce",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMobile()!=null&&this.getMobileDirtyFlag()){
			map.put("mobile",this.getMobile());
		}else if(this.getMobileDirtyFlag()){
			map.put("mobile",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOrder_ids()!=null&&this.getOrder_idsDirtyFlag()){
			map.put("order_ids",this.getOrder_ids());
		}else if(this.getOrder_idsDirtyFlag()){
			map.put("order_ids",false);
		}
		if(this.getPartner_address_email()!=null&&this.getPartner_address_emailDirtyFlag()){
			map.put("partner_address_email",this.getPartner_address_email());
		}else if(this.getPartner_address_emailDirtyFlag()){
			map.put("partner_address_email",false);
		}
		if(this.getPartner_address_mobile()!=null&&this.getPartner_address_mobileDirtyFlag()){
			map.put("partner_address_mobile",this.getPartner_address_mobile());
		}else if(this.getPartner_address_mobileDirtyFlag()){
			map.put("partner_address_mobile",false);
		}
		if(this.getPartner_address_name()!=null&&this.getPartner_address_nameDirtyFlag()){
			map.put("partner_address_name",this.getPartner_address_name());
		}else if(this.getPartner_address_nameDirtyFlag()){
			map.put("partner_address_name",false);
		}
		if(this.getPartner_address_phone()!=null&&this.getPartner_address_phoneDirtyFlag()){
			map.put("partner_address_phone",this.getPartner_address_phone());
		}else if(this.getPartner_address_phoneDirtyFlag()){
			map.put("partner_address_phone",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_is_blacklisted()!=null&&this.getPartner_is_blacklistedDirtyFlag()){
			map.put("partner_is_blacklisted",Boolean.parseBoolean(this.getPartner_is_blacklisted()));		
		}		if(this.getPartner_name()!=null&&this.getPartner_nameDirtyFlag()){
			map.put("partner_name",this.getPartner_name());
		}else if(this.getPartner_nameDirtyFlag()){
			map.put("partner_name",false);
		}
		if(this.getPhone()!=null&&this.getPhoneDirtyFlag()){
			map.put("phone",this.getPhone());
		}else if(this.getPhoneDirtyFlag()){
			map.put("phone",false);
		}
		if(this.getPlanned_revenue()!=null&&this.getPlanned_revenueDirtyFlag()){
			map.put("planned_revenue",this.getPlanned_revenue());
		}else if(this.getPlanned_revenueDirtyFlag()){
			map.put("planned_revenue",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getProbability()!=null&&this.getProbabilityDirtyFlag()){
			map.put("probability",this.getProbability());
		}else if(this.getProbabilityDirtyFlag()){
			map.put("probability",false);
		}
		if(this.getReferred()!=null&&this.getReferredDirtyFlag()){
			map.put("referred",this.getReferred());
		}else if(this.getReferredDirtyFlag()){
			map.put("referred",false);
		}
		if(this.getSale_amount_total()!=null&&this.getSale_amount_totalDirtyFlag()){
			map.put("sale_amount_total",this.getSale_amount_total());
		}else if(this.getSale_amount_totalDirtyFlag()){
			map.put("sale_amount_total",false);
		}
		if(this.getSale_number()!=null&&this.getSale_numberDirtyFlag()){
			map.put("sale_number",this.getSale_number());
		}else if(this.getSale_numberDirtyFlag()){
			map.put("sale_number",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getSource_id_text()!=null&&this.getSource_id_textDirtyFlag()){
			//忽略文本外键source_id_text
		}else if(this.getSource_id_textDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getStage_id()!=null&&this.getStage_idDirtyFlag()){
			map.put("stage_id",this.getStage_id());
		}else if(this.getStage_idDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getStage_id_text()!=null&&this.getStage_id_textDirtyFlag()){
			//忽略文本外键stage_id_text
		}else if(this.getStage_id_textDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getState_id()!=null&&this.getState_idDirtyFlag()){
			map.put("state_id",this.getState_id());
		}else if(this.getState_idDirtyFlag()){
			map.put("state_id",false);
		}
		if(this.getState_id_text()!=null&&this.getState_id_textDirtyFlag()){
			//忽略文本外键state_id_text
		}else if(this.getState_id_textDirtyFlag()){
			map.put("state_id",false);
		}
		if(this.getStreet()!=null&&this.getStreetDirtyFlag()){
			map.put("street",this.getStreet());
		}else if(this.getStreetDirtyFlag()){
			map.put("street",false);
		}
		if(this.getStreet2()!=null&&this.getStreet2DirtyFlag()){
			map.put("street2",this.getStreet2());
		}else if(this.getStreet2DirtyFlag()){
			map.put("street2",false);
		}
		if(this.getTag_ids()!=null&&this.getTag_idsDirtyFlag()){
			map.put("tag_ids",this.getTag_ids());
		}else if(this.getTag_idsDirtyFlag()){
			map.put("tag_ids",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTitle()!=null&&this.getTitleDirtyFlag()){
			map.put("title",this.getTitle());
		}else if(this.getTitleDirtyFlag()){
			map.put("title",false);
		}
		if(this.getTitle_text()!=null&&this.getTitle_textDirtyFlag()){
			//忽略文本外键title_text
		}else if(this.getTitle_textDirtyFlag()){
			map.put("title",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUser_email()!=null&&this.getUser_emailDirtyFlag()){
			map.put("user_email",this.getUser_email());
		}else if(this.getUser_emailDirtyFlag()){
			map.put("user_email",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_login()!=null&&this.getUser_loginDirtyFlag()){
			map.put("user_login",this.getUser_login());
		}else if(this.getUser_loginDirtyFlag()){
			map.put("user_login",false);
		}
		if(this.getWebsite()!=null&&this.getWebsiteDirtyFlag()){
			map.put("website",this.getWebsite());
		}else if(this.getWebsiteDirtyFlag()){
			map.put("website",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getZip()!=null&&this.getZipDirtyFlag()){
			map.put("zip",this.getZip());
		}else if(this.getZipDirtyFlag()){
			map.put("zip",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
