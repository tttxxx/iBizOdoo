package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_bank_statement_lineClientModel;

/**
 * 实体[银行对账单明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_lineServiceImpl implements IAccount_bank_statement_lineService {

    @Autowired
    account_bank_statement_lineOdooClient account_bank_statement_lineOdooClient;


    @Override
    public boolean update(Account_bank_statement_line et) {
        account_bank_statement_lineClientModel clientModel = convert2Model(et,null);
		account_bank_statement_lineOdooClient.update(clientModel);
        Account_bank_statement_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_bank_statement_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_bank_statement_lineClientModel clientModel = new account_bank_statement_lineClientModel();
        clientModel.setId(id);
		account_bank_statement_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_bank_statement_line et) {
        account_bank_statement_lineClientModel clientModel = convert2Model(et,null);
		account_bank_statement_lineOdooClient.create(clientModel);
        Account_bank_statement_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_line> list){
    }

    @Override
    public Account_bank_statement_line get(Integer id) {
        account_bank_statement_lineClientModel clientModel = new account_bank_statement_lineClientModel();
        clientModel.setId(id);
		account_bank_statement_lineOdooClient.get(clientModel);
        Account_bank_statement_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_bank_statement_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context) {
        List<Account_bank_statement_line> list = new ArrayList<Account_bank_statement_line>();
        Page<account_bank_statement_lineClientModel> clientModelList = account_bank_statement_lineOdooClient.search(context);
        for(account_bank_statement_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_bank_statement_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_bank_statement_lineClientModel convert2Model(Account_bank_statement_line domain , account_bank_statement_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_bank_statement_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("partner_namedirtyflag"))
                model.setPartner_name(domain.getPartnerName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("account_numberdirtyflag"))
                model.setAccount_number(domain.getAccountNumber());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("journal_entry_idsdirtyflag"))
                model.setJournal_entry_ids(domain.getJournalEntryIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("move_namedirtyflag"))
                model.setMove_name(domain.getMoveName());
            if((Boolean) domain.getExtensionparams().get("unique_import_iddirtyflag"))
                model.setUnique_import_id(domain.getUniqueImportId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("pos_statement_iddirtyflag"))
                model.setPos_statement_id(domain.getPosStatementId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("amount_currencydirtyflag"))
                model.setAmount_currency(domain.getAmountCurrency());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("journal_currency_iddirtyflag"))
                model.setJournal_currency_id(domain.getJournalCurrencyId());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("statement_id_textdirtyflag"))
                model.setStatement_id_text(domain.getStatementIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("bank_account_iddirtyflag"))
                model.setBank_account_id(domain.getBankAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("statement_iddirtyflag"))
                model.setStatement_id(domain.getStatementId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_bank_statement_line convert2Domain( account_bank_statement_lineClientModel model ,Account_bank_statement_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_bank_statement_line();
        }

        if(model.getPartner_nameDirtyFlag())
            domain.setPartnerName(model.getPartner_name());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getAccount_numberDirtyFlag())
            domain.setAccountNumber(model.getAccount_number());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getJournal_entry_idsDirtyFlag())
            domain.setJournalEntryIds(model.getJournal_entry_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMove_nameDirtyFlag())
            domain.setMoveName(model.getMove_name());
        if(model.getUnique_import_idDirtyFlag())
            domain.setUniqueImportId(model.getUnique_import_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getPos_statement_idDirtyFlag())
            domain.setPosStatementId(model.getPos_statement_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAmount_currencyDirtyFlag())
            domain.setAmountCurrency(model.getAmount_currency());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getJournal_currency_idDirtyFlag())
            domain.setJournalCurrencyId(model.getJournal_currency_id());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getStatement_id_textDirtyFlag())
            domain.setStatementIdText(model.getStatement_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getBank_account_idDirtyFlag())
            domain.setBankAccountId(model.getBank_account_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getStatement_idDirtyFlag())
            domain.setStatementId(model.getStatement_id());
        return domain ;
    }

}

    



