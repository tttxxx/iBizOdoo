package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;

/**
 * 实体 [联系人] 存储对象
 */
public interface Res_partnerRepository extends Repository<Res_partner> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner> searchDefault(Res_partnerSearchContext context);

    Res_partner convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner domain , Res_partner po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner convert2Domain( Res_partner po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner domain) ;

}
