package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_move] 对象
 */
public interface Istock_move {

    /**
     * 获取 [会计凭证]
     */
    public void setAccount_move_ids(String account_move_ids);
    
    /**
     * 设置 [会计凭证]
     */
    public String getAccount_move_ids();

    /**
     * 获取 [会计凭证]脏标记
     */
    public boolean getAccount_move_idsDirtyFlag();
    /**
     * 获取 [批次]
     */
    public void setActive_move_line_ids(String active_move_line_ids);
    
    /**
     * 设置 [批次]
     */
    public String getActive_move_line_ids();

    /**
     * 获取 [批次]脏标记
     */
    public boolean getActive_move_line_idsDirtyFlag();
    /**
     * 获取 [在分拣确认后是否添加了移动]
     */
    public void setAdditional(String additional);
    
    /**
     * 设置 [在分拣确认后是否添加了移动]
     */
    public String getAdditional();

    /**
     * 获取 [在分拣确认后是否添加了移动]脏标记
     */
    public boolean getAdditionalDirtyFlag();
    /**
     * 获取 [预测数量]
     */
    public void setAvailability(Double availability);
    
    /**
     * 设置 [预测数量]
     */
    public Double getAvailability();

    /**
     * 获取 [预测数量]脏标记
     */
    public boolean getAvailabilityDirtyFlag();
    /**
     * 获取 [欠单]
     */
    public void setBackorder_id(Integer backorder_id);
    
    /**
     * 设置 [欠单]
     */
    public Integer getBackorder_id();

    /**
     * 获取 [欠单]脏标记
     */
    public boolean getBackorder_idDirtyFlag();
    /**
     * 获取 [BOM行]
     */
    public void setBom_line_id(Integer bom_line_id);
    
    /**
     * 设置 [BOM行]
     */
    public Integer getBom_line_id();

    /**
     * 获取 [BOM行]脏标记
     */
    public boolean getBom_line_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [拆卸单]
     */
    public void setConsume_unbuild_id(Integer consume_unbuild_id);
    
    /**
     * 设置 [拆卸单]
     */
    public Integer getConsume_unbuild_id();

    /**
     * 获取 [拆卸单]脏标记
     */
    public boolean getConsume_unbuild_idDirtyFlag();
    /**
     * 获取 [拆卸单]
     */
    public void setConsume_unbuild_id_text(String consume_unbuild_id_text);
    
    /**
     * 设置 [拆卸单]
     */
    public String getConsume_unbuild_id_text();

    /**
     * 获取 [拆卸单]脏标记
     */
    public boolean getConsume_unbuild_id_textDirtyFlag();
    /**
     * 获取 [创建生产订单]
     */
    public void setCreated_production_id(Integer created_production_id);
    
    /**
     * 设置 [创建生产订单]
     */
    public Integer getCreated_production_id();

    /**
     * 获取 [创建生产订单]脏标记
     */
    public boolean getCreated_production_idDirtyFlag();
    /**
     * 获取 [创建生产订单]
     */
    public void setCreated_production_id_text(String created_production_id_text);
    
    /**
     * 设置 [创建生产订单]
     */
    public String getCreated_production_id_text();

    /**
     * 获取 [创建生产订单]脏标记
     */
    public boolean getCreated_production_id_textDirtyFlag();
    /**
     * 获取 [创建采购订单行]
     */
    public void setCreated_purchase_line_id(Integer created_purchase_line_id);
    
    /**
     * 设置 [创建采购订单行]
     */
    public Integer getCreated_purchase_line_id();

    /**
     * 获取 [创建采购订单行]脏标记
     */
    public boolean getCreated_purchase_line_idDirtyFlag();
    /**
     * 获取 [创建采购订单行]
     */
    public void setCreated_purchase_line_id_text(String created_purchase_line_id_text);
    
    /**
     * 设置 [创建采购订单行]
     */
    public String getCreated_purchase_line_id_text();

    /**
     * 获取 [创建采购订单行]脏标记
     */
    public boolean getCreated_purchase_line_id_textDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [预计日期]
     */
    public void setDate_expected(Timestamp date_expected);
    
    /**
     * 设置 [预计日期]
     */
    public Timestamp getDate_expected();

    /**
     * 获取 [预计日期]脏标记
     */
    public boolean getDate_expectedDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [完工批次已存在]
     */
    public void setFinished_lots_exist(String finished_lots_exist);
    
    /**
     * 设置 [完工批次已存在]
     */
    public String getFinished_lots_exist();

    /**
     * 获取 [完工批次已存在]脏标记
     */
    public boolean getFinished_lots_existDirtyFlag();
    /**
     * 获取 [补货组]
     */
    public void setGroup_id(Integer group_id);
    
    /**
     * 设置 [补货组]
     */
    public Integer getGroup_id();

    /**
     * 获取 [补货组]脏标记
     */
    public boolean getGroup_idDirtyFlag();
    /**
     * 获取 [有移动行]
     */
    public void setHas_move_lines(String has_move_lines);
    
    /**
     * 设置 [有移动行]
     */
    public String getHas_move_lines();

    /**
     * 获取 [有移动行]脏标记
     */
    public boolean getHas_move_linesDirtyFlag();
    /**
     * 获取 [使用追踪的产品]
     */
    public void setHas_tracking(String has_tracking);
    
    /**
     * 设置 [使用追踪的产品]
     */
    public String getHas_tracking();

    /**
     * 获取 [使用追踪的产品]脏标记
     */
    public boolean getHas_trackingDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [库存]
     */
    public void setInventory_id(Integer inventory_id);
    
    /**
     * 设置 [库存]
     */
    public Integer getInventory_id();

    /**
     * 获取 [库存]脏标记
     */
    public boolean getInventory_idDirtyFlag();
    /**
     * 获取 [库存]
     */
    public void setInventory_id_text(String inventory_id_text);
    
    /**
     * 设置 [库存]
     */
    public String getInventory_id_text();

    /**
     * 获取 [库存]脏标记
     */
    public boolean getInventory_id_textDirtyFlag();
    /**
     * 获取 [完成]
     */
    public void setIs_done(String is_done);
    
    /**
     * 设置 [完成]
     */
    public String getIs_done();

    /**
     * 获取 [完成]脏标记
     */
    public boolean getIs_doneDirtyFlag();
    /**
     * 获取 [初始需求是否可以编辑]
     */
    public void setIs_initial_demand_editable(String is_initial_demand_editable);
    
    /**
     * 设置 [初始需求是否可以编辑]
     */
    public String getIs_initial_demand_editable();

    /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    public boolean getIs_initial_demand_editableDirtyFlag();
    /**
     * 获取 [是锁定]
     */
    public void setIs_locked(String is_locked);
    
    /**
     * 设置 [是锁定]
     */
    public String getIs_locked();

    /**
     * 获取 [是锁定]脏标记
     */
    public boolean getIs_lockedDirtyFlag();
    /**
     * 获取 [完成数量是否可以编辑]
     */
    public void setIs_quantity_done_editable(String is_quantity_done_editable);
    
    /**
     * 设置 [完成数量是否可以编辑]
     */
    public String getIs_quantity_done_editable();

    /**
     * 获取 [完成数量是否可以编辑]脏标记
     */
    public boolean getIs_quantity_done_editableDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_dest_id(Integer location_dest_id);
    
    /**
     * 设置 [目的位置]
     */
    public Integer getLocation_dest_id();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_dest_idDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_dest_id_text(String location_dest_id_text);
    
    /**
     * 设置 [目的位置]
     */
    public String getLocation_dest_id_text();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_dest_id_textDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [源位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [源位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [目的地移动]
     */
    public void setMove_dest_ids(String move_dest_ids);
    
    /**
     * 设置 [目的地移动]
     */
    public String getMove_dest_ids();

    /**
     * 获取 [目的地移动]脏标记
     */
    public boolean getMove_dest_idsDirtyFlag();
    /**
     * 获取 [凭证明细]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [凭证明细]
     */
    public String getMove_line_ids();

    /**
     * 获取 [凭证明细]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [移动行无建议]
     */
    public void setMove_line_nosuggest_ids(String move_line_nosuggest_ids);
    
    /**
     * 设置 [移动行无建议]
     */
    public String getMove_line_nosuggest_ids();

    /**
     * 获取 [移动行无建议]脏标记
     */
    public boolean getMove_line_nosuggest_idsDirtyFlag();
    /**
     * 获取 [原始移动]
     */
    public void setMove_orig_ids(String move_orig_ids);
    
    /**
     * 设置 [原始移动]
     */
    public String getMove_orig_ids();

    /**
     * 获取 [原始移动]脏标记
     */
    public boolean getMove_orig_idsDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setNeeds_lots(String needs_lots);
    
    /**
     * 设置 [追踪]
     */
    public String getNeeds_lots();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getNeeds_lotsDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [待加工的作业]
     */
    public void setOperation_id(Integer operation_id);
    
    /**
     * 设置 [待加工的作业]
     */
    public Integer getOperation_id();

    /**
     * 获取 [待加工的作业]脏标记
     */
    public boolean getOperation_idDirtyFlag();
    /**
     * 获取 [待加工的作业]
     */
    public void setOperation_id_text(String operation_id_text);
    
    /**
     * 设置 [待加工的作业]
     */
    public String getOperation_id_text();

    /**
     * 获取 [待加工的作业]脏标记
     */
    public boolean getOperation_id_textDirtyFlag();
    /**
     * 获取 [订单完成批次]
     */
    public void setOrder_finished_lot_ids(String order_finished_lot_ids);
    
    /**
     * 设置 [订单完成批次]
     */
    public String getOrder_finished_lot_ids();

    /**
     * 获取 [订单完成批次]脏标记
     */
    public boolean getOrder_finished_lot_idsDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [原始退回移动]
     */
    public void setOrigin_returned_move_id(Integer origin_returned_move_id);
    
    /**
     * 设置 [原始退回移动]
     */
    public Integer getOrigin_returned_move_id();

    /**
     * 获取 [原始退回移动]脏标记
     */
    public boolean getOrigin_returned_move_idDirtyFlag();
    /**
     * 获取 [原始退回移动]
     */
    public void setOrigin_returned_move_id_text(String origin_returned_move_id_text);
    
    /**
     * 设置 [原始退回移动]
     */
    public String getOrigin_returned_move_id_text();

    /**
     * 获取 [原始退回移动]脏标记
     */
    public boolean getOrigin_returned_move_id_textDirtyFlag();
    /**
     * 获取 [包裹层级]
     */
    public void setPackage_level_id(Integer package_level_id);
    
    /**
     * 设置 [包裹层级]
     */
    public Integer getPackage_level_id();

    /**
     * 获取 [包裹层级]脏标记
     */
    public boolean getPackage_level_idDirtyFlag();
    /**
     * 获取 [目的地地址]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [目的地地址]
     */
    public Integer getPartner_id();

    /**
     * 获取 [目的地地址]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [目的地地址]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [目的地地址]
     */
    public String getPartner_id_text();

    /**
     * 获取 [目的地地址]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [作业的类型]
     */
    public void setPicking_code(String picking_code);
    
    /**
     * 设置 [作业的类型]
     */
    public String getPicking_code();

    /**
     * 获取 [作业的类型]脏标记
     */
    public boolean getPicking_codeDirtyFlag();
    /**
     * 获取 [调拨参照]
     */
    public void setPicking_id(Integer picking_id);
    
    /**
     * 设置 [调拨参照]
     */
    public Integer getPicking_id();

    /**
     * 获取 [调拨参照]脏标记
     */
    public boolean getPicking_idDirtyFlag();
    /**
     * 获取 [调拨参照]
     */
    public void setPicking_id_text(String picking_id_text);
    
    /**
     * 设置 [调拨参照]
     */
    public String getPicking_id_text();

    /**
     * 获取 [调拨参照]脏标记
     */
    public boolean getPicking_id_textDirtyFlag();
    /**
     * 获取 [调拨目的地地址]
     */
    public void setPicking_partner_id(Integer picking_partner_id);
    
    /**
     * 设置 [调拨目的地地址]
     */
    public Integer getPicking_partner_id();

    /**
     * 获取 [调拨目的地地址]脏标记
     */
    public boolean getPicking_partner_idDirtyFlag();
    /**
     * 获取 [移动整个包裹]
     */
    public void setPicking_type_entire_packs(String picking_type_entire_packs);
    
    /**
     * 设置 [移动整个包裹]
     */
    public String getPicking_type_entire_packs();

    /**
     * 获取 [移动整个包裹]脏标记
     */
    public boolean getPicking_type_entire_packsDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [作业类型]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [作业类型]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setPrice_unit(Double price_unit);
    
    /**
     * 设置 [单价]
     */
    public Double getPrice_unit();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getPrice_unitDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [供应方法]
     */
    public void setProcure_method(String procure_method);
    
    /**
     * 设置 [供应方法]
     */
    public String getProcure_method();

    /**
     * 获取 [供应方法]脏标记
     */
    public boolean getProcure_methodDirtyFlag();
    /**
     * 获取 [成品的生产订单]
     */
    public void setProduction_id(Integer production_id);
    
    /**
     * 设置 [成品的生产订单]
     */
    public Integer getProduction_id();

    /**
     * 获取 [成品的生产订单]脏标记
     */
    public boolean getProduction_idDirtyFlag();
    /**
     * 获取 [成品的生产订单]
     */
    public void setProduction_id_text(String production_id_text);
    
    /**
     * 设置 [成品的生产订单]
     */
    public String getProduction_id_text();

    /**
     * 获取 [成品的生产订单]脏标记
     */
    public boolean getProduction_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [首选包装]
     */
    public void setProduct_packaging(Integer product_packaging);
    
    /**
     * 设置 [首选包装]
     */
    public Integer getProduct_packaging();

    /**
     * 获取 [首选包装]脏标记
     */
    public boolean getProduct_packagingDirtyFlag();
    /**
     * 获取 [首选包装]
     */
    public void setProduct_packaging_text(String product_packaging_text);
    
    /**
     * 设置 [首选包装]
     */
    public String getProduct_packaging_text();

    /**
     * 获取 [首选包装]脏标记
     */
    public boolean getProduct_packaging_textDirtyFlag();
    /**
     * 获取 [实际数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [实际数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [实际数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品类型]
     */
    public void setProduct_type(String product_type);
    
    /**
     * 设置 [产品类型]
     */
    public String getProduct_type();

    /**
     * 获取 [产品类型]脏标记
     */
    public boolean getProduct_typeDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [初始需求]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [初始需求]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [初始需求]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [传播取消以及拆分]
     */
    public void setPropagate(String propagate);
    
    /**
     * 设置 [传播取消以及拆分]
     */
    public String getPropagate();

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    public boolean getPropagateDirtyFlag();
    /**
     * 获取 [采购订单行]
     */
    public void setPurchase_line_id(Integer purchase_line_id);
    
    /**
     * 设置 [采购订单行]
     */
    public Integer getPurchase_line_id();

    /**
     * 获取 [采购订单行]脏标记
     */
    public boolean getPurchase_line_idDirtyFlag();
    /**
     * 获取 [采购订单行]
     */
    public void setPurchase_line_id_text(String purchase_line_id_text);
    
    /**
     * 设置 [采购订单行]
     */
    public String getPurchase_line_id_text();

    /**
     * 获取 [采购订单行]脏标记
     */
    public boolean getPurchase_line_id_textDirtyFlag();
    /**
     * 获取 [完成数量]
     */
    public void setQuantity_done(Double quantity_done);
    
    /**
     * 设置 [完成数量]
     */
    public Double getQuantity_done();

    /**
     * 获取 [完成数量]脏标记
     */
    public boolean getQuantity_doneDirtyFlag();
    /**
     * 获取 [原材料的生产订单]
     */
    public void setRaw_material_production_id(Integer raw_material_production_id);
    
    /**
     * 设置 [原材料的生产订单]
     */
    public Integer getRaw_material_production_id();

    /**
     * 获取 [原材料的生产订单]脏标记
     */
    public boolean getRaw_material_production_idDirtyFlag();
    /**
     * 获取 [原材料的生产订单]
     */
    public void setRaw_material_production_id_text(String raw_material_production_id_text);
    
    /**
     * 设置 [原材料的生产订单]
     */
    public String getRaw_material_production_id_text();

    /**
     * 获取 [原材料的生产订单]脏标记
     */
    public boolean getRaw_material_production_id_textDirtyFlag();
    /**
     * 获取 [编号]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [编号]
     */
    public String getReference();

    /**
     * 获取 [编号]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [剩余数量]
     */
    public void setRemaining_qty(Double remaining_qty);
    
    /**
     * 设置 [剩余数量]
     */
    public Double getRemaining_qty();

    /**
     * 获取 [剩余数量]脏标记
     */
    public boolean getRemaining_qtyDirtyFlag();
    /**
     * 获取 [剩余价值]
     */
    public void setRemaining_value(Double remaining_value);
    
    /**
     * 设置 [剩余价值]
     */
    public Double getRemaining_value();

    /**
     * 获取 [剩余价值]脏标记
     */
    public boolean getRemaining_valueDirtyFlag();
    /**
     * 获取 [维修]
     */
    public void setRepair_id(Integer repair_id);
    
    /**
     * 设置 [维修]
     */
    public Integer getRepair_id();

    /**
     * 获取 [维修]脏标记
     */
    public boolean getRepair_idDirtyFlag();
    /**
     * 获取 [维修]
     */
    public void setRepair_id_text(String repair_id_text);
    
    /**
     * 设置 [维修]
     */
    public String getRepair_id_text();

    /**
     * 获取 [维修]脏标记
     */
    public boolean getRepair_id_textDirtyFlag();
    /**
     * 获取 [已预留数量]
     */
    public void setReserved_availability(Double reserved_availability);
    
    /**
     * 设置 [已预留数量]
     */
    public Double getReserved_availability();

    /**
     * 获取 [已预留数量]脏标记
     */
    public boolean getReserved_availabilityDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setRestrict_partner_id(Integer restrict_partner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getRestrict_partner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getRestrict_partner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setRestrict_partner_id_text(String restrict_partner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getRestrict_partner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getRestrict_partner_id_textDirtyFlag();
    /**
     * 获取 [全部退回移动]
     */
    public void setReturned_move_ids(String returned_move_ids);
    
    /**
     * 设置 [全部退回移动]
     */
    public String getReturned_move_ids();

    /**
     * 获取 [全部退回移动]脏标记
     */
    public boolean getReturned_move_idsDirtyFlag();
    /**
     * 获取 [目的路线]
     */
    public void setRoute_ids(String route_ids);
    
    /**
     * 设置 [目的路线]
     */
    public String getRoute_ids();

    /**
     * 获取 [目的路线]脏标记
     */
    public boolean getRoute_idsDirtyFlag();
    /**
     * 获取 [库存规则]
     */
    public void setRule_id(Integer rule_id);
    
    /**
     * 设置 [库存规则]
     */
    public Integer getRule_id();

    /**
     * 获取 [库存规则]脏标记
     */
    public boolean getRule_idDirtyFlag();
    /**
     * 获取 [库存规则]
     */
    public void setRule_id_text(String rule_id_text);
    
    /**
     * 设置 [库存规则]
     */
    public String getRule_id_text();

    /**
     * 获取 [库存规则]脏标记
     */
    public boolean getRule_id_textDirtyFlag();
    /**
     * 获取 [销售明细行]
     */
    public void setSale_line_id(Integer sale_line_id);
    
    /**
     * 设置 [销售明细行]
     */
    public Integer getSale_line_id();

    /**
     * 获取 [销售明细行]脏标记
     */
    public boolean getSale_line_idDirtyFlag();
    /**
     * 获取 [销售明细行]
     */
    public void setSale_line_id_text(String sale_line_id_text);
    
    /**
     * 设置 [销售明细行]
     */
    public String getSale_line_id_text();

    /**
     * 获取 [销售明细行]脏标记
     */
    public boolean getSale_line_id_textDirtyFlag();
    /**
     * 获取 [已报废]
     */
    public void setScrapped(String scrapped);
    
    /**
     * 设置 [已报废]
     */
    public String getScrapped();

    /**
     * 获取 [已报废]脏标记
     */
    public boolean getScrappedDirtyFlag();
    /**
     * 获取 [报废]
     */
    public void setScrap_ids(String scrap_ids);
    
    /**
     * 设置 [报废]
     */
    public String getScrap_ids();

    /**
     * 获取 [报废]脏标记
     */
    public boolean getScrap_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [详情可见]
     */
    public void setShow_details_visible(String show_details_visible);
    
    /**
     * 设置 [详情可见]
     */
    public String getShow_details_visible();

    /**
     * 获取 [详情可见]脏标记
     */
    public boolean getShow_details_visibleDirtyFlag();
    /**
     * 获取 [显示详细作业]
     */
    public void setShow_operations(String show_operations);
    
    /**
     * 设置 [显示详细作业]
     */
    public String getShow_operations();

    /**
     * 获取 [显示详细作业]脏标记
     */
    public boolean getShow_operationsDirtyFlag();
    /**
     * 获取 [从供应商]
     */
    public void setShow_reserved_availability(String show_reserved_availability);
    
    /**
     * 设置 [从供应商]
     */
    public String getShow_reserved_availability();

    /**
     * 获取 [从供应商]脏标记
     */
    public boolean getShow_reserved_availabilityDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [可用量]
     */
    public void setString_availability_info(String string_availability_info);
    
    /**
     * 设置 [可用量]
     */
    public String getString_availability_info();

    /**
     * 获取 [可用量]脏标记
     */
    public boolean getString_availability_infoDirtyFlag();
    /**
     * 获取 [退款 (更新 SO/PO)]
     */
    public void setTo_refund(String to_refund);
    
    /**
     * 设置 [退款 (更新 SO/PO)]
     */
    public String getTo_refund();

    /**
     * 获取 [退款 (更新 SO/PO)]脏标记
     */
    public boolean getTo_refundDirtyFlag();
    /**
     * 获取 [拆卸顺序]
     */
    public void setUnbuild_id(Integer unbuild_id);
    
    /**
     * 设置 [拆卸顺序]
     */
    public Integer getUnbuild_id();

    /**
     * 获取 [拆卸顺序]脏标记
     */
    public boolean getUnbuild_idDirtyFlag();
    /**
     * 获取 [拆卸顺序]
     */
    public void setUnbuild_id_text(String unbuild_id_text);
    
    /**
     * 设置 [拆卸顺序]
     */
    public String getUnbuild_id_text();

    /**
     * 获取 [拆卸顺序]脏标记
     */
    public boolean getUnbuild_id_textDirtyFlag();
    /**
     * 获取 [单位因子]
     */
    public void setUnit_factor(Double unit_factor);
    
    /**
     * 设置 [单位因子]
     */
    public Double getUnit_factor();

    /**
     * 获取 [单位因子]脏标记
     */
    public boolean getUnit_factorDirtyFlag();
    /**
     * 获取 [值]
     */
    public void setValue(Double value);
    
    /**
     * 设置 [值]
     */
    public Double getValue();

    /**
     * 获取 [值]脏标记
     */
    public boolean getValueDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id_text(String warehouse_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_id_textDirtyFlag();
    /**
     * 获取 [待消耗的工单]
     */
    public void setWorkorder_id(Integer workorder_id);
    
    /**
     * 设置 [待消耗的工单]
     */
    public Integer getWorkorder_id();

    /**
     * 获取 [待消耗的工单]脏标记
     */
    public boolean getWorkorder_idDirtyFlag();
    /**
     * 获取 [待消耗的工单]
     */
    public void setWorkorder_id_text(String workorder_id_text);
    
    /**
     * 设置 [待消耗的工单]
     */
    public String getWorkorder_id_text();

    /**
     * 获取 [待消耗的工单]脏标记
     */
    public boolean getWorkorder_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
