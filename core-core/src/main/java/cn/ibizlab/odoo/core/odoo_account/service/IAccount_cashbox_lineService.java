package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;


/**
 * 实体[Account_cashbox_line] 服务对象接口
 */
public interface IAccount_cashbox_lineService{

    boolean create(Account_cashbox_line et) ;
    void createBatch(List<Account_cashbox_line> list) ;
    boolean update(Account_cashbox_line et) ;
    void updateBatch(List<Account_cashbox_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_cashbox_line get(Integer key) ;
    Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context) ;

}



