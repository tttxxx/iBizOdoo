package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;

/**
 * 实体 [请假摘要/报告] 存储模型
 */
public interface Hr_leave_report{

    /**
     * 结束日期
     */
    Timestamp getDate_to();

    void setDate_to(Timestamp date_to);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_toDirtyFlag();

    /**
     * 申请类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [申请类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 天数
     */
    Double getNumber_of_days();

    void setNumber_of_days(Double number_of_days);

    /**
     * 获取 [天数]脏标记
     */
    boolean getNumber_of_daysDirtyFlag();

    /**
     * 反映在最近的工资单中
     */
    String getPayslip_status();

    void setPayslip_status(String payslip_status);

    /**
     * 获取 [反映在最近的工资单中]脏标记
     */
    boolean getPayslip_statusDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_from();

    void setDate_from(Timestamp date_from);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_fromDirtyFlag();

    /**
     * 分配模式
     */
    String getHoliday_type();

    void setHoliday_type(String holiday_type);

    /**
     * 获取 [分配模式]脏标记
     */
    boolean getHoliday_typeDirtyFlag();

    /**
     * 休假类型
     */
    String getHoliday_status_id_text();

    void setHoliday_status_id_text(String holiday_status_id_text);

    /**
     * 获取 [休假类型]脏标记
     */
    boolean getHoliday_status_id_textDirtyFlag();

    /**
     * 员工标签
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [员工标签]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 员工标签
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [员工标签]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 休假类型
     */
    Integer getHoliday_status_id();

    void setHoliday_status_id(Integer holiday_status_id);

    /**
     * 获取 [休假类型]脏标记
     */
    boolean getHoliday_status_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

}
