package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_alias_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_alias_mixinSearchContext;

/**
 * 实体 [EMail别名 Mixin] 存储对象
 */
public interface Mail_alias_mixinRepository extends Repository<Mail_alias_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_alias_mixin> searchDefault(Mail_alias_mixinSearchContext context);

    Mail_alias_mixin convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin domain , Mail_alias_mixin po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin convert2Domain( Mail_alias_mixin po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin domain) ;

}
