package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_product_qtyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_change_product_qtyOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_change_product_qtyClientModel;

/**
 * 实体[更改产品数量] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_change_product_qtyServiceImpl implements IStock_change_product_qtyService {

    @Autowired
    stock_change_product_qtyOdooClient stock_change_product_qtyOdooClient;


    @Override
    public boolean update(Stock_change_product_qty et) {
        stock_change_product_qtyClientModel clientModel = convert2Model(et,null);
		stock_change_product_qtyOdooClient.update(clientModel);
        Stock_change_product_qty rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_change_product_qty> list){
    }

    @Override
    public boolean create(Stock_change_product_qty et) {
        stock_change_product_qtyClientModel clientModel = convert2Model(et,null);
		stock_change_product_qtyOdooClient.create(clientModel);
        Stock_change_product_qty rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_change_product_qty> list){
    }

    @Override
    public Stock_change_product_qty get(Integer id) {
        stock_change_product_qtyClientModel clientModel = new stock_change_product_qtyClientModel();
        clientModel.setId(id);
		stock_change_product_qtyOdooClient.get(clientModel);
        Stock_change_product_qty et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_change_product_qty();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_change_product_qtyClientModel clientModel = new stock_change_product_qtyClientModel();
        clientModel.setId(id);
		stock_change_product_qtyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_change_product_qty> searchDefault(Stock_change_product_qtySearchContext context) {
        List<Stock_change_product_qty> list = new ArrayList<Stock_change_product_qty>();
        Page<stock_change_product_qtyClientModel> clientModelList = stock_change_product_qtyOdooClient.search(context);
        for(stock_change_product_qtyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_change_product_qty>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_change_product_qtyClientModel convert2Model(Stock_change_product_qty domain , stock_change_product_qtyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_change_product_qtyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("new_quantitydirtyflag"))
                model.setNew_quantity(domain.getNewQuantity());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("product_variant_countdirtyflag"))
                model.setProduct_variant_count(domain.getProductVariantCount());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_change_product_qty convert2Domain( stock_change_product_qtyClientModel model ,Stock_change_product_qty domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_change_product_qty();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNew_quantityDirtyFlag())
            domain.setNewQuantity(model.getNew_quantity());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getProduct_variant_countDirtyFlag())
            domain.setProductVariantCount(model.getProduct_variant_count());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



