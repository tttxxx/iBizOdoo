package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [maintenance_equipment] 对象
 */
public interface maintenance_equipment {

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public Timestamp getAssign_date();

    public void setAssign_date(Timestamp assign_date);

    public Integer getCategory_id();

    public void setCategory_id(Integer category_id);

    public String getCategory_id_text();

    public void setCategory_id_text(String category_id_text);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Double getCost();

    public void setCost(Double cost);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Timestamp getEffective_date();

    public void setEffective_date(Timestamp effective_date);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public String getEquipment_assign_to();

    public void setEquipment_assign_to(String equipment_assign_to);

    public Integer getId();

    public void setId(Integer id);

    public String getLocation();

    public void setLocation(String location);

    public Integer getMaintenance_count();

    public void setMaintenance_count(Integer maintenance_count);

    public Double getMaintenance_duration();

    public void setMaintenance_duration(Double maintenance_duration);

    public String getMaintenance_ids();

    public void setMaintenance_ids(String maintenance_ids);

    public Integer getMaintenance_open_count();

    public void setMaintenance_open_count(Integer maintenance_open_count);

    public Integer getMaintenance_team_id();

    public void setMaintenance_team_id(Integer maintenance_team_id);

    public String getMaintenance_team_id_text();

    public void setMaintenance_team_id_text(String maintenance_team_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getModel();

    public void setModel(String model);

    public String getName();

    public void setName(String name);

    public Timestamp getNext_action_date();

    public void setNext_action_date(Timestamp next_action_date);

    public String getNote();

    public void setNote(String note);

    public Integer getOwner_user_id();

    public void setOwner_user_id(Integer owner_user_id);

    public String getOwner_user_id_text();

    public void setOwner_user_id_text(String owner_user_id_text);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getPartner_ref();

    public void setPartner_ref(String partner_ref);

    public Integer getPeriod();

    public void setPeriod(Integer period);

    public Timestamp getScrap_date();

    public void setScrap_date(Timestamp scrap_date);

    public String getSerial_no();

    public void setSerial_no(String serial_no);

    public Integer getTechnician_user_id();

    public void setTechnician_user_id(Integer technician_user_id);

    public String getTechnician_user_id_text();

    public void setTechnician_user_id_text(String technician_user_id_text);

    public Timestamp getWarranty_date();

    public void setWarranty_date(Timestamp warranty_date);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
