package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;

/**
 * 实体 [供应商价格表] 存储对象
 */
public interface Product_supplierinfoRepository extends Repository<Product_supplierinfo> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_supplierinfo> searchDefault(Product_supplierinfoSearchContext context);

    Product_supplierinfo convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo domain , Product_supplierinfo po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo convert2Domain( Product_supplierinfo po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo domain) ;

}
