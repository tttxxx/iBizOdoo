package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_reportClientModel;

/**
 * 实体[发票统计] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_reportServiceImpl implements IAccount_invoice_reportService {

    @Autowired
    account_invoice_reportOdooClient account_invoice_reportOdooClient;


    @Override
    public Account_invoice_report get(Integer id) {
        account_invoice_reportClientModel clientModel = new account_invoice_reportClientModel();
        clientModel.setId(id);
		account_invoice_reportOdooClient.get(clientModel);
        Account_invoice_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_invoice_report et) {
        account_invoice_reportClientModel clientModel = convert2Model(et,null);
		account_invoice_reportOdooClient.create(clientModel);
        Account_invoice_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_invoice_reportClientModel clientModel = new account_invoice_reportClientModel();
        clientModel.setId(id);
		account_invoice_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_invoice_report et) {
        account_invoice_reportClientModel clientModel = convert2Model(et,null);
		account_invoice_reportOdooClient.update(clientModel);
        Account_invoice_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_report> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context) {
        List<Account_invoice_report> list = new ArrayList<Account_invoice_report>();
        Page<account_invoice_reportClientModel> clientModelList = account_invoice_reportOdooClient.search(context);
        for(account_invoice_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_reportClientModel convert2Model(Account_invoice_report domain , account_invoice_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("numberdirtyflag"))
                model.setNumber(domain.getNumber());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("user_currency_price_totaldirtyflag"))
                model.setUser_currency_price_total(domain.getUserCurrencyPriceTotal());
            if((Boolean) domain.getExtensionparams().get("residualdirtyflag"))
                model.setResidual(domain.getResidual());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("date_duedirtyflag"))
                model.setDate_due(domain.getDateDue());
            if((Boolean) domain.getExtensionparams().get("nbrdirtyflag"))
                model.setNbr(domain.getNbr());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("uom_namedirtyflag"))
                model.setUom_name(domain.getUomName());
            if((Boolean) domain.getExtensionparams().get("price_averagedirtyflag"))
                model.setPrice_average(domain.getPriceAverage());
            if((Boolean) domain.getExtensionparams().get("currency_ratedirtyflag"))
                model.setCurrency_rate(domain.getCurrencyRate());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("user_currency_price_averagedirtyflag"))
                model.setUser_currency_price_average(domain.getUserCurrencyPriceAverage());
            if((Boolean) domain.getExtensionparams().get("user_currency_residualdirtyflag"))
                model.setUser_currency_residual(domain.getUserCurrencyResidual());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("account_analytic_id_textdirtyflag"))
                model.setAccount_analytic_id_text(domain.getAccountAnalyticIdText());
            if((Boolean) domain.getExtensionparams().get("categ_id_textdirtyflag"))
                model.setCateg_id_text(domain.getCategIdText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_id_textdirtyflag"))
                model.setFiscal_position_id_text(domain.getFiscalPositionIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_id_textdirtyflag"))
                model.setInvoice_id_text(domain.getInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("payment_term_id_textdirtyflag"))
                model.setPayment_term_id_text(domain.getPaymentTermIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("account_line_id_textdirtyflag"))
                model.setAccount_line_id_text(domain.getAccountLineIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_id_textdirtyflag"))
                model.setCommercial_partner_id_text(domain.getCommercialPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("partner_bank_iddirtyflag"))
                model.setPartner_bank_id(domain.getPartnerBankId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("categ_iddirtyflag"))
                model.setCateg_id(domain.getCategId());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_iddirtyflag"))
                model.setCommercial_partner_id(domain.getCommercialPartnerId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("payment_term_iddirtyflag"))
                model.setPayment_term_id(domain.getPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("account_analytic_iddirtyflag"))
                model.setAccount_analytic_id(domain.getAccountAnalyticId());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_iddirtyflag"))
                model.setFiscal_position_id(domain.getFiscalPositionId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("account_line_iddirtyflag"))
                model.setAccount_line_id(domain.getAccountLineId());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("invoice_iddirtyflag"))
                model.setInvoice_id(domain.getInvoiceId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_report convert2Domain( account_invoice_reportClientModel model ,Account_invoice_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_report();
        }

        if(model.getNumberDirtyFlag())
            domain.setNumber(model.getNumber());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getUser_currency_price_totalDirtyFlag())
            domain.setUserCurrencyPriceTotal(model.getUser_currency_price_total());
        if(model.getResidualDirtyFlag())
            domain.setResidual(model.getResidual());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDate_dueDirtyFlag())
            domain.setDateDue(model.getDate_due());
        if(model.getNbrDirtyFlag())
            domain.setNbr(model.getNbr());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getUom_nameDirtyFlag())
            domain.setUomName(model.getUom_name());
        if(model.getPrice_averageDirtyFlag())
            domain.setPriceAverage(model.getPrice_average());
        if(model.getCurrency_rateDirtyFlag())
            domain.setCurrencyRate(model.getCurrency_rate());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.getUser_currency_price_averageDirtyFlag())
            domain.setUserCurrencyPriceAverage(model.getUser_currency_price_average());
        if(model.getUser_currency_residualDirtyFlag())
            domain.setUserCurrencyResidual(model.getUser_currency_residual());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getAccount_analytic_id_textDirtyFlag())
            domain.setAccountAnalyticIdText(model.getAccount_analytic_id_text());
        if(model.getCateg_id_textDirtyFlag())
            domain.setCategIdText(model.getCateg_id_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getFiscal_position_id_textDirtyFlag())
            domain.setFiscalPositionIdText(model.getFiscal_position_id_text());
        if(model.getInvoice_id_textDirtyFlag())
            domain.setInvoiceIdText(model.getInvoice_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getPayment_term_id_textDirtyFlag())
            domain.setPaymentTermIdText(model.getPayment_term_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getAccount_line_id_textDirtyFlag())
            domain.setAccountLineIdText(model.getAccount_line_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCommercial_partner_id_textDirtyFlag())
            domain.setCommercialPartnerIdText(model.getCommercial_partner_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getPartner_bank_idDirtyFlag())
            domain.setPartnerBankId(model.getPartner_bank_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCateg_idDirtyFlag())
            domain.setCategId(model.getCateg_id());
        if(model.getCommercial_partner_idDirtyFlag())
            domain.setCommercialPartnerId(model.getCommercial_partner_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getPayment_term_idDirtyFlag())
            domain.setPaymentTermId(model.getPayment_term_id());
        if(model.getAccount_analytic_idDirtyFlag())
            domain.setAccountAnalyticId(model.getAccount_analytic_id());
        if(model.getFiscal_position_idDirtyFlag())
            domain.setFiscalPositionId(model.getFiscal_position_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getAccount_line_idDirtyFlag())
            domain.setAccountLineId(model.getAccount_line_id());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getInvoice_idDirtyFlag())
            domain.setInvoiceId(model.getInvoice_id());
        return domain ;
    }

}

    



