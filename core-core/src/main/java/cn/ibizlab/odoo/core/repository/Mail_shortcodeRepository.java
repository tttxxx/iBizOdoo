package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;

/**
 * 实体 [自动回复] 存储对象
 */
public interface Mail_shortcodeRepository extends Repository<Mail_shortcode> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context);

    Mail_shortcode convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode domain , Mail_shortcode po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode convert2Domain( Mail_shortcode po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode domain) ;

}
