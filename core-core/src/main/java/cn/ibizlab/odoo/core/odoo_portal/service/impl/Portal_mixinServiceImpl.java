package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_portal.client.portal_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_portal.clientmodel.portal_mixinClientModel;

/**
 * 实体[门户Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_mixinServiceImpl implements IPortal_mixinService {

    @Autowired
    portal_mixinOdooClient portal_mixinOdooClient;


    @Override
    public Portal_mixin get(Integer id) {
        portal_mixinClientModel clientModel = new portal_mixinClientModel();
        clientModel.setId(id);
		portal_mixinOdooClient.get(clientModel);
        Portal_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Portal_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        portal_mixinClientModel clientModel = new portal_mixinClientModel();
        clientModel.setId(id);
		portal_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Portal_mixin et) {
        portal_mixinClientModel clientModel = convert2Model(et,null);
		portal_mixinOdooClient.update(clientModel);
        Portal_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Portal_mixin> list){
    }

    @Override
    public boolean create(Portal_mixin et) {
        portal_mixinClientModel clientModel = convert2Model(et,null);
		portal_mixinOdooClient.create(clientModel);
        Portal_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_mixin> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context) {
        List<Portal_mixin> list = new ArrayList<Portal_mixin>();
        Page<portal_mixinClientModel> clientModelList = portal_mixinOdooClient.search(context);
        for(portal_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Portal_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public portal_mixinClientModel convert2Model(Portal_mixin domain , portal_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new portal_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Portal_mixin convert2Domain( portal_mixinClientModel model ,Portal_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Portal_mixin();
        }

        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        return domain ;
    }

}

    



