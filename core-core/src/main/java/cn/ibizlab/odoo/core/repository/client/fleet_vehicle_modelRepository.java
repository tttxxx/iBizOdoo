package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_model;

/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
public interface fleet_vehicle_modelRepository{


    public fleet_vehicle_model createPO() ;
        public void remove(String id);

        public void create(fleet_vehicle_model fleet_vehicle_model);

        public void update(fleet_vehicle_model fleet_vehicle_model);

        public void get(String id);

        public void updateBatch(fleet_vehicle_model fleet_vehicle_model);

        public List<fleet_vehicle_model> search();

        public void createBatch(fleet_vehicle_model fleet_vehicle_model);

        public void removeBatch(String id);


}
