package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_optionSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_optionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_optionOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_order_optionClientModel;

/**
 * 实体[销售选项] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_optionServiceImpl implements ISale_order_optionService {

    @Autowired
    sale_order_optionOdooClient sale_order_optionOdooClient;


    @Override
    public boolean remove(Integer id) {
        sale_order_optionClientModel clientModel = new sale_order_optionClientModel();
        clientModel.setId(id);
		sale_order_optionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Sale_order_option get(Integer id) {
        sale_order_optionClientModel clientModel = new sale_order_optionClientModel();
        clientModel.setId(id);
		sale_order_optionOdooClient.get(clientModel);
        Sale_order_option et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_order_option();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Sale_order_option et) {
        sale_order_optionClientModel clientModel = convert2Model(et,null);
		sale_order_optionOdooClient.create(clientModel);
        Sale_order_option rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_option> list){
    }

    @Override
    public boolean update(Sale_order_option et) {
        sale_order_optionClientModel clientModel = convert2Model(et,null);
		sale_order_optionOdooClient.update(clientModel);
        Sale_order_option rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_order_option> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_option> searchDefault(Sale_order_optionSearchContext context) {
        List<Sale_order_option> list = new ArrayList<Sale_order_option>();
        Page<sale_order_optionClientModel> clientModelList = sale_order_optionOdooClient.search(context);
        for(sale_order_optionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_order_option>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_order_optionClientModel convert2Model(Sale_order_option domain , sale_order_optionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_order_optionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("quantitydirtyflag"))
                model.setQuantity(domain.getQuantity());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("discountdirtyflag"))
                model.setDiscount(domain.getDiscount());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("order_id_textdirtyflag"))
                model.setOrder_id_text(domain.getOrderIdText());
            if((Boolean) domain.getExtensionparams().get("line_id_textdirtyflag"))
                model.setLine_id_text(domain.getLineIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("uom_id_textdirtyflag"))
                model.setUom_id_text(domain.getUomIdText());
            if((Boolean) domain.getExtensionparams().get("uom_iddirtyflag"))
                model.setUom_id(domain.getUomId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("line_iddirtyflag"))
                model.setLine_id(domain.getLineId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_order_option convert2Domain( sale_order_optionClientModel model ,Sale_order_option domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_order_option();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getQuantityDirtyFlag())
            domain.setQuantity(model.getQuantity());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDiscountDirtyFlag())
            domain.setDiscount(model.getDiscount());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getOrder_id_textDirtyFlag())
            domain.setOrderIdText(model.getOrder_id_text());
        if(model.getLine_id_textDirtyFlag())
            domain.setLineIdText(model.getLine_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getUom_id_textDirtyFlag())
            domain.setUomIdText(model.getUom_id_text());
        if(model.getUom_idDirtyFlag())
            domain.setUomId(model.getUom_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getLine_idDirtyFlag())
            domain.setLineId(model.getLine_id());
        return domain ;
    }

}

    



