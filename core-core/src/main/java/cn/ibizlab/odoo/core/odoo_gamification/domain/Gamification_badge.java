package cn.ibizlab.odoo.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [游戏化徽章] 对象
 */
@Data
public class Gamification_badge extends EntityClient implements Serializable {

    /**
     * 允许授予
     */
    @DEField(name = "rule_auth")
    @JSONField(name = "rule_auth")
    @JsonProperty("rule_auth")
    private String ruleAuth;

    /**
     * 限制数量
     */
    @DEField(name = "rule_max_number")
    @JSONField(name = "rule_max_number")
    @JsonProperty("rule_max_number")
    private Integer ruleMaxNumber;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 月度限额发放
     */
    @DEField(name = "rule_max")
    @JSONField(name = "rule_max")
    @JsonProperty("rule_max")
    private String ruleMax;

    /**
     * 授权用户
     */
    @JSONField(name = "rule_auth_user_ids")
    @JsonProperty("rule_auth_user_ids")
    private String ruleAuthUserIds;

    /**
     * 需要徽章
     */
    @JSONField(name = "rule_auth_badge_ids")
    @JsonProperty("rule_auth_badge_ids")
    private String ruleAuthBadgeIds;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 其他的允许发送
     */
    @JSONField(name = "remaining_sending")
    @JsonProperty("remaining_sending")
    private Integer remainingSending;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 我的总计
     */
    @JSONField(name = "stat_my")
    @JsonProperty("stat_my")
    private Integer statMy;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 唯一的所有者
     */
    @JSONField(name = "unique_owner_ids")
    @JsonProperty("unique_owner_ids")
    private String uniqueOwnerIds;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 论坛徽章等级
     */
    @JSONField(name = "level")
    @JsonProperty("level")
    private String level;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 所有者
     */
    @JSONField(name = "owner_ids")
    @JsonProperty("owner_ids")
    private String ownerIds;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 奖励按照
     */
    @JSONField(name = "goal_definition_ids")
    @JsonProperty("goal_definition_ids")
    private String goalDefinitionIds;

    /**
     * 月度发放总数
     */
    @JSONField(name = "stat_my_monthly_sending")
    @JsonProperty("stat_my_monthly_sending")
    private Integer statMyMonthlySending;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 总计
     */
    @JSONField(name = "stat_count")
    @JsonProperty("stat_count")
    private Integer statCount;

    /**
     * 徽章
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 每月总数
     */
    @JSONField(name = "stat_this_month")
    @JsonProperty("stat_this_month")
    private Integer statThisMonth;

    /**
     * 需要激活
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 用户数量
     */
    @JSONField(name = "stat_count_distinct")
    @JsonProperty("stat_count_distinct")
    private Integer statCountDistinct;

    /**
     * 我的月份总计
     */
    @JSONField(name = "stat_my_this_month")
    @JsonProperty("stat_my_this_month")
    private Integer statMyThisMonth;

    /**
     * 挑战的奖励
     */
    @JSONField(name = "challenge_ids")
    @JsonProperty("challenge_ids")
    private String challengeIds;

    /**
     * 授予的员工人数
     */
    @JSONField(name = "granted_employees_count")
    @JsonProperty("granted_employees_count")
    private Integer grantedEmployeesCount;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [允许授予]
     */
    public void setRuleAuth(String ruleAuth){
        this.ruleAuth = ruleAuth ;
        this.modify("rule_auth",ruleAuth);
    }
    /**
     * 设置 [限制数量]
     */
    public void setRuleMaxNumber(Integer ruleMaxNumber){
        this.ruleMaxNumber = ruleMaxNumber ;
        this.modify("rule_max_number",ruleMaxNumber);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [月度限额发放]
     */
    public void setRuleMax(String ruleMax){
        this.ruleMax = ruleMax ;
        this.modify("rule_max",ruleMax);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [论坛徽章等级]
     */
    public void setLevel(String level){
        this.level = level ;
        this.modify("level",level);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [徽章]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

}


