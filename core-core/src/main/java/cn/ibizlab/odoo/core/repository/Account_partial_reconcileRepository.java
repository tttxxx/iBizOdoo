package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;

/**
 * 实体 [部分核销] 存储对象
 */
public interface Account_partial_reconcileRepository extends Repository<Account_partial_reconcile> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context);

    Account_partial_reconcile convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile domain , Account_partial_reconcile po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile convert2Domain( Account_partial_reconcile po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile domain) ;

}
