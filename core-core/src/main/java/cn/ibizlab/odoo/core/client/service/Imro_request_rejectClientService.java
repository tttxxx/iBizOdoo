package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_request_reject;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_request_reject] 服务对象接口
 */
public interface Imro_request_rejectClientService{

    public Imro_request_reject createModel() ;

    public Page<Imro_request_reject> search(SearchContext context);

    public void update(Imro_request_reject mro_request_reject);

    public void updateBatch(List<Imro_request_reject> mro_request_rejects);

    public void create(Imro_request_reject mro_request_reject);

    public void remove(Imro_request_reject mro_request_reject);

    public void removeBatch(List<Imro_request_reject> mro_request_rejects);

    public void createBatch(List<Imro_request_reject> mro_request_rejects);

    public void get(Imro_request_reject mro_request_reject);

    public Page<Imro_request_reject> select(SearchContext context);

}
