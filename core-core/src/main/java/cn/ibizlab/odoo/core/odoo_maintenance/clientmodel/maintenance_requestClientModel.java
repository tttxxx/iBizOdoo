package cn.ibizlab.odoo.core.odoo_maintenance.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[maintenance_request] 对象
 */
public class maintenance_requestClientModel implements Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 归档
     */
    public String archive;

    @JsonIgnore
    public boolean archiveDirtyFlag;
    
    /**
     * 类别
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 类别
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 关闭日期
     */
    public Timestamp close_date;

    @JsonIgnore
    public boolean close_dateDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 持续时间
     */
    public Double duration;

    @JsonIgnore
    public boolean durationDirtyFlag;
    
    /**
     * 员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * 设备
     */
    public Integer equipment_id;

    @JsonIgnore
    public boolean equipment_idDirtyFlag;
    
    /**
     * 设备
     */
    public String equipment_id_text;

    @JsonIgnore
    public boolean equipment_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 看板状态
     */
    public String kanban_state;

    @JsonIgnore
    public boolean kanban_stateDirtyFlag;
    
    /**
     * 团队
     */
    public Integer maintenance_team_id;

    @JsonIgnore
    public boolean maintenance_team_idDirtyFlag;
    
    /**
     * 团队
     */
    public String maintenance_team_id_text;

    @JsonIgnore
    public boolean maintenance_team_id_textDirtyFlag;
    
    /**
     * 保养类型
     */
    public String maintenance_type;

    @JsonIgnore
    public boolean maintenance_typeDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 主题
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer owner_user_id;

    @JsonIgnore
    public boolean owner_user_idDirtyFlag;
    
    /**
     * 创建人
     */
    public String owner_user_id_text;

    @JsonIgnore
    public boolean owner_user_id_textDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 请求日期
     */
    public Timestamp request_date;

    @JsonIgnore
    public boolean request_dateDirtyFlag;
    
    /**
     * 计划日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp schedule_date;

    @JsonIgnore
    public boolean schedule_dateDirtyFlag;
    
    /**
     * 阶段
     */
    public Integer stage_id;

    @JsonIgnore
    public boolean stage_idDirtyFlag;
    
    /**
     * 阶段
     */
    public String stage_id_text;

    @JsonIgnore
    public boolean stage_id_textDirtyFlag;
    
    /**
     * 技术员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 技术员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [归档]
     */
    @JsonProperty("archive")
    public String getArchive(){
        return this.archive ;
    }

    /**
     * 设置 [归档]
     */
    @JsonProperty("archive")
    public void setArchive(String  archive){
        this.archive = archive ;
        this.archiveDirtyFlag = true ;
    }

     /**
     * 获取 [归档]脏标记
     */
    @JsonIgnore
    public boolean getArchiveDirtyFlag(){
        return this.archiveDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [关闭日期]
     */
    @JsonProperty("close_date")
    public Timestamp getClose_date(){
        return this.close_date ;
    }

    /**
     * 设置 [关闭日期]
     */
    @JsonProperty("close_date")
    public void setClose_date(Timestamp  close_date){
        this.close_date = close_date ;
        this.close_dateDirtyFlag = true ;
    }

     /**
     * 获取 [关闭日期]脏标记
     */
    @JsonIgnore
    public boolean getClose_dateDirtyFlag(){
        return this.close_dateDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [持续时间]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [持续时间]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [设备]
     */
    @JsonProperty("equipment_id")
    public Integer getEquipment_id(){
        return this.equipment_id ;
    }

    /**
     * 设置 [设备]
     */
    @JsonProperty("equipment_id")
    public void setEquipment_id(Integer  equipment_id){
        this.equipment_id = equipment_id ;
        this.equipment_idDirtyFlag = true ;
    }

     /**
     * 获取 [设备]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_idDirtyFlag(){
        return this.equipment_idDirtyFlag ;
    }   

    /**
     * 获取 [设备]
     */
    @JsonProperty("equipment_id_text")
    public String getEquipment_id_text(){
        return this.equipment_id_text ;
    }

    /**
     * 设置 [设备]
     */
    @JsonProperty("equipment_id_text")
    public void setEquipment_id_text(String  equipment_id_text){
        this.equipment_id_text = equipment_id_text ;
        this.equipment_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [设备]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_id_textDirtyFlag(){
        return this.equipment_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [看板状态]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return this.kanban_state ;
    }

    /**
     * 设置 [看板状态]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

     /**
     * 获取 [看板状态]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return this.kanban_stateDirtyFlag ;
    }   

    /**
     * 获取 [团队]
     */
    @JsonProperty("maintenance_team_id")
    public Integer getMaintenance_team_id(){
        return this.maintenance_team_id ;
    }

    /**
     * 设置 [团队]
     */
    @JsonProperty("maintenance_team_id")
    public void setMaintenance_team_id(Integer  maintenance_team_id){
        this.maintenance_team_id = maintenance_team_id ;
        this.maintenance_team_idDirtyFlag = true ;
    }

     /**
     * 获取 [团队]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_idDirtyFlag(){
        return this.maintenance_team_idDirtyFlag ;
    }   

    /**
     * 获取 [团队]
     */
    @JsonProperty("maintenance_team_id_text")
    public String getMaintenance_team_id_text(){
        return this.maintenance_team_id_text ;
    }

    /**
     * 设置 [团队]
     */
    @JsonProperty("maintenance_team_id_text")
    public void setMaintenance_team_id_text(String  maintenance_team_id_text){
        this.maintenance_team_id_text = maintenance_team_id_text ;
        this.maintenance_team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [团队]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_id_textDirtyFlag(){
        return this.maintenance_team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [保养类型]
     */
    @JsonProperty("maintenance_type")
    public String getMaintenance_type(){
        return this.maintenance_type ;
    }

    /**
     * 设置 [保养类型]
     */
    @JsonProperty("maintenance_type")
    public void setMaintenance_type(String  maintenance_type){
        this.maintenance_type = maintenance_type ;
        this.maintenance_typeDirtyFlag = true ;
    }

     /**
     * 获取 [保养类型]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_typeDirtyFlag(){
        return this.maintenance_typeDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [主题]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("owner_user_id")
    public Integer getOwner_user_id(){
        return this.owner_user_id ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("owner_user_id")
    public void setOwner_user_id(Integer  owner_user_id){
        this.owner_user_id = owner_user_id ;
        this.owner_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_idDirtyFlag(){
        return this.owner_user_idDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("owner_user_id_text")
    public String getOwner_user_id_text(){
        return this.owner_user_id_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("owner_user_id_text")
    public void setOwner_user_id_text(String  owner_user_id_text){
        this.owner_user_id_text = owner_user_id_text ;
        this.owner_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_id_textDirtyFlag(){
        return this.owner_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [请求日期]
     */
    @JsonProperty("request_date")
    public Timestamp getRequest_date(){
        return this.request_date ;
    }

    /**
     * 设置 [请求日期]
     */
    @JsonProperty("request_date")
    public void setRequest_date(Timestamp  request_date){
        this.request_date = request_date ;
        this.request_dateDirtyFlag = true ;
    }

     /**
     * 获取 [请求日期]脏标记
     */
    @JsonIgnore
    public boolean getRequest_dateDirtyFlag(){
        return this.request_dateDirtyFlag ;
    }   

    /**
     * 获取 [计划日期]
     */
    @JsonProperty("schedule_date")
    public Timestamp getSchedule_date(){
        return this.schedule_date ;
    }

    /**
     * 设置 [计划日期]
     */
    @JsonProperty("schedule_date")
    public void setSchedule_date(Timestamp  schedule_date){
        this.schedule_date = schedule_date ;
        this.schedule_dateDirtyFlag = true ;
    }

     /**
     * 获取 [计划日期]脏标记
     */
    @JsonIgnore
    public boolean getSchedule_dateDirtyFlag(){
        return this.schedule_dateDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }   

    /**
     * 获取 [技术员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [技术员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [技术员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [技术员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [技术员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [技术员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(map.get("archive") instanceof Boolean){
			this.setArchive(((Boolean)map.get("archive"))? "true" : "false");
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 0){
				this.setCategory_id((Integer)objs[0]);
			}
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 1){
				this.setCategory_id_text((String)objs[1]);
			}
		}
		if(!(map.get("close_date") instanceof Boolean)&& map.get("close_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("close_date"));
   			this.setClose_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 0){
				this.setDepartment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 1){
				this.setDepartment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("duration") instanceof Boolean)&& map.get("duration")!=null){
			this.setDuration((Double)map.get("duration"));
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 0){
				this.setEmployee_id((Integer)objs[0]);
			}
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 1){
				this.setEmployee_id_text((String)objs[1]);
			}
		}
		if(!(map.get("equipment_id") instanceof Boolean)&& map.get("equipment_id")!=null){
			Object[] objs = (Object[])map.get("equipment_id");
			if(objs.length > 0){
				this.setEquipment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("equipment_id") instanceof Boolean)&& map.get("equipment_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("equipment_id");
			if(objs.length > 1){
				this.setEquipment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("kanban_state") instanceof Boolean)&& map.get("kanban_state")!=null){
			this.setKanban_state((String)map.get("kanban_state"));
		}
		if(!(map.get("maintenance_team_id") instanceof Boolean)&& map.get("maintenance_team_id")!=null){
			Object[] objs = (Object[])map.get("maintenance_team_id");
			if(objs.length > 0){
				this.setMaintenance_team_id((Integer)objs[0]);
			}
		}
		if(!(map.get("maintenance_team_id") instanceof Boolean)&& map.get("maintenance_team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("maintenance_team_id");
			if(objs.length > 1){
				this.setMaintenance_team_id_text((String)objs[1]);
			}
		}
		if(!(map.get("maintenance_type") instanceof Boolean)&& map.get("maintenance_type")!=null){
			this.setMaintenance_type((String)map.get("maintenance_type"));
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("owner_user_id") instanceof Boolean)&& map.get("owner_user_id")!=null){
			Object[] objs = (Object[])map.get("owner_user_id");
			if(objs.length > 0){
				this.setOwner_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("owner_user_id") instanceof Boolean)&& map.get("owner_user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("owner_user_id");
			if(objs.length > 1){
				this.setOwner_user_id_text((String)objs[1]);
			}
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((String)map.get("priority"));
		}
		if(!(map.get("request_date") instanceof Boolean)&& map.get("request_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("request_date"));
   			this.setRequest_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("schedule_date") instanceof Boolean)&& map.get("schedule_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("schedule_date"));
   			this.setSchedule_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 0){
				this.setStage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 1){
				this.setStage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getArchive()!=null&&this.getArchiveDirtyFlag()){
			map.put("archive",Boolean.parseBoolean(this.getArchive()));		
		}		if(this.getCategory_id()!=null&&this.getCategory_idDirtyFlag()){
			map.put("category_id",this.getCategory_id());
		}else if(this.getCategory_idDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCategory_id_text()!=null&&this.getCategory_id_textDirtyFlag()){
			//忽略文本外键category_id_text
		}else if(this.getCategory_id_textDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getClose_date()!=null&&this.getClose_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getClose_date());
			map.put("close_date",datetimeStr);
		}else if(this.getClose_dateDirtyFlag()){
			map.put("close_date",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDepartment_id()!=null&&this.getDepartment_idDirtyFlag()){
			map.put("department_id",this.getDepartment_id());
		}else if(this.getDepartment_idDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDepartment_id_text()!=null&&this.getDepartment_id_textDirtyFlag()){
			//忽略文本外键department_id_text
		}else if(this.getDepartment_id_textDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDuration()!=null&&this.getDurationDirtyFlag()){
			map.put("duration",this.getDuration());
		}else if(this.getDurationDirtyFlag()){
			map.put("duration",false);
		}
		if(this.getEmployee_id()!=null&&this.getEmployee_idDirtyFlag()){
			map.put("employee_id",this.getEmployee_id());
		}else if(this.getEmployee_idDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getEmployee_id_text()!=null&&this.getEmployee_id_textDirtyFlag()){
			//忽略文本外键employee_id_text
		}else if(this.getEmployee_id_textDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getEquipment_id()!=null&&this.getEquipment_idDirtyFlag()){
			map.put("equipment_id",this.getEquipment_id());
		}else if(this.getEquipment_idDirtyFlag()){
			map.put("equipment_id",false);
		}
		if(this.getEquipment_id_text()!=null&&this.getEquipment_id_textDirtyFlag()){
			//忽略文本外键equipment_id_text
		}else if(this.getEquipment_id_textDirtyFlag()){
			map.put("equipment_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getKanban_state()!=null&&this.getKanban_stateDirtyFlag()){
			map.put("kanban_state",this.getKanban_state());
		}else if(this.getKanban_stateDirtyFlag()){
			map.put("kanban_state",false);
		}
		if(this.getMaintenance_team_id()!=null&&this.getMaintenance_team_idDirtyFlag()){
			map.put("maintenance_team_id",this.getMaintenance_team_id());
		}else if(this.getMaintenance_team_idDirtyFlag()){
			map.put("maintenance_team_id",false);
		}
		if(this.getMaintenance_team_id_text()!=null&&this.getMaintenance_team_id_textDirtyFlag()){
			//忽略文本外键maintenance_team_id_text
		}else if(this.getMaintenance_team_id_textDirtyFlag()){
			map.put("maintenance_team_id",false);
		}
		if(this.getMaintenance_type()!=null&&this.getMaintenance_typeDirtyFlag()){
			map.put("maintenance_type",this.getMaintenance_type());
		}else if(this.getMaintenance_typeDirtyFlag()){
			map.put("maintenance_type",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOwner_user_id()!=null&&this.getOwner_user_idDirtyFlag()){
			map.put("owner_user_id",this.getOwner_user_id());
		}else if(this.getOwner_user_idDirtyFlag()){
			map.put("owner_user_id",false);
		}
		if(this.getOwner_user_id_text()!=null&&this.getOwner_user_id_textDirtyFlag()){
			//忽略文本外键owner_user_id_text
		}else if(this.getOwner_user_id_textDirtyFlag()){
			map.put("owner_user_id",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getRequest_date()!=null&&this.getRequest_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getRequest_date());
			map.put("request_date",datetimeStr);
		}else if(this.getRequest_dateDirtyFlag()){
			map.put("request_date",false);
		}
		if(this.getSchedule_date()!=null&&this.getSchedule_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getSchedule_date());
			map.put("schedule_date",datetimeStr);
		}else if(this.getSchedule_dateDirtyFlag()){
			map.put("schedule_date",false);
		}
		if(this.getStage_id()!=null&&this.getStage_idDirtyFlag()){
			map.put("stage_id",this.getStage_id());
		}else if(this.getStage_idDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getStage_id_text()!=null&&this.getStage_id_textDirtyFlag()){
			//忽略文本外键stage_id_text
		}else if(this.getStage_id_textDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
