package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_bankService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_partner_bankOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_partner_bankClientModel;

/**
 * 实体[银行账户] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_bankServiceImpl implements IRes_partner_bankService {

    @Autowired
    res_partner_bankOdooClient res_partner_bankOdooClient;


    @Override
    public boolean update(Res_partner_bank et) {
        res_partner_bankClientModel clientModel = convert2Model(et,null);
		res_partner_bankOdooClient.update(clientModel);
        Res_partner_bank rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_partner_bank> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_partner_bankClientModel clientModel = new res_partner_bankClientModel();
        clientModel.setId(id);
		res_partner_bankOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_partner_bank et) {
        res_partner_bankClientModel clientModel = convert2Model(et,null);
		res_partner_bankOdooClient.create(clientModel);
        Res_partner_bank rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_bank> list){
    }

    @Override
    public Res_partner_bank get(Integer id) {
        res_partner_bankClientModel clientModel = new res_partner_bankClientModel();
        clientModel.setId(id);
		res_partner_bankOdooClient.get(clientModel);
        Res_partner_bank et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_partner_bank();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context) {
        List<Res_partner_bank> list = new ArrayList<Res_partner_bank>();
        Page<res_partner_bankClientModel> clientModelList = res_partner_bankOdooClient.search(context);
        for(res_partner_bankClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_partner_bank>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_partner_bankClientModel convert2Model(Res_partner_bank domain , res_partner_bankClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_partner_bankClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sanitized_acc_numberdirtyflag"))
                model.setSanitized_acc_number(domain.getSanitizedAccNumber());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("acc_numberdirtyflag"))
                model.setAcc_number(domain.getAccNumber());
            if((Boolean) domain.getExtensionparams().get("acc_typedirtyflag"))
                model.setAcc_type(domain.getAccType());
            if((Boolean) domain.getExtensionparams().get("qr_code_validdirtyflag"))
                model.setQr_code_valid(domain.getQrCodeValid());
            if((Boolean) domain.getExtensionparams().get("acc_holder_namedirtyflag"))
                model.setAcc_holder_name(domain.getAccHolderName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("bank_namedirtyflag"))
                model.setBank_name(domain.getBankName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("bank_bicdirtyflag"))
                model.setBank_bic(domain.getBankBic());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("bank_iddirtyflag"))
                model.setBank_id(domain.getBankId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_partner_bank convert2Domain( res_partner_bankClientModel model ,Res_partner_bank domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_partner_bank();
        }

        if(model.getSanitized_acc_numberDirtyFlag())
            domain.setSanitizedAccNumber(model.getSanitized_acc_number());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAcc_numberDirtyFlag())
            domain.setAccNumber(model.getAcc_number());
        if(model.getAcc_typeDirtyFlag())
            domain.setAccType(model.getAcc_type());
        if(model.getQr_code_validDirtyFlag())
            domain.setQrCodeValid(model.getQr_code_valid());
        if(model.getAcc_holder_nameDirtyFlag())
            domain.setAccHolderName(model.getAcc_holder_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getBank_nameDirtyFlag())
            domain.setBankName(model.getBank_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getBank_bicDirtyFlag())
            domain.setBankBic(model.getBank_bic());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getBank_idDirtyFlag())
            domain.setBankId(model.getBank_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        return domain ;
    }

}

    



