package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [价格表明细] 对象
 */
@Data
public class Product_pricelist_item extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 固定价格
     */
    @DEField(name = "fixed_price")
    @JSONField(name = "fixed_price")
    @JsonProperty("fixed_price")
    private Double fixedPrice;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 价格
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private String price;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 基于
     */
    @JSONField(name = "base")
    @JsonProperty("base")
    private String base;

    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 百分比价格
     */
    @DEField(name = "percent_price")
    @JSONField(name = "percent_price")
    @JsonProperty("percent_price")
    private Double percentPrice;

    /**
     * 价格舍入
     */
    @DEField(name = "price_round")
    @JSONField(name = "price_round")
    @JsonProperty("price_round")
    private Double priceRound;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 计算价格
     */
    @DEField(name = "compute_price")
    @JSONField(name = "compute_price")
    @JsonProperty("compute_price")
    private String computePrice;

    /**
     * 最小价格毛利
     */
    @DEField(name = "price_min_margin")
    @JSONField(name = "price_min_margin")
    @JsonProperty("price_min_margin")
    private Double priceMinMargin;

    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 最大价格毛利
     */
    @DEField(name = "price_max_margin")
    @JSONField(name = "price_max_margin")
    @JsonProperty("price_max_margin")
    private Double priceMaxMargin;

    /**
     * 应用于
     */
    @DEField(name = "applied_on")
    @JSONField(name = "applied_on")
    @JsonProperty("applied_on")
    private String appliedOn;

    /**
     * 最小数量
     */
    @DEField(name = "min_quantity")
    @JSONField(name = "min_quantity")
    @JsonProperty("min_quantity")
    private Integer minQuantity;

    /**
     * 价格附加费用
     */
    @DEField(name = "price_surcharge")
    @JSONField(name = "price_surcharge")
    @JsonProperty("price_surcharge")
    private Double priceSurcharge;

    /**
     * 价格折扣
     */
    @DEField(name = "price_discount")
    @JSONField(name = "price_discount")
    @JsonProperty("price_discount")
    private Double priceDiscount;

    /**
     * 价格表
     */
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 产品种类
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;

    /**
     * 其他价格表
     */
    @JSONField(name = "base_pricelist_id_text")
    @JsonProperty("base_pricelist_id_text")
    private String basePricelistIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 产品模板
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    private String productTmplIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 其他价格表
     */
    @DEField(name = "base_pricelist_id")
    @JSONField(name = "base_pricelist_id")
    @JsonProperty("base_pricelist_id")
    private Integer basePricelistId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 产品种类
     */
    @DEField(name = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 产品模板
     */
    @DEField(name = "product_tmpl_id")
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;


    /**
     * 
     */
    @JSONField(name = "odoocateg")
    @JsonProperty("odoocateg")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_category odooCateg;

    /**
     * 
     */
    @JSONField(name = "odoobasepricelist")
    @JsonProperty("odoobasepricelist")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist odooBasePricelist;

    /**
     * 
     */
    @JSONField(name = "odoopricelist")
    @JsonProperty("odoopricelist")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odooproducttmpl")
    @JsonProperty("odooproducttmpl")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_template odooProductTmpl;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [固定价格]
     */
    public void setFixedPrice(Double fixedPrice){
        this.fixedPrice = fixedPrice ;
        this.modify("fixed_price",fixedPrice);
    }
    /**
     * 设置 [基于]
     */
    public void setBase(String base){
        this.base = base ;
        this.modify("base",base);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }
    /**
     * 设置 [百分比价格]
     */
    public void setPercentPrice(Double percentPrice){
        this.percentPrice = percentPrice ;
        this.modify("percent_price",percentPrice);
    }
    /**
     * 设置 [价格舍入]
     */
    public void setPriceRound(Double priceRound){
        this.priceRound = priceRound ;
        this.modify("price_round",priceRound);
    }
    /**
     * 设置 [计算价格]
     */
    public void setComputePrice(String computePrice){
        this.computePrice = computePrice ;
        this.modify("compute_price",computePrice);
    }
    /**
     * 设置 [最小价格毛利]
     */
    public void setPriceMinMargin(Double priceMinMargin){
        this.priceMinMargin = priceMinMargin ;
        this.modify("price_min_margin",priceMinMargin);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }
    /**
     * 设置 [最大价格毛利]
     */
    public void setPriceMaxMargin(Double priceMaxMargin){
        this.priceMaxMargin = priceMaxMargin ;
        this.modify("price_max_margin",priceMaxMargin);
    }
    /**
     * 设置 [应用于]
     */
    public void setAppliedOn(String appliedOn){
        this.appliedOn = appliedOn ;
        this.modify("applied_on",appliedOn);
    }
    /**
     * 设置 [最小数量]
     */
    public void setMinQuantity(Integer minQuantity){
        this.minQuantity = minQuantity ;
        this.modify("min_quantity",minQuantity);
    }
    /**
     * 设置 [价格附加费用]
     */
    public void setPriceSurcharge(Double priceSurcharge){
        this.priceSurcharge = priceSurcharge ;
        this.modify("price_surcharge",priceSurcharge);
    }
    /**
     * 设置 [价格折扣]
     */
    public void setPriceDiscount(Double priceDiscount){
        this.priceDiscount = priceDiscount ;
        this.modify("price_discount",priceDiscount);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Integer pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [其他价格表]
     */
    public void setBasePricelistId(Integer basePricelistId){
        this.basePricelistId = basePricelistId ;
        this.modify("base_pricelist_id",basePricelistId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [产品种类]
     */
    public void setCategId(Integer categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }
    /**
     * 设置 [产品模板]
     */
    public void setProductTmplId(Integer productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

}


