package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_attribute_valueOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_attribute_valueClientModel;

/**
 * 实体[属性值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attribute_valueServiceImpl implements IProduct_attribute_valueService {

    @Autowired
    product_attribute_valueOdooClient product_attribute_valueOdooClient;


    @Override
    public boolean create(Product_attribute_value et) {
        product_attribute_valueClientModel clientModel = convert2Model(et,null);
		product_attribute_valueOdooClient.create(clientModel);
        Product_attribute_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute_value> list){
    }

    @Override
    public Product_attribute_value get(Integer id) {
        product_attribute_valueClientModel clientModel = new product_attribute_valueClientModel();
        clientModel.setId(id);
		product_attribute_valueOdooClient.get(clientModel);
        Product_attribute_value et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_attribute_value();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Product_attribute_value et) {
        product_attribute_valueClientModel clientModel = convert2Model(et,null);
		product_attribute_valueOdooClient.update(clientModel);
        Product_attribute_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_attribute_value> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_attribute_valueClientModel clientModel = new product_attribute_valueClientModel();
        clientModel.setId(id);
		product_attribute_valueOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute_value> searchDefault(Product_attribute_valueSearchContext context) {
        List<Product_attribute_value> list = new ArrayList<Product_attribute_value>();
        Page<product_attribute_valueClientModel> clientModelList = product_attribute_valueOdooClient.search(context);
        for(product_attribute_valueClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_attribute_value>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_attribute_valueClientModel convert2Model(Product_attribute_value domain , product_attribute_valueClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_attribute_valueClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("html_colordirtyflag"))
                model.setHtml_color(domain.getHtmlColor());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("is_customdirtyflag"))
                model.setIs_custom(domain.getIsCustom());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("attribute_id_textdirtyflag"))
                model.setAttribute_id_text(domain.getAttributeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("attribute_iddirtyflag"))
                model.setAttribute_id(domain.getAttributeId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_attribute_value convert2Domain( product_attribute_valueClientModel model ,Product_attribute_value domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_attribute_value();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getHtml_colorDirtyFlag())
            domain.setHtmlColor(model.getHtml_color());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIs_customDirtyFlag())
            domain.setIsCustom(model.getIs_custom());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAttribute_id_textDirtyFlag())
            domain.setAttributeIdText(model.getAttribute_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getAttribute_idDirtyFlag())
            domain.setAttributeId(model.getAttribute_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



