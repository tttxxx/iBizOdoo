package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;

/**
 * 实体 [调查的功能EMail撰写向导] 存储模型
 */
public interface Survey_mail_compose_message{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 删除邮件
     */
    String getAuto_delete();

    void setAuto_delete(String auto_delete);

    /**
     * 获取 [删除邮件]脏标记
     */
    boolean getAuto_deleteDirtyFlag();

    /**
     * 相关评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [相关评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 待处理的业务伙伴
     */
    String getNeedaction_partner_ids();

    void setNeedaction_partner_ids(String needaction_partner_ids);

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    boolean getNeedaction_partner_idsDirtyFlag();

    /**
     * 评级值
     */
    Double getRating_value();

    void setRating_value(Double rating_value);

    /**
     * 获取 [评级值]脏标记
     */
    boolean getRating_valueDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * Message-Id
     */
    String getMessage_id();

    void setMessage_id(String message_id);

    /**
     * 获取 [Message-Id]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 发件服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [发件服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * 内容
     */
    String getBody();

    void setBody(String body);

    /**
     * 获取 [内容]脏标记
     */
    boolean getBodyDirtyFlag();

    /**
     * 收藏夹
     */
    String getStarred_partner_ids();

    void setStarred_partner_ids(String starred_partner_ids);

    /**
     * 获取 [收藏夹]脏标记
     */
    boolean getStarred_partner_idsDirtyFlag();

    /**
     * 频道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [频道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 星标邮件
     */
    String getStarred();

    void setStarred(String starred);

    /**
     * 获取 [星标邮件]脏标记
     */
    boolean getStarredDirtyFlag();

    /**
     * 需要审核
     */
    String getNeed_moderation();

    void setNeed_moderation(String need_moderation);

    /**
     * 获取 [需要审核]脏标记
     */
    boolean getNeed_moderationDirtyFlag();

    /**
     * 追踪值
     */
    String getTracking_value_ids();

    void setTracking_value_ids(String tracking_value_ids);

    /**
     * 获取 [追踪值]脏标记
     */
    boolean getTracking_value_idsDirtyFlag();

    /**
     * 邀请回馈的截止日期有效
     */
    Timestamp getDate_deadline();

    void setDate_deadline(Timestamp date_deadline);

    /**
     * 获取 [邀请回馈的截止日期有效]脏标记
     */
    boolean getDate_deadlineDirtyFlag();

    /**
     * 线程无应答
     */
    String getNo_auto_thread();

    void setNo_auto_thread(String no_auto_thread);

    /**
     * 获取 [线程无应答]脏标记
     */
    boolean getNo_auto_threadDirtyFlag();

    /**
     * 主题
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 现有联系人
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [现有联系人]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有误差
     */
    String getHas_error();

    void setHas_error(String has_error);

    /**
     * 获取 [有误差]脏标记
     */
    boolean getHas_errorDirtyFlag();

    /**
     * 邮件列表
     */
    String getMulti_email();

    void setMulti_email(String multi_email);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getMulti_emailDirtyFlag();

    /**
     * 使用有效域
     */
    String getUse_active_domain();

    void setUse_active_domain(String use_active_domain);

    /**
     * 获取 [使用有效域]脏标记
     */
    boolean getUse_active_domainDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 待处理
     */
    String getNeedaction();

    void setNeedaction(String needaction);

    /**
     * 获取 [待处理]脏标记
     */
    boolean getNeedactionDirtyFlag();

    /**
     * 布局
     */
    String getLayout();

    void setLayout(String layout);

    /**
     * 获取 [布局]脏标记
     */
    boolean getLayoutDirtyFlag();

    /**
     * 群发邮件标题
     */
    String getMass_mailing_name();

    void setMass_mailing_name(String mass_mailing_name);

    /**
     * 获取 [群发邮件标题]脏标记
     */
    boolean getMass_mailing_nameDirtyFlag();

    /**
     * 删除消息副本
     */
    String getAuto_delete_message();

    void setAuto_delete_message(String auto_delete_message);

    /**
     * 获取 [删除消息副本]脏标记
     */
    boolean getAuto_delete_messageDirtyFlag();

    /**
     * 公开HTML网站链接
     */
    String getPublic_url_html();

    void setPublic_url_html(String public_url_html);

    /**
     * 获取 [公开HTML网站链接]脏标记
     */
    boolean getPublic_url_htmlDirtyFlag();

    /**
     * 消息记录名称
     */
    String getRecord_name();

    void setRecord_name(String record_name);

    /**
     * 获取 [消息记录名称]脏标记
     */
    boolean getRecord_nameDirtyFlag();

    /**
     * 公开网址
     */
    String getPublic_url();

    void setPublic_url(String public_url);

    /**
     * 获取 [公开网址]脏标记
     */
    boolean getPublic_urlDirtyFlag();

    /**
     * 下级消息
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级消息]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 通知
     */
    String getNotification_ids();

    void setNotification_ids(String notification_ids);

    /**
     * 获取 [通知]脏标记
     */
    boolean getNotification_idsDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 记录内部备注
     */
    String getIs_log();

    void setIs_log(String is_log);

    /**
     * 获取 [记录内部备注]脏标记
     */
    boolean getIs_logDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 添加签名
     */
    String getAdd_sign();

    void setAdd_sign(String add_sign);

    /**
     * 获取 [添加签名]脏标记
     */
    boolean getAdd_signDirtyFlag();

    /**
     * 审核状态
     */
    String getModeration_status();

    void setModeration_status(String moderation_status);

    /**
     * 获取 [审核状态]脏标记
     */
    boolean getModeration_statusDirtyFlag();

    /**
     * 类型
     */
    String getMessage_type();

    void setMessage_type(String message_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getMessage_typeDirtyFlag();

    /**
     * 分享选项
     */
    String getIbizpublic();

    void setIbizpublic(String ibizpublic);

    /**
     * 获取 [分享选项]脏标记
     */
    boolean getIbizpublicDirtyFlag();

    /**
     * 有效域
     */
    String getActive_domain();

    void setActive_domain(String active_domain);

    /**
     * 获取 [有效域]脏标记
     */
    boolean getActive_domainDirtyFlag();

    /**
     * 通知关注者
     */
    String getNotify();

    void setNotify(String notify);

    /**
     * 获取 [通知关注者]脏标记
     */
    boolean getNotifyDirtyFlag();

    /**
     * 写作模式
     */
    String getComposition_mode();

    void setComposition_mode(String composition_mode);

    /**
     * 获取 [写作模式]脏标记
     */
    boolean getComposition_modeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 已发布
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 邮件列表
     */
    String getMailing_list_ids();

    void setMailing_list_ids(String mailing_list_ids);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getMailing_list_idsDirtyFlag();

    /**
     * 作者
     */
    String getAuthor_id_text();

    void setAuthor_id_text(String author_id_text);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_id_textDirtyFlag();

    /**
     * 子类型
     */
    String getSubtype_id_text();

    void setSubtype_id_text(String subtype_id_text);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_id_textDirtyFlag();

    /**
     * 使用模版
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [使用模版]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 作者头像
     */
    byte[] getAuthor_avatar();

    void setAuthor_avatar(byte[] author_avatar);

    /**
     * 获取 [作者头像]脏标记
     */
    boolean getAuthor_avatarDirtyFlag();

    /**
     * 邮件活动类型
     */
    String getMail_activity_type_id_text();

    void setMail_activity_type_id_text(String mail_activity_type_id_text);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 审核人
     */
    String getModerator_id_text();

    void setModerator_id_text(String moderator_id_text);

    /**
     * 获取 [审核人]脏标记
     */
    boolean getModerator_id_textDirtyFlag();

    /**
     * 群发邮件
     */
    String getMass_mailing_id_text();

    void setMass_mailing_id_text(String mass_mailing_id_text);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 群发邮件营销
     */
    String getMass_mailing_campaign_id_text();

    void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 问卷
     */
    Integer getSurvey_id();

    void setSurvey_id(Integer survey_id);

    /**
     * 获取 [问卷]脏标记
     */
    boolean getSurvey_idDirtyFlag();

    /**
     * 作者
     */
    Integer getAuthor_id();

    void setAuthor_id(Integer author_id);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_idDirtyFlag();

    /**
     * 群发邮件营销
     */
    Integer getMass_mailing_campaign_id();

    void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_idDirtyFlag();

    /**
     * 上级消息
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级消息]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 子类型
     */
    Integer getSubtype_id();

    void setSubtype_id(Integer subtype_id);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idDirtyFlag();

    /**
     * 使用模版
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [使用模版]脏标记
     */
    boolean getTemplate_idDirtyFlag();

    /**
     * 群发邮件
     */
    Integer getMass_mailing_id();

    void setMass_mailing_id(Integer mass_mailing_id);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_idDirtyFlag();

    /**
     * 审核人
     */
    Integer getModerator_id();

    void setModerator_id(Integer moderator_id);

    /**
     * 获取 [审核人]脏标记
     */
    boolean getModerator_idDirtyFlag();

    /**
     * 邮件活动类型
     */
    Integer getMail_activity_type_id();

    void setMail_activity_type_id(Integer mail_activity_type_id);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
