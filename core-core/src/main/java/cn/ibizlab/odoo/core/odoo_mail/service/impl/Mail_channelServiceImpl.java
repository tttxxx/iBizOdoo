package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_channelOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_channelClientModel;

/**
 * 实体[讨论频道] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_channelServiceImpl implements IMail_channelService {

    @Autowired
    mail_channelOdooClient mail_channelOdooClient;


    @Override
    public boolean update(Mail_channel et) {
        mail_channelClientModel clientModel = convert2Model(et,null);
		mail_channelOdooClient.update(clientModel);
        Mail_channel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_channel> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_channelClientModel clientModel = new mail_channelClientModel();
        clientModel.setId(id);
		mail_channelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_channel et) {
        mail_channelClientModel clientModel = convert2Model(et,null);
		mail_channelOdooClient.create(clientModel);
        Mail_channel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_channel> list){
    }

    @Override
    public Mail_channel get(Integer id) {
        mail_channelClientModel clientModel = new mail_channelClientModel();
        clientModel.setId(id);
		mail_channelOdooClient.get(clientModel);
        Mail_channel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_channel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_channel> searchDefault(Mail_channelSearchContext context) {
        List<Mail_channel> list = new ArrayList<Mail_channel>();
        Page<mail_channelClientModel> clientModelList = mail_channelOdooClient.search(context);
        for(mail_channelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_channel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_channelClientModel convert2Model(Mail_channel domain , mail_channelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_channelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("moderation_countdirtyflag"))
                model.setModeration_count(domain.getModerationCount());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("moderation_notifydirtyflag"))
                model.setModeration_notify(domain.getModerationNotify());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("moderationdirtyflag"))
                model.setModeration(domain.getModeration());
            if((Boolean) domain.getExtensionparams().get("is_moderatordirtyflag"))
                model.setIs_moderator(domain.getIsModerator());
            if((Boolean) domain.getExtensionparams().get("moderation_notify_msgdirtyflag"))
                model.setModeration_notify_msg(domain.getModerationNotifyMsg());
            if((Boolean) domain.getExtensionparams().get("moderation_idsdirtyflag"))
                model.setModeration_ids(domain.getModerationIds());
            if((Boolean) domain.getExtensionparams().get("rating_last_imagedirtyflag"))
                model.setRating_last_image(domain.getRatingLastImage());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("moderation_guidelinesdirtyflag"))
                model.setModeration_guidelines(domain.getModerationGuidelines());
            if((Boolean) domain.getExtensionparams().get("rating_last_feedbackdirtyflag"))
                model.setRating_last_feedback(domain.getRatingLastFeedback());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("subscription_department_idsdirtyflag"))
                model.setSubscription_department_ids(domain.getSubscriptionDepartmentIds());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("ibizpublicdirtyflag"))
                model.setIbizpublic(domain.getIbizpublic());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("anonymous_namedirtyflag"))
                model.setAnonymous_name(domain.getAnonymousName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("is_memberdirtyflag"))
                model.setIs_member(domain.getIsMember());
            if((Boolean) domain.getExtensionparams().get("is_subscribeddirtyflag"))
                model.setIs_subscribed(domain.getIsSubscribed());
            if((Boolean) domain.getExtensionparams().get("moderation_guidelines_msgdirtyflag"))
                model.setModeration_guidelines_msg(domain.getModerationGuidelinesMsg());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("channel_partner_idsdirtyflag"))
                model.setChannel_partner_ids(domain.getChannelPartnerIds());
            if((Boolean) domain.getExtensionparams().get("moderator_idsdirtyflag"))
                model.setModerator_ids(domain.getModeratorIds());
            if((Boolean) domain.getExtensionparams().get("group_idsdirtyflag"))
                model.setGroup_ids(domain.getGroupIds());
            if((Boolean) domain.getExtensionparams().get("email_senddirtyflag"))
                model.setEmail_send(domain.getEmailSend());
            if((Boolean) domain.getExtensionparams().get("channel_last_seen_partner_idsdirtyflag"))
                model.setChannel_last_seen_partner_ids(domain.getChannelLastSeenPartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("uuiddirtyflag"))
                model.setUuid(domain.getUuid());
            if((Boolean) domain.getExtensionparams().get("rating_countdirtyflag"))
                model.setRating_count(domain.getRatingCount());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("channel_message_idsdirtyflag"))
                model.setChannel_message_ids(domain.getChannelMessageIds());
            if((Boolean) domain.getExtensionparams().get("channel_typedirtyflag"))
                model.setChannel_type(domain.getChannelType());
            if((Boolean) domain.getExtensionparams().get("is_chatdirtyflag"))
                model.setIs_chat(domain.getIsChat());
            if((Boolean) domain.getExtensionparams().get("rating_last_valuedirtyflag"))
                model.setRating_last_value(domain.getRatingLastValue());
            if((Boolean) domain.getExtensionparams().get("alias_force_thread_iddirtyflag"))
                model.setAlias_force_thread_id(domain.getAliasForceThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_defaultsdirtyflag"))
                model.setAlias_defaults(domain.getAliasDefaults());
            if((Boolean) domain.getExtensionparams().get("alias_user_iddirtyflag"))
                model.setAlias_user_id(domain.getAliasUserId());
            if((Boolean) domain.getExtensionparams().get("alias_parent_model_iddirtyflag"))
                model.setAlias_parent_model_id(domain.getAliasParentModelId());
            if((Boolean) domain.getExtensionparams().get("group_public_id_textdirtyflag"))
                model.setGroup_public_id_text(domain.getGroupPublicIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("alias_parent_thread_iddirtyflag"))
                model.setAlias_parent_thread_id(domain.getAliasParentThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("alias_model_iddirtyflag"))
                model.setAlias_model_id(domain.getAliasModelId());
            if((Boolean) domain.getExtensionparams().get("livechat_channel_id_textdirtyflag"))
                model.setLivechat_channel_id_text(domain.getLivechatChannelIdText());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("group_public_iddirtyflag"))
                model.setGroup_public_id(domain.getGroupPublicId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            if((Boolean) domain.getExtensionparams().get("livechat_channel_iddirtyflag"))
                model.setLivechat_channel_id(domain.getLivechatChannelId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_channel convert2Domain( mail_channelClientModel model ,Mail_channel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_channel();
        }

        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getModeration_countDirtyFlag())
            domain.setModerationCount(model.getModeration_count());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getModeration_notifyDirtyFlag())
            domain.setModerationNotify(model.getModeration_notify());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getModerationDirtyFlag())
            domain.setModeration(model.getModeration());
        if(model.getIs_moderatorDirtyFlag())
            domain.setIsModerator(model.getIs_moderator());
        if(model.getModeration_notify_msgDirtyFlag())
            domain.setModerationNotifyMsg(model.getModeration_notify_msg());
        if(model.getModeration_idsDirtyFlag())
            domain.setModerationIds(model.getModeration_ids());
        if(model.getRating_last_imageDirtyFlag())
            domain.setRatingLastImage(model.getRating_last_image());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getModeration_guidelinesDirtyFlag())
            domain.setModerationGuidelines(model.getModeration_guidelines());
        if(model.getRating_last_feedbackDirtyFlag())
            domain.setRatingLastFeedback(model.getRating_last_feedback());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getSubscription_department_idsDirtyFlag())
            domain.setSubscriptionDepartmentIds(model.getSubscription_department_ids());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIbizpublicDirtyFlag())
            domain.setIbizpublic(model.getIbizpublic());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getAnonymous_nameDirtyFlag())
            domain.setAnonymousName(model.getAnonymous_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIs_memberDirtyFlag())
            domain.setIsMember(model.getIs_member());
        if(model.getIs_subscribedDirtyFlag())
            domain.setIsSubscribed(model.getIs_subscribed());
        if(model.getModeration_guidelines_msgDirtyFlag())
            domain.setModerationGuidelinesMsg(model.getModeration_guidelines_msg());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getChannel_partner_idsDirtyFlag())
            domain.setChannelPartnerIds(model.getChannel_partner_ids());
        if(model.getModerator_idsDirtyFlag())
            domain.setModeratorIds(model.getModerator_ids());
        if(model.getGroup_idsDirtyFlag())
            domain.setGroupIds(model.getGroup_ids());
        if(model.getEmail_sendDirtyFlag())
            domain.setEmailSend(model.getEmail_send());
        if(model.getChannel_last_seen_partner_idsDirtyFlag())
            domain.setChannelLastSeenPartnerIds(model.getChannel_last_seen_partner_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getUuidDirtyFlag())
            domain.setUuid(model.getUuid());
        if(model.getRating_countDirtyFlag())
            domain.setRatingCount(model.getRating_count());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getChannel_message_idsDirtyFlag())
            domain.setChannelMessageIds(model.getChannel_message_ids());
        if(model.getChannel_typeDirtyFlag())
            domain.setChannelType(model.getChannel_type());
        if(model.getIs_chatDirtyFlag())
            domain.setIsChat(model.getIs_chat());
        if(model.getRating_last_valueDirtyFlag())
            domain.setRatingLastValue(model.getRating_last_value());
        if(model.getAlias_force_thread_idDirtyFlag())
            domain.setAliasForceThreadId(model.getAlias_force_thread_id());
        if(model.getAlias_defaultsDirtyFlag())
            domain.setAliasDefaults(model.getAlias_defaults());
        if(model.getAlias_user_idDirtyFlag())
            domain.setAliasUserId(model.getAlias_user_id());
        if(model.getAlias_parent_model_idDirtyFlag())
            domain.setAliasParentModelId(model.getAlias_parent_model_id());
        if(model.getGroup_public_id_textDirtyFlag())
            domain.setGroupPublicIdText(model.getGroup_public_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAlias_parent_thread_idDirtyFlag())
            domain.setAliasParentThreadId(model.getAlias_parent_thread_id());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getAlias_model_idDirtyFlag())
            domain.setAliasModelId(model.getAlias_model_id());
        if(model.getLivechat_channel_id_textDirtyFlag())
            domain.setLivechatChannelIdText(model.getLivechat_channel_id_text());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getGroup_public_idDirtyFlag())
            domain.setGroupPublicId(model.getGroup_public_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        if(model.getLivechat_channel_idDirtyFlag())
            domain.setLivechatChannelId(model.getLivechat_channel_id());
        return domain ;
    }

}

    



