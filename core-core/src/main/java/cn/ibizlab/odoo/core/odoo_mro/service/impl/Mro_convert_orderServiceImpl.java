package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_convert_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_convert_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_convert_orderClientModel;

/**
 * 实体[Convert Order to Task] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_convert_orderServiceImpl implements IMro_convert_orderService {

    @Autowired
    mro_convert_orderOdooClient mro_convert_orderOdooClient;


    @Override
    public boolean create(Mro_convert_order et) {
        mro_convert_orderClientModel clientModel = convert2Model(et,null);
		mro_convert_orderOdooClient.create(clientModel);
        Mro_convert_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_convert_order> list){
    }

    @Override
    public Mro_convert_order get(Integer id) {
        mro_convert_orderClientModel clientModel = new mro_convert_orderClientModel();
        clientModel.setId(id);
		mro_convert_orderOdooClient.get(clientModel);
        Mro_convert_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_convert_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mro_convert_order et) {
        mro_convert_orderClientModel clientModel = convert2Model(et,null);
		mro_convert_orderOdooClient.update(clientModel);
        Mro_convert_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_convert_order> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_convert_orderClientModel clientModel = new mro_convert_orderClientModel();
        clientModel.setId(id);
		mro_convert_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context) {
        List<Mro_convert_order> list = new ArrayList<Mro_convert_order>();
        Page<mro_convert_orderClientModel> clientModelList = mro_convert_orderOdooClient.search(context);
        for(mro_convert_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_convert_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_convert_orderClientModel convert2Model(Mro_convert_order domain , mro_convert_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_convert_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_convert_order convert2Domain( mro_convert_orderClientModel model ,Mro_convert_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_convert_order();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



