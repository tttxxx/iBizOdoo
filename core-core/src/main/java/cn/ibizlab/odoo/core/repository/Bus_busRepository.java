package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;

/**
 * 实体 [通讯总线] 存储对象
 */
public interface Bus_busRepository extends Repository<Bus_bus> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Bus_bus> searchDefault(Bus_busSearchContext context);

    Bus_bus convert2PO(cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus domain , Bus_bus po) ;

    cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus convert2Domain( Bus_bus po ,cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus domain) ;

}
