package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;

/**
 * 实体 [邮件统计] 存储模型
 */
public interface Mail_mail_statistics{

    /**
     * 安排
     */
    Timestamp getScheduled();

    void setScheduled(Timestamp scheduled);

    /**
     * 获取 [安排]脏标记
     */
    boolean getScheduledDirtyFlag();

    /**
     * 点击率
     */
    Timestamp getClicked();

    void setClicked(Timestamp clicked);

    /**
     * 获取 [点击率]脏标记
     */
    boolean getClickedDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 收件人email地址
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [收件人email地址]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 状态更新
     */
    Timestamp getState_update();

    void setState_update(Timestamp state_update);

    /**
     * 获取 [状态更新]脏标记
     */
    boolean getState_updateDirtyFlag();

    /**
     * 已回复
     */
    Timestamp getReplied();

    void setReplied(Timestamp replied);

    /**
     * 获取 [已回复]脏标记
     */
    boolean getRepliedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 消息ID
     */
    String getMessage_id();

    void setMessage_id(String message_id);

    /**
     * 获取 [消息ID]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 异常
     */
    Timestamp getException();

    void setException(Timestamp exception);

    /**
     * 获取 [异常]脏标记
     */
    boolean getExceptionDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 被退回
     */
    Timestamp getBounced();

    void setBounced(Timestamp bounced);

    /**
     * 获取 [被退回]脏标记
     */
    boolean getBouncedDirtyFlag();

    /**
     * 忽略
     */
    Timestamp getIgnored();

    void setIgnored(Timestamp ignored);

    /**
     * 获取 [忽略]脏标记
     */
    boolean getIgnoredDirtyFlag();

    /**
     * 文档ID
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [文档ID]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 已汇
     */
    Timestamp getSent();

    void setSent(Timestamp sent);

    /**
     * 获取 [已汇]脏标记
     */
    boolean getSentDirtyFlag();

    /**
     * 邮件ID（技术）
     */
    Integer getMail_mail_id_int();

    void setMail_mail_id_int(Integer mail_mail_id_int);

    /**
     * 获取 [邮件ID（技术）]脏标记
     */
    boolean getMail_mail_id_intDirtyFlag();

    /**
     * 点击链接
     */
    String getLinks_click_ids();

    void setLinks_click_ids(String links_click_ids);

    /**
     * 获取 [点击链接]脏标记
     */
    boolean getLinks_click_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 已开启
     */
    Timestamp getOpened();

    void setOpened(Timestamp opened);

    /**
     * 获取 [已开启]脏标记
     */
    boolean getOpenedDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 群发邮件营销
     */
    String getMass_mailing_campaign_id_text();

    void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_id_textDirtyFlag();

    /**
     * 群发邮件
     */
    String getMass_mailing_id_text();

    void setMass_mailing_id_text(String mass_mailing_id_text);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 群发邮件营销
     */
    Integer getMass_mailing_campaign_id();

    void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_idDirtyFlag();

    /**
     * 邮件
     */
    Integer getMail_mail_id();

    void setMail_mail_id(Integer mail_mail_id);

    /**
     * 获取 [邮件]脏标记
     */
    boolean getMail_mail_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 群发邮件
     */
    Integer getMass_mailing_id();

    void setMass_mailing_id(Integer mass_mailing_id);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_idDirtyFlag();

}
