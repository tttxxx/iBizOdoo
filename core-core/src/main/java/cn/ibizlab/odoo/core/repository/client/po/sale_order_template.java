package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_order_template] 对象
 */
public interface sale_order_template {

    public String getActive();

    public void setActive(String active);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMail_template_id();

    public void setMail_template_id(Integer mail_template_id);

    public String getMail_template_id_text();

    public void setMail_template_id_text(String mail_template_id_text);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public Integer getNumber_of_days();

    public void setNumber_of_days(Integer number_of_days);

    public String getRequire_payment();

    public void setRequire_payment(String require_payment);

    public String getRequire_signature();

    public void setRequire_signature(String require_signature);

    public String getSale_order_template_line_ids();

    public void setSale_order_template_line_ids(String sale_order_template_line_ids);

    public String getSale_order_template_option_ids();

    public void setSale_order_template_option_ids(String sale_order_template_option_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
