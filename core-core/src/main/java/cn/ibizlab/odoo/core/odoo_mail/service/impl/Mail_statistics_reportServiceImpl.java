package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_statistics_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_statistics_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_statistics_reportClientModel;

/**
 * 实体[群发邮件阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_statistics_reportServiceImpl implements IMail_statistics_reportService {

    @Autowired
    mail_statistics_reportOdooClient mail_statistics_reportOdooClient;


    @Override
    public boolean update(Mail_statistics_report et) {
        mail_statistics_reportClientModel clientModel = convert2Model(et,null);
		mail_statistics_reportOdooClient.update(clientModel);
        Mail_statistics_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_statistics_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_statistics_reportClientModel clientModel = new mail_statistics_reportClientModel();
        clientModel.setId(id);
		mail_statistics_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mail_statistics_report get(Integer id) {
        mail_statistics_reportClientModel clientModel = new mail_statistics_reportClientModel();
        clientModel.setId(id);
		mail_statistics_reportOdooClient.get(clientModel);
        Mail_statistics_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_statistics_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_statistics_report et) {
        mail_statistics_reportClientModel clientModel = convert2Model(et,null);
		mail_statistics_reportOdooClient.create(clientModel);
        Mail_statistics_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_statistics_report> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context) {
        List<Mail_statistics_report> list = new ArrayList<Mail_statistics_report>();
        Page<mail_statistics_reportClientModel> clientModelList = mail_statistics_reportOdooClient.search(context);
        for(mail_statistics_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_statistics_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_statistics_reportClientModel convert2Model(Mail_statistics_report domain , mail_statistics_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_statistics_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("clickeddirtyflag"))
                model.setClicked(domain.getClicked());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("bounceddirtyflag"))
                model.setBounced(domain.getBounced());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("replieddirtyflag"))
                model.setReplied(domain.getReplied());
            if((Boolean) domain.getExtensionparams().get("campaigndirtyflag"))
                model.setCampaign(domain.getCampaign());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("sentdirtyflag"))
                model.setSent(domain.getSent());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("delivereddirtyflag"))
                model.setDelivered(domain.getDelivered());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("openeddirtyflag"))
                model.setOpened(domain.getOpened());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_statistics_report convert2Domain( mail_statistics_reportClientModel model ,Mail_statistics_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_statistics_report();
        }

        if(model.getClickedDirtyFlag())
            domain.setClicked(model.getClicked());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getBouncedDirtyFlag())
            domain.setBounced(model.getBounced());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getRepliedDirtyFlag())
            domain.setReplied(model.getReplied());
        if(model.getCampaignDirtyFlag())
            domain.setCampaign(model.getCampaign());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getSentDirtyFlag())
            domain.setSent(model.getSent());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getDeliveredDirtyFlag())
            domain.setDelivered(model.getDelivered());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getOpenedDirtyFlag())
            domain.setOpened(model.getOpened());
        return domain ;
    }

}

    



