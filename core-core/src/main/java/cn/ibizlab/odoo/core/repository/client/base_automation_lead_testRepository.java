package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.base_automation_lead_test;

/**
 * 实体[base_automation_lead_test] 服务对象接口
 */
public interface base_automation_lead_testRepository{


    public base_automation_lead_test createPO() ;
        public void createBatch(base_automation_lead_test base_automation_lead_test);

        public void updateBatch(base_automation_lead_test base_automation_lead_test);

        public void update(base_automation_lead_test base_automation_lead_test);

        public List<base_automation_lead_test> search();

        public void create(base_automation_lead_test base_automation_lead_test);

        public void removeBatch(String id);

        public void remove(String id);

        public void get(String id);


}
