package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;

/**
 * 实体 [群发邮件订阅信息] 存储对象
 */
public interface Mail_mass_mailing_list_contact_relRepository extends Repository<Mail_mass_mailing_list_contact_rel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context);

    Mail_mass_mailing_list_contact_rel convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel domain , Mail_mass_mailing_list_contact_rel po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel convert2Domain( Mail_mass_mailing_list_contact_rel po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel domain) ;

}
