package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;

/**
 * 实体 [测试:基本导入模型，字符只读] 存储对象
 */
public interface Base_import_tests_models_char_readonlyRepository extends Repository<Base_import_tests_models_char_readonly> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_tests_models_char_readonly> searchDefault(Base_import_tests_models_char_readonlySearchContext context);

    Base_import_tests_models_char_readonly convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly domain , Base_import_tests_models_char_readonly po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly convert2Domain( Base_import_tests_models_char_readonly po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly domain) ;

}
