package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;

/**
 * 实体 [活动类型] 存储模型
 */
public interface Mail_activity_type{

    /**
     * 模型已更改
     */
    String getRes_model_change();

    void setRes_model_change(String res_model_change);

    /**
     * 获取 [模型已更改]脏标记
     */
    boolean getRes_model_changeDirtyFlag();

    /**
     * 摘要
     */
    String getSummary();

    void setSummary(String summary);

    /**
     * 获取 [摘要]脏标记
     */
    boolean getSummaryDirtyFlag();

    /**
     * 自动安排下一个活动
     */
    String getForce_next();

    void setForce_next(String force_next);

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    boolean getForce_nextDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 类别
     */
    String getCategory();

    void setCategory(String category);

    /**
     * 获取 [类别]脏标记
     */
    boolean getCategoryDirtyFlag();

    /**
     * 图标
     */
    String getIcon();

    void setIcon(String icon);

    /**
     * 获取 [图标]脏标记
     */
    boolean getIconDirtyFlag();

    /**
     * 之后
     */
    Integer getDelay_count();

    void setDelay_count(Integer delay_count);

    /**
     * 获取 [之后]脏标记
     */
    boolean getDelay_countDirtyFlag();

    /**
     * 预先活动
     */
    String getPrevious_type_ids();

    void setPrevious_type_ids(String previous_type_ids);

    /**
     * 获取 [预先活动]脏标记
     */
    boolean getPrevious_type_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 延迟类型
     */
    String getDelay_from();

    void setDelay_from(String delay_from);

    /**
     * 获取 [延迟类型]脏标记
     */
    boolean getDelay_fromDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 邮件模板
     */
    String getMail_template_ids();

    void setMail_template_ids(String mail_template_ids);

    /**
     * 获取 [邮件模板]脏标记
     */
    boolean getMail_template_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 模型
     */
    Integer getRes_model_id();

    void setRes_model_id(Integer res_model_id);

    /**
     * 获取 [模型]脏标记
     */
    boolean getRes_model_idDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 排版类型
     */
    String getDecoration_type();

    void setDecoration_type(String decoration_type);

    /**
     * 获取 [排版类型]脏标记
     */
    boolean getDecoration_typeDirtyFlag();

    /**
     * 推荐的下一活动
     */
    String getNext_type_ids();

    void setNext_type_ids(String next_type_ids);

    /**
     * 获取 [推荐的下一活动]脏标记
     */
    boolean getNext_type_idsDirtyFlag();

    /**
     * 延迟单位
     */
    String getDelay_unit();

    void setDelay_unit(String delay_unit);

    /**
     * 获取 [延迟单位]脏标记
     */
    boolean getDelay_unitDirtyFlag();

    /**
     * 初始模型
     */
    Integer getInitial_res_model_id();

    void setInitial_res_model_id(Integer initial_res_model_id);

    /**
     * 获取 [初始模型]脏标记
     */
    boolean getInitial_res_model_idDirtyFlag();

    /**
     * 设置默认下一个活动
     */
    String getDefault_next_type_id_text();

    void setDefault_next_type_id_text(String default_next_type_id_text);

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    boolean getDefault_next_type_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 设置默认下一个活动
     */
    Integer getDefault_next_type_id();

    void setDefault_next_type_id(Integer default_next_type_id);

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    boolean getDefault_next_type_idDirtyFlag();

}
