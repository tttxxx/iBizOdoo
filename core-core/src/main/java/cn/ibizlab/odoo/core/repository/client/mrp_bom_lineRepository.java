package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_bom_line;

/**
 * 实体[mrp_bom_line] 服务对象接口
 */
public interface mrp_bom_lineRepository{


    public mrp_bom_line createPO() ;
        public void updateBatch(mrp_bom_line mrp_bom_line);

        public List<mrp_bom_line> search();

        public void createBatch(mrp_bom_line mrp_bom_line);

        public void removeBatch(String id);

        public void update(mrp_bom_line mrp_bom_line);

        public void get(String id);

        public void remove(String id);

        public void create(mrp_bom_line mrp_bom_line);


}
