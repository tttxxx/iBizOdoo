package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_employeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_holidays_summary_employeeOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_holidays_summary_employeeClientModel;

/**
 * 实体[按员工的休假摘要报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_holidays_summary_employeeServiceImpl implements IHr_holidays_summary_employeeService {

    @Autowired
    hr_holidays_summary_employeeOdooClient hr_holidays_summary_employeeOdooClient;


    @Override
    public boolean update(Hr_holidays_summary_employee et) {
        hr_holidays_summary_employeeClientModel clientModel = convert2Model(et,null);
		hr_holidays_summary_employeeOdooClient.update(clientModel);
        Hr_holidays_summary_employee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_holidays_summary_employee> list){
    }

    @Override
    public Hr_holidays_summary_employee get(Integer id) {
        hr_holidays_summary_employeeClientModel clientModel = new hr_holidays_summary_employeeClientModel();
        clientModel.setId(id);
		hr_holidays_summary_employeeOdooClient.get(clientModel);
        Hr_holidays_summary_employee et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_holidays_summary_employee();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Hr_holidays_summary_employee et) {
        hr_holidays_summary_employeeClientModel clientModel = convert2Model(et,null);
		hr_holidays_summary_employeeOdooClient.create(clientModel);
        Hr_holidays_summary_employee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_holidays_summary_employee> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_holidays_summary_employeeClientModel clientModel = new hr_holidays_summary_employeeClientModel();
        clientModel.setId(id);
		hr_holidays_summary_employeeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context) {
        List<Hr_holidays_summary_employee> list = new ArrayList<Hr_holidays_summary_employee>();
        Page<hr_holidays_summary_employeeClientModel> clientModelList = hr_holidays_summary_employeeOdooClient.search(context);
        for(hr_holidays_summary_employeeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_holidays_summary_employee>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_holidays_summary_employeeClientModel convert2Model(Hr_holidays_summary_employee domain , hr_holidays_summary_employeeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_holidays_summary_employeeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("holiday_typedirtyflag"))
                model.setHoliday_type(domain.getHolidayType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("empdirtyflag"))
                model.setEmp(domain.getEmp());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_holidays_summary_employee convert2Domain( hr_holidays_summary_employeeClientModel model ,Hr_holidays_summary_employee domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_holidays_summary_employee();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getHoliday_typeDirtyFlag())
            domain.setHolidayType(model.getHoliday_type());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getEmpDirtyFlag())
            domain.setEmp(model.getEmp());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



