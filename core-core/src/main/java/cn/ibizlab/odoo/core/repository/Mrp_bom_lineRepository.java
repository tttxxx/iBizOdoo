package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;

/**
 * 实体 [物料清单明细行] 存储对象
 */
public interface Mrp_bom_lineRepository extends Repository<Mrp_bom_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context);

    Mrp_bom_line convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line domain , Mrp_bom_line po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line convert2Domain( Mrp_bom_line po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line domain) ;

}
