package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [fleet_vehicle_log_contract] 对象
 */
public interface Ifleet_vehicle_log_contract {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [总价]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [总价]
     */
    public Double getAmount();

    /**
     * 获取 [总价]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [自动生成]
     */
    public void setAuto_generated(String auto_generated);
    
    /**
     * 设置 [自动生成]
     */
    public String getAuto_generated();

    /**
     * 获取 [自动生成]脏标记
     */
    public boolean getAuto_generatedDirtyFlag();
    /**
     * 获取 [合同]
     */
    public void setContract_id(Integer contract_id);
    
    /**
     * 设置 [合同]
     */
    public Integer getContract_id();

    /**
     * 获取 [合同]脏标记
     */
    public boolean getContract_idDirtyFlag();
    /**
     * 获取 [总额]
     */
    public void setCost_amount(Double cost_amount);
    
    /**
     * 设置 [总额]
     */
    public Double getCost_amount();

    /**
     * 获取 [总额]脏标记
     */
    public boolean getCost_amountDirtyFlag();
    /**
     * 获取 [经常成本频率]
     */
    public void setCost_frequency(String cost_frequency);
    
    /**
     * 设置 [经常成本频率]
     */
    public String getCost_frequency();

    /**
     * 获取 [经常成本频率]脏标记
     */
    public boolean getCost_frequencyDirtyFlag();
    /**
     * 获取 [经常成本数量]
     */
    public void setCost_generated(Double cost_generated);
    
    /**
     * 设置 [经常成本数量]
     */
    public Double getCost_generated();

    /**
     * 获取 [经常成本数量]脏标记
     */
    public boolean getCost_generatedDirtyFlag();
    /**
     * 获取 [成本]
     */
    public void setCost_id(Integer cost_id);
    
    /**
     * 设置 [成本]
     */
    public Integer getCost_id();

    /**
     * 获取 [成本]脏标记
     */
    public boolean getCost_idDirtyFlag();
    /**
     * 获取 [包括服务]
     */
    public void setCost_ids(String cost_ids);
    
    /**
     * 设置 [包括服务]
     */
    public String getCost_ids();

    /**
     * 获取 [包括服务]脏标记
     */
    public boolean getCost_idsDirtyFlag();
    /**
     * 获取 [成本]
     */
    public void setCost_id_text(String cost_id_text);
    
    /**
     * 设置 [成本]
     */
    public String getCost_id_text();

    /**
     * 获取 [成本]脏标记
     */
    public boolean getCost_id_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setCost_subtype_id(Integer cost_subtype_id);
    
    /**
     * 设置 [类型]
     */
    public Integer getCost_subtype_id();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getCost_subtype_idDirtyFlag();
    /**
     * 获取 [费用所属类别]
     */
    public void setCost_type(String cost_type);
    
    /**
     * 设置 [费用所属类别]
     */
    public String getCost_type();

    /**
     * 获取 [费用所属类别]脏标记
     */
    public boolean getCost_typeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [警告日期]
     */
    public void setDays_left(Integer days_left);
    
    /**
     * 设置 [警告日期]
     */
    public Integer getDays_left();

    /**
     * 获取 [警告日期]脏标记
     */
    public boolean getDays_leftDirtyFlag();
    /**
     * 获取 [成本说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [成本说明]
     */
    public String getDescription();

    /**
     * 获取 [成本说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [合同到期日期]
     */
    public void setExpiration_date(Timestamp expiration_date);
    
    /**
     * 设置 [合同到期日期]
     */
    public Timestamp getExpiration_date();

    /**
     * 获取 [合同到期日期]脏标记
     */
    public boolean getExpiration_dateDirtyFlag();
    /**
     * 获取 [已发生费用]
     */
    public void setGenerated_cost_ids(String generated_cost_ids);
    
    /**
     * 设置 [已发生费用]
     */
    public String getGenerated_cost_ids();

    /**
     * 获取 [已发生费用]脏标记
     */
    public boolean getGenerated_cost_idsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setInsurer_id(Integer insurer_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getInsurer_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getInsurer_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setInsurer_id_text(String insurer_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getInsurer_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getInsurer_id_textDirtyFlag();
    /**
     * 获取 [合同参考]
     */
    public void setIns_ref(String ins_ref);
    
    /**
     * 设置 [合同参考]
     */
    public String getIns_ref();

    /**
     * 获取 [合同参考]脏标记
     */
    public boolean getIns_refDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [条款和条件]
     */
    public void setNotes(String notes);
    
    /**
     * 设置 [条款和条件]
     */
    public String getNotes();

    /**
     * 获取 [条款和条件]脏标记
     */
    public boolean getNotesDirtyFlag();
    /**
     * 获取 [创建时的里程表]
     */
    public void setOdometer(Double odometer);
    
    /**
     * 设置 [创建时的里程表]
     */
    public Double getOdometer();

    /**
     * 获取 [创建时的里程表]脏标记
     */
    public boolean getOdometerDirtyFlag();
    /**
     * 获取 [里程表]
     */
    public void setOdometer_id(Integer odometer_id);
    
    /**
     * 设置 [里程表]
     */
    public Integer getOdometer_id();

    /**
     * 获取 [里程表]脏标记
     */
    public boolean getOdometer_idDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setOdometer_unit(String odometer_unit);
    
    /**
     * 设置 [单位]
     */
    public String getOdometer_unit();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getOdometer_unitDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setPurchaser_id(Integer purchaser_id);
    
    /**
     * 设置 [驾驶员]
     */
    public Integer getPurchaser_id();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getPurchaser_idDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setPurchaser_id_text(String purchaser_id_text);
    
    /**
     * 设置 [驾驶员]
     */
    public String getPurchaser_id_text();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getPurchaser_id_textDirtyFlag();
    /**
     * 获取 [合同开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [合同开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [合同开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [指标性成本总计]
     */
    public void setSum_cost(Double sum_cost);
    
    /**
     * 设置 [指标性成本总计]
     */
    public Double getSum_cost();

    /**
     * 获取 [指标性成本总计]脏标记
     */
    public boolean getSum_costDirtyFlag();
    /**
     * 获取 [负责]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [车辆]
     */
    public void setVehicle_id(Integer vehicle_id);
    
    /**
     * 设置 [车辆]
     */
    public Integer getVehicle_id();

    /**
     * 获取 [车辆]脏标记
     */
    public boolean getVehicle_idDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
