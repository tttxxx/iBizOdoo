package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;

/**
 * 实体 [工作细节] 存储模型
 */
public interface Resource_calendar_attendance{

    /**
     * 星期
     */
    String getDayofweek();

    void setDayofweek(String dayofweek);

    /**
     * 获取 [星期]脏标记
     */
    boolean getDayofweekDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 工作截止
     */
    Double getHour_to();

    void setHour_to(Double hour_to);

    /**
     * 获取 [工作截止]脏标记
     */
    boolean getHour_toDirtyFlag();

    /**
     * 日期
     */
    String getDay_period();

    void setDay_period(String day_period);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDay_periodDirtyFlag();

    /**
     * 工作起始
     */
    Double getHour_from();

    void setHour_from(Double hour_from);

    /**
     * 获取 [工作起始]脏标记
     */
    boolean getHour_fromDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_to();

    void setDate_to(Timestamp date_to);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_toDirtyFlag();

    /**
     * 起始日期
     */
    Timestamp getDate_from();

    void setDate_from(Timestamp date_from);

    /**
     * 获取 [起始日期]脏标记
     */
    boolean getDate_fromDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 资源的日历
     */
    String getCalendar_id_text();

    void setCalendar_id_text(String calendar_id_text);

    /**
     * 获取 [资源的日历]脏标记
     */
    boolean getCalendar_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 资源的日历
     */
    Integer getCalendar_id();

    void setCalendar_id(Integer calendar_id);

    /**
     * 获取 [资源的日历]脏标记
     */
    boolean getCalendar_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
