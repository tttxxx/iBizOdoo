package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_templateClientModel;

/**
 * 实体[EMail模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_templateServiceImpl implements IMail_templateService {

    @Autowired
    mail_templateOdooClient mail_templateOdooClient;


    @Override
    public Mail_template get(Integer id) {
        mail_templateClientModel clientModel = new mail_templateClientModel();
        clientModel.setId(id);
		mail_templateOdooClient.get(clientModel);
        Mail_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mail_template et) {
        mail_templateClientModel clientModel = convert2Model(et,null);
		mail_templateOdooClient.update(clientModel);
        Mail_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_template> list){
    }

    @Override
    public boolean create(Mail_template et) {
        mail_templateClientModel clientModel = convert2Model(et,null);
		mail_templateOdooClient.create(clientModel);
        Mail_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_template> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_templateClientModel clientModel = new mail_templateClientModel();
        clientModel.setId(id);
		mail_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_template> searchDefault(Mail_templateSearchContext context) {
        List<Mail_template> list = new ArrayList<Mail_template>();
        Page<mail_templateClientModel> clientModelList = mail_templateOdooClient.search(context);
        for(mail_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_templateClientModel convert2Model(Mail_template domain , mail_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("use_default_todirtyflag"))
                model.setUse_default_to(domain.getUseDefaultTo());
            if((Boolean) domain.getExtensionparams().get("subjectdirtyflag"))
                model.setSubject(domain.getSubject());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("sub_objectdirtyflag"))
                model.setSub_object(domain.getSubObject());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("model_iddirtyflag"))
                model.setModel_id(domain.getModelId());
            if((Boolean) domain.getExtensionparams().get("mail_server_iddirtyflag"))
                model.setMail_server_id(domain.getMailServerId());
            if((Boolean) domain.getExtensionparams().get("report_templatedirtyflag"))
                model.setReport_template(domain.getReportTemplate());
            if((Boolean) domain.getExtensionparams().get("body_htmldirtyflag"))
                model.setBody_html(domain.getBodyHtml());
            if((Boolean) domain.getExtensionparams().get("ref_ir_act_windowdirtyflag"))
                model.setRef_ir_act_window(domain.getRefIrActWindow());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("partner_todirtyflag"))
                model.setPartner_to(domain.getPartnerTo());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("auto_deletedirtyflag"))
                model.setAuto_delete(domain.getAutoDelete());
            if((Boolean) domain.getExtensionparams().get("email_ccdirtyflag"))
                model.setEmail_cc(domain.getEmailCc());
            if((Boolean) domain.getExtensionparams().get("report_namedirtyflag"))
                model.setReport_name(domain.getReportName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("model_object_fielddirtyflag"))
                model.setModel_object_field(domain.getModelObjectField());
            if((Boolean) domain.getExtensionparams().get("email_todirtyflag"))
                model.setEmail_to(domain.getEmailTo());
            if((Boolean) domain.getExtensionparams().get("null_valuedirtyflag"))
                model.setNull_value(domain.getNullValue());
            if((Boolean) domain.getExtensionparams().get("copyvaluedirtyflag"))
                model.setCopyvalue(domain.getCopyvalue());
            if((Boolean) domain.getExtensionparams().get("user_signaturedirtyflag"))
                model.setUser_signature(domain.getUserSignature());
            if((Boolean) domain.getExtensionparams().get("sub_model_object_fielddirtyflag"))
                model.setSub_model_object_field(domain.getSubModelObjectField());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("langdirtyflag"))
                model.setLang(domain.getLang());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_template convert2Domain( mail_templateClientModel model ,Mail_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_template();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getUse_default_toDirtyFlag())
            domain.setUseDefaultTo(model.getUse_default_to());
        if(model.getSubjectDirtyFlag())
            domain.setSubject(model.getSubject());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getSub_objectDirtyFlag())
            domain.setSubObject(model.getSub_object());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getModel_idDirtyFlag())
            domain.setModelId(model.getModel_id());
        if(model.getMail_server_idDirtyFlag())
            domain.setMailServerId(model.getMail_server_id());
        if(model.getReport_templateDirtyFlag())
            domain.setReportTemplate(model.getReport_template());
        if(model.getBody_htmlDirtyFlag())
            domain.setBodyHtml(model.getBody_html());
        if(model.getRef_ir_act_windowDirtyFlag())
            domain.setRefIrActWindow(model.getRef_ir_act_window());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPartner_toDirtyFlag())
            domain.setPartnerTo(model.getPartner_to());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getAuto_deleteDirtyFlag())
            domain.setAutoDelete(model.getAuto_delete());
        if(model.getEmail_ccDirtyFlag())
            domain.setEmailCc(model.getEmail_cc());
        if(model.getReport_nameDirtyFlag())
            domain.setReportName(model.getReport_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getModel_object_fieldDirtyFlag())
            domain.setModelObjectField(model.getModel_object_field());
        if(model.getEmail_toDirtyFlag())
            domain.setEmailTo(model.getEmail_to());
        if(model.getNull_valueDirtyFlag())
            domain.setNullValue(model.getNull_value());
        if(model.getCopyvalueDirtyFlag())
            domain.setCopyvalue(model.getCopyvalue());
        if(model.getUser_signatureDirtyFlag())
            domain.setUserSignature(model.getUser_signature());
        if(model.getSub_model_object_fieldDirtyFlag())
            domain.setSubModelObjectField(model.getSub_model_object_field());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLangDirtyFlag())
            domain.setLang(model.getLang());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



