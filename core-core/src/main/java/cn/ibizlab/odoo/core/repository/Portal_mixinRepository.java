package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;

/**
 * 实体 [门户Mixin] 存储对象
 */
public interface Portal_mixinRepository extends Repository<Portal_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context);

    Portal_mixin convert2PO(cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin domain , Portal_mixin po) ;

    cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin convert2Domain( Portal_mixin po ,cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin domain) ;

}
