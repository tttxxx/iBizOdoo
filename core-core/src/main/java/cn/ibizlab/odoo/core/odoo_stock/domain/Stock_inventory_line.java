package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存明细] 对象
 */
@Data
public class Stock_inventory_line extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 理论数量
     */
    @DEField(name = "theoretical_qty")
    @JSONField(name = "theoretical_qty")
    @JsonProperty("theoretical_qty")
    private Double theoreticalQty;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 已检查的数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 包裹
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;

    /**
     * 度量单位分类
     */
    @JSONField(name = "product_uom_category_id")
    @JsonProperty("product_uom_category_id")
    private Integer productUomCategoryId;

    /**
     * 库存
     */
    @JSONField(name = "inventory_id_text")
    @JsonProperty("inventory_id_text")
    private String inventoryIdText;

    /**
     * 库存位置
     */
    @JSONField(name = "inventory_location_id")
    @JsonProperty("inventory_location_id")
    private Integer inventoryLocationId;

    /**
     * 所有者
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 产品量度单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 批次/序列号码
     */
    @JSONField(name = "prod_lot_id_text")
    @JsonProperty("prod_lot_id_text")
    private String prodLotIdText;

    /**
     * 追踪
     */
    @JSONField(name = "product_tracking")
    @JsonProperty("product_tracking")
    private String productTracking;

    /**
     * 位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 批次/序列号码
     */
    @DEField(name = "prod_lot_id")
    @JSONField(name = "prod_lot_id")
    @JsonProperty("prod_lot_id")
    private Integer prodLotId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 包裹
     */
    @DEField(name = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Integer packageId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 所有者
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 库存
     */
    @DEField(name = "inventory_id")
    @JSONField(name = "inventory_id")
    @JsonProperty("inventory_id")
    private Integer inventoryId;

    /**
     * 产品量度单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;


    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odooinventory")
    @JsonProperty("odooinventory")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory odooInventory;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odooprodlot")
    @JsonProperty("odooprodlot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooProdLot;

    /**
     * 
     */
    @JSONField(name = "odoopackage")
    @JsonProperty("odoopackage")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package odooPackage;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [理论数量]
     */
    public void setTheoreticalQty(Double theoreticalQty){
        this.theoreticalQty = theoreticalQty ;
        this.modify("theoretical_qty",theoreticalQty);
    }
    /**
     * 设置 [已检查的数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [批次/序列号码]
     */
    public void setProdLotId(Integer prodLotId){
        this.prodLotId = prodLotId ;
        this.modify("prod_lot_id",prodLotId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [包裹]
     */
    public void setPackageId(Integer packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }
    /**
     * 设置 [所有者]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [库存]
     */
    public void setInventoryId(Integer inventoryId){
        this.inventoryId = inventoryId ;
        this.modify("inventory_id",inventoryId);
    }
    /**
     * 设置 [产品量度单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

}


