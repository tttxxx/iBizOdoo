package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [税科目调整映射会计模板] 对象
 */
@Data
public class Account_fiscal_position_account_template extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 财务映射
     */
    @JSONField(name = "position_id_text")
    @JsonProperty("position_id_text")
    private String positionIdText;

    /**
     * 科目目标
     */
    @JSONField(name = "account_dest_id_text")
    @JsonProperty("account_dest_id_text")
    private String accountDestIdText;

    /**
     * 科目来源
     */
    @JSONField(name = "account_src_id_text")
    @JsonProperty("account_src_id_text")
    private String accountSrcIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 科目目标
     */
    @DEField(name = "account_dest_id")
    @JSONField(name = "account_dest_id")
    @JsonProperty("account_dest_id")
    private Integer accountDestId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 科目来源
     */
    @DEField(name = "account_src_id")
    @JSONField(name = "account_src_id")
    @JsonProperty("account_src_id")
    private Integer accountSrcId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 财务映射
     */
    @DEField(name = "position_id")
    @JSONField(name = "position_id")
    @JsonProperty("position_id")
    private Integer positionId;


    /**
     * 
     */
    @JSONField(name = "odooaccountdest")
    @JsonProperty("odooaccountdest")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooAccountDest;

    /**
     * 
     */
    @JSONField(name = "odooaccountsrc")
    @JsonProperty("odooaccountsrc")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooAccountSrc;

    /**
     * 
     */
    @JSONField(name = "odooposition")
    @JsonProperty("odooposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_template odooPosition;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [科目目标]
     */
    public void setAccountDestId(Integer accountDestId){
        this.accountDestId = accountDestId ;
        this.modify("account_dest_id",accountDestId);
    }
    /**
     * 设置 [科目来源]
     */
    public void setAccountSrcId(Integer accountSrcId){
        this.accountSrcId = accountSrcId ;
        this.modify("account_src_id",accountSrcId);
    }
    /**
     * 设置 [财务映射]
     */
    public void setPositionId(Integer positionId){
        this.positionId = positionId ;
        this.modify("position_id",positionId);
    }

}


