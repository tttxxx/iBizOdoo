package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_user_input;

/**
 * 实体[survey_user_input] 服务对象接口
 */
public interface survey_user_inputRepository{


    public survey_user_input createPO() ;
        public void remove(String id);

        public void create(survey_user_input survey_user_input);

        public void createBatch(survey_user_input survey_user_input);

        public void update(survey_user_input survey_user_input);

        public void updateBatch(survey_user_input survey_user_input);

        public List<survey_user_input> search();

        public void get(String id);

        public void removeBatch(String id);


}
