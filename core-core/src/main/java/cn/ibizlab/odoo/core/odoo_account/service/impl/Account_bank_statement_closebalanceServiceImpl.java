package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_closebalanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_closebalanceOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_bank_statement_closebalanceClientModel;

/**
 * 实体[银行对账单期末余额] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_closebalanceServiceImpl implements IAccount_bank_statement_closebalanceService {

    @Autowired
    account_bank_statement_closebalanceOdooClient account_bank_statement_closebalanceOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_bank_statement_closebalanceClientModel clientModel = new account_bank_statement_closebalanceClientModel();
        clientModel.setId(id);
		account_bank_statement_closebalanceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_bank_statement_closebalance et) {
        account_bank_statement_closebalanceClientModel clientModel = convert2Model(et,null);
		account_bank_statement_closebalanceOdooClient.create(clientModel);
        Account_bank_statement_closebalance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_closebalance> list){
    }

    @Override
    public Account_bank_statement_closebalance get(Integer id) {
        account_bank_statement_closebalanceClientModel clientModel = new account_bank_statement_closebalanceClientModel();
        clientModel.setId(id);
		account_bank_statement_closebalanceOdooClient.get(clientModel);
        Account_bank_statement_closebalance et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_bank_statement_closebalance();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_bank_statement_closebalance et) {
        account_bank_statement_closebalanceClientModel clientModel = convert2Model(et,null);
		account_bank_statement_closebalanceOdooClient.update(clientModel);
        Account_bank_statement_closebalance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_bank_statement_closebalance> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context) {
        List<Account_bank_statement_closebalance> list = new ArrayList<Account_bank_statement_closebalance>();
        Page<account_bank_statement_closebalanceClientModel> clientModelList = account_bank_statement_closebalanceOdooClient.search(context);
        for(account_bank_statement_closebalanceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_bank_statement_closebalance>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_bank_statement_closebalanceClientModel convert2Model(Account_bank_statement_closebalance domain , account_bank_statement_closebalanceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_bank_statement_closebalanceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_bank_statement_closebalance convert2Domain( account_bank_statement_closebalanceClientModel model ,Account_bank_statement_closebalance domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_bank_statement_closebalance();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



