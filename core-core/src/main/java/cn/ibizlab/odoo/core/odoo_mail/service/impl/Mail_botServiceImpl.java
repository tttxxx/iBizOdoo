package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_botService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_botOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_botClientModel;

/**
 * 实体[邮件机器人] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_botServiceImpl implements IMail_botService {

    @Autowired
    mail_botOdooClient mail_botOdooClient;


    @Override
    public boolean update(Mail_bot et) {
        mail_botClientModel clientModel = convert2Model(et,null);
		mail_botOdooClient.update(clientModel);
        Mail_bot rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_bot> list){
    }

    @Override
    public Mail_bot get(Integer id) {
        mail_botClientModel clientModel = new mail_botClientModel();
        clientModel.setId(id);
		mail_botOdooClient.get(clientModel);
        Mail_bot et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_bot();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_bot et) {
        mail_botClientModel clientModel = convert2Model(et,null);
		mail_botOdooClient.create(clientModel);
        Mail_bot rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_bot> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_botClientModel clientModel = new mail_botClientModel();
        clientModel.setId(id);
		mail_botOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_bot> searchDefault(Mail_botSearchContext context) {
        List<Mail_bot> list = new ArrayList<Mail_bot>();
        Page<mail_botClientModel> clientModelList = mail_botOdooClient.search(context);
        for(mail_botClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_bot>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_botClientModel convert2Model(Mail_bot domain , mail_botClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_botClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_bot convert2Domain( mail_botClientModel model ,Mail_bot domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_bot();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        return domain ;
    }

}

    



