package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_country_state;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_country_state] 服务对象接口
 */
public interface Ires_country_stateClientService{

    public Ires_country_state createModel() ;

    public void update(Ires_country_state res_country_state);

    public void createBatch(List<Ires_country_state> res_country_states);

    public void removeBatch(List<Ires_country_state> res_country_states);

    public void remove(Ires_country_state res_country_state);

    public void get(Ires_country_state res_country_state);

    public Page<Ires_country_state> search(SearchContext context);

    public void create(Ires_country_state res_country_state);

    public void updateBatch(List<Ires_country_state> res_country_states);

    public Page<Ires_country_state> select(SearchContext context);

}
