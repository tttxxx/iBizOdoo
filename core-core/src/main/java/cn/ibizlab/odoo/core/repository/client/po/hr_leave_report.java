package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_leave_report] 对象
 */
public interface hr_leave_report {

    public Integer getCategory_id();

    public void setCategory_id(Integer category_id);

    public String getCategory_id_text();

    public void setCategory_id_text(String category_id_text);

    public Timestamp getDate_from();

    public void setDate_from(Timestamp date_from);

    public Timestamp getDate_to();

    public void setDate_to(Timestamp date_to);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public Integer getHoliday_status_id();

    public void setHoliday_status_id(Integer holiday_status_id);

    public String getHoliday_status_id_text();

    public void setHoliday_status_id_text(String holiday_status_id_text);

    public String getHoliday_type();

    public void setHoliday_type(String holiday_type);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public Double getNumber_of_days();

    public void setNumber_of_days(Double number_of_days);

    public String getPayslip_status();

    public void setPayslip_status(String payslip_status);

    public String getState();

    public void setState(String state);

    public String getType();

    public void setType(String type);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
