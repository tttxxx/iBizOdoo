package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_unbuildService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_unbuildOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_unbuildClientModel;

/**
 * 实体[拆解单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_unbuildServiceImpl implements IMrp_unbuildService {

    @Autowired
    mrp_unbuildOdooClient mrp_unbuildOdooClient;


    @Override
    public boolean update(Mrp_unbuild et) {
        mrp_unbuildClientModel clientModel = convert2Model(et,null);
		mrp_unbuildOdooClient.update(clientModel);
        Mrp_unbuild rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_unbuild> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_unbuildClientModel clientModel = new mrp_unbuildClientModel();
        clientModel.setId(id);
		mrp_unbuildOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_unbuild get(Integer id) {
        mrp_unbuildClientModel clientModel = new mrp_unbuildClientModel();
        clientModel.setId(id);
		mrp_unbuildOdooClient.get(clientModel);
        Mrp_unbuild et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_unbuild();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mrp_unbuild et) {
        mrp_unbuildClientModel clientModel = convert2Model(et,null);
		mrp_unbuildOdooClient.create(clientModel);
        Mrp_unbuild rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_unbuild> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context) {
        List<Mrp_unbuild> list = new ArrayList<Mrp_unbuild>();
        Page<mrp_unbuildClientModel> clientModelList = mrp_unbuildOdooClient.search(context);
        for(mrp_unbuildClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_unbuild>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_unbuildClientModel convert2Model(Mrp_unbuild domain , mrp_unbuildClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_unbuildClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("consume_line_idsdirtyflag"))
                model.setConsume_line_ids(domain.getConsumeLineIds());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("produce_line_idsdirtyflag"))
                model.setProduce_line_ids(domain.getProduceLineIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("mo_id_textdirtyflag"))
                model.setMo_id_text(domain.getMoIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("has_trackingdirtyflag"))
                model.setHas_tracking(domain.getHasTracking());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("bom_iddirtyflag"))
                model.setBom_id(domain.getBomId());
            if((Boolean) domain.getExtensionparams().get("mo_iddirtyflag"))
                model.setMo_id(domain.getMoId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_unbuild convert2Domain( mrp_unbuildClientModel model ,Mrp_unbuild domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_unbuild();
        }

        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getConsume_line_idsDirtyFlag())
            domain.setConsumeLineIds(model.getConsume_line_ids());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getProduce_line_idsDirtyFlag())
            domain.setProduceLineIds(model.getProduce_line_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMo_id_textDirtyFlag())
            domain.setMoIdText(model.getMo_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getHas_trackingDirtyFlag())
            domain.setHasTracking(model.getHas_tracking());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getBom_idDirtyFlag())
            domain.setBomId(model.getBom_id());
        if(model.getMo_idDirtyFlag())
            domain.setMoId(model.getMo_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        return domain ;
    }

}

    



