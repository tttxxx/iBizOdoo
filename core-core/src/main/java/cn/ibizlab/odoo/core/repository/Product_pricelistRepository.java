package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;

/**
 * 实体 [价格表] 存储对象
 */
public interface Product_pricelistRepository extends Repository<Product_pricelist> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context);

    Product_pricelist convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist domain , Product_pricelist po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist convert2Domain( Product_pricelist po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist domain) ;

}
