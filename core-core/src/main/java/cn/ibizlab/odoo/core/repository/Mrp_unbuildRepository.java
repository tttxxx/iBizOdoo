package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;

/**
 * 实体 [拆解单] 存储对象
 */
public interface Mrp_unbuildRepository extends Repository<Mrp_unbuild> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context);

    Mrp_unbuild convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild domain , Mrp_unbuild po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild convert2Domain( Mrp_unbuild po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild domain) ;

}
