package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_production] 对象
 */
public interface mrp_production {

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getAvailability();

    public void setAvailability(String availability);

    public Integer getBom_id();

    public void setBom_id(Integer bom_id);

    public String getCheck_to_done();

    public void setCheck_to_done(String check_to_done);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public String getConsumed_less_than_planned();

    public void setConsumed_less_than_planned(String consumed_less_than_planned);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_finished();

    public void setDate_finished(Timestamp date_finished);

    public Timestamp getDate_planned_finished();

    public void setDate_planned_finished(Timestamp date_planned_finished);

    public Timestamp getDate_planned_start();

    public void setDate_planned_start(Timestamp date_planned_start);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public Integer getDelivery_count();

    public void setDelivery_count(Integer delivery_count);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFinished_move_line_ids();

    public void setFinished_move_line_ids(String finished_move_line_ids);

    public String getHas_moves();

    public void setHas_moves(String has_moves);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_locked();

    public void setIs_locked(String is_locked);

    public Integer getLocation_dest_id();

    public void setLocation_dest_id(Integer location_dest_id);

    public String getLocation_dest_id_text();

    public void setLocation_dest_id_text(String location_dest_id_text);

    public Integer getLocation_src_id();

    public void setLocation_src_id(Integer location_src_id);

    public String getLocation_src_id_text();

    public void setLocation_src_id_text(String location_src_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getMove_dest_ids();

    public void setMove_dest_ids(String move_dest_ids);

    public String getMove_finished_ids();

    public void setMove_finished_ids(String move_finished_ids);

    public String getMove_raw_ids();

    public void setMove_raw_ids(String move_raw_ids);

    public String getName();

    public void setName(String name);

    public String getOrigin();

    public void setOrigin(String origin);

    public String getPicking_ids();

    public void setPicking_ids(String picking_ids);

    public Integer getPicking_type_id();

    public void setPicking_type_id(Integer picking_type_id);

    public String getPicking_type_id_text();

    public void setPicking_type_id_text(String picking_type_id_text);

    public String getPost_visible();

    public void setPost_visible(String post_visible);

    public String getPriority();

    public void setPriority(String priority);

    public Integer getProduction_location_id();

    public void setProduction_location_id(Integer production_location_id);

    public String getProduction_location_id_text();

    public void setProduction_location_id_text(String production_location_id_text);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Double getProduct_qty();

    public void setProduct_qty(Double product_qty);

    public Integer getProduct_tmpl_id();

    public void setProduct_tmpl_id(Integer product_tmpl_id);

    public String getProduct_tmpl_id_text();

    public void setProduct_tmpl_id_text(String product_tmpl_id_text);

    public Integer getProduct_uom_id();

    public void setProduct_uom_id(Integer product_uom_id);

    public String getProduct_uom_id_text();

    public void setProduct_uom_id_text(String product_uom_id_text);

    public Double getProduct_uom_qty();

    public void setProduct_uom_qty(Double product_uom_qty);

    public String getPropagate();

    public void setPropagate(String propagate);

    public Double getQty_produced();

    public void setQty_produced(Double qty_produced);

    public Integer getRouting_id();

    public void setRouting_id(Integer routing_id);

    public String getRouting_id_text();

    public void setRouting_id_text(String routing_id_text);

    public Integer getScrap_count();

    public void setScrap_count(Integer scrap_count);

    public String getScrap_ids();

    public void setScrap_ids(String scrap_ids);

    public String getShow_final_lots();

    public void setShow_final_lots(String show_final_lots);

    public String getState();

    public void setState(String state);

    public String getUnreserve_visible();

    public void setUnreserve_visible(String unreserve_visible);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Integer getWorkorder_count();

    public void setWorkorder_count(Integer workorder_count);

    public Integer getWorkorder_done_count();

    public void setWorkorder_done_count(Integer workorder_done_count);

    public String getWorkorder_ids();

    public void setWorkorder_ids(String workorder_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
