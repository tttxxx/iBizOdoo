package cn.ibizlab.odoo.core.odoo_mrp.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mrp_product_produce_line] 对象
 */
public class mrp_product_produce_lineClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public Integer lot_id;

    @JsonIgnore
    public boolean lot_idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public String lot_id_text;

    @JsonIgnore
    public boolean lot_id_textDirtyFlag;
    
    /**
     * 分录
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 分录
     */
    public String move_id_text;

    @JsonIgnore
    public boolean move_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品生产
     */
    public Integer product_produce_id;

    @JsonIgnore
    public boolean product_produce_idDirtyFlag;
    
    /**
     * 追踪
     */
    public String product_tracking;

    @JsonIgnore
    public boolean product_trackingDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 消耗的
     */
    public Double qty_done;

    @JsonIgnore
    public boolean qty_doneDirtyFlag;
    
    /**
     * 已预留
     */
    public Double qty_reserved;

    @JsonIgnore
    public boolean qty_reservedDirtyFlag;
    
    /**
     * 待消耗
     */
    public Double qty_to_consume;

    @JsonIgnore
    public boolean qty_to_consumeDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return this.lot_id ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return this.lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return this.lot_id_text ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return this.lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分录]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [分录]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [分录]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return this.move_id_text ;
    }

    /**
     * 设置 [分录]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return this.move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品生产]
     */
    @JsonProperty("product_produce_id")
    public Integer getProduct_produce_id(){
        return this.product_produce_id ;
    }

    /**
     * 设置 [产品生产]
     */
    @JsonProperty("product_produce_id")
    public void setProduct_produce_id(Integer  product_produce_id){
        this.product_produce_id = product_produce_id ;
        this.product_produce_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品生产]脏标记
     */
    @JsonIgnore
    public boolean getProduct_produce_idDirtyFlag(){
        return this.product_produce_idDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("product_tracking")
    public String getProduct_tracking(){
        return this.product_tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("product_tracking")
    public void setProduct_tracking(String  product_tracking){
        this.product_tracking = product_tracking ;
        this.product_trackingDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getProduct_trackingDirtyFlag(){
        return this.product_trackingDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [消耗的]
     */
    @JsonProperty("qty_done")
    public Double getQty_done(){
        return this.qty_done ;
    }

    /**
     * 设置 [消耗的]
     */
    @JsonProperty("qty_done")
    public void setQty_done(Double  qty_done){
        this.qty_done = qty_done ;
        this.qty_doneDirtyFlag = true ;
    }

     /**
     * 获取 [消耗的]脏标记
     */
    @JsonIgnore
    public boolean getQty_doneDirtyFlag(){
        return this.qty_doneDirtyFlag ;
    }   

    /**
     * 获取 [已预留]
     */
    @JsonProperty("qty_reserved")
    public Double getQty_reserved(){
        return this.qty_reserved ;
    }

    /**
     * 设置 [已预留]
     */
    @JsonProperty("qty_reserved")
    public void setQty_reserved(Double  qty_reserved){
        this.qty_reserved = qty_reserved ;
        this.qty_reservedDirtyFlag = true ;
    }

     /**
     * 获取 [已预留]脏标记
     */
    @JsonIgnore
    public boolean getQty_reservedDirtyFlag(){
        return this.qty_reservedDirtyFlag ;
    }   

    /**
     * 获取 [待消耗]
     */
    @JsonProperty("qty_to_consume")
    public Double getQty_to_consume(){
        return this.qty_to_consume ;
    }

    /**
     * 设置 [待消耗]
     */
    @JsonProperty("qty_to_consume")
    public void setQty_to_consume(Double  qty_to_consume){
        this.qty_to_consume = qty_to_consume ;
        this.qty_to_consumeDirtyFlag = true ;
    }

     /**
     * 获取 [待消耗]脏标记
     */
    @JsonIgnore
    public boolean getQty_to_consumeDirtyFlag(){
        return this.qty_to_consumeDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 0){
				this.setLot_id((Integer)objs[0]);
			}
		}
		if(!(map.get("lot_id") instanceof Boolean)&& map.get("lot_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("lot_id");
			if(objs.length > 1){
				this.setLot_id_text((String)objs[1]);
			}
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 0){
				this.setMove_id((Integer)objs[0]);
			}
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 1){
				this.setMove_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_produce_id") instanceof Boolean)&& map.get("product_produce_id")!=null){
			Object[] objs = (Object[])map.get("product_produce_id");
			if(objs.length > 0){
				this.setProduct_produce_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_tracking") instanceof Boolean)&& map.get("product_tracking")!=null){
			this.setProduct_tracking((String)map.get("product_tracking"));
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 1){
				this.setProduct_uom_id_text((String)objs[1]);
			}
		}
		if(!(map.get("qty_done") instanceof Boolean)&& map.get("qty_done")!=null){
			this.setQty_done((Double)map.get("qty_done"));
		}
		if(!(map.get("qty_reserved") instanceof Boolean)&& map.get("qty_reserved")!=null){
			this.setQty_reserved((Double)map.get("qty_reserved"));
		}
		if(!(map.get("qty_to_consume") instanceof Boolean)&& map.get("qty_to_consume")!=null){
			this.setQty_to_consume((Double)map.get("qty_to_consume"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLot_id()!=null&&this.getLot_idDirtyFlag()){
			map.put("lot_id",this.getLot_id());
		}else if(this.getLot_idDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getLot_id_text()!=null&&this.getLot_id_textDirtyFlag()){
			//忽略文本外键lot_id_text
		}else if(this.getLot_id_textDirtyFlag()){
			map.put("lot_id",false);
		}
		if(this.getMove_id()!=null&&this.getMove_idDirtyFlag()){
			map.put("move_id",this.getMove_id());
		}else if(this.getMove_idDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getMove_id_text()!=null&&this.getMove_id_textDirtyFlag()){
			//忽略文本外键move_id_text
		}else if(this.getMove_id_textDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_produce_id()!=null&&this.getProduct_produce_idDirtyFlag()){
			map.put("product_produce_id",this.getProduct_produce_id());
		}else if(this.getProduct_produce_idDirtyFlag()){
			map.put("product_produce_id",false);
		}
		if(this.getProduct_tracking()!=null&&this.getProduct_trackingDirtyFlag()){
			map.put("product_tracking",this.getProduct_tracking());
		}else if(this.getProduct_trackingDirtyFlag()){
			map.put("product_tracking",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_id_text()!=null&&this.getProduct_uom_id_textDirtyFlag()){
			//忽略文本外键product_uom_id_text
		}else if(this.getProduct_uom_id_textDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getQty_done()!=null&&this.getQty_doneDirtyFlag()){
			map.put("qty_done",this.getQty_done());
		}else if(this.getQty_doneDirtyFlag()){
			map.put("qty_done",false);
		}
		if(this.getQty_reserved()!=null&&this.getQty_reservedDirtyFlag()){
			map.put("qty_reserved",this.getQty_reserved());
		}else if(this.getQty_reservedDirtyFlag()){
			map.put("qty_reserved",false);
		}
		if(this.getQty_to_consume()!=null&&this.getQty_to_consumeDirtyFlag()){
			map.put("qty_to_consume",this.getQty_to_consume());
		}else if(this.getQty_to_consumeDirtyFlag()){
			map.put("qty_to_consume",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
