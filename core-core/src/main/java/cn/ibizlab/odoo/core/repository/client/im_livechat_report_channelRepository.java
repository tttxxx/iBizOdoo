package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.im_livechat_report_channel;

/**
 * 实体[im_livechat_report_channel] 服务对象接口
 */
public interface im_livechat_report_channelRepository{


    public im_livechat_report_channel createPO() ;
        public List<im_livechat_report_channel> search();

        public void createBatch(im_livechat_report_channel im_livechat_report_channel);

        public void get(String id);

        public void create(im_livechat_report_channel im_livechat_report_channel);

        public void update(im_livechat_report_channel im_livechat_report_channel);

        public void remove(String id);

        public void updateBatch(im_livechat_report_channel im_livechat_report_channel);

        public void removeBatch(String id);


}
