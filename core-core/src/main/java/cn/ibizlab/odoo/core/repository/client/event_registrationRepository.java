package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_registration;

/**
 * 实体[event_registration] 服务对象接口
 */
public interface event_registrationRepository{


    public event_registration createPO() ;
        public void update(event_registration event_registration);

        public void updateBatch(event_registration event_registration);

        public void remove(String id);

        public void get(String id);

        public void create(event_registration event_registration);

        public void createBatch(event_registration event_registration);

        public void removeBatch(String id);

        public List<event_registration> search();


}
