package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_expense_sheet_register_payment_wizard;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
public interface hr_expense_sheet_register_payment_wizardRepository{


    public hr_expense_sheet_register_payment_wizard createPO() ;
        public void updateBatch(hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void update(hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void removeBatch(String id);

        public void create(hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public List<hr_expense_sheet_register_payment_wizard> search();

        public void remove(String id);

        public void createBatch(hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void get(String id);


}
