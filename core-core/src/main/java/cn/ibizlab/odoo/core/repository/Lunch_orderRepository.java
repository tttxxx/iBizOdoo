package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;

/**
 * 实体 [午餐订单] 存储对象
 */
public interface Lunch_orderRepository extends Repository<Lunch_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_order> searchDefault(Lunch_orderSearchContext context);

    Lunch_order convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order domain , Lunch_order po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order convert2Domain( Lunch_order po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order domain) ;

}
