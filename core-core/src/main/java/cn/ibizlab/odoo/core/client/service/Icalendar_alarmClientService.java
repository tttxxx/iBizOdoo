package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_alarm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_alarm] 服务对象接口
 */
public interface Icalendar_alarmClientService{

    public Icalendar_alarm createModel() ;

    public void updateBatch(List<Icalendar_alarm> calendar_alarms);

    public void create(Icalendar_alarm calendar_alarm);

    public void update(Icalendar_alarm calendar_alarm);

    public void remove(Icalendar_alarm calendar_alarm);

    public Page<Icalendar_alarm> search(SearchContext context);

    public void createBatch(List<Icalendar_alarm> calendar_alarms);

    public void removeBatch(List<Icalendar_alarm> calendar_alarms);

    public void get(Icalendar_alarm calendar_alarm);

    public Page<Icalendar_alarm> select(SearchContext context);

}
