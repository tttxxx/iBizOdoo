package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_campaignService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_utm.client.utm_campaignOdooClient;
import cn.ibizlab.odoo.core.odoo_utm.clientmodel.utm_campaignClientModel;

/**
 * 实体[UTM 营销活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_campaignServiceImpl implements IUtm_campaignService {

    @Autowired
    utm_campaignOdooClient utm_campaignOdooClient;


    @Override
    public boolean update(Utm_campaign et) {
        utm_campaignClientModel clientModel = convert2Model(et,null);
		utm_campaignOdooClient.update(clientModel);
        Utm_campaign rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Utm_campaign> list){
    }

    @Override
    public Utm_campaign get(Integer id) {
        utm_campaignClientModel clientModel = new utm_campaignClientModel();
        clientModel.setId(id);
		utm_campaignOdooClient.get(clientModel);
        Utm_campaign et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Utm_campaign();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        utm_campaignClientModel clientModel = new utm_campaignClientModel();
        clientModel.setId(id);
		utm_campaignOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Utm_campaign et) {
        utm_campaignClientModel clientModel = convert2Model(et,null);
		utm_campaignOdooClient.create(clientModel);
        Utm_campaign rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_campaign> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_campaign> searchDefault(Utm_campaignSearchContext context) {
        List<Utm_campaign> list = new ArrayList<Utm_campaign>();
        Page<utm_campaignClientModel> clientModelList = utm_campaignOdooClient.search(context);
        for(utm_campaignClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Utm_campaign>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public utm_campaignClientModel convert2Model(Utm_campaign domain , utm_campaignClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new utm_campaignClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Utm_campaign convert2Domain( utm_campaignClientModel model ,Utm_campaign domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Utm_campaign();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



