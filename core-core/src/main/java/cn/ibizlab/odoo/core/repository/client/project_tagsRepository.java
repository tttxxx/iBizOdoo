package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.project_tags;

/**
 * 实体[project_tags] 服务对象接口
 */
public interface project_tagsRepository{


    public project_tags createPO() ;
        public List<project_tags> search();

        public void remove(String id);

        public void update(project_tags project_tags);

        public void removeBatch(String id);

        public void create(project_tags project_tags);

        public void createBatch(project_tags project_tags);

        public void get(String id);

        public void updateBatch(project_tags project_tags);


}
