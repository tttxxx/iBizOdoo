package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;

/**
 * 实体 [产品价格历史列表] 存储对象
 */
public interface Product_price_historyRepository extends Repository<Product_price_history> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_price_history> searchDefault(Product_price_historySearchContext context);

    Product_price_history convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history domain , Product_price_history po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history convert2Domain( Product_price_history po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history domain) ;

}
