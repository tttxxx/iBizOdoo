package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_activity_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_activity_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_activity_reportClientModel;

/**
 * 实体[CRM活动分析] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_activity_reportServiceImpl implements ICrm_activity_reportService {

    @Autowired
    crm_activity_reportOdooClient crm_activity_reportOdooClient;


    @Override
    public boolean create(Crm_activity_report et) {
        crm_activity_reportClientModel clientModel = convert2Model(et,null);
		crm_activity_reportOdooClient.create(clientModel);
        Crm_activity_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_activity_report> list){
    }

    @Override
    public boolean update(Crm_activity_report et) {
        crm_activity_reportClientModel clientModel = convert2Model(et,null);
		crm_activity_reportOdooClient.update(clientModel);
        Crm_activity_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_activity_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_activity_reportClientModel clientModel = new crm_activity_reportClientModel();
        clientModel.setId(id);
		crm_activity_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Crm_activity_report get(Integer id) {
        crm_activity_reportClientModel clientModel = new crm_activity_reportClientModel();
        clientModel.setId(id);
		crm_activity_reportOdooClient.get(clientModel);
        Crm_activity_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_activity_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context) {
        List<Crm_activity_report> list = new ArrayList<Crm_activity_report>();
        Page<crm_activity_reportClientModel> clientModelList = crm_activity_reportOdooClient.search(context);
        for(crm_activity_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_activity_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_activity_reportClientModel convert2Model(Crm_activity_report domain , crm_activity_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_activity_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("probabilitydirtyflag"))
                model.setProbability(domain.getProbability());
            if((Boolean) domain.getExtensionparams().get("lead_typedirtyflag"))
                model.setLead_type(domain.getLeadType());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("subjectdirtyflag"))
                model.setSubject(domain.getSubject());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_id_textdirtyflag"))
                model.setMail_activity_type_id_text(domain.getMailActivityTypeIdText());
            if((Boolean) domain.getExtensionparams().get("lead_id_textdirtyflag"))
                model.setLead_id_text(domain.getLeadIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("subtype_id_textdirtyflag"))
                model.setSubtype_id_text(domain.getSubtypeIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("author_id_textdirtyflag"))
                model.setAuthor_id_text(domain.getAuthorIdText());
            if((Boolean) domain.getExtensionparams().get("author_iddirtyflag"))
                model.setAuthor_id(domain.getAuthorId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("subtype_iddirtyflag"))
                model.setSubtype_id(domain.getSubtypeId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("lead_iddirtyflag"))
                model.setLead_id(domain.getLeadId());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_iddirtyflag"))
                model.setMail_activity_type_id(domain.getMailActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_activity_report convert2Domain( crm_activity_reportClientModel model ,Crm_activity_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_activity_report();
        }

        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getProbabilityDirtyFlag())
            domain.setProbability(model.getProbability());
        if(model.getLead_typeDirtyFlag())
            domain.setLeadType(model.getLead_type());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getSubjectDirtyFlag())
            domain.setSubject(model.getSubject());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getMail_activity_type_id_textDirtyFlag())
            domain.setMailActivityTypeIdText(model.getMail_activity_type_id_text());
        if(model.getLead_id_textDirtyFlag())
            domain.setLeadIdText(model.getLead_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getSubtype_id_textDirtyFlag())
            domain.setSubtypeIdText(model.getSubtype_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getAuthor_id_textDirtyFlag())
            domain.setAuthorIdText(model.getAuthor_id_text());
        if(model.getAuthor_idDirtyFlag())
            domain.setAuthorId(model.getAuthor_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getSubtype_idDirtyFlag())
            domain.setSubtypeId(model.getSubtype_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getLead_idDirtyFlag())
            domain.setLeadId(model.getLead_id());
        if(model.getMail_activity_type_idDirtyFlag())
            domain.setMailActivityTypeId(model.getMail_activity_type_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        return domain ;
    }

}

    



