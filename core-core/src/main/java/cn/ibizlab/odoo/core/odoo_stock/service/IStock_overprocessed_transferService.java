package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;


/**
 * 实体[Stock_overprocessed_transfer] 服务对象接口
 */
public interface IStock_overprocessed_transferService{

    boolean create(Stock_overprocessed_transfer et) ;
    void createBatch(List<Stock_overprocessed_transfer> list) ;
    Stock_overprocessed_transfer get(Integer key) ;
    boolean update(Stock_overprocessed_transfer et) ;
    void updateBatch(List<Stock_overprocessed_transfer> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_overprocessed_transfer> searchDefault(Stock_overprocessed_transferSearchContext context) ;

}



