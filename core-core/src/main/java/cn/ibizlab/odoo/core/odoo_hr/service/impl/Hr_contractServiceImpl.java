package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contractService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_contractOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_contractClientModel;

/**
 * 实体[Contract] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_contractServiceImpl implements IHr_contractService {

    @Autowired
    hr_contractOdooClient hr_contractOdooClient;


    @Override
    public boolean remove(Integer id) {
        hr_contractClientModel clientModel = new hr_contractClientModel();
        clientModel.setId(id);
		hr_contractOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Hr_contract et) {
        hr_contractClientModel clientModel = convert2Model(et,null);
		hr_contractOdooClient.update(clientModel);
        Hr_contract rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_contract> list){
    }

    @Override
    public boolean create(Hr_contract et) {
        hr_contractClientModel clientModel = convert2Model(et,null);
		hr_contractOdooClient.create(clientModel);
        Hr_contract rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_contract> list){
    }

    @Override
    public Hr_contract get(Integer id) {
        hr_contractClientModel clientModel = new hr_contractClientModel();
        clientModel.setId(id);
		hr_contractOdooClient.get(clientModel);
        Hr_contract et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_contract();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_contract> searchDefault(Hr_contractSearchContext context) {
        List<Hr_contract> list = new ArrayList<Hr_contract>();
        Page<hr_contractClientModel> clientModelList = hr_contractOdooClient.search(context);
        for(hr_contractClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_contract>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_contractClientModel convert2Model(Hr_contract domain , hr_contractClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_contractClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("advantagesdirtyflag"))
                model.setAdvantages(domain.getAdvantages());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("wagedirtyflag"))
                model.setWage(domain.getWage());
            if((Boolean) domain.getExtensionparams().get("reported_to_secretariatdirtyflag"))
                model.setReported_to_secretariat(domain.getReportedToSecretariat());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("trial_date_enddirtyflag"))
                model.setTrial_date_end(domain.getTrialDateEnd());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("permit_nodirtyflag"))
                model.setPermit_no(domain.getPermitNo());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("job_id_textdirtyflag"))
                model.setJob_id_text(domain.getJobIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("type_id_textdirtyflag"))
                model.setType_id_text(domain.getTypeIdText());
            if((Boolean) domain.getExtensionparams().get("visa_expiredirtyflag"))
                model.setVisa_expire(domain.getVisaExpire());
            if((Boolean) domain.getExtensionparams().get("visa_nodirtyflag"))
                model.setVisa_no(domain.getVisaNo());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("job_iddirtyflag"))
                model.setJob_id(domain.getJobId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("type_iddirtyflag"))
                model.setType_id(domain.getTypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_contract convert2Domain( hr_contractClientModel model ,Hr_contract domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_contract();
        }

        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getAdvantagesDirtyFlag())
            domain.setAdvantages(model.getAdvantages());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWageDirtyFlag())
            domain.setWage(model.getWage());
        if(model.getReported_to_secretariatDirtyFlag())
            domain.setReportedToSecretariat(model.getReported_to_secretariat());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getTrial_date_endDirtyFlag())
            domain.setTrialDateEnd(model.getTrial_date_end());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPermit_noDirtyFlag())
            domain.setPermitNo(model.getPermit_no());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getJob_id_textDirtyFlag())
            domain.setJobIdText(model.getJob_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getType_id_textDirtyFlag())
            domain.setTypeIdText(model.getType_id_text());
        if(model.getVisa_expireDirtyFlag())
            domain.setVisaExpire(model.getVisa_expire());
        if(model.getVisa_noDirtyFlag())
            domain.setVisaNo(model.getVisa_no());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getJob_idDirtyFlag())
            domain.setJobId(model.getJob_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getType_idDirtyFlag())
            domain.setTypeId(model.getType_id());
        return domain ;
    }

}

    



