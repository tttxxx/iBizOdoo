package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [gamification_challenge] 对象
 */
public interface gamification_challenge {

    public String getCategory();

    public void setCategory(String category);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Timestamp getEnd_date();

    public void setEnd_date(Timestamp end_date);

    public Integer getId();

    public void setId(Integer id);

    public String getInvited_user_ids();

    public void setInvited_user_ids(String invited_user_ids);

    public Timestamp getLast_report_date();

    public void setLast_report_date(Timestamp last_report_date);

    public String getLine_ids();

    public void setLine_ids(String line_ids);

    public Integer getManager_id();

    public void setManager_id(Integer manager_id);

    public String getManager_id_text();

    public void setManager_id_text(String manager_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Timestamp getNext_report_date();

    public void setNext_report_date(Timestamp next_report_date);

    public String getPeriod();

    public void setPeriod(String period);

    public Integer getRemind_update_delay();

    public void setRemind_update_delay(Integer remind_update_delay);

    public String getReport_message_frequency();

    public void setReport_message_frequency(String report_message_frequency);

    public Integer getReport_message_group_id();

    public void setReport_message_group_id(Integer report_message_group_id);

    public String getReport_message_group_id_text();

    public void setReport_message_group_id_text(String report_message_group_id_text);

    public Integer getReport_template_id();

    public void setReport_template_id(Integer report_template_id);

    public String getReport_template_id_text();

    public void setReport_template_id_text(String report_template_id_text);

    public String getReward_failure();

    public void setReward_failure(String reward_failure);

    public Integer getReward_first_id();

    public void setReward_first_id(Integer reward_first_id);

    public String getReward_first_id_text();

    public void setReward_first_id_text(String reward_first_id_text);

    public Integer getReward_id();

    public void setReward_id(Integer reward_id);

    public String getReward_id_text();

    public void setReward_id_text(String reward_id_text);

    public String getReward_realtime();

    public void setReward_realtime(String reward_realtime);

    public Integer getReward_second_id();

    public void setReward_second_id(Integer reward_second_id);

    public String getReward_second_id_text();

    public void setReward_second_id_text(String reward_second_id_text);

    public Integer getReward_third_id();

    public void setReward_third_id(Integer reward_third_id);

    public String getReward_third_id_text();

    public void setReward_third_id_text(String reward_third_id_text);

    public Timestamp getStart_date();

    public void setStart_date(Timestamp start_date);

    public String getState();

    public void setState(String state);

    public String getUser_domain();

    public void setUser_domain(String user_domain);

    public String getUser_ids();

    public void setUser_ids(String user_ids);

    public String getVisibility_mode();

    public void setVisibility_mode(String visibility_mode);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
