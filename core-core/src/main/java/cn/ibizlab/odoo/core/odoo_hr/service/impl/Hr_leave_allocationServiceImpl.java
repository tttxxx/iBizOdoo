package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_allocationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_allocationOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_leave_allocationClientModel;

/**
 * 实体[休假分配] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_allocationServiceImpl implements IHr_leave_allocationService {

    @Autowired
    hr_leave_allocationOdooClient hr_leave_allocationOdooClient;


    @Override
    public Hr_leave_allocation get(Integer id) {
        hr_leave_allocationClientModel clientModel = new hr_leave_allocationClientModel();
        clientModel.setId(id);
		hr_leave_allocationOdooClient.get(clientModel);
        Hr_leave_allocation et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_leave_allocation();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        hr_leave_allocationClientModel clientModel = new hr_leave_allocationClientModel();
        clientModel.setId(id);
		hr_leave_allocationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Hr_leave_allocation et) {
        hr_leave_allocationClientModel clientModel = convert2Model(et,null);
		hr_leave_allocationOdooClient.update(clientModel);
        Hr_leave_allocation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_leave_allocation> list){
    }

    @Override
    public boolean create(Hr_leave_allocation et) {
        hr_leave_allocationClientModel clientModel = convert2Model(et,null);
		hr_leave_allocationOdooClient.create(clientModel);
        Hr_leave_allocation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_allocation> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_allocation> searchDefault(Hr_leave_allocationSearchContext context) {
        List<Hr_leave_allocation> list = new ArrayList<Hr_leave_allocation>();
        Page<hr_leave_allocationClientModel> clientModelList = hr_leave_allocationOdooClient.search(context);
        for(hr_leave_allocationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_leave_allocation>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_leave_allocationClientModel convert2Model(Hr_leave_allocation domain , hr_leave_allocationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_leave_allocationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("number_per_intervaldirtyflag"))
                model.setNumber_per_interval(domain.getNumberPerInterval());
            if((Boolean) domain.getExtensionparams().get("number_of_days_displaydirtyflag"))
                model.setNumber_of_days_display(domain.getNumberOfDaysDisplay());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("holiday_typedirtyflag"))
                model.setHoliday_type(domain.getHolidayType());
            if((Boolean) domain.getExtensionparams().get("interval_numberdirtyflag"))
                model.setInterval_number(domain.getIntervalNumber());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("number_of_hours_displaydirtyflag"))
                model.setNumber_of_hours_display(domain.getNumberOfHoursDisplay());
            if((Boolean) domain.getExtensionparams().get("accrualdirtyflag"))
                model.setAccrual(domain.getAccrual());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("linked_request_idsdirtyflag"))
                model.setLinked_request_ids(domain.getLinkedRequestIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("nextcalldirtyflag"))
                model.setNextcall(domain.getNextcall());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("number_of_daysdirtyflag"))
                model.setNumber_of_days(domain.getNumberOfDays());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("interval_unitdirtyflag"))
                model.setInterval_unit(domain.getIntervalUnit());
            if((Boolean) domain.getExtensionparams().get("duration_displaydirtyflag"))
                model.setDuration_display(domain.getDurationDisplay());
            if((Boolean) domain.getExtensionparams().get("can_approvedirtyflag"))
                model.setCan_approve(domain.getCanApprove());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("can_resetdirtyflag"))
                model.setCan_reset(domain.getCanReset());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("unit_per_intervaldirtyflag"))
                model.setUnit_per_interval(domain.getUnitPerInterval());
            if((Boolean) domain.getExtensionparams().get("accrual_limitdirtyflag"))
                model.setAccrual_limit(domain.getAccrualLimit());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("validation_typedirtyflag"))
                model.setValidation_type(domain.getValidationType());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("mode_company_id_textdirtyflag"))
                model.setMode_company_id_text(domain.getModeCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("first_approver_id_textdirtyflag"))
                model.setFirst_approver_id_text(domain.getFirstApproverIdText());
            if((Boolean) domain.getExtensionparams().get("holiday_status_id_textdirtyflag"))
                model.setHoliday_status_id_text(domain.getHolidayStatusIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("type_request_unitdirtyflag"))
                model.setType_request_unit(domain.getTypeRequestUnit());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("second_approver_id_textdirtyflag"))
                model.setSecond_approver_id_text(domain.getSecondApproverIdText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("holiday_status_iddirtyflag"))
                model.setHoliday_status_id(domain.getHolidayStatusId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("second_approver_iddirtyflag"))
                model.setSecond_approver_id(domain.getSecondApproverId());
            if((Boolean) domain.getExtensionparams().get("mode_company_iddirtyflag"))
                model.setMode_company_id(domain.getModeCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("first_approver_iddirtyflag"))
                model.setFirst_approver_id(domain.getFirstApproverId());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_leave_allocation convert2Domain( hr_leave_allocationClientModel model ,Hr_leave_allocation domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_leave_allocation();
        }

        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getNumber_per_intervalDirtyFlag())
            domain.setNumberPerInterval(model.getNumber_per_interval());
        if(model.getNumber_of_days_displayDirtyFlag())
            domain.setNumberOfDaysDisplay(model.getNumber_of_days_display());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getHoliday_typeDirtyFlag())
            domain.setHolidayType(model.getHoliday_type());
        if(model.getInterval_numberDirtyFlag())
            domain.setIntervalNumber(model.getInterval_number());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getNumber_of_hours_displayDirtyFlag())
            domain.setNumberOfHoursDisplay(model.getNumber_of_hours_display());
        if(model.getAccrualDirtyFlag())
            domain.setAccrual(model.getAccrual());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getLinked_request_idsDirtyFlag())
            domain.setLinkedRequestIds(model.getLinked_request_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getNextcallDirtyFlag())
            domain.setNextcall(model.getNextcall());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getNumber_of_daysDirtyFlag())
            domain.setNumberOfDays(model.getNumber_of_days());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getInterval_unitDirtyFlag())
            domain.setIntervalUnit(model.getInterval_unit());
        if(model.getDuration_displayDirtyFlag())
            domain.setDurationDisplay(model.getDuration_display());
        if(model.getCan_approveDirtyFlag())
            domain.setCanApprove(model.getCan_approve());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCan_resetDirtyFlag())
            domain.setCanReset(model.getCan_reset());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getUnit_per_intervalDirtyFlag())
            domain.setUnitPerInterval(model.getUnit_per_interval());
        if(model.getAccrual_limitDirtyFlag())
            domain.setAccrualLimit(model.getAccrual_limit());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getValidation_typeDirtyFlag())
            domain.setValidationType(model.getValidation_type());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMode_company_id_textDirtyFlag())
            domain.setModeCompanyIdText(model.getMode_company_id_text());
        if(model.getFirst_approver_id_textDirtyFlag())
            domain.setFirstApproverIdText(model.getFirst_approver_id_text());
        if(model.getHoliday_status_id_textDirtyFlag())
            domain.setHolidayStatusIdText(model.getHoliday_status_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getType_request_unitDirtyFlag())
            domain.setTypeRequestUnit(model.getType_request_unit());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getSecond_approver_id_textDirtyFlag())
            domain.setSecondApproverIdText(model.getSecond_approver_id_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getHoliday_status_idDirtyFlag())
            domain.setHolidayStatusId(model.getHoliday_status_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getSecond_approver_idDirtyFlag())
            domain.setSecondApproverId(model.getSecond_approver_id());
        if(model.getMode_company_idDirtyFlag())
            domain.setModeCompanyId(model.getMode_company_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getFirst_approver_idDirtyFlag())
            domain.setFirstApproverId(model.getFirst_approver_id());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        return domain ;
    }

}

    



