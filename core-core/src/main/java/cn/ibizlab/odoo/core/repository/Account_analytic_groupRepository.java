package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_analytic_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_groupSearchContext;

/**
 * 实体 [分析类别] 存储对象
 */
public interface Account_analytic_groupRepository extends Repository<Account_analytic_group> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_analytic_group> searchDefault(Account_analytic_groupSearchContext context);

    Account_analytic_group convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group domain , Account_analytic_group po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group convert2Domain( Account_analytic_group po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group domain) ;

}
