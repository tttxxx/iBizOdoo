package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.im_livechat_channel_rule;

/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
public interface im_livechat_channel_ruleRepository{


    public im_livechat_channel_rule createPO() ;
        public void createBatch(im_livechat_channel_rule im_livechat_channel_rule);

        public void removeBatch(String id);

        public void get(String id);

        public void update(im_livechat_channel_rule im_livechat_channel_rule);

        public void create(im_livechat_channel_rule im_livechat_channel_rule);

        public void remove(String id);

        public List<im_livechat_channel_rule> search();

        public void updateBatch(im_livechat_channel_rule im_livechat_channel_rule);


}
