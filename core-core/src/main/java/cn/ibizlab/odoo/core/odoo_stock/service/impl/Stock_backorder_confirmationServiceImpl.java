package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_backorder_confirmationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_backorder_confirmationOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_backorder_confirmationClientModel;

/**
 * 实体[欠单确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_backorder_confirmationServiceImpl implements IStock_backorder_confirmationService {

    @Autowired
    stock_backorder_confirmationOdooClient stock_backorder_confirmationOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_backorder_confirmationClientModel clientModel = new stock_backorder_confirmationClientModel();
        clientModel.setId(id);
		stock_backorder_confirmationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_backorder_confirmation get(Integer id) {
        stock_backorder_confirmationClientModel clientModel = new stock_backorder_confirmationClientModel();
        clientModel.setId(id);
		stock_backorder_confirmationOdooClient.get(clientModel);
        Stock_backorder_confirmation et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_backorder_confirmation();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Stock_backorder_confirmation et) {
        stock_backorder_confirmationClientModel clientModel = convert2Model(et,null);
		stock_backorder_confirmationOdooClient.update(clientModel);
        Stock_backorder_confirmation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_backorder_confirmation> list){
    }

    @Override
    public boolean create(Stock_backorder_confirmation et) {
        stock_backorder_confirmationClientModel clientModel = convert2Model(et,null);
		stock_backorder_confirmationOdooClient.create(clientModel);
        Stock_backorder_confirmation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_backorder_confirmation> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context) {
        List<Stock_backorder_confirmation> list = new ArrayList<Stock_backorder_confirmation>();
        Page<stock_backorder_confirmationClientModel> clientModelList = stock_backorder_confirmationOdooClient.search(context);
        for(stock_backorder_confirmationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_backorder_confirmation>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_backorder_confirmationClientModel convert2Model(Stock_backorder_confirmation domain , stock_backorder_confirmationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_backorder_confirmationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("pick_idsdirtyflag"))
                model.setPick_ids(domain.getPickIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_backorder_confirmation convert2Domain( stock_backorder_confirmationClientModel model ,Stock_backorder_confirmation domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_backorder_confirmation();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPick_idsDirtyFlag())
            domain.setPickIds(model.getPick_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



