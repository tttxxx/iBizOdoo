package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;

/**
 * 实体 [维护设备类别] 存储对象
 */
public interface Maintenance_equipment_categoryRepository extends Repository<Maintenance_equipment_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context);

    Maintenance_equipment_category convert2PO(cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category domain , Maintenance_equipment_category po) ;

    cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category convert2Domain( Maintenance_equipment_category po ,cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category domain) ;

}
