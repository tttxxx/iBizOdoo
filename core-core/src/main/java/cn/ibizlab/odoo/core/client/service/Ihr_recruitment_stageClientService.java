package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
public interface Ihr_recruitment_stageClientService{

    public Ihr_recruitment_stage createModel() ;

    public void updateBatch(List<Ihr_recruitment_stage> hr_recruitment_stages);

    public Page<Ihr_recruitment_stage> search(SearchContext context);

    public void create(Ihr_recruitment_stage hr_recruitment_stage);

    public void remove(Ihr_recruitment_stage hr_recruitment_stage);

    public void get(Ihr_recruitment_stage hr_recruitment_stage);

    public void createBatch(List<Ihr_recruitment_stage> hr_recruitment_stages);

    public void update(Ihr_recruitment_stage hr_recruitment_stage);

    public void removeBatch(List<Ihr_recruitment_stage> hr_recruitment_stages);

    public Page<Ihr_recruitment_stage> select(SearchContext context);

}
