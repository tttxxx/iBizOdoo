package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.repair_cancel;

/**
 * 实体[repair_cancel] 服务对象接口
 */
public interface repair_cancelRepository{


    public repair_cancel createPO() ;
        public void create(repair_cancel repair_cancel);

        public void createBatch(repair_cancel repair_cancel);

        public List<repair_cancel> search();

        public void get(String id);

        public void update(repair_cancel repair_cancel);

        public void updateBatch(repair_cancel repair_cancel);

        public void remove(String id);

        public void removeBatch(String id);


}
