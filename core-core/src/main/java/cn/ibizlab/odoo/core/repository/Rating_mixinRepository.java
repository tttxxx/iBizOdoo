package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;

/**
 * 实体 [混合评级] 存储对象
 */
public interface Rating_mixinRepository extends Repository<Rating_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context);

    Rating_mixin convert2PO(cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin domain , Rating_mixin po) ;

    cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin convert2Domain( Rating_mixin po ,cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin domain) ;

}
