package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_job] 对象
 */
public interface hr_job {

    public Integer getAddress_id();

    public void setAddress_id(Integer address_id);

    public String getAddress_id_text();

    public void setAddress_id_text(String address_id_text);

    public String getAlias_contact();

    public void setAlias_contact(String alias_contact);

    public String getAlias_defaults();

    public void setAlias_defaults(String alias_defaults);

    public String getAlias_domain();

    public void setAlias_domain(String alias_domain);

    public Integer getAlias_force_thread_id();

    public void setAlias_force_thread_id(Integer alias_force_thread_id);

    public Integer getAlias_id();

    public void setAlias_id(Integer alias_id);

    public String getAlias_name();

    public void setAlias_name(String alias_name);

    public Integer getAlias_parent_thread_id();

    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    public Integer getAlias_user_id();

    public void setAlias_user_id(Integer alias_user_id);

    public String getAlias_user_id_text();

    public void setAlias_user_id_text(String alias_user_id_text);

    public Integer getApplication_count();

    public void setApplication_count(Integer application_count);

    public String getApplication_ids();

    public void setApplication_ids(String application_ids);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getDocuments_count();

    public void setDocuments_count(Integer documents_count);

    public String getDocument_ids();

    public void setDocument_ids(String document_ids);

    public String getEmployee_ids();

    public void setEmployee_ids(String employee_ids);

    public Integer getExpected_employees();

    public void setExpected_employees(Integer expected_employees);

    public Integer getHr_responsible_id();

    public void setHr_responsible_id(Integer hr_responsible_id);

    public String getHr_responsible_id_text();

    public void setHr_responsible_id_text(String hr_responsible_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_published();

    public void setIs_published(String is_published);

    public String getIs_seo_optimized();

    public void setIs_seo_optimized(String is_seo_optimized);

    public Integer getManager_id();

    public void setManager_id(Integer manager_id);

    public String getManager_id_text();

    public void setManager_id_text(String manager_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Integer getNo_of_employee();

    public void setNo_of_employee(Integer no_of_employee);

    public Integer getNo_of_hired_employee();

    public void setNo_of_hired_employee(Integer no_of_hired_employee);

    public Integer getNo_of_recruitment();

    public void setNo_of_recruitment(Integer no_of_recruitment);

    public String getRequirements();

    public void setRequirements(String requirements);

    public String getState();

    public void setState(String state);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_description();

    public void setWebsite_description(String website_description);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public String getWebsite_meta_description();

    public void setWebsite_meta_description(String website_meta_description);

    public String getWebsite_meta_keywords();

    public void setWebsite_meta_keywords(String website_meta_keywords);

    public String getWebsite_meta_og_img();

    public void setWebsite_meta_og_img(String website_meta_og_img);

    public String getWebsite_meta_title();

    public void setWebsite_meta_title(String website_meta_title);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
