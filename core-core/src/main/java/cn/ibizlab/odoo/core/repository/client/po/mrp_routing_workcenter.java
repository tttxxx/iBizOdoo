package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_routing_workcenter] 对象
 */
public interface mrp_routing_workcenter {

    public String getBatch();

    public void setBatch(String batch);

    public Double getBatch_size();

    public void setBatch_size(Double batch_size);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public Integer getRouting_id();

    public void setRouting_id(Integer routing_id);

    public String getRouting_id_text();

    public void setRouting_id_text(String routing_id_text);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Double getTime_cycle();

    public void setTime_cycle(Double time_cycle);

    public Double getTime_cycle_manual();

    public void setTime_cycle_manual(Double time_cycle_manual);

    public String getTime_mode();

    public void setTime_mode(String time_mode);

    public Integer getTime_mode_batch();

    public void setTime_mode_batch(Integer time_mode_batch);

    public Integer getWorkcenter_id();

    public void setWorkcenter_id(Integer workcenter_id);

    public String getWorkcenter_id_text();

    public void setWorkcenter_id_text(String workcenter_id_text);

    public Integer getWorkorder_count();

    public void setWorkorder_count(Integer workorder_count);

    public String getWorkorder_ids();

    public void setWorkorder_ids(String workorder_ids);

    public byte[] getWorksheet();

    public void setWorksheet(byte[] worksheet);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
