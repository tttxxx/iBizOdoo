package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;

/**
 * 实体 [完全核销] 存储模型
 */
public interface Account_full_reconcile{

    /**
     *  核销部分
     */
    String getPartial_reconcile_ids();

    void setPartial_reconcile_ids(String partial_reconcile_ids);

    /**
     * 获取 [ 核销部分]脏标记
     */
    boolean getPartial_reconcile_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 号码
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [号码]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 匹配的日记账项目
     */
    String getReconciled_line_ids();

    void setReconciled_line_ids(String reconciled_line_ids);

    /**
     * 获取 [匹配的日记账项目]脏标记
     */
    boolean getReconciled_line_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 外汇交易
     */
    String getExchange_move_id_text();

    void setExchange_move_id_text(String exchange_move_id_text);

    /**
     * 获取 [外汇交易]脏标记
     */
    boolean getExchange_move_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 外汇交易
     */
    Integer getExchange_move_id();

    void setExchange_move_id(Integer exchange_move_id);

    /**
     * 获取 [外汇交易]脏标记
     */
    boolean getExchange_move_idDirtyFlag();

}
