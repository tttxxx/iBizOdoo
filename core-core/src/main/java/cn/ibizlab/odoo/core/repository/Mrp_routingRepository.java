package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;

/**
 * 实体 [工艺] 存储对象
 */
public interface Mrp_routingRepository extends Repository<Mrp_routing> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context);

    Mrp_routing convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing domain , Mrp_routing po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing convert2Domain( Mrp_routing po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing domain) ;

}
