package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
public interface Iaccount_fiscal_position_accountClientService{

    public Iaccount_fiscal_position_account createModel() ;

    public void updateBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts);

    public void create(Iaccount_fiscal_position_account account_fiscal_position_account);

    public void createBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts);

    public void update(Iaccount_fiscal_position_account account_fiscal_position_account);

    public void get(Iaccount_fiscal_position_account account_fiscal_position_account);

    public void remove(Iaccount_fiscal_position_account account_fiscal_position_account);

    public Page<Iaccount_fiscal_position_account> search(SearchContext context);

    public void removeBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts);

    public Page<Iaccount_fiscal_position_account> select(SearchContext context);

}
