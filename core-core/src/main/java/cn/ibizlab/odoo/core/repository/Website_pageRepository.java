package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;

/**
 * 实体 [页] 存储对象
 */
public interface Website_pageRepository extends Repository<Website_page> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_page> searchDefault(Website_pageSearchContext context);

    Website_page convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_page domain , Website_page po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_page convert2Domain( Website_page po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_page domain) ;

}
