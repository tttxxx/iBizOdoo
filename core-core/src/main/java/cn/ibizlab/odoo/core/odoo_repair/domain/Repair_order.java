package cn.ibizlab.odoo.core.odoo_repair.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [维修单] 对象
 */
@Data
public class Repair_order extends EntityClient implements Serializable {

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 内部备注
     */
    @DEField(name = "internal_notes")
    @JSONField(name = "internal_notes")
    @JsonProperty("internal_notes")
    private String internalNotes;

    /**
     * 默认地址
     */
    @JSONField(name = "default_address_id")
    @JsonProperty("default_address_id")
    private Integer defaultAddressId;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 开票方式
     */
    @DEField(name = "invoice_method")
    @JSONField(name = "invoice_method")
    @JsonProperty("invoice_method")
    private String invoiceMethod;

    /**
     * 已开票
     */
    @JSONField(name = "invoiced")
    @JsonProperty("invoiced")
    private String invoiced;

    /**
     * 报价单说明
     */
    @DEField(name = "quotation_notes")
    @JSONField(name = "quotation_notes")
    @JsonProperty("quotation_notes")
    private String quotationNotes;

    /**
     * 维修参照
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 合计
     */
    @DEField(name = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 零件
     */
    @JSONField(name = "operations")
    @JsonProperty("operations")
    private String operations;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 作业
     */
    @JSONField(name = "fees_lines")
    @JsonProperty("fees_lines")
    private String feesLines;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 已维修
     */
    @JSONField(name = "repaired")
    @JsonProperty("repaired")
    private String repaired;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private Double amountUntaxed;

    /**
     * 质保到期
     */
    @DEField(name = "guarantee_limit")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "guarantee_limit" , format="yyyy-MM-dd")
    @JsonProperty("guarantee_limit")
    private Timestamp guaranteeLimit;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 税
     */
    @DEField(name = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private Double amountTax;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 价格表
     */
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;

    /**
     * 批次/序列号
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 待维修产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 发票
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 产品量度单位
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 创建者
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 开票地址
     */
    @JSONField(name = "partner_invoice_id_text")
    @JsonProperty("partner_invoice_id_text")
    private String partnerInvoiceIdText;

    /**
     * 收货地址
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;

    /**
     * 地点
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 追踪
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 移动
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 移动
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 待维修产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 开票地址
     */
    @DEField(name = "partner_invoice_id")
    @JSONField(name = "partner_invoice_id")
    @JsonProperty("partner_invoice_id")
    private Integer partnerInvoiceId;

    /**
     * 地点
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 产品量度单位
     */
    @DEField(name = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Integer productUom;

    /**
     * 创建者
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 收货地址
     */
    @DEField(name = "address_id")
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Integer addressId;

    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 批次/序列号
     */
    @DEField(name = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;


    /**
     * 
     */
    @JSONField(name = "odooinvoice")
    @JsonProperty("odooinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JSONField(name = "odoopricelist")
    @JsonProperty("odoopricelist")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odooaddress")
    @JsonProperty("odooaddress")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAddress;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartnerinvoice")
    @JsonProperty("odoopartnerinvoice")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartnerInvoice;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoolot")
    @JsonProperty("odoolot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [内部备注]
     */
    public void setInternalNotes(String internalNotes){
        this.internalNotes = internalNotes ;
        this.modify("internal_notes",internalNotes);
    }
    /**
     * 设置 [开票方式]
     */
    public void setInvoiceMethod(String invoiceMethod){
        this.invoiceMethod = invoiceMethod ;
        this.modify("invoice_method",invoiceMethod);
    }
    /**
     * 设置 [已开票]
     */
    public void setInvoiced(String invoiced){
        this.invoiced = invoiced ;
        this.modify("invoiced",invoiced);
    }
    /**
     * 设置 [报价单说明]
     */
    public void setQuotationNotes(String quotationNotes){
        this.quotationNotes = quotationNotes ;
        this.modify("quotation_notes",quotationNotes);
    }
    /**
     * 设置 [维修参照]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [合计]
     */
    public void setAmountTotal(Double amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [已维修]
     */
    public void setRepaired(String repaired){
        this.repaired = repaired ;
        this.modify("repaired",repaired);
    }
    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(Double amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }
    /**
     * 设置 [质保到期]
     */
    public void setGuaranteeLimit(Timestamp guaranteeLimit){
        this.guaranteeLimit = guaranteeLimit ;
        this.modify("guarantee_limit",guaranteeLimit);
    }
    /**
     * 设置 [税]
     */
    public void setAmountTax(Double amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }
    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [移动]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Integer pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }
    /**
     * 设置 [待维修产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [开票地址]
     */
    public void setPartnerInvoiceId(Integer partnerInvoiceId){
        this.partnerInvoiceId = partnerInvoiceId ;
        this.modify("partner_invoice_id",partnerInvoiceId);
    }
    /**
     * 设置 [地点]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [产品量度单位]
     */
    public void setProductUom(Integer productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }
    /**
     * 设置 [收货地址]
     */
    public void setAddressId(Integer addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Integer invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }
    /**
     * 设置 [批次/序列号]
     */
    public void setLotId(Integer lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

}


