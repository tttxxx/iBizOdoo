package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;


/**
 * 实体[Res_currency_rate] 服务对象接口
 */
public interface IRes_currency_rateService{

    boolean create(Res_currency_rate et) ;
    void createBatch(List<Res_currency_rate> list) ;
    Res_currency_rate get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Res_currency_rate et) ;
    void updateBatch(List<Res_currency_rate> list) ;
    Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context) ;

}



