package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_ruleOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_ruleClientModel;

/**
 * 实体[库存规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_ruleServiceImpl implements IStock_ruleService {

    @Autowired
    stock_ruleOdooClient stock_ruleOdooClient;


    @Override
    public Stock_rule get(Integer id) {
        stock_ruleClientModel clientModel = new stock_ruleClientModel();
        clientModel.setId(id);
		stock_ruleOdooClient.get(clientModel);
        Stock_rule et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_rule();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_ruleClientModel clientModel = new stock_ruleClientModel();
        clientModel.setId(id);
		stock_ruleOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_rule et) {
        stock_ruleClientModel clientModel = convert2Model(et,null);
		stock_ruleOdooClient.update(clientModel);
        Stock_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_rule> list){
    }

    @Override
    public boolean create(Stock_rule et) {
        stock_ruleClientModel clientModel = convert2Model(et,null);
		stock_ruleOdooClient.create(clientModel);
        Stock_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_rule> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_rule> searchDefault(Stock_ruleSearchContext context) {
        List<Stock_rule> list = new ArrayList<Stock_rule>();
        Page<stock_ruleClientModel> clientModelList = stock_ruleOdooClient.search(context);
        for(stock_ruleClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_rule>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_ruleClientModel convert2Model(Stock_rule domain , stock_ruleClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_ruleClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("procure_methoddirtyflag"))
                model.setProcure_method(domain.getProcureMethod());
            if((Boolean) domain.getExtensionparams().get("propagatedirtyflag"))
                model.setPropagate(domain.getPropagate());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("rule_messagedirtyflag"))
                model.setRule_message(domain.getRuleMessage());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("autodirtyflag"))
                model.setAuto(domain.getAuto());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("delaydirtyflag"))
                model.setDelay(domain.getDelay());
            if((Boolean) domain.getExtensionparams().get("group_propagation_optiondirtyflag"))
                model.setGroup_propagation_option(domain.getGroupPropagationOption());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("actiondirtyflag"))
                model.setAction(domain.getAction());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_address_id_textdirtyflag"))
                model.setPartner_address_id_text(domain.getPartnerAddressIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("route_id_textdirtyflag"))
                model.setRoute_id_text(domain.getRouteIdText());
            if((Boolean) domain.getExtensionparams().get("route_sequencedirtyflag"))
                model.setRoute_sequence(domain.getRouteSequence());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("propagate_warehouse_id_textdirtyflag"))
                model.setPropagate_warehouse_id_text(domain.getPropagateWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("location_src_id_textdirtyflag"))
                model.setLocation_src_id_text(domain.getLocationSrcIdText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("partner_address_iddirtyflag"))
                model.setPartner_address_id(domain.getPartnerAddressId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            if((Boolean) domain.getExtensionparams().get("route_iddirtyflag"))
                model.setRoute_id(domain.getRouteId());
            if((Boolean) domain.getExtensionparams().get("location_src_iddirtyflag"))
                model.setLocation_src_id(domain.getLocationSrcId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("propagate_warehouse_iddirtyflag"))
                model.setPropagate_warehouse_id(domain.getPropagateWarehouseId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_rule convert2Domain( stock_ruleClientModel model ,Stock_rule domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_rule();
        }

        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getProcure_methodDirtyFlag())
            domain.setProcureMethod(model.getProcure_method());
        if(model.getPropagateDirtyFlag())
            domain.setPropagate(model.getPropagate());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getRule_messageDirtyFlag())
            domain.setRuleMessage(model.getRule_message());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAutoDirtyFlag())
            domain.setAuto(model.getAuto());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDelayDirtyFlag())
            domain.setDelay(model.getDelay());
        if(model.getGroup_propagation_optionDirtyFlag())
            domain.setGroupPropagationOption(model.getGroup_propagation_option());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getActionDirtyFlag())
            domain.setAction(model.getAction());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_address_id_textDirtyFlag())
            domain.setPartnerAddressIdText(model.getPartner_address_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getRoute_id_textDirtyFlag())
            domain.setRouteIdText(model.getRoute_id_text());
        if(model.getRoute_sequenceDirtyFlag())
            domain.setRouteSequence(model.getRoute_sequence());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getPropagate_warehouse_id_textDirtyFlag())
            domain.setPropagateWarehouseIdText(model.getPropagate_warehouse_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getLocation_src_id_textDirtyFlag())
            domain.setLocationSrcIdText(model.getLocation_src_id_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getPartner_address_idDirtyFlag())
            domain.setPartnerAddressId(model.getPartner_address_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        if(model.getRoute_idDirtyFlag())
            domain.setRouteId(model.getRoute_id());
        if(model.getLocation_src_idDirtyFlag())
            domain.setLocationSrcId(model.getLocation_src_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPropagate_warehouse_idDirtyFlag())
            domain.setPropagateWarehouseId(model.getPropagate_warehouse_id());
        return domain ;
    }

}

    



