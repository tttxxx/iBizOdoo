package cn.ibizlab.odoo.core.odoo_im_livechat.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;


/**
 * 实体[Im_livechat_report_channel] 服务对象接口
 */
public interface IIm_livechat_report_channelService{

    boolean create(Im_livechat_report_channel et) ;
    void createBatch(List<Im_livechat_report_channel> list) ;
    Im_livechat_report_channel get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Im_livechat_report_channel et) ;
    void updateBatch(List<Im_livechat_report_channel> list) ;
    Page<Im_livechat_report_channel> searchDefault(Im_livechat_report_channelSearchContext context) ;

}



