package cn.ibizlab.odoo.core.odoo_board.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_board.domain.Board_board;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;
import cn.ibizlab.odoo.core.odoo_board.service.IBoard_boardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_board.client.board_boardOdooClient;
import cn.ibizlab.odoo.core.odoo_board.clientmodel.board_boardClientModel;

/**
 * 实体[仪表板] 服务对象接口实现
 */
@Slf4j
@Service
public class Board_boardServiceImpl implements IBoard_boardService {

    @Autowired
    board_boardOdooClient board_boardOdooClient;


    @Override
    public boolean update(Board_board et) {
        board_boardClientModel clientModel = convert2Model(et,null);
		board_boardOdooClient.update(clientModel);
        Board_board rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Board_board> list){
    }

    @Override
    public boolean remove(Integer id) {
        board_boardClientModel clientModel = new board_boardClientModel();
        clientModel.setId(id);
		board_boardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Board_board et) {
        board_boardClientModel clientModel = convert2Model(et,null);
		board_boardOdooClient.create(clientModel);
        Board_board rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Board_board> list){
    }

    @Override
    public Board_board get(Integer id) {
        board_boardClientModel clientModel = new board_boardClientModel();
        clientModel.setId(id);
		board_boardOdooClient.get(clientModel);
        Board_board et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Board_board();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Board_board> searchDefault(Board_boardSearchContext context) {
        List<Board_board> list = new ArrayList<Board_board>();
        Page<board_boardClientModel> clientModelList = board_boardOdooClient.search(context);
        for(board_boardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Board_board>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public board_boardClientModel convert2Model(Board_board domain , board_boardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new board_boardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Board_board convert2Domain( board_boardClientModel model ,Board_board domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Board_board();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        return domain ;
    }

}

    



