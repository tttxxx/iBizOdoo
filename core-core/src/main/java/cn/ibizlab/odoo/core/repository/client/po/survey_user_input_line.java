package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [survey_user_input_line] 对象
 */
public interface survey_user_input_line {

    public String getAnswer_type();

    public void setAnswer_type(String answer_type);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_create();

    public void setDate_create(Timestamp date_create);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getPage_id();

    public void setPage_id(Integer page_id);

    public Integer getQuestion_id();

    public void setQuestion_id(Integer question_id);

    public Double getQuizz_mark();

    public void setQuizz_mark(Double quizz_mark);

    public String getSkipped();

    public void setSkipped(String skipped);

    public Integer getSurvey_id();

    public void setSurvey_id(Integer survey_id);

    public Integer getUser_input_id();

    public void setUser_input_id(Integer user_input_id);

    public Timestamp getValue_date();

    public void setValue_date(Timestamp value_date);

    public String getValue_free_text();

    public void setValue_free_text(String value_free_text);

    public Double getValue_number();

    public void setValue_number(Double value_number);

    public Integer getValue_suggested();

    public void setValue_suggested(Integer value_suggested);

    public Integer getValue_suggested_row();

    public void setValue_suggested_row(Integer value_suggested_row);

    public String getValue_text();

    public void setValue_text(String value_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
