package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;

/**
 * 实体 [游戏化目标] 存储模型
 */
public interface Gamification_goal{

    /**
     * 开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * 更新
     */
    String getTo_update();

    void setTo_update(String to_update);

    /**
     * 获取 [更新]脏标记
     */
    boolean getTo_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 提醒延迟
     */
    Integer getRemind_update_delay();

    void setRemind_update_delay(Integer remind_update_delay);

    /**
     * 获取 [提醒延迟]脏标记
     */
    boolean getRemind_update_delayDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 完整性
     */
    Double getCompleteness();

    void setCompleteness(Double completeness);

    /**
     * 获取 [完整性]脏标记
     */
    boolean getCompletenessDirtyFlag();

    /**
     * 达到
     */
    Double getTarget_goal();

    void setTarget_goal(Double target_goal);

    /**
     * 获取 [达到]脏标记
     */
    boolean getTarget_goalDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 关闭的目标
     */
    String getClosed();

    void setClosed(String closed);

    /**
     * 获取 [关闭的目标]脏标记
     */
    boolean getClosedDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最近更新
     */
    Timestamp getLast_update();

    void setLast_update(Timestamp last_update);

    /**
     * 获取 [最近更新]脏标记
     */
    boolean getLast_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getEnd_date();

    void setEnd_date(Timestamp end_date);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getEnd_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 当前值
     */
    Double getCurrent();

    void setCurrent(Double current);

    /**
     * 获取 [当前值]脏标记
     */
    boolean getCurrentDirtyFlag();

    /**
     * 定义说明
     */
    String getDefinition_description();

    void setDefinition_description(String definition_description);

    /**
     * 获取 [定义说明]脏标记
     */
    boolean getDefinition_descriptionDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 后缀
     */
    String getDefinition_suffix();

    void setDefinition_suffix(String definition_suffix);

    /**
     * 获取 [后缀]脏标记
     */
    boolean getDefinition_suffixDirtyFlag();

    /**
     * 挑战
     */
    String getChallenge_id_text();

    void setChallenge_id_text(String challenge_id_text);

    /**
     * 获取 [挑战]脏标记
     */
    boolean getChallenge_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 目标定义
     */
    String getDefinition_id_text();

    void setDefinition_id_text(String definition_id_text);

    /**
     * 获取 [目标定义]脏标记
     */
    boolean getDefinition_id_textDirtyFlag();

    /**
     * 显示为
     */
    String getDefinition_display();

    void setDefinition_display(String definition_display);

    /**
     * 获取 [显示为]脏标记
     */
    boolean getDefinition_displayDirtyFlag();

    /**
     * 挑战行
     */
    String getLine_id_text();

    void setLine_id_text(String line_id_text);

    /**
     * 获取 [挑战行]脏标记
     */
    boolean getLine_id_textDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 目标绩效
     */
    String getDefinition_condition();

    void setDefinition_condition(String definition_condition);

    /**
     * 获取 [目标绩效]脏标记
     */
    boolean getDefinition_conditionDirtyFlag();

    /**
     * 计算模式
     */
    String getComputation_mode();

    void setComputation_mode(String computation_mode);

    /**
     * 获取 [计算模式]脏标记
     */
    boolean getComputation_modeDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 挑战行
     */
    Integer getLine_id();

    void setLine_id(Integer line_id);

    /**
     * 获取 [挑战行]脏标记
     */
    boolean getLine_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 目标定义
     */
    Integer getDefinition_id();

    void setDefinition_id(Integer definition_id);

    /**
     * 获取 [目标定义]脏标记
     */
    boolean getDefinition_idDirtyFlag();

    /**
     * 挑战
     */
    Integer getChallenge_id();

    void setChallenge_id(Integer challenge_id);

    /**
     * 获取 [挑战]脏标记
     */
    boolean getChallenge_idDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

}
