package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;

/**
 * 实体 [寄出邮件] 存储对象
 */
public interface Mail_mailRepository extends Repository<Mail_mail> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mail> searchDefault(Mail_mailSearchContext context);

    Mail_mail convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail domain , Mail_mail po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail convert2Domain( Mail_mail po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail domain) ;

}
