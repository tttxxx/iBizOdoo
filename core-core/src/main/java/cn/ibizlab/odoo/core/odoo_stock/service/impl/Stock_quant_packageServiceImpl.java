package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quant_packageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_quant_packageOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_quant_packageClientModel;

/**
 * 实体[包裹] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_quant_packageServiceImpl implements IStock_quant_packageService {

    @Autowired
    stock_quant_packageOdooClient stock_quant_packageOdooClient;


    @Override
    public boolean update(Stock_quant_package et) {
        stock_quant_packageClientModel clientModel = convert2Model(et,null);
		stock_quant_packageOdooClient.update(clientModel);
        Stock_quant_package rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_quant_package> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_quant_packageClientModel clientModel = new stock_quant_packageClientModel();
        clientModel.setId(id);
		stock_quant_packageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Stock_quant_package et) {
        stock_quant_packageClientModel clientModel = convert2Model(et,null);
		stock_quant_packageOdooClient.create(clientModel);
        Stock_quant_package rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_quant_package> list){
    }

    @Override
    public Stock_quant_package get(Integer id) {
        stock_quant_packageClientModel clientModel = new stock_quant_packageClientModel();
        clientModel.setId(id);
		stock_quant_packageOdooClient.get(clientModel);
        Stock_quant_package et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_quant_package();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context) {
        List<Stock_quant_package> list = new ArrayList<Stock_quant_package>();
        Page<stock_quant_packageClientModel> clientModelList = stock_quant_packageOdooClient.search(context);
        for(stock_quant_packageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_quant_package>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_quant_packageClientModel convert2Model(Stock_quant_package domain , stock_quant_packageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_quant_packageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("owner_iddirtyflag"))
                model.setOwner_id(domain.getOwnerId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("packaging_id_textdirtyflag"))
                model.setPackaging_id_text(domain.getPackagingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("packaging_iddirtyflag"))
                model.setPackaging_id(domain.getPackagingId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_quant_package convert2Domain( stock_quant_packageClientModel model ,Stock_quant_package domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_quant_package();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getOwner_idDirtyFlag())
            domain.setOwnerId(model.getOwner_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPackaging_id_textDirtyFlag())
            domain.setPackagingIdText(model.getPackaging_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getPackaging_idDirtyFlag())
            domain.setPackagingId(model.getPackaging_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



