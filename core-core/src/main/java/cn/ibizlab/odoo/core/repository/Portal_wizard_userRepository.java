package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;

/**
 * 实体 [门户用户配置] 存储对象
 */
public interface Portal_wizard_userRepository extends Repository<Portal_wizard_user> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context);

    Portal_wizard_user convert2PO(cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user domain , Portal_wizard_user po) ;

    cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user convert2Domain( Portal_wizard_user po ,cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user domain) ;

}
