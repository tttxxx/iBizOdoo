package cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_challenge;

import cn.ibizlab.odoo.core.odoo_gamification.valuerule.validator.gamification_challenge.Gamification_challengeVisibility_modeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Gamification_challenge
 * 属性：Visibility_mode
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Gamification_challengeVisibility_modeDefaultValidator.class})
public @interface Gamification_challengeVisibility_modeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
