package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_teamService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_teamOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_teamClientModel;

/**
 * 实体[销售渠道] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_teamServiceImpl implements ICrm_teamService {

    @Autowired
    crm_teamOdooClient crm_teamOdooClient;


    @Override
    public Crm_team get(Integer id) {
        crm_teamClientModel clientModel = new crm_teamClientModel();
        clientModel.setId(id);
		crm_teamOdooClient.get(clientModel);
        Crm_team et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_team();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Crm_team et) {
        crm_teamClientModel clientModel = convert2Model(et,null);
		crm_teamOdooClient.update(clientModel);
        Crm_team rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_team> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_teamClientModel clientModel = new crm_teamClientModel();
        clientModel.setId(id);
		crm_teamOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Crm_team et) {
        crm_teamClientModel clientModel = convert2Model(et,null);
		crm_teamOdooClient.create(clientModel);
        Crm_team rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_team> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_team> searchDefault(Crm_teamSearchContext context) {
        List<Crm_team> list = new ArrayList<Crm_team>();
        Page<crm_teamClientModel> clientModelList = crm_teamOdooClient.search(context);
        for(crm_teamClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_team>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_teamClientModel convert2Model(Crm_team domain , crm_teamClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_teamClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("opportunities_amountdirtyflag"))
                model.setOpportunities_amount(domain.getOpportunitiesAmount());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("use_opportunitiesdirtyflag"))
                model.setUse_opportunities(domain.getUseOpportunities());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("quotations_countdirtyflag"))
                model.setQuotations_count(domain.getQuotationsCount());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_typedirtyflag"))
                model.setDashboard_graph_type(domain.getDashboardGraphType());
            if((Boolean) domain.getExtensionparams().get("is_favoritedirtyflag"))
                model.setIs_favorite(domain.getIsFavorite());
            if((Boolean) domain.getExtensionparams().get("opportunities_countdirtyflag"))
                model.setOpportunities_count(domain.getOpportunitiesCount());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("use_quotationsdirtyflag"))
                model.setUse_quotations(domain.getUseQuotations());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("favorite_user_idsdirtyflag"))
                model.setFavorite_user_ids(domain.getFavoriteUserIds());
            if((Boolean) domain.getExtensionparams().get("team_typedirtyflag"))
                model.setTeam_type(domain.getTeamType());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_modeldirtyflag"))
                model.setDashboard_graph_model(domain.getDashboardGraphModel());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_period_pipelinedirtyflag"))
                model.setDashboard_graph_period_pipeline(domain.getDashboardGraphPeriodPipeline());
            if((Boolean) domain.getExtensionparams().get("abandoned_carts_countdirtyflag"))
                model.setAbandoned_carts_count(domain.getAbandonedCartsCount());
            if((Boolean) domain.getExtensionparams().get("pos_config_idsdirtyflag"))
                model.setPos_config_ids(domain.getPosConfigIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("use_leadsdirtyflag"))
                model.setUse_leads(domain.getUseLeads());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("website_idsdirtyflag"))
                model.setWebsite_ids(domain.getWebsiteIds());
            if((Boolean) domain.getExtensionparams().get("quotations_amountdirtyflag"))
                model.setQuotations_amount(domain.getQuotationsAmount());
            if((Boolean) domain.getExtensionparams().get("invoiceddirtyflag"))
                model.setInvoiced(domain.getInvoiced());
            if((Boolean) domain.getExtensionparams().get("abandoned_carts_amountdirtyflag"))
                model.setAbandoned_carts_amount(domain.getAbandonedCartsAmount());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("sales_to_invoice_countdirtyflag"))
                model.setSales_to_invoice_count(domain.getSalesToInvoiceCount());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("unassigned_leads_countdirtyflag"))
                model.setUnassigned_leads_count(domain.getUnassignedLeadsCount());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_group_posdirtyflag"))
                model.setDashboard_graph_group_pos(domain.getDashboardGraphGroupPos());
            if((Boolean) domain.getExtensionparams().get("member_idsdirtyflag"))
                model.setMember_ids(domain.getMemberIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("invoiced_targetdirtyflag"))
                model.setInvoiced_target(domain.getInvoicedTarget());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_groupdirtyflag"))
                model.setDashboard_graph_group(domain.getDashboardGraphGroup());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_datadirtyflag"))
                model.setDashboard_graph_data(domain.getDashboardGraphData());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_perioddirtyflag"))
                model.setDashboard_graph_period(domain.getDashboardGraphPeriod());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("pos_sessions_open_countdirtyflag"))
                model.setPos_sessions_open_count(domain.getPosSessionsOpenCount());
            if((Boolean) domain.getExtensionparams().get("use_invoicesdirtyflag"))
                model.setUse_invoices(domain.getUseInvoices());
            if((Boolean) domain.getExtensionparams().get("dashboard_button_namedirtyflag"))
                model.setDashboard_button_name(domain.getDashboardButtonName());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("dashboard_graph_group_pipelinedirtyflag"))
                model.setDashboard_graph_group_pipeline(domain.getDashboardGraphGroupPipeline());
            if((Boolean) domain.getExtensionparams().get("pos_order_amount_totaldirtyflag"))
                model.setPos_order_amount_total(domain.getPosOrderAmountTotal());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("alias_model_iddirtyflag"))
                model.setAlias_model_id(domain.getAliasModelId());
            if((Boolean) domain.getExtensionparams().get("alias_parent_model_iddirtyflag"))
                model.setAlias_parent_model_id(domain.getAliasParentModelId());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("alias_user_iddirtyflag"))
                model.setAlias_user_id(domain.getAliasUserId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("alias_defaultsdirtyflag"))
                model.setAlias_defaults(domain.getAliasDefaults());
            if((Boolean) domain.getExtensionparams().get("alias_parent_thread_iddirtyflag"))
                model.setAlias_parent_thread_id(domain.getAliasParentThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_force_thread_iddirtyflag"))
                model.setAlias_force_thread_id(domain.getAliasForceThreadId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_team convert2Domain( crm_teamClientModel model ,Crm_team domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_team();
        }

        if(model.getOpportunities_amountDirtyFlag())
            domain.setOpportunitiesAmount(model.getOpportunities_amount());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUse_opportunitiesDirtyFlag())
            domain.setUseOpportunities(model.getUse_opportunities());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQuotations_countDirtyFlag())
            domain.setQuotationsCount(model.getQuotations_count());
        if(model.getDashboard_graph_typeDirtyFlag())
            domain.setDashboardGraphType(model.getDashboard_graph_type());
        if(model.getIs_favoriteDirtyFlag())
            domain.setIsFavorite(model.getIs_favorite());
        if(model.getOpportunities_countDirtyFlag())
            domain.setOpportunitiesCount(model.getOpportunities_count());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getUse_quotationsDirtyFlag())
            domain.setUseQuotations(model.getUse_quotations());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getFavorite_user_idsDirtyFlag())
            domain.setFavoriteUserIds(model.getFavorite_user_ids());
        if(model.getTeam_typeDirtyFlag())
            domain.setTeamType(model.getTeam_type());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getDashboard_graph_modelDirtyFlag())
            domain.setDashboardGraphModel(model.getDashboard_graph_model());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getDashboard_graph_period_pipelineDirtyFlag())
            domain.setDashboardGraphPeriodPipeline(model.getDashboard_graph_period_pipeline());
        if(model.getAbandoned_carts_countDirtyFlag())
            domain.setAbandonedCartsCount(model.getAbandoned_carts_count());
        if(model.getPos_config_idsDirtyFlag())
            domain.setPosConfigIds(model.getPos_config_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getUse_leadsDirtyFlag())
            domain.setUseLeads(model.getUse_leads());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getWebsite_idsDirtyFlag())
            domain.setWebsiteIds(model.getWebsite_ids());
        if(model.getQuotations_amountDirtyFlag())
            domain.setQuotationsAmount(model.getQuotations_amount());
        if(model.getInvoicedDirtyFlag())
            domain.setInvoiced(model.getInvoiced());
        if(model.getAbandoned_carts_amountDirtyFlag())
            domain.setAbandonedCartsAmount(model.getAbandoned_carts_amount());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getSales_to_invoice_countDirtyFlag())
            domain.setSalesToInvoiceCount(model.getSales_to_invoice_count());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getUnassigned_leads_countDirtyFlag())
            domain.setUnassignedLeadsCount(model.getUnassigned_leads_count());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getDashboard_graph_group_posDirtyFlag())
            domain.setDashboardGraphGroupPos(model.getDashboard_graph_group_pos());
        if(model.getMember_idsDirtyFlag())
            domain.setMemberIds(model.getMember_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getInvoiced_targetDirtyFlag())
            domain.setInvoicedTarget(model.getInvoiced_target());
        if(model.getDashboard_graph_groupDirtyFlag())
            domain.setDashboardGraphGroup(model.getDashboard_graph_group());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getDashboard_graph_dataDirtyFlag())
            domain.setDashboardGraphData(model.getDashboard_graph_data());
        if(model.getDashboard_graph_periodDirtyFlag())
            domain.setDashboardGraphPeriod(model.getDashboard_graph_period());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getPos_sessions_open_countDirtyFlag())
            domain.setPosSessionsOpenCount(model.getPos_sessions_open_count());
        if(model.getUse_invoicesDirtyFlag())
            domain.setUseInvoices(model.getUse_invoices());
        if(model.getDashboard_button_nameDirtyFlag())
            domain.setDashboardButtonName(model.getDashboard_button_name());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getDashboard_graph_group_pipelineDirtyFlag())
            domain.setDashboardGraphGroupPipeline(model.getDashboard_graph_group_pipeline());
        if(model.getPos_order_amount_totalDirtyFlag())
            domain.setPosOrderAmountTotal(model.getPos_order_amount_total());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getAlias_model_idDirtyFlag())
            domain.setAliasModelId(model.getAlias_model_id());
        if(model.getAlias_parent_model_idDirtyFlag())
            domain.setAliasParentModelId(model.getAlias_parent_model_id());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getAlias_user_idDirtyFlag())
            domain.setAliasUserId(model.getAlias_user_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getAlias_defaultsDirtyFlag())
            domain.setAliasDefaults(model.getAlias_defaults());
        if(model.getAlias_parent_thread_idDirtyFlag())
            domain.setAliasParentThreadId(model.getAlias_parent_thread_id());
        if(model.getAlias_force_thread_idDirtyFlag())
            domain.setAliasForceThreadId(model.getAlias_force_thread_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



