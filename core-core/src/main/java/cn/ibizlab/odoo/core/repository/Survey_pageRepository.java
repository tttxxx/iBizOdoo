package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;

/**
 * 实体 [问卷页面] 存储对象
 */
public interface Survey_pageRepository extends Repository<Survey_page> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_page> searchDefault(Survey_pageSearchContext context);

    Survey_page convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page domain , Survey_page po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page convert2Domain( Survey_page po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page domain) ;

}
