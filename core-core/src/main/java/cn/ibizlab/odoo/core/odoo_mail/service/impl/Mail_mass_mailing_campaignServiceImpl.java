package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_campaignService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_campaignOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailing_campaignClientModel;

/**
 * 实体[群发邮件营销] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_campaignServiceImpl implements IMail_mass_mailing_campaignService {

    @Autowired
    mail_mass_mailing_campaignOdooClient mail_mass_mailing_campaignOdooClient;


    @Override
    public Mail_mass_mailing_campaign get(Integer id) {
        mail_mass_mailing_campaignClientModel clientModel = new mail_mass_mailing_campaignClientModel();
        clientModel.setId(id);
		mail_mass_mailing_campaignOdooClient.get(clientModel);
        Mail_mass_mailing_campaign et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing_campaign();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_mass_mailing_campaign et) {
        mail_mass_mailing_campaignClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_campaignOdooClient.create(clientModel);
        Mail_mass_mailing_campaign rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_campaign> list){
    }

    @Override
    public boolean update(Mail_mass_mailing_campaign et) {
        mail_mass_mailing_campaignClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_campaignOdooClient.update(clientModel);
        Mail_mass_mailing_campaign rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing_campaign> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_mass_mailing_campaignClientModel clientModel = new mail_mass_mailing_campaignClientModel();
        clientModel.setId(id);
		mail_mass_mailing_campaignOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context) {
        List<Mail_mass_mailing_campaign> list = new ArrayList<Mail_mass_mailing_campaign>();
        Page<mail_mass_mailing_campaignClientModel> clientModelList = mail_mass_mailing_campaignOdooClient.search(context);
        for(mail_mass_mailing_campaignClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing_campaign>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailing_campaignClientModel convert2Model(Mail_mass_mailing_campaign domain , mail_mass_mailing_campaignClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailing_campaignClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("totaldirtyflag"))
                model.setTotal(domain.getTotal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_idsdirtyflag"))
                model.setMass_mailing_ids(domain.getMassMailingIds());
            if((Boolean) domain.getExtensionparams().get("opened_ratiodirtyflag"))
                model.setOpened_ratio(domain.getOpenedRatio());
            if((Boolean) domain.getExtensionparams().get("faileddirtyflag"))
                model.setFailed(domain.getFailed());
            if((Boolean) domain.getExtensionparams().get("scheduleddirtyflag"))
                model.setScheduled(domain.getScheduled());
            if((Boolean) domain.getExtensionparams().get("bounceddirtyflag"))
                model.setBounced(domain.getBounced());
            if((Boolean) domain.getExtensionparams().get("clicks_ratiodirtyflag"))
                model.setClicks_ratio(domain.getClicksRatio());
            if((Boolean) domain.getExtensionparams().get("sentdirtyflag"))
                model.setSent(domain.getSent());
            if((Boolean) domain.getExtensionparams().get("received_ratiodirtyflag"))
                model.setReceived_ratio(domain.getReceivedRatio());
            if((Boolean) domain.getExtensionparams().get("ignoreddirtyflag"))
                model.setIgnored(domain.getIgnored());
            if((Boolean) domain.getExtensionparams().get("unique_ab_testingdirtyflag"))
                model.setUnique_ab_testing(domain.getUniqueAbTesting());
            if((Boolean) domain.getExtensionparams().get("replieddirtyflag"))
                model.setReplied(domain.getReplied());
            if((Boolean) domain.getExtensionparams().get("total_mailingsdirtyflag"))
                model.setTotal_mailings(domain.getTotalMailings());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("bounced_ratiodirtyflag"))
                model.setBounced_ratio(domain.getBouncedRatio());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("openeddirtyflag"))
                model.setOpened(domain.getOpened());
            if((Boolean) domain.getExtensionparams().get("replied_ratiodirtyflag"))
                model.setReplied_ratio(domain.getRepliedRatio());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("delivereddirtyflag"))
                model.setDelivered(domain.getDelivered());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing_campaign convert2Domain( mail_mass_mailing_campaignClientModel model ,Mail_mass_mailing_campaign domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing_campaign();
        }

        if(model.getTotalDirtyFlag())
            domain.setTotal(model.getTotal());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMass_mailing_idsDirtyFlag())
            domain.setMassMailingIds(model.getMass_mailing_ids());
        if(model.getOpened_ratioDirtyFlag())
            domain.setOpenedRatio(model.getOpened_ratio());
        if(model.getFailedDirtyFlag())
            domain.setFailed(model.getFailed());
        if(model.getScheduledDirtyFlag())
            domain.setScheduled(model.getScheduled());
        if(model.getBouncedDirtyFlag())
            domain.setBounced(model.getBounced());
        if(model.getClicks_ratioDirtyFlag())
            domain.setClicksRatio(model.getClicks_ratio());
        if(model.getSentDirtyFlag())
            domain.setSent(model.getSent());
        if(model.getReceived_ratioDirtyFlag())
            domain.setReceivedRatio(model.getReceived_ratio());
        if(model.getIgnoredDirtyFlag())
            domain.setIgnored(model.getIgnored());
        if(model.getUnique_ab_testingDirtyFlag())
            domain.setUniqueAbTesting(model.getUnique_ab_testing());
        if(model.getRepliedDirtyFlag())
            domain.setReplied(model.getReplied());
        if(model.getTotal_mailingsDirtyFlag())
            domain.setTotalMailings(model.getTotal_mailings());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getBounced_ratioDirtyFlag())
            domain.setBouncedRatio(model.getBounced_ratio());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getOpenedDirtyFlag())
            domain.setOpened(model.getOpened());
        if(model.getReplied_ratioDirtyFlag())
            domain.setRepliedRatio(model.getReplied_ratio());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDeliveredDirtyFlag())
            domain.setDelivered(model.getDelivered());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        return domain ;
    }

}

    



