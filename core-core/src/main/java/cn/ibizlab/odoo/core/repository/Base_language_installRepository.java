package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;

/**
 * 实体 [安装语言] 存储对象
 */
public interface Base_language_installRepository extends Repository<Base_language_install> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_language_install> searchDefault(Base_language_installSearchContext context);

    Base_language_install convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install domain , Base_language_install po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install convert2Domain( Base_language_install po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install domain) ;

}
