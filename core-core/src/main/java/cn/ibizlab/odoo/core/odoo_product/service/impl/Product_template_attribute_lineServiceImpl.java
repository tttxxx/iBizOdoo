package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_template_attribute_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_template_attribute_lineClientModel;

/**
 * 实体[产品模板属性明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_template_attribute_lineServiceImpl implements IProduct_template_attribute_lineService {

    @Autowired
    product_template_attribute_lineOdooClient product_template_attribute_lineOdooClient;


    @Override
    public Product_template_attribute_line get(Integer id) {
        product_template_attribute_lineClientModel clientModel = new product_template_attribute_lineClientModel();
        clientModel.setId(id);
		product_template_attribute_lineOdooClient.get(clientModel);
        Product_template_attribute_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_template_attribute_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        product_template_attribute_lineClientModel clientModel = new product_template_attribute_lineClientModel();
        clientModel.setId(id);
		product_template_attribute_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_template_attribute_line et) {
        product_template_attribute_lineClientModel clientModel = convert2Model(et,null);
		product_template_attribute_lineOdooClient.update(clientModel);
        Product_template_attribute_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_template_attribute_line> list){
    }

    @Override
    public boolean create(Product_template_attribute_line et) {
        product_template_attribute_lineClientModel clientModel = convert2Model(et,null);
		product_template_attribute_lineOdooClient.create(clientModel);
        Product_template_attribute_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template_attribute_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template_attribute_line> searchDefault(Product_template_attribute_lineSearchContext context) {
        List<Product_template_attribute_line> list = new ArrayList<Product_template_attribute_line>();
        Page<product_template_attribute_lineClientModel> clientModelList = product_template_attribute_lineOdooClient.search(context);
        for(product_template_attribute_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_template_attribute_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_template_attribute_lineClientModel convert2Model(Product_template_attribute_line domain , product_template_attribute_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_template_attribute_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("product_template_value_idsdirtyflag"))
                model.setProduct_template_value_ids(domain.getProductTemplateValueIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("value_idsdirtyflag"))
                model.setValue_ids(domain.getValueIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("attribute_id_textdirtyflag"))
                model.setAttribute_id_text(domain.getAttributeIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("attribute_iddirtyflag"))
                model.setAttribute_id(domain.getAttributeId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_template_attribute_line convert2Domain( product_template_attribute_lineClientModel model ,Product_template_attribute_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_template_attribute_line();
        }

        if(model.getProduct_template_value_idsDirtyFlag())
            domain.setProductTemplateValueIds(model.getProduct_template_value_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getValue_idsDirtyFlag())
            domain.setValueIds(model.getValue_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAttribute_id_textDirtyFlag())
            domain.setAttributeIdText(model.getAttribute_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getAttribute_idDirtyFlag())
            domain.setAttributeId(model.getAttribute_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



