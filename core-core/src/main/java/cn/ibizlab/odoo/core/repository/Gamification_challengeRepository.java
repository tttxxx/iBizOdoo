package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;

/**
 * 实体 [游戏化挑战] 存储对象
 */
public interface Gamification_challengeRepository extends Repository<Gamification_challenge> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context);

    Gamification_challenge convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge domain , Gamification_challenge po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge convert2Domain( Gamification_challenge po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge domain) ;

}
