package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_immediate_transferService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_immediate_transferOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_immediate_transferClientModel;

/**
 * 实体[立即调拨] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_immediate_transferServiceImpl implements IStock_immediate_transferService {

    @Autowired
    stock_immediate_transferOdooClient stock_immediate_transferOdooClient;


    @Override
    public boolean update(Stock_immediate_transfer et) {
        stock_immediate_transferClientModel clientModel = convert2Model(et,null);
		stock_immediate_transferOdooClient.update(clientModel);
        Stock_immediate_transfer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_immediate_transfer> list){
    }

    @Override
    public Stock_immediate_transfer get(Integer id) {
        stock_immediate_transferClientModel clientModel = new stock_immediate_transferClientModel();
        clientModel.setId(id);
		stock_immediate_transferOdooClient.get(clientModel);
        Stock_immediate_transfer et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_immediate_transfer();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_immediate_transfer et) {
        stock_immediate_transferClientModel clientModel = convert2Model(et,null);
		stock_immediate_transferOdooClient.create(clientModel);
        Stock_immediate_transfer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_immediate_transfer> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_immediate_transferClientModel clientModel = new stock_immediate_transferClientModel();
        clientModel.setId(id);
		stock_immediate_transferOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context) {
        List<Stock_immediate_transfer> list = new ArrayList<Stock_immediate_transfer>();
        Page<stock_immediate_transferClientModel> clientModelList = stock_immediate_transferOdooClient.search(context);
        for(stock_immediate_transferClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_immediate_transfer>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_immediate_transferClientModel convert2Model(Stock_immediate_transfer domain , stock_immediate_transferClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_immediate_transferClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("pick_idsdirtyflag"))
                model.setPick_ids(domain.getPickIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_immediate_transfer convert2Domain( stock_immediate_transferClientModel model ,Stock_immediate_transfer domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_immediate_transfer();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPick_idsDirtyFlag())
            domain.setPickIds(model.getPick_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



