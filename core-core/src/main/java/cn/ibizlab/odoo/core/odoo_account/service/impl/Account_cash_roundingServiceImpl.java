package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cash_roundingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_cash_roundingOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_cash_roundingClientModel;

/**
 * 实体[帐户现金舍入] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_cash_roundingServiceImpl implements IAccount_cash_roundingService {

    @Autowired
    account_cash_roundingOdooClient account_cash_roundingOdooClient;


    @Override
    public boolean create(Account_cash_rounding et) {
        account_cash_roundingClientModel clientModel = convert2Model(et,null);
		account_cash_roundingOdooClient.create(clientModel);
        Account_cash_rounding rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_cash_rounding> list){
    }

    @Override
    public Account_cash_rounding get(Integer id) {
        account_cash_roundingClientModel clientModel = new account_cash_roundingClientModel();
        clientModel.setId(id);
		account_cash_roundingOdooClient.get(clientModel);
        Account_cash_rounding et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_cash_rounding();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_cash_roundingClientModel clientModel = new account_cash_roundingClientModel();
        clientModel.setId(id);
		account_cash_roundingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_cash_rounding et) {
        account_cash_roundingClientModel clientModel = convert2Model(et,null);
		account_cash_roundingOdooClient.update(clientModel);
        Account_cash_rounding rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_cash_rounding> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context) {
        List<Account_cash_rounding> list = new ArrayList<Account_cash_rounding>();
        Page<account_cash_roundingClientModel> clientModelList = account_cash_roundingOdooClient.search(context);
        for(account_cash_roundingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_cash_rounding>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_cash_roundingClientModel convert2Model(Account_cash_rounding domain , account_cash_roundingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_cash_roundingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("rounding_methoddirtyflag"))
                model.setRounding_method(domain.getRoundingMethod());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("strategydirtyflag"))
                model.setStrategy(domain.getStrategy());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("roundingdirtyflag"))
                model.setRounding(domain.getRounding());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_cash_rounding convert2Domain( account_cash_roundingClientModel model ,Account_cash_rounding domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_cash_rounding();
        }

        if(model.getRounding_methodDirtyFlag())
            domain.setRoundingMethod(model.getRounding_method());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStrategyDirtyFlag())
            domain.setStrategy(model.getStrategy());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getRoundingDirtyFlag())
            domain.setRounding(model.getRounding());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



