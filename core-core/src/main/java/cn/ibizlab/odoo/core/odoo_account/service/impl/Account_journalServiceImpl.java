package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_journalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_journalOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_journalClientModel;

/**
 * 实体[日记账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_journalServiceImpl implements IAccount_journalService {

    @Autowired
    account_journalOdooClient account_journalOdooClient;


    @Override
    public Account_journal get(Integer id) {
        account_journalClientModel clientModel = new account_journalClientModel();
        clientModel.setId(id);
		account_journalOdooClient.get(clientModel);
        Account_journal et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_journal();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_journal et) {
        account_journalClientModel clientModel = convert2Model(et,null);
		account_journalOdooClient.create(clientModel);
        Account_journal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_journal> list){
    }

    @Override
    public boolean update(Account_journal et) {
        account_journalClientModel clientModel = convert2Model(et,null);
		account_journalOdooClient.update(clientModel);
        Account_journal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_journal> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_journalClientModel clientModel = new account_journalClientModel();
        clientModel.setId(id);
		account_journalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_journal> searchDefault(Account_journalSearchContext context) {
        List<Account_journal> list = new ArrayList<Account_journal>();
        Page<account_journalClientModel> clientModelList = account_journalOdooClient.search(context);
        for(account_journalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_journal>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_journalClientModel convert2Model(Account_journal domain , account_journalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_journalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("refund_sequence_number_nextdirtyflag"))
                model.setRefund_sequence_number_next(domain.getRefundSequenceNumberNext());
            if((Boolean) domain.getExtensionparams().get("type_control_idsdirtyflag"))
                model.setType_control_ids(domain.getTypeControlIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("at_least_one_outbounddirtyflag"))
                model.setAt_least_one_outbound(domain.getAtLeastOneOutbound());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("sequence_iddirtyflag"))
                model.setSequence_id(domain.getSequenceId());
            if((Boolean) domain.getExtensionparams().get("show_on_dashboarddirtyflag"))
                model.setShow_on_dashboard(domain.getShowOnDashboard());
            if((Boolean) domain.getExtensionparams().get("journal_userdirtyflag"))
                model.setJournal_user(domain.getJournalUser());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("bank_statements_sourcedirtyflag"))
                model.setBank_statements_source(domain.getBankStatementsSource());
            if((Boolean) domain.getExtensionparams().get("at_least_one_inbounddirtyflag"))
                model.setAt_least_one_inbound(domain.getAtLeastOneInbound());
            if((Boolean) domain.getExtensionparams().get("post_at_bank_recdirtyflag"))
                model.setPost_at_bank_rec(domain.getPostAtBankRec());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("sequence_number_nextdirtyflag"))
                model.setSequence_number_next(domain.getSequenceNumberNext());
            if((Boolean) domain.getExtensionparams().get("belongs_to_companydirtyflag"))
                model.setBelongs_to_company(domain.getBelongsToCompany());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("inbound_payment_method_idsdirtyflag"))
                model.setInbound_payment_method_ids(domain.getInboundPaymentMethodIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("group_invoice_linesdirtyflag"))
                model.setGroup_invoice_lines(domain.getGroupInvoiceLines());
            if((Boolean) domain.getExtensionparams().get("refund_sequence_iddirtyflag"))
                model.setRefund_sequence_id(domain.getRefundSequenceId());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("account_control_idsdirtyflag"))
                model.setAccount_control_ids(domain.getAccountControlIds());
            if((Boolean) domain.getExtensionparams().get("update_posteddirtyflag"))
                model.setUpdate_posted(domain.getUpdatePosted());
            if((Boolean) domain.getExtensionparams().get("amount_authorized_diffdirtyflag"))
                model.setAmount_authorized_diff(domain.getAmountAuthorizedDiff());
            if((Boolean) domain.getExtensionparams().get("kanban_dashboard_graphdirtyflag"))
                model.setKanban_dashboard_graph(domain.getKanbanDashboardGraph());
            if((Boolean) domain.getExtensionparams().get("outbound_payment_method_idsdirtyflag"))
                model.setOutbound_payment_method_ids(domain.getOutboundPaymentMethodIds());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("kanban_dashboarddirtyflag"))
                model.setKanban_dashboard(domain.getKanbanDashboard());
            if((Boolean) domain.getExtensionparams().get("refund_sequencedirtyflag"))
                model.setRefund_sequence(domain.getRefundSequence());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("profit_account_id_textdirtyflag"))
                model.setProfit_account_id_text(domain.getProfitAccountIdText());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("bank_acc_numberdirtyflag"))
                model.setBank_acc_number(domain.getBankAccNumber());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("bank_iddirtyflag"))
                model.setBank_id(domain.getBankId());
            if((Boolean) domain.getExtensionparams().get("loss_account_id_textdirtyflag"))
                model.setLoss_account_id_text(domain.getLossAccountIdText());
            if((Boolean) domain.getExtensionparams().get("default_credit_account_id_textdirtyflag"))
                model.setDefault_credit_account_id_text(domain.getDefaultCreditAccountIdText());
            if((Boolean) domain.getExtensionparams().get("company_partner_iddirtyflag"))
                model.setCompany_partner_id(domain.getCompanyPartnerId());
            if((Boolean) domain.getExtensionparams().get("default_debit_account_id_textdirtyflag"))
                model.setDefault_debit_account_id_text(domain.getDefaultDebitAccountIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("bank_account_iddirtyflag"))
                model.setBank_account_id(domain.getBankAccountId());
            if((Boolean) domain.getExtensionparams().get("loss_account_iddirtyflag"))
                model.setLoss_account_id(domain.getLossAccountId());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("default_credit_account_iddirtyflag"))
                model.setDefault_credit_account_id(domain.getDefaultCreditAccountId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("default_debit_account_iddirtyflag"))
                model.setDefault_debit_account_id(domain.getDefaultDebitAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("profit_account_iddirtyflag"))
                model.setProfit_account_id(domain.getProfitAccountId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_journal convert2Domain( account_journalClientModel model ,Account_journal domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_journal();
        }

        if(model.getRefund_sequence_number_nextDirtyFlag())
            domain.setRefundSequenceNumberNext(model.getRefund_sequence_number_next());
        if(model.getType_control_idsDirtyFlag())
            domain.setTypeControlIds(model.getType_control_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAt_least_one_outboundDirtyFlag())
            domain.setAtLeastOneOutbound(model.getAt_least_one_outbound());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getSequence_idDirtyFlag())
            domain.setSequenceId(model.getSequence_id());
        if(model.getShow_on_dashboardDirtyFlag())
            domain.setShowOnDashboard(model.getShow_on_dashboard());
        if(model.getJournal_userDirtyFlag())
            domain.setJournalUser(model.getJournal_user());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getBank_statements_sourceDirtyFlag())
            domain.setBankStatementsSource(model.getBank_statements_source());
        if(model.getAt_least_one_inboundDirtyFlag())
            domain.setAtLeastOneInbound(model.getAt_least_one_inbound());
        if(model.getPost_at_bank_recDirtyFlag())
            domain.setPostAtBankRec(model.getPost_at_bank_rec());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSequence_number_nextDirtyFlag())
            domain.setSequenceNumberNext(model.getSequence_number_next());
        if(model.getBelongs_to_companyDirtyFlag())
            domain.setBelongsToCompany(model.getBelongs_to_company());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getInbound_payment_method_idsDirtyFlag())
            domain.setInboundPaymentMethodIds(model.getInbound_payment_method_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getGroup_invoice_linesDirtyFlag())
            domain.setGroupInvoiceLines(model.getGroup_invoice_lines());
        if(model.getRefund_sequence_idDirtyFlag())
            domain.setRefundSequenceId(model.getRefund_sequence_id());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getAccount_control_idsDirtyFlag())
            domain.setAccountControlIds(model.getAccount_control_ids());
        if(model.getUpdate_postedDirtyFlag())
            domain.setUpdatePosted(model.getUpdate_posted());
        if(model.getAmount_authorized_diffDirtyFlag())
            domain.setAmountAuthorizedDiff(model.getAmount_authorized_diff());
        if(model.getKanban_dashboard_graphDirtyFlag())
            domain.setKanbanDashboardGraph(model.getKanban_dashboard_graph());
        if(model.getOutbound_payment_method_idsDirtyFlag())
            domain.setOutboundPaymentMethodIds(model.getOutbound_payment_method_ids());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getKanban_dashboardDirtyFlag())
            domain.setKanbanDashboard(model.getKanban_dashboard());
        if(model.getRefund_sequenceDirtyFlag())
            domain.setRefundSequence(model.getRefund_sequence());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProfit_account_id_textDirtyFlag())
            domain.setProfitAccountIdText(model.getProfit_account_id_text());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getBank_acc_numberDirtyFlag())
            domain.setBankAccNumber(model.getBank_acc_number());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getBank_idDirtyFlag())
            domain.setBankId(model.getBank_id());
        if(model.getLoss_account_id_textDirtyFlag())
            domain.setLossAccountIdText(model.getLoss_account_id_text());
        if(model.getDefault_credit_account_id_textDirtyFlag())
            domain.setDefaultCreditAccountIdText(model.getDefault_credit_account_id_text());
        if(model.getCompany_partner_idDirtyFlag())
            domain.setCompanyPartnerId(model.getCompany_partner_id());
        if(model.getDefault_debit_account_id_textDirtyFlag())
            domain.setDefaultDebitAccountIdText(model.getDefault_debit_account_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getBank_account_idDirtyFlag())
            domain.setBankAccountId(model.getBank_account_id());
        if(model.getLoss_account_idDirtyFlag())
            domain.setLossAccountId(model.getLoss_account_id());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getDefault_credit_account_idDirtyFlag())
            domain.setDefaultCreditAccountId(model.getDefault_credit_account_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getDefault_debit_account_idDirtyFlag())
            domain.setDefaultDebitAccountId(model.getDefault_debit_account_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProfit_account_idDirtyFlag())
            domain.setProfitAccountId(model.getProfit_account_id());
        return domain ;
    }

}

    



