package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;

/**
 * 实体 [工作中心生产力日志] 存储对象
 */
public interface Mrp_workcenter_productivityRepository extends Repository<Mrp_workcenter_productivity> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context);

    Mrp_workcenter_productivity convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity domain , Mrp_workcenter_productivity po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity convert2Domain( Mrp_workcenter_productivity po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity domain) ;

}
