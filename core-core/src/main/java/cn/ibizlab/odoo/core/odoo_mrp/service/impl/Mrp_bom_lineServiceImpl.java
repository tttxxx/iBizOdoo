package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bom_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_bom_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_bom_lineClientModel;

/**
 * 实体[物料清单明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_bom_lineServiceImpl implements IMrp_bom_lineService {

    @Autowired
    mrp_bom_lineOdooClient mrp_bom_lineOdooClient;


    @Override
    public boolean remove(Integer id) {
        mrp_bom_lineClientModel clientModel = new mrp_bom_lineClientModel();
        clientModel.setId(id);
		mrp_bom_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mrp_bom_line et) {
        mrp_bom_lineClientModel clientModel = convert2Model(et,null);
		mrp_bom_lineOdooClient.update(clientModel);
        Mrp_bom_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_bom_line> list){
    }

    @Override
    public boolean create(Mrp_bom_line et) {
        mrp_bom_lineClientModel clientModel = convert2Model(et,null);
		mrp_bom_lineOdooClient.create(clientModel);
        Mrp_bom_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_bom_line> list){
    }

    @Override
    public Mrp_bom_line get(Integer id) {
        mrp_bom_lineClientModel clientModel = new mrp_bom_lineClientModel();
        clientModel.setId(id);
		mrp_bom_lineOdooClient.get(clientModel);
        Mrp_bom_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_bom_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context) {
        List<Mrp_bom_line> list = new ArrayList<Mrp_bom_line>();
        Page<mrp_bom_lineClientModel> clientModelList = mrp_bom_lineOdooClient.search(context);
        for(mrp_bom_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_bom_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_bom_lineClientModel convert2Model(Mrp_bom_line domain , mrp_bom_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_bom_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("attribute_value_idsdirtyflag"))
                model.setAttribute_value_ids(domain.getAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("child_line_idsdirtyflag"))
                model.setChild_line_ids(domain.getChildLineIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("valid_product_attribute_value_idsdirtyflag"))
                model.setValid_product_attribute_value_ids(domain.getValidProductAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("has_attachmentsdirtyflag"))
                model.setHas_attachments(domain.getHasAttachments());
            if((Boolean) domain.getExtensionparams().get("child_bom_iddirtyflag"))
                model.setChild_bom_id(domain.getChildBomId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("routing_id_textdirtyflag"))
                model.setRouting_id_text(domain.getRoutingIdText());
            if((Boolean) domain.getExtensionparams().get("parent_product_tmpl_iddirtyflag"))
                model.setParent_product_tmpl_id(domain.getParentProductTmplId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("operation_id_textdirtyflag"))
                model.setOperation_id_text(domain.getOperationIdText());
            if((Boolean) domain.getExtensionparams().get("operation_iddirtyflag"))
                model.setOperation_id(domain.getOperationId());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("bom_iddirtyflag"))
                model.setBom_id(domain.getBomId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("routing_iddirtyflag"))
                model.setRouting_id(domain.getRoutingId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_bom_line convert2Domain( mrp_bom_lineClientModel model ,Mrp_bom_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_bom_line();
        }

        if(model.getAttribute_value_idsDirtyFlag())
            domain.setAttributeValueIds(model.getAttribute_value_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getChild_line_idsDirtyFlag())
            domain.setChildLineIds(model.getChild_line_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getValid_product_attribute_value_idsDirtyFlag())
            domain.setValidProductAttributeValueIds(model.getValid_product_attribute_value_ids());
        if(model.getHas_attachmentsDirtyFlag())
            domain.setHasAttachments(model.getHas_attachments());
        if(model.getChild_bom_idDirtyFlag())
            domain.setChildBomId(model.getChild_bom_id());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getRouting_id_textDirtyFlag())
            domain.setRoutingIdText(model.getRouting_id_text());
        if(model.getParent_product_tmpl_idDirtyFlag())
            domain.setParentProductTmplId(model.getParent_product_tmpl_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOperation_id_textDirtyFlag())
            domain.setOperationIdText(model.getOperation_id_text());
        if(model.getOperation_idDirtyFlag())
            domain.setOperationId(model.getOperation_id());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getBom_idDirtyFlag())
            domain.setBomId(model.getBom_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getRouting_idDirtyFlag())
            domain.setRoutingId(model.getRouting_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        return domain ;
    }

}

    



