package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;

/**
 * 实体 [销售订单] 存储对象
 */
public interface Sale_orderRepository extends Repository<Sale_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_order> searchDefault(Sale_orderSearchContext context);

    Sale_order convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order domain , Sale_order po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order convert2Domain( Sale_order po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order domain) ;

}
