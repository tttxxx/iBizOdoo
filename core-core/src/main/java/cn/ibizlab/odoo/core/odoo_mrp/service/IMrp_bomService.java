package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bomSearchContext;


/**
 * 实体[Mrp_bom] 服务对象接口
 */
public interface IMrp_bomService{

    boolean update(Mrp_bom et) ;
    void updateBatch(List<Mrp_bom> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mrp_bom et) ;
    void createBatch(List<Mrp_bom> list) ;
    Mrp_bom get(Integer key) ;
    Page<Mrp_bom> searchDefault(Mrp_bomSearchContext context) ;

}



