package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [uom_uom] 对象
 */
public interface Iuom_uom {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [类别]
     */
    public Integer getCategory_id();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [类别]
     */
    public String getCategory_id_text();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [比例]
     */
    public void setFactor(Double factor);
    
    /**
     * 设置 [比例]
     */
    public Double getFactor();

    /**
     * 获取 [比例]脏标记
     */
    public boolean getFactorDirtyFlag();
    /**
     * 获取 [更大比率]
     */
    public void setFactor_inv(Double factor_inv);
    
    /**
     * 设置 [更大比率]
     */
    public Double getFactor_inv();

    /**
     * 获取 [更大比率]脏标记
     */
    public boolean getFactor_invDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [分组销售点中的产品]
     */
    public void setIs_pos_groupable(String is_pos_groupable);
    
    /**
     * 设置 [分组销售点中的产品]
     */
    public String getIs_pos_groupable();

    /**
     * 获取 [分组销售点中的产品]脏标记
     */
    public boolean getIs_pos_groupableDirtyFlag();
    /**
     * 获取 [计量单位的类别]
     */
    public void setMeasure_type(String measure_type);
    
    /**
     * 设置 [计量单位的类别]
     */
    public String getMeasure_type();

    /**
     * 获取 [计量单位的类别]脏标记
     */
    public boolean getMeasure_typeDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setName(String name);
    
    /**
     * 设置 [单位]
     */
    public String getName();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [舍入精度]
     */
    public void setRounding(Double rounding);
    
    /**
     * 设置 [舍入精度]
     */
    public Double getRounding();

    /**
     * 获取 [舍入精度]脏标记
     */
    public boolean getRoundingDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setUom_type(String uom_type);
    
    /**
     * 设置 [类型]
     */
    public String getUom_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getUom_typeDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
