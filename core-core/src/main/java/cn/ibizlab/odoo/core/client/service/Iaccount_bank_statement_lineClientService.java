package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
public interface Iaccount_bank_statement_lineClientService{

    public Iaccount_bank_statement_line createModel() ;

    public void remove(Iaccount_bank_statement_line account_bank_statement_line);

    public void updateBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines);

    public void createBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines);

    public void get(Iaccount_bank_statement_line account_bank_statement_line);

    public void removeBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines);

    public Page<Iaccount_bank_statement_line> search(SearchContext context);

    public void update(Iaccount_bank_statement_line account_bank_statement_line);

    public void create(Iaccount_bank_statement_line account_bank_statement_line);

    public Page<Iaccount_bank_statement_line> select(SearchContext context);

}
