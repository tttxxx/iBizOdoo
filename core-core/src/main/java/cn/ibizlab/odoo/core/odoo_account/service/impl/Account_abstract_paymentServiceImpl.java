package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_abstract_paymentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_abstract_paymentOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_abstract_paymentClientModel;

/**
 * 实体[包含在允许登记付款的模块之间共享的逻辑] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_abstract_paymentServiceImpl implements IAccount_abstract_paymentService {

    @Autowired
    account_abstract_paymentOdooClient account_abstract_paymentOdooClient;


    @Override
    public boolean update(Account_abstract_payment et) {
        account_abstract_paymentClientModel clientModel = convert2Model(et,null);
		account_abstract_paymentOdooClient.update(clientModel);
        Account_abstract_payment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_abstract_payment> list){
    }

    @Override
    public Account_abstract_payment get(Integer id) {
        account_abstract_paymentClientModel clientModel = new account_abstract_paymentClientModel();
        clientModel.setId(id);
		account_abstract_paymentOdooClient.get(clientModel);
        Account_abstract_payment et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_abstract_payment();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_abstract_paymentClientModel clientModel = new account_abstract_paymentClientModel();
        clientModel.setId(id);
		account_abstract_paymentOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_abstract_payment et) {
        account_abstract_paymentClientModel clientModel = convert2Model(et,null);
		account_abstract_paymentOdooClient.create(clientModel);
        Account_abstract_payment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_abstract_payment> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_abstract_payment> searchDefault(Account_abstract_paymentSearchContext context) {
        List<Account_abstract_payment> list = new ArrayList<Account_abstract_payment>();
        Page<account_abstract_paymentClientModel> clientModelList = account_abstract_paymentOdooClient.search(context);
        for(account_abstract_paymentClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_abstract_payment>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_abstract_paymentClientModel convert2Model(Account_abstract_payment domain , account_abstract_paymentClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_abstract_paymentClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("payment_difference_handlingdirtyflag"))
                model.setPayment_difference_handling(domain.getPaymentDifferenceHandling());
            if((Boolean) domain.getExtensionparams().get("partner_typedirtyflag"))
                model.setPartner_type(domain.getPartnerType());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("payment_differencedirtyflag"))
                model.setPayment_difference(domain.getPaymentDifference());
            if((Boolean) domain.getExtensionparams().get("multidirtyflag"))
                model.setMulti(domain.getMulti());
            if((Boolean) domain.getExtensionparams().get("writeoff_labeldirtyflag"))
                model.setWriteoff_label(domain.getWriteoffLabel());
            if((Boolean) domain.getExtensionparams().get("show_partner_bank_accountdirtyflag"))
                model.setShow_partner_bank_account(domain.getShowPartnerBankAccount());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("hide_payment_methoddirtyflag"))
                model.setHide_payment_method(domain.getHidePaymentMethod());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("communicationdirtyflag"))
                model.setCommunication(domain.getCommunication());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("payment_typedirtyflag"))
                model.setPayment_type(domain.getPaymentType());
            if((Boolean) domain.getExtensionparams().get("payment_datedirtyflag"))
                model.setPayment_date(domain.getPaymentDate());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("payment_method_codedirtyflag"))
                model.setPayment_method_code(domain.getPaymentMethodCode());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_id_textdirtyflag"))
                model.setWriteoff_account_id_text(domain.getWriteoffAccountIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("payment_method_id_textdirtyflag"))
                model.setPayment_method_id_text(domain.getPaymentMethodIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("partner_bank_account_iddirtyflag"))
                model.setPartner_bank_account_id(domain.getPartnerBankAccountId());
            if((Boolean) domain.getExtensionparams().get("payment_method_iddirtyflag"))
                model.setPayment_method_id(domain.getPaymentMethodId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_iddirtyflag"))
                model.setWriteoff_account_id(domain.getWriteoffAccountId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_abstract_payment convert2Domain( account_abstract_paymentClientModel model ,Account_abstract_payment domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_abstract_payment();
        }

        if(model.getPayment_difference_handlingDirtyFlag())
            domain.setPaymentDifferenceHandling(model.getPayment_difference_handling());
        if(model.getPartner_typeDirtyFlag())
            domain.setPartnerType(model.getPartner_type());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getPayment_differenceDirtyFlag())
            domain.setPaymentDifference(model.getPayment_difference());
        if(model.getMultiDirtyFlag())
            domain.setMulti(model.getMulti());
        if(model.getWriteoff_labelDirtyFlag())
            domain.setWriteoffLabel(model.getWriteoff_label());
        if(model.getShow_partner_bank_accountDirtyFlag())
            domain.setShowPartnerBankAccount(model.getShow_partner_bank_account());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getHide_payment_methodDirtyFlag())
            domain.setHidePaymentMethod(model.getHide_payment_method());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCommunicationDirtyFlag())
            domain.setCommunication(model.getCommunication());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPayment_typeDirtyFlag())
            domain.setPaymentType(model.getPayment_type());
        if(model.getPayment_dateDirtyFlag())
            domain.setPaymentDate(model.getPayment_date());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getPayment_method_codeDirtyFlag())
            domain.setPaymentMethodCode(model.getPayment_method_code());
        if(model.getWriteoff_account_id_textDirtyFlag())
            domain.setWriteoffAccountIdText(model.getWriteoff_account_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getPayment_method_id_textDirtyFlag())
            domain.setPaymentMethodIdText(model.getPayment_method_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getPartner_bank_account_idDirtyFlag())
            domain.setPartnerBankAccountId(model.getPartner_bank_account_id());
        if(model.getPayment_method_idDirtyFlag())
            domain.setPaymentMethodId(model.getPayment_method_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWriteoff_account_idDirtyFlag())
            domain.setWriteoffAccountId(model.getWriteoff_account_id());
        return domain ;
    }

}

    



