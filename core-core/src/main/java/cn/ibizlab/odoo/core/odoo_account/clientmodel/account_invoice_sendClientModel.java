package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_invoice_send] 对象
 */
public class account_invoice_sendClientModel implements Serializable{

    /**
     * 有效域
     */
    public String active_domain;

    @JsonIgnore
    public boolean active_domainDirtyFlag;
    
    /**
     * 添加签名
     */
    public String add_sign;

    @JsonIgnore
    public boolean add_signDirtyFlag;
    
    /**
     * 附件
     */
    public String attachment_ids;

    @JsonIgnore
    public boolean attachment_idsDirtyFlag;
    
    /**
     * 作者头像
     */
    public byte[] author_avatar;

    @JsonIgnore
    public boolean author_avatarDirtyFlag;
    
    /**
     * 作者
     */
    public Integer author_id;

    @JsonIgnore
    public boolean author_idDirtyFlag;
    
    /**
     * 删除邮件
     */
    public String auto_delete;

    @JsonIgnore
    public boolean auto_deleteDirtyFlag;
    
    /**
     * 删除消息副本
     */
    public String auto_delete_message;

    @JsonIgnore
    public boolean auto_delete_messageDirtyFlag;
    
    /**
     * 内容
     */
    public String body;

    @JsonIgnore
    public boolean bodyDirtyFlag;
    
    /**
     * 频道
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 下级信息
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 邮件撰写者
     */
    public Integer composer_id;

    @JsonIgnore
    public boolean composer_idDirtyFlag;
    
    /**
     * 写作模式
     */
    public String composition_mode;

    @JsonIgnore
    public boolean composition_modeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 来自
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * 有误差
     */
    public String has_error;

    @JsonIgnore
    public boolean has_errorDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * 不发送的发票
     */
    public String invoice_without_email;

    @JsonIgnore
    public boolean invoice_without_emailDirtyFlag;
    
    /**
     * EMail
     */
    public String is_email;

    @JsonIgnore
    public boolean is_emailDirtyFlag;
    
    /**
     * 记录内部备注
     */
    public String is_log;

    @JsonIgnore
    public boolean is_logDirtyFlag;
    
    /**
     * 打印
     */
    public String is_print;

    @JsonIgnore
    public boolean is_printDirtyFlag;
    
    /**
     * 布局
     */
    public String layout;

    @JsonIgnore
    public boolean layoutDirtyFlag;
    
    /**
     * 信
     */
    public String letter_ids;

    @JsonIgnore
    public boolean letter_idsDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String mailing_list_ids;

    @JsonIgnore
    public boolean mailing_list_idsDirtyFlag;
    
    /**
     * 邮件活动类型
     */
    public Integer mail_activity_type_id;

    @JsonIgnore
    public boolean mail_activity_type_idDirtyFlag;
    
    /**
     * 邮件发送服务器
     */
    public Integer mail_server_id;

    @JsonIgnore
    public boolean mail_server_idDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public Integer mass_mailing_campaign_id;

    @JsonIgnore
    public boolean mass_mailing_campaign_idDirtyFlag;
    
    /**
     * 群发邮件
     */
    public Integer mass_mailing_id;

    @JsonIgnore
    public boolean mass_mailing_idDirtyFlag;
    
    /**
     * 群发邮件标题
     */
    public String mass_mailing_name;

    @JsonIgnore
    public boolean mass_mailing_nameDirtyFlag;
    
    /**
     * Message-Id
     */
    public String message_id;

    @JsonIgnore
    public boolean message_idDirtyFlag;
    
    /**
     * 类型
     */
    public String message_type;

    @JsonIgnore
    public boolean message_typeDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 审核状态
     */
    public String moderation_status;

    @JsonIgnore
    public boolean moderation_statusDirtyFlag;
    
    /**
     * 审核人
     */
    public Integer moderator_id;

    @JsonIgnore
    public boolean moderator_idDirtyFlag;
    
    /**
     * 待处理
     */
    public String needaction;

    @JsonIgnore
    public boolean needactionDirtyFlag;
    
    /**
     * 待处理的业务伙伴
     */
    public String needaction_partner_ids;

    @JsonIgnore
    public boolean needaction_partner_idsDirtyFlag;
    
    /**
     * 需要审核
     */
    public String need_moderation;

    @JsonIgnore
    public boolean need_moderationDirtyFlag;
    
    /**
     * 通知
     */
    public String notification_ids;

    @JsonIgnore
    public boolean notification_idsDirtyFlag;
    
    /**
     * 通知关注者
     */
    public String notify;

    @JsonIgnore
    public boolean notifyDirtyFlag;
    
    /**
     * 线程无应答
     */
    public String no_auto_thread;

    @JsonIgnore
    public boolean no_auto_threadDirtyFlag;
    
    /**
     * 上级消息
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 额外的联系人
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 已打印
     */
    public String printed;

    @JsonIgnore
    public boolean printedDirtyFlag;
    
    /**
     * 相关评级
     */
    public String rating_ids;

    @JsonIgnore
    public boolean rating_idsDirtyFlag;
    
    /**
     * 评级值
     */
    public Double rating_value;

    @JsonIgnore
    public boolean rating_valueDirtyFlag;
    
    /**
     * 消息记录名称
     */
    public String record_name;

    @JsonIgnore
    public boolean record_nameDirtyFlag;
    
    /**
     * 回复 至
     */
    public String reply_to;

    @JsonIgnore
    public boolean reply_toDirtyFlag;
    
    /**
     * 相关文档编号
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 邮戳(s)
     */
    public Double snailmail_cost;

    @JsonIgnore
    public boolean snailmail_costDirtyFlag;
    
    /**
     * 透过邮递
     */
    public String snailmail_is_letter;

    @JsonIgnore
    public boolean snailmail_is_letterDirtyFlag;
    
    /**
     * 标星号邮件
     */
    public String starred;

    @JsonIgnore
    public boolean starredDirtyFlag;
    
    /**
     * 收藏夹
     */
    public String starred_partner_ids;

    @JsonIgnore
    public boolean starred_partner_idsDirtyFlag;
    
    /**
     * 主题
     */
    public String subject;

    @JsonIgnore
    public boolean subjectDirtyFlag;
    
    /**
     * 子类型
     */
    public Integer subtype_id;

    @JsonIgnore
    public boolean subtype_idDirtyFlag;
    
    /**
     * 使用模版
     */
    public Integer template_id;

    @JsonIgnore
    public boolean template_idDirtyFlag;
    
    /**
     * 使用模版
     */
    public String template_id_text;

    @JsonIgnore
    public boolean template_id_textDirtyFlag;
    
    /**
     * 追踪值
     */
    public String tracking_value_ids;

    @JsonIgnore
    public boolean tracking_value_idsDirtyFlag;
    
    /**
     * 使用有效域
     */
    public String use_active_domain;

    @JsonIgnore
    public boolean use_active_domainDirtyFlag;
    
    /**
     * 已发布
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效域]
     */
    @JsonProperty("active_domain")
    public String getActive_domain(){
        return this.active_domain ;
    }

    /**
     * 设置 [有效域]
     */
    @JsonProperty("active_domain")
    public void setActive_domain(String  active_domain){
        this.active_domain = active_domain ;
        this.active_domainDirtyFlag = true ;
    }

     /**
     * 获取 [有效域]脏标记
     */
    @JsonIgnore
    public boolean getActive_domainDirtyFlag(){
        return this.active_domainDirtyFlag ;
    }   

    /**
     * 获取 [添加签名]
     */
    @JsonProperty("add_sign")
    public String getAdd_sign(){
        return this.add_sign ;
    }

    /**
     * 设置 [添加签名]
     */
    @JsonProperty("add_sign")
    public void setAdd_sign(String  add_sign){
        this.add_sign = add_sign ;
        this.add_signDirtyFlag = true ;
    }

     /**
     * 获取 [添加签名]脏标记
     */
    @JsonIgnore
    public boolean getAdd_signDirtyFlag(){
        return this.add_signDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return this.attachment_ids ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return this.attachment_idsDirtyFlag ;
    }   

    /**
     * 获取 [作者头像]
     */
    @JsonProperty("author_avatar")
    public byte[] getAuthor_avatar(){
        return this.author_avatar ;
    }

    /**
     * 设置 [作者头像]
     */
    @JsonProperty("author_avatar")
    public void setAuthor_avatar(byte[]  author_avatar){
        this.author_avatar = author_avatar ;
        this.author_avatarDirtyFlag = true ;
    }

     /**
     * 获取 [作者头像]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_avatarDirtyFlag(){
        return this.author_avatarDirtyFlag ;
    }   

    /**
     * 获取 [作者]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return this.author_id ;
    }

    /**
     * 设置 [作者]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

     /**
     * 获取 [作者]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return this.author_idDirtyFlag ;
    }   

    /**
     * 获取 [删除邮件]
     */
    @JsonProperty("auto_delete")
    public String getAuto_delete(){
        return this.auto_delete ;
    }

    /**
     * 设置 [删除邮件]
     */
    @JsonProperty("auto_delete")
    public void setAuto_delete(String  auto_delete){
        this.auto_delete = auto_delete ;
        this.auto_deleteDirtyFlag = true ;
    }

     /**
     * 获取 [删除邮件]脏标记
     */
    @JsonIgnore
    public boolean getAuto_deleteDirtyFlag(){
        return this.auto_deleteDirtyFlag ;
    }   

    /**
     * 获取 [删除消息副本]
     */
    @JsonProperty("auto_delete_message")
    public String getAuto_delete_message(){
        return this.auto_delete_message ;
    }

    /**
     * 设置 [删除消息副本]
     */
    @JsonProperty("auto_delete_message")
    public void setAuto_delete_message(String  auto_delete_message){
        this.auto_delete_message = auto_delete_message ;
        this.auto_delete_messageDirtyFlag = true ;
    }

     /**
     * 获取 [删除消息副本]脏标记
     */
    @JsonIgnore
    public boolean getAuto_delete_messageDirtyFlag(){
        return this.auto_delete_messageDirtyFlag ;
    }   

    /**
     * 获取 [内容]
     */
    @JsonProperty("body")
    public String getBody(){
        return this.body ;
    }

    /**
     * 设置 [内容]
     */
    @JsonProperty("body")
    public void setBody(String  body){
        this.body = body ;
        this.bodyDirtyFlag = true ;
    }

     /**
     * 获取 [内容]脏标记
     */
    @JsonIgnore
    public boolean getBodyDirtyFlag(){
        return this.bodyDirtyFlag ;
    }   

    /**
     * 获取 [频道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [频道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [频道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [下级信息]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下级信息]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下级信息]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [邮件撰写者]
     */
    @JsonProperty("composer_id")
    public Integer getComposer_id(){
        return this.composer_id ;
    }

    /**
     * 设置 [邮件撰写者]
     */
    @JsonProperty("composer_id")
    public void setComposer_id(Integer  composer_id){
        this.composer_id = composer_id ;
        this.composer_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件撰写者]脏标记
     */
    @JsonIgnore
    public boolean getComposer_idDirtyFlag(){
        return this.composer_idDirtyFlag ;
    }   

    /**
     * 获取 [写作模式]
     */
    @JsonProperty("composition_mode")
    public String getComposition_mode(){
        return this.composition_mode ;
    }

    /**
     * 设置 [写作模式]
     */
    @JsonProperty("composition_mode")
    public void setComposition_mode(String  composition_mode){
        this.composition_mode = composition_mode ;
        this.composition_modeDirtyFlag = true ;
    }

     /**
     * 获取 [写作模式]脏标记
     */
    @JsonIgnore
    public boolean getComposition_modeDirtyFlag(){
        return this.composition_modeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [来自]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [来自]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [来自]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [有误差]
     */
    @JsonProperty("has_error")
    public String getHas_error(){
        return this.has_error ;
    }

    /**
     * 设置 [有误差]
     */
    @JsonProperty("has_error")
    public void setHas_error(String  has_error){
        this.has_error = has_error ;
        this.has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [有误差]脏标记
     */
    @JsonIgnore
    public boolean getHas_errorDirtyFlag(){
        return this.has_errorDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [不发送的发票]
     */
    @JsonProperty("invoice_without_email")
    public String getInvoice_without_email(){
        return this.invoice_without_email ;
    }

    /**
     * 设置 [不发送的发票]
     */
    @JsonProperty("invoice_without_email")
    public void setInvoice_without_email(String  invoice_without_email){
        this.invoice_without_email = invoice_without_email ;
        this.invoice_without_emailDirtyFlag = true ;
    }

     /**
     * 获取 [不发送的发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_without_emailDirtyFlag(){
        return this.invoice_without_emailDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("is_email")
    public String getIs_email(){
        return this.is_email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("is_email")
    public void setIs_email(String  is_email){
        this.is_email = is_email ;
        this.is_emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getIs_emailDirtyFlag(){
        return this.is_emailDirtyFlag ;
    }   

    /**
     * 获取 [记录内部备注]
     */
    @JsonProperty("is_log")
    public String getIs_log(){
        return this.is_log ;
    }

    /**
     * 设置 [记录内部备注]
     */
    @JsonProperty("is_log")
    public void setIs_log(String  is_log){
        this.is_log = is_log ;
        this.is_logDirtyFlag = true ;
    }

     /**
     * 获取 [记录内部备注]脏标记
     */
    @JsonIgnore
    public boolean getIs_logDirtyFlag(){
        return this.is_logDirtyFlag ;
    }   

    /**
     * 获取 [打印]
     */
    @JsonProperty("is_print")
    public String getIs_print(){
        return this.is_print ;
    }

    /**
     * 设置 [打印]
     */
    @JsonProperty("is_print")
    public void setIs_print(String  is_print){
        this.is_print = is_print ;
        this.is_printDirtyFlag = true ;
    }

     /**
     * 获取 [打印]脏标记
     */
    @JsonIgnore
    public boolean getIs_printDirtyFlag(){
        return this.is_printDirtyFlag ;
    }   

    /**
     * 获取 [布局]
     */
    @JsonProperty("layout")
    public String getLayout(){
        return this.layout ;
    }

    /**
     * 设置 [布局]
     */
    @JsonProperty("layout")
    public void setLayout(String  layout){
        this.layout = layout ;
        this.layoutDirtyFlag = true ;
    }

     /**
     * 获取 [布局]脏标记
     */
    @JsonIgnore
    public boolean getLayoutDirtyFlag(){
        return this.layoutDirtyFlag ;
    }   

    /**
     * 获取 [信]
     */
    @JsonProperty("letter_ids")
    public String getLetter_ids(){
        return this.letter_ids ;
    }

    /**
     * 设置 [信]
     */
    @JsonProperty("letter_ids")
    public void setLetter_ids(String  letter_ids){
        this.letter_ids = letter_ids ;
        this.letter_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信]脏标记
     */
    @JsonIgnore
    public boolean getLetter_idsDirtyFlag(){
        return this.letter_idsDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("mailing_list_ids")
    public String getMailing_list_ids(){
        return this.mailing_list_ids ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("mailing_list_ids")
    public void setMailing_list_ids(String  mailing_list_ids){
        this.mailing_list_ids = mailing_list_ids ;
        this.mailing_list_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getMailing_list_idsDirtyFlag(){
        return this.mailing_list_idsDirtyFlag ;
    }   

    /**
     * 获取 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return this.mail_activity_type_id ;
    }

    /**
     * 设置 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return this.mail_activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return this.mail_server_id ;
    }

    /**
     * 设置 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件发送服务器]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return this.mail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return this.mass_mailing_campaign_id ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return this.mass_mailing_campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mass_mailing_id")
    public Integer getMass_mailing_id(){
        return this.mass_mailing_id ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mass_mailing_id")
    public void setMass_mailing_id(Integer  mass_mailing_id){
        this.mass_mailing_id = mass_mailing_id ;
        this.mass_mailing_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idDirtyFlag(){
        return this.mass_mailing_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件标题]
     */
    @JsonProperty("mass_mailing_name")
    public String getMass_mailing_name(){
        return this.mass_mailing_name ;
    }

    /**
     * 设置 [群发邮件标题]
     */
    @JsonProperty("mass_mailing_name")
    public void setMass_mailing_name(String  mass_mailing_name){
        this.mass_mailing_name = mass_mailing_name ;
        this.mass_mailing_nameDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件标题]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_nameDirtyFlag(){
        return this.mass_mailing_nameDirtyFlag ;
    }   

    /**
     * 获取 [Message-Id]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [Message-Id]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

     /**
     * 获取 [Message-Id]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("message_type")
    public String getMessage_type(){
        return this.message_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("message_type")
    public void setMessage_type(String  message_type){
        this.message_type = message_type ;
        this.message_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getMessage_typeDirtyFlag(){
        return this.message_typeDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [审核状态]
     */
    @JsonProperty("moderation_status")
    public String getModeration_status(){
        return this.moderation_status ;
    }

    /**
     * 设置 [审核状态]
     */
    @JsonProperty("moderation_status")
    public void setModeration_status(String  moderation_status){
        this.moderation_status = moderation_status ;
        this.moderation_statusDirtyFlag = true ;
    }

     /**
     * 获取 [审核状态]脏标记
     */
    @JsonIgnore
    public boolean getModeration_statusDirtyFlag(){
        return this.moderation_statusDirtyFlag ;
    }   

    /**
     * 获取 [审核人]
     */
    @JsonProperty("moderator_id")
    public Integer getModerator_id(){
        return this.moderator_id ;
    }

    /**
     * 设置 [审核人]
     */
    @JsonProperty("moderator_id")
    public void setModerator_id(Integer  moderator_id){
        this.moderator_id = moderator_id ;
        this.moderator_idDirtyFlag = true ;
    }

     /**
     * 获取 [审核人]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idDirtyFlag(){
        return this.moderator_idDirtyFlag ;
    }   

    /**
     * 获取 [待处理]
     */
    @JsonProperty("needaction")
    public String getNeedaction(){
        return this.needaction ;
    }

    /**
     * 设置 [待处理]
     */
    @JsonProperty("needaction")
    public void setNeedaction(String  needaction){
        this.needaction = needaction ;
        this.needactionDirtyFlag = true ;
    }

     /**
     * 获取 [待处理]脏标记
     */
    @JsonIgnore
    public boolean getNeedactionDirtyFlag(){
        return this.needactionDirtyFlag ;
    }   

    /**
     * 获取 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public String getNeedaction_partner_ids(){
        return this.needaction_partner_ids ;
    }

    /**
     * 设置 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public void setNeedaction_partner_ids(String  needaction_partner_ids){
        this.needaction_partner_ids = needaction_partner_ids ;
        this.needaction_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getNeedaction_partner_idsDirtyFlag(){
        return this.needaction_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [需要审核]
     */
    @JsonProperty("need_moderation")
    public String getNeed_moderation(){
        return this.need_moderation ;
    }

    /**
     * 设置 [需要审核]
     */
    @JsonProperty("need_moderation")
    public void setNeed_moderation(String  need_moderation){
        this.need_moderation = need_moderation ;
        this.need_moderationDirtyFlag = true ;
    }

     /**
     * 获取 [需要审核]脏标记
     */
    @JsonIgnore
    public boolean getNeed_moderationDirtyFlag(){
        return this.need_moderationDirtyFlag ;
    }   

    /**
     * 获取 [通知]
     */
    @JsonProperty("notification_ids")
    public String getNotification_ids(){
        return this.notification_ids ;
    }

    /**
     * 设置 [通知]
     */
    @JsonProperty("notification_ids")
    public void setNotification_ids(String  notification_ids){
        this.notification_ids = notification_ids ;
        this.notification_idsDirtyFlag = true ;
    }

     /**
     * 获取 [通知]脏标记
     */
    @JsonIgnore
    public boolean getNotification_idsDirtyFlag(){
        return this.notification_idsDirtyFlag ;
    }   

    /**
     * 获取 [通知关注者]
     */
    @JsonProperty("notify")
    public String getNotify(){
        return this.notify ;
    }

    /**
     * 设置 [通知关注者]
     */
    @JsonProperty("notify")
    public void setNotify(String  notify){
        this.notify = notify ;
        this.notifyDirtyFlag = true ;
    }

     /**
     * 获取 [通知关注者]脏标记
     */
    @JsonIgnore
    public boolean getNotifyDirtyFlag(){
        return this.notifyDirtyFlag ;
    }   

    /**
     * 获取 [线程无应答]
     */
    @JsonProperty("no_auto_thread")
    public String getNo_auto_thread(){
        return this.no_auto_thread ;
    }

    /**
     * 设置 [线程无应答]
     */
    @JsonProperty("no_auto_thread")
    public void setNo_auto_thread(String  no_auto_thread){
        this.no_auto_thread = no_auto_thread ;
        this.no_auto_threadDirtyFlag = true ;
    }

     /**
     * 获取 [线程无应答]脏标记
     */
    @JsonIgnore
    public boolean getNo_auto_threadDirtyFlag(){
        return this.no_auto_threadDirtyFlag ;
    }   

    /**
     * 获取 [上级消息]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级消息]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级消息]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [额外的联系人]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [额外的联系人]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [额外的联系人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [已打印]
     */
    @JsonProperty("printed")
    public String getPrinted(){
        return this.printed ;
    }

    /**
     * 设置 [已打印]
     */
    @JsonProperty("printed")
    public void setPrinted(String  printed){
        this.printed = printed ;
        this.printedDirtyFlag = true ;
    }

     /**
     * 获取 [已打印]脏标记
     */
    @JsonIgnore
    public boolean getPrintedDirtyFlag(){
        return this.printedDirtyFlag ;
    }   

    /**
     * 获取 [相关评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [相关评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

     /**
     * 获取 [相关评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }   

    /**
     * 获取 [评级值]
     */
    @JsonProperty("rating_value")
    public Double getRating_value(){
        return this.rating_value ;
    }

    /**
     * 设置 [评级值]
     */
    @JsonProperty("rating_value")
    public void setRating_value(Double  rating_value){
        this.rating_value = rating_value ;
        this.rating_valueDirtyFlag = true ;
    }

     /**
     * 获取 [评级值]脏标记
     */
    @JsonIgnore
    public boolean getRating_valueDirtyFlag(){
        return this.rating_valueDirtyFlag ;
    }   

    /**
     * 获取 [消息记录名称]
     */
    @JsonProperty("record_name")
    public String getRecord_name(){
        return this.record_name ;
    }

    /**
     * 设置 [消息记录名称]
     */
    @JsonProperty("record_name")
    public void setRecord_name(String  record_name){
        this.record_name = record_name ;
        this.record_nameDirtyFlag = true ;
    }

     /**
     * 获取 [消息记录名称]脏标记
     */
    @JsonIgnore
    public boolean getRecord_nameDirtyFlag(){
        return this.record_nameDirtyFlag ;
    }   

    /**
     * 获取 [回复 至]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复 至]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

     /**
     * 获取 [回复 至]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }   

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [邮戳(s)]
     */
    @JsonProperty("snailmail_cost")
    public Double getSnailmail_cost(){
        return this.snailmail_cost ;
    }

    /**
     * 设置 [邮戳(s)]
     */
    @JsonProperty("snailmail_cost")
    public void setSnailmail_cost(Double  snailmail_cost){
        this.snailmail_cost = snailmail_cost ;
        this.snailmail_costDirtyFlag = true ;
    }

     /**
     * 获取 [邮戳(s)]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_costDirtyFlag(){
        return this.snailmail_costDirtyFlag ;
    }   

    /**
     * 获取 [透过邮递]
     */
    @JsonProperty("snailmail_is_letter")
    public String getSnailmail_is_letter(){
        return this.snailmail_is_letter ;
    }

    /**
     * 设置 [透过邮递]
     */
    @JsonProperty("snailmail_is_letter")
    public void setSnailmail_is_letter(String  snailmail_is_letter){
        this.snailmail_is_letter = snailmail_is_letter ;
        this.snailmail_is_letterDirtyFlag = true ;
    }

     /**
     * 获取 [透过邮递]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_is_letterDirtyFlag(){
        return this.snailmail_is_letterDirtyFlag ;
    }   

    /**
     * 获取 [标星号邮件]
     */
    @JsonProperty("starred")
    public String getStarred(){
        return this.starred ;
    }

    /**
     * 设置 [标星号邮件]
     */
    @JsonProperty("starred")
    public void setStarred(String  starred){
        this.starred = starred ;
        this.starredDirtyFlag = true ;
    }

     /**
     * 获取 [标星号邮件]脏标记
     */
    @JsonIgnore
    public boolean getStarredDirtyFlag(){
        return this.starredDirtyFlag ;
    }   

    /**
     * 获取 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public String getStarred_partner_ids(){
        return this.starred_partner_ids ;
    }

    /**
     * 设置 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public void setStarred_partner_ids(String  starred_partner_ids){
        this.starred_partner_ids = starred_partner_ids ;
        this.starred_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [收藏夹]脏标记
     */
    @JsonIgnore
    public boolean getStarred_partner_idsDirtyFlag(){
        return this.starred_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [主题]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

     /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }   

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return this.subtype_id ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return this.subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [使用模版]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return this.template_id ;
    }

    /**
     * 设置 [使用模版]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

     /**
     * 获取 [使用模版]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return this.template_idDirtyFlag ;
    }   

    /**
     * 获取 [使用模版]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return this.template_id_text ;
    }

    /**
     * 设置 [使用模版]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [使用模版]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return this.template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public String getTracking_value_ids(){
        return this.tracking_value_ids ;
    }

    /**
     * 设置 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public void setTracking_value_ids(String  tracking_value_ids){
        this.tracking_value_ids = tracking_value_ids ;
        this.tracking_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [追踪值]脏标记
     */
    @JsonIgnore
    public boolean getTracking_value_idsDirtyFlag(){
        return this.tracking_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [使用有效域]
     */
    @JsonProperty("use_active_domain")
    public String getUse_active_domain(){
        return this.use_active_domain ;
    }

    /**
     * 设置 [使用有效域]
     */
    @JsonProperty("use_active_domain")
    public void setUse_active_domain(String  use_active_domain){
        this.use_active_domain = use_active_domain ;
        this.use_active_domainDirtyFlag = true ;
    }

     /**
     * 获取 [使用有效域]脏标记
     */
    @JsonIgnore
    public boolean getUse_active_domainDirtyFlag(){
        return this.use_active_domainDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("active_domain") instanceof Boolean)&& map.get("active_domain")!=null){
			this.setActive_domain((String)map.get("active_domain"));
		}
		if(map.get("add_sign") instanceof Boolean){
			this.setAdd_sign(((Boolean)map.get("add_sign"))? "true" : "false");
		}
		if(!(map.get("attachment_ids") instanceof Boolean)&& map.get("attachment_ids")!=null){
			Object[] objs = (Object[])map.get("attachment_ids");
			if(objs.length > 0){
				Integer[] attachment_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttachment_ids(Arrays.toString(attachment_ids).replace(" ",""));
			}
		}
		if(!(map.get("author_avatar") instanceof Boolean)&& map.get("author_avatar")!=null){
			//暂时忽略
			//this.setAuthor_avatar(((String)map.get("author_avatar")).getBytes("UTF-8"));
		}
		if(!(map.get("author_id") instanceof Boolean)&& map.get("author_id")!=null){
			Object[] objs = (Object[])map.get("author_id");
			if(objs.length > 0){
				this.setAuthor_id((Integer)objs[0]);
			}
		}
		if(map.get("auto_delete") instanceof Boolean){
			this.setAuto_delete(((Boolean)map.get("auto_delete"))? "true" : "false");
		}
		if(map.get("auto_delete_message") instanceof Boolean){
			this.setAuto_delete_message(((Boolean)map.get("auto_delete_message"))? "true" : "false");
		}
		if(!(map.get("body") instanceof Boolean)&& map.get("body")!=null){
			this.setBody((String)map.get("body"));
		}
		if(!(map.get("channel_ids") instanceof Boolean)&& map.get("channel_ids")!=null){
			Object[] objs = (Object[])map.get("channel_ids");
			if(objs.length > 0){
				Integer[] channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_ids(Arrays.toString(channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("child_ids") instanceof Boolean)&& map.get("child_ids")!=null){
			Object[] objs = (Object[])map.get("child_ids");
			if(objs.length > 0){
				Integer[] child_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_ids(Arrays.toString(child_ids).replace(" ",""));
			}
		}
		if(!(map.get("composer_id") instanceof Boolean)&& map.get("composer_id")!=null){
			Object[] objs = (Object[])map.get("composer_id");
			if(objs.length > 0){
				this.setComposer_id((Integer)objs[0]);
			}
		}
		if(!(map.get("composition_mode") instanceof Boolean)&& map.get("composition_mode")!=null){
			this.setComposition_mode((String)map.get("composition_mode"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_from") instanceof Boolean)&& map.get("email_from")!=null){
			this.setEmail_from((String)map.get("email_from"));
		}
		if(map.get("has_error") instanceof Boolean){
			this.setHas_error(((Boolean)map.get("has_error"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_ids") instanceof Boolean)&& map.get("invoice_ids")!=null){
			Object[] objs = (Object[])map.get("invoice_ids");
			if(objs.length > 0){
				Integer[] invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_ids(Arrays.toString(invoice_ids).replace(" ",""));
			}
		}
		if(!(map.get("invoice_without_email") instanceof Boolean)&& map.get("invoice_without_email")!=null){
			this.setInvoice_without_email((String)map.get("invoice_without_email"));
		}
		if(map.get("is_email") instanceof Boolean){
			this.setIs_email(((Boolean)map.get("is_email"))? "true" : "false");
		}
		if(map.get("is_log") instanceof Boolean){
			this.setIs_log(((Boolean)map.get("is_log"))? "true" : "false");
		}
		if(map.get("is_print") instanceof Boolean){
			this.setIs_print(((Boolean)map.get("is_print"))? "true" : "false");
		}
		if(!(map.get("layout") instanceof Boolean)&& map.get("layout")!=null){
			this.setLayout((String)map.get("layout"));
		}
		if(!(map.get("letter_ids") instanceof Boolean)&& map.get("letter_ids")!=null){
			Object[] objs = (Object[])map.get("letter_ids");
			if(objs.length > 0){
				Integer[] letter_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setLetter_ids(Arrays.toString(letter_ids).replace(" ",""));
			}
		}
		if(!(map.get("mailing_list_ids") instanceof Boolean)&& map.get("mailing_list_ids")!=null){
			Object[] objs = (Object[])map.get("mailing_list_ids");
			if(objs.length > 0){
				Integer[] mailing_list_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMailing_list_ids(Arrays.toString(mailing_list_ids).replace(" ",""));
			}
		}
		if(!(map.get("mail_activity_type_id") instanceof Boolean)&& map.get("mail_activity_type_id")!=null){
			Object[] objs = (Object[])map.get("mail_activity_type_id");
			if(objs.length > 0){
				this.setMail_activity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_server_id") instanceof Boolean)&& map.get("mail_server_id")!=null){
			Object[] objs = (Object[])map.get("mail_server_id");
			if(objs.length > 0){
				this.setMail_server_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mass_mailing_campaign_id") instanceof Boolean)&& map.get("mass_mailing_campaign_id")!=null){
			Object[] objs = (Object[])map.get("mass_mailing_campaign_id");
			if(objs.length > 0){
				this.setMass_mailing_campaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mass_mailing_id") instanceof Boolean)&& map.get("mass_mailing_id")!=null){
			Object[] objs = (Object[])map.get("mass_mailing_id");
			if(objs.length > 0){
				this.setMass_mailing_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mass_mailing_name") instanceof Boolean)&& map.get("mass_mailing_name")!=null){
			this.setMass_mailing_name((String)map.get("mass_mailing_name"));
		}
		if(!(map.get("message_id") instanceof Boolean)&& map.get("message_id")!=null){
			this.setMessage_id((String)map.get("message_id"));
		}
		if(!(map.get("message_type") instanceof Boolean)&& map.get("message_type")!=null){
			this.setMessage_type((String)map.get("message_type"));
		}
		if(!(map.get("model") instanceof Boolean)&& map.get("model")!=null){
			this.setModel((String)map.get("model"));
		}
		if(!(map.get("moderation_status") instanceof Boolean)&& map.get("moderation_status")!=null){
			this.setModeration_status((String)map.get("moderation_status"));
		}
		if(!(map.get("moderator_id") instanceof Boolean)&& map.get("moderator_id")!=null){
			Object[] objs = (Object[])map.get("moderator_id");
			if(objs.length > 0){
				this.setModerator_id((Integer)objs[0]);
			}
		}
		if(map.get("needaction") instanceof Boolean){
			this.setNeedaction(((Boolean)map.get("needaction"))? "true" : "false");
		}
		if(!(map.get("needaction_partner_ids") instanceof Boolean)&& map.get("needaction_partner_ids")!=null){
			Object[] objs = (Object[])map.get("needaction_partner_ids");
			if(objs.length > 0){
				Integer[] needaction_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setNeedaction_partner_ids(Arrays.toString(needaction_partner_ids).replace(" ",""));
			}
		}
		if(map.get("need_moderation") instanceof Boolean){
			this.setNeed_moderation(((Boolean)map.get("need_moderation"))? "true" : "false");
		}
		if(!(map.get("notification_ids") instanceof Boolean)&& map.get("notification_ids")!=null){
			Object[] objs = (Object[])map.get("notification_ids");
			if(objs.length > 0){
				Integer[] notification_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setNotification_ids(Arrays.toString(notification_ids).replace(" ",""));
			}
		}
		if(map.get("notify") instanceof Boolean){
			this.setNotify(((Boolean)map.get("notify"))? "true" : "false");
		}
		if(map.get("no_auto_thread") instanceof Boolean){
			this.setNo_auto_thread(((Boolean)map.get("no_auto_thread"))? "true" : "false");
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_ids") instanceof Boolean)&& map.get("partner_ids")!=null){
			Object[] objs = (Object[])map.get("partner_ids");
			if(objs.length > 0){
				Integer[] partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPartner_ids(Arrays.toString(partner_ids).replace(" ",""));
			}
		}
		if(map.get("printed") instanceof Boolean){
			this.setPrinted(((Boolean)map.get("printed"))? "true" : "false");
		}
		if(!(map.get("rating_ids") instanceof Boolean)&& map.get("rating_ids")!=null){
			Object[] objs = (Object[])map.get("rating_ids");
			if(objs.length > 0){
				Integer[] rating_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRating_ids(Arrays.toString(rating_ids).replace(" ",""));
			}
		}
		if(!(map.get("rating_value") instanceof Boolean)&& map.get("rating_value")!=null){
			this.setRating_value((Double)map.get("rating_value"));
		}
		if(!(map.get("record_name") instanceof Boolean)&& map.get("record_name")!=null){
			this.setRecord_name((String)map.get("record_name"));
		}
		if(!(map.get("reply_to") instanceof Boolean)&& map.get("reply_to")!=null){
			this.setReply_to((String)map.get("reply_to"));
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("snailmail_cost") instanceof Boolean)&& map.get("snailmail_cost")!=null){
			this.setSnailmail_cost((Double)map.get("snailmail_cost"));
		}
		if(map.get("snailmail_is_letter") instanceof Boolean){
			this.setSnailmail_is_letter(((Boolean)map.get("snailmail_is_letter"))? "true" : "false");
		}
		if(map.get("starred") instanceof Boolean){
			this.setStarred(((Boolean)map.get("starred"))? "true" : "false");
		}
		if(!(map.get("starred_partner_ids") instanceof Boolean)&& map.get("starred_partner_ids")!=null){
			Object[] objs = (Object[])map.get("starred_partner_ids");
			if(objs.length > 0){
				Integer[] starred_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setStarred_partner_ids(Arrays.toString(starred_partner_ids).replace(" ",""));
			}
		}
		if(!(map.get("subject") instanceof Boolean)&& map.get("subject")!=null){
			this.setSubject((String)map.get("subject"));
		}
		if(!(map.get("subtype_id") instanceof Boolean)&& map.get("subtype_id")!=null){
			Object[] objs = (Object[])map.get("subtype_id");
			if(objs.length > 0){
				this.setSubtype_id((Integer)objs[0]);
			}
		}
		if(!(map.get("template_id") instanceof Boolean)&& map.get("template_id")!=null){
			Object[] objs = (Object[])map.get("template_id");
			if(objs.length > 0){
				this.setTemplate_id((Integer)objs[0]);
			}
		}
		if(!(map.get("template_id") instanceof Boolean)&& map.get("template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("template_id");
			if(objs.length > 1){
				this.setTemplate_id_text((String)objs[1]);
			}
		}
		if(!(map.get("tracking_value_ids") instanceof Boolean)&& map.get("tracking_value_ids")!=null){
			Object[] objs = (Object[])map.get("tracking_value_ids");
			if(objs.length > 0){
				Integer[] tracking_value_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTracking_value_ids(Arrays.toString(tracking_value_ids).replace(" ",""));
			}
		}
		if(map.get("use_active_domain") instanceof Boolean){
			this.setUse_active_domain(((Boolean)map.get("use_active_domain"))? "true" : "false");
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive_domain()!=null&&this.getActive_domainDirtyFlag()){
			map.put("active_domain",this.getActive_domain());
		}else if(this.getActive_domainDirtyFlag()){
			map.put("active_domain",false);
		}
		if(this.getAdd_sign()!=null&&this.getAdd_signDirtyFlag()){
			map.put("add_sign",Boolean.parseBoolean(this.getAdd_sign()));		
		}		if(this.getAttachment_ids()!=null&&this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",this.getAttachment_ids());
		}else if(this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",false);
		}
		if(this.getAuthor_avatar()!=null&&this.getAuthor_avatarDirtyFlag()){
			//暂不支持binary类型author_avatar
		}else if(this.getAuthor_avatarDirtyFlag()){
			map.put("author_avatar",false);
		}
		if(this.getAuthor_id()!=null&&this.getAuthor_idDirtyFlag()){
			map.put("author_id",this.getAuthor_id());
		}else if(this.getAuthor_idDirtyFlag()){
			map.put("author_id",false);
		}
		if(this.getAuto_delete()!=null&&this.getAuto_deleteDirtyFlag()){
			map.put("auto_delete",Boolean.parseBoolean(this.getAuto_delete()));		
		}		if(this.getAuto_delete_message()!=null&&this.getAuto_delete_messageDirtyFlag()){
			map.put("auto_delete_message",Boolean.parseBoolean(this.getAuto_delete_message()));		
		}		if(this.getBody()!=null&&this.getBodyDirtyFlag()){
			map.put("body",this.getBody());
		}else if(this.getBodyDirtyFlag()){
			map.put("body",false);
		}
		if(this.getChannel_ids()!=null&&this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",this.getChannel_ids());
		}else if(this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",false);
		}
		if(this.getChild_ids()!=null&&this.getChild_idsDirtyFlag()){
			map.put("child_ids",this.getChild_ids());
		}else if(this.getChild_idsDirtyFlag()){
			map.put("child_ids",false);
		}
		if(this.getComposer_id()!=null&&this.getComposer_idDirtyFlag()){
			map.put("composer_id",this.getComposer_id());
		}else if(this.getComposer_idDirtyFlag()){
			map.put("composer_id",false);
		}
		if(this.getComposition_mode()!=null&&this.getComposition_modeDirtyFlag()){
			map.put("composition_mode",this.getComposition_mode());
		}else if(this.getComposition_modeDirtyFlag()){
			map.put("composition_mode",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_from()!=null&&this.getEmail_fromDirtyFlag()){
			map.put("email_from",this.getEmail_from());
		}else if(this.getEmail_fromDirtyFlag()){
			map.put("email_from",false);
		}
		if(this.getHas_error()!=null&&this.getHas_errorDirtyFlag()){
			map.put("has_error",Boolean.parseBoolean(this.getHas_error()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_ids()!=null&&this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",this.getInvoice_ids());
		}else if(this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",false);
		}
		if(this.getInvoice_without_email()!=null&&this.getInvoice_without_emailDirtyFlag()){
			map.put("invoice_without_email",this.getInvoice_without_email());
		}else if(this.getInvoice_without_emailDirtyFlag()){
			map.put("invoice_without_email",false);
		}
		if(this.getIs_email()!=null&&this.getIs_emailDirtyFlag()){
			map.put("is_email",Boolean.parseBoolean(this.getIs_email()));		
		}		if(this.getIs_log()!=null&&this.getIs_logDirtyFlag()){
			map.put("is_log",Boolean.parseBoolean(this.getIs_log()));		
		}		if(this.getIs_print()!=null&&this.getIs_printDirtyFlag()){
			map.put("is_print",Boolean.parseBoolean(this.getIs_print()));		
		}		if(this.getLayout()!=null&&this.getLayoutDirtyFlag()){
			map.put("layout",this.getLayout());
		}else if(this.getLayoutDirtyFlag()){
			map.put("layout",false);
		}
		if(this.getLetter_ids()!=null&&this.getLetter_idsDirtyFlag()){
			map.put("letter_ids",this.getLetter_ids());
		}else if(this.getLetter_idsDirtyFlag()){
			map.put("letter_ids",false);
		}
		if(this.getMailing_list_ids()!=null&&this.getMailing_list_idsDirtyFlag()){
			map.put("mailing_list_ids",this.getMailing_list_ids());
		}else if(this.getMailing_list_idsDirtyFlag()){
			map.put("mailing_list_ids",false);
		}
		if(this.getMail_activity_type_id()!=null&&this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",this.getMail_activity_type_id());
		}else if(this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",false);
		}
		if(this.getMail_server_id()!=null&&this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",this.getMail_server_id());
		}else if(this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",false);
		}
		if(this.getMass_mailing_campaign_id()!=null&&this.getMass_mailing_campaign_idDirtyFlag()){
			map.put("mass_mailing_campaign_id",this.getMass_mailing_campaign_id());
		}else if(this.getMass_mailing_campaign_idDirtyFlag()){
			map.put("mass_mailing_campaign_id",false);
		}
		if(this.getMass_mailing_id()!=null&&this.getMass_mailing_idDirtyFlag()){
			map.put("mass_mailing_id",this.getMass_mailing_id());
		}else if(this.getMass_mailing_idDirtyFlag()){
			map.put("mass_mailing_id",false);
		}
		if(this.getMass_mailing_name()!=null&&this.getMass_mailing_nameDirtyFlag()){
			map.put("mass_mailing_name",this.getMass_mailing_name());
		}else if(this.getMass_mailing_nameDirtyFlag()){
			map.put("mass_mailing_name",false);
		}
		if(this.getMessage_id()!=null&&this.getMessage_idDirtyFlag()){
			map.put("message_id",this.getMessage_id());
		}else if(this.getMessage_idDirtyFlag()){
			map.put("message_id",false);
		}
		if(this.getMessage_type()!=null&&this.getMessage_typeDirtyFlag()){
			map.put("message_type",this.getMessage_type());
		}else if(this.getMessage_typeDirtyFlag()){
			map.put("message_type",false);
		}
		if(this.getModel()!=null&&this.getModelDirtyFlag()){
			map.put("model",this.getModel());
		}else if(this.getModelDirtyFlag()){
			map.put("model",false);
		}
		if(this.getModeration_status()!=null&&this.getModeration_statusDirtyFlag()){
			map.put("moderation_status",this.getModeration_status());
		}else if(this.getModeration_statusDirtyFlag()){
			map.put("moderation_status",false);
		}
		if(this.getModerator_id()!=null&&this.getModerator_idDirtyFlag()){
			map.put("moderator_id",this.getModerator_id());
		}else if(this.getModerator_idDirtyFlag()){
			map.put("moderator_id",false);
		}
		if(this.getNeedaction()!=null&&this.getNeedactionDirtyFlag()){
			map.put("needaction",Boolean.parseBoolean(this.getNeedaction()));		
		}		if(this.getNeedaction_partner_ids()!=null&&this.getNeedaction_partner_idsDirtyFlag()){
			map.put("needaction_partner_ids",this.getNeedaction_partner_ids());
		}else if(this.getNeedaction_partner_idsDirtyFlag()){
			map.put("needaction_partner_ids",false);
		}
		if(this.getNeed_moderation()!=null&&this.getNeed_moderationDirtyFlag()){
			map.put("need_moderation",Boolean.parseBoolean(this.getNeed_moderation()));		
		}		if(this.getNotification_ids()!=null&&this.getNotification_idsDirtyFlag()){
			map.put("notification_ids",this.getNotification_ids());
		}else if(this.getNotification_idsDirtyFlag()){
			map.put("notification_ids",false);
		}
		if(this.getNotify()!=null&&this.getNotifyDirtyFlag()){
			map.put("notify",Boolean.parseBoolean(this.getNotify()));		
		}		if(this.getNo_auto_thread()!=null&&this.getNo_auto_threadDirtyFlag()){
			map.put("no_auto_thread",Boolean.parseBoolean(this.getNo_auto_thread()));		
		}		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_ids()!=null&&this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",this.getPartner_ids());
		}else if(this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",false);
		}
		if(this.getPrinted()!=null&&this.getPrintedDirtyFlag()){
			map.put("printed",Boolean.parseBoolean(this.getPrinted()));		
		}		if(this.getRating_ids()!=null&&this.getRating_idsDirtyFlag()){
			map.put("rating_ids",this.getRating_ids());
		}else if(this.getRating_idsDirtyFlag()){
			map.put("rating_ids",false);
		}
		if(this.getRating_value()!=null&&this.getRating_valueDirtyFlag()){
			map.put("rating_value",this.getRating_value());
		}else if(this.getRating_valueDirtyFlag()){
			map.put("rating_value",false);
		}
		if(this.getRecord_name()!=null&&this.getRecord_nameDirtyFlag()){
			map.put("record_name",this.getRecord_name());
		}else if(this.getRecord_nameDirtyFlag()){
			map.put("record_name",false);
		}
		if(this.getReply_to()!=null&&this.getReply_toDirtyFlag()){
			map.put("reply_to",this.getReply_to());
		}else if(this.getReply_toDirtyFlag()){
			map.put("reply_to",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getSnailmail_cost()!=null&&this.getSnailmail_costDirtyFlag()){
			map.put("snailmail_cost",this.getSnailmail_cost());
		}else if(this.getSnailmail_costDirtyFlag()){
			map.put("snailmail_cost",false);
		}
		if(this.getSnailmail_is_letter()!=null&&this.getSnailmail_is_letterDirtyFlag()){
			map.put("snailmail_is_letter",Boolean.parseBoolean(this.getSnailmail_is_letter()));		
		}		if(this.getStarred()!=null&&this.getStarredDirtyFlag()){
			map.put("starred",Boolean.parseBoolean(this.getStarred()));		
		}		if(this.getStarred_partner_ids()!=null&&this.getStarred_partner_idsDirtyFlag()){
			map.put("starred_partner_ids",this.getStarred_partner_ids());
		}else if(this.getStarred_partner_idsDirtyFlag()){
			map.put("starred_partner_ids",false);
		}
		if(this.getSubject()!=null&&this.getSubjectDirtyFlag()){
			map.put("subject",this.getSubject());
		}else if(this.getSubjectDirtyFlag()){
			map.put("subject",false);
		}
		if(this.getSubtype_id()!=null&&this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",this.getSubtype_id());
		}else if(this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",false);
		}
		if(this.getTemplate_id()!=null&&this.getTemplate_idDirtyFlag()){
			map.put("template_id",this.getTemplate_id());
		}else if(this.getTemplate_idDirtyFlag()){
			map.put("template_id",false);
		}
		if(this.getTemplate_id_text()!=null&&this.getTemplate_id_textDirtyFlag()){
			//忽略文本外键template_id_text
		}else if(this.getTemplate_id_textDirtyFlag()){
			map.put("template_id",false);
		}
		if(this.getTracking_value_ids()!=null&&this.getTracking_value_idsDirtyFlag()){
			map.put("tracking_value_ids",this.getTracking_value_ids());
		}else if(this.getTracking_value_idsDirtyFlag()){
			map.put("tracking_value_ids",false);
		}
		if(this.getUse_active_domain()!=null&&this.getUse_active_domainDirtyFlag()){
			map.put("use_active_domain",Boolean.parseBoolean(this.getUse_active_domain()));		
		}		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
