package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;

/**
 * 实体 [工作餐产品类别] 存储对象
 */
public interface Lunch_product_categoryRepository extends Repository<Lunch_product_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context);

    Lunch_product_category convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category domain , Lunch_product_category po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category convert2Domain( Lunch_product_category po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category domain) ;

}
