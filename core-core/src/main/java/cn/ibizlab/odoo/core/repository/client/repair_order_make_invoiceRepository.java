package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.repair_order_make_invoice;

/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
public interface repair_order_make_invoiceRepository{


    public repair_order_make_invoice createPO() ;
        public List<repair_order_make_invoice> search();

        public void update(repair_order_make_invoice repair_order_make_invoice);

        public void create(repair_order_make_invoice repair_order_make_invoice);

        public void get(String id);

        public void updateBatch(repair_order_make_invoice repair_order_make_invoice);

        public void createBatch(repair_order_make_invoice repair_order_make_invoice);

        public void removeBatch(String id);

        public void remove(String id);


}
