package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;

/**
 * 实体 [配置安装器] 存储对象
 */
public interface Res_config_installerRepository extends Repository<Res_config_installer> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context);

    Res_config_installer convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer domain , Res_config_installer po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer convert2Domain( Res_config_installer po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer domain) ;

}
