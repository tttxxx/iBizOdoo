package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_document;

/**
 * 实体[mrp_document] 服务对象接口
 */
public interface mrp_documentRepository{


    public mrp_document createPO() ;
        public void removeBatch(String id);

        public void create(mrp_document mrp_document);

        public List<mrp_document> search();

        public void updateBatch(mrp_document mrp_document);

        public void createBatch(mrp_document mrp_document);

        public void get(String id);

        public void remove(String id);

        public void update(mrp_document mrp_document);


}
