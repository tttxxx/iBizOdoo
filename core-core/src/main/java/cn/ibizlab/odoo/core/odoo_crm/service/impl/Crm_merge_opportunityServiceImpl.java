package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_merge_opportunityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_merge_opportunityOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_merge_opportunityClientModel;

/**
 * 实体[合并商机] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_merge_opportunityServiceImpl implements ICrm_merge_opportunityService {

    @Autowired
    crm_merge_opportunityOdooClient crm_merge_opportunityOdooClient;


    @Override
    public boolean update(Crm_merge_opportunity et) {
        crm_merge_opportunityClientModel clientModel = convert2Model(et,null);
		crm_merge_opportunityOdooClient.update(clientModel);
        Crm_merge_opportunity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_merge_opportunity> list){
    }

    @Override
    public boolean create(Crm_merge_opportunity et) {
        crm_merge_opportunityClientModel clientModel = convert2Model(et,null);
		crm_merge_opportunityOdooClient.create(clientModel);
        Crm_merge_opportunity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_merge_opportunity> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_merge_opportunityClientModel clientModel = new crm_merge_opportunityClientModel();
        clientModel.setId(id);
		crm_merge_opportunityOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Crm_merge_opportunity get(Integer id) {
        crm_merge_opportunityClientModel clientModel = new crm_merge_opportunityClientModel();
        clientModel.setId(id);
		crm_merge_opportunityOdooClient.get(clientModel);
        Crm_merge_opportunity et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_merge_opportunity();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_merge_opportunity> searchDefault(Crm_merge_opportunitySearchContext context) {
        List<Crm_merge_opportunity> list = new ArrayList<Crm_merge_opportunity>();
        Page<crm_merge_opportunityClientModel> clientModelList = crm_merge_opportunityOdooClient.search(context);
        for(crm_merge_opportunityClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_merge_opportunity>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_merge_opportunityClientModel convert2Model(Crm_merge_opportunity domain , crm_merge_opportunityClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_merge_opportunityClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("opportunity_idsdirtyflag"))
                model.setOpportunity_ids(domain.getOpportunityIds());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_merge_opportunity convert2Domain( crm_merge_opportunityClientModel model ,Crm_merge_opportunity domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_merge_opportunity();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getOpportunity_idsDirtyFlag())
            domain.setOpportunityIds(model.getOpportunity_ids());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        return domain ;
    }

}

    



