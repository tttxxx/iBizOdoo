package cn.ibizlab.odoo.core.odoo_lunch.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [午餐提醒] 对象
 */
@Data
public class Lunch_alert extends EntityClient implements Serializable {

    /**
     * 日
     */
    @DEField(name = "specific_day")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "specific_day" , format="yyyy-MM-dd")
    @JsonProperty("specific_day")
    private Timestamp specificDay;

    /**
     * 显示
     */
    @JSONField(name = "display")
    @JsonProperty("display")
    private String display;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 周四
     */
    @JSONField(name = "thursday")
    @JsonProperty("thursday")
    private String thursday;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 周三
     */
    @JSONField(name = "wednesday")
    @JsonProperty("wednesday")
    private String wednesday;

    /**
     * 周二
     */
    @JSONField(name = "tuesday")
    @JsonProperty("tuesday")
    private String tuesday;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 消息
     */
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;

    /**
     * 重新提起
     */
    @DEField(name = "alert_type")
    @JSONField(name = "alert_type")
    @JsonProperty("alert_type")
    private String alertType;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 周六
     */
    @JSONField(name = "saturday")
    @JsonProperty("saturday")
    private String saturday;

    /**
     * 介于
     */
    @DEField(name = "start_hour")
    @JSONField(name = "start_hour")
    @JsonProperty("start_hour")
    private Double startHour;

    /**
     * 周一
     */
    @JSONField(name = "monday")
    @JsonProperty("monday")
    private String monday;

    /**
     * 周五
     */
    @JSONField(name = "friday")
    @JsonProperty("friday")
    private String friday;

    /**
     * 和
     */
    @DEField(name = "end_hour")
    @JSONField(name = "end_hour")
    @JsonProperty("end_hour")
    private Double endHour;

    /**
     * 周日
     */
    @JSONField(name = "sunday")
    @JsonProperty("sunday")
    private String sunday;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 供应商
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 供应商
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [日]
     */
    public void setSpecificDay(Timestamp specificDay){
        this.specificDay = specificDay ;
        this.modify("specific_day",specificDay);
    }
    /**
     * 设置 [周四]
     */
    public void setThursday(String thursday){
        this.thursday = thursday ;
        this.modify("thursday",thursday);
    }
    /**
     * 设置 [周三]
     */
    public void setWednesday(String wednesday){
        this.wednesday = wednesday ;
        this.modify("wednesday",wednesday);
    }
    /**
     * 设置 [周二]
     */
    public void setTuesday(String tuesday){
        this.tuesday = tuesday ;
        this.modify("tuesday",tuesday);
    }
    /**
     * 设置 [消息]
     */
    public void setMessage(String message){
        this.message = message ;
        this.modify("message",message);
    }
    /**
     * 设置 [重新提起]
     */
    public void setAlertType(String alertType){
        this.alertType = alertType ;
        this.modify("alert_type",alertType);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [周六]
     */
    public void setSaturday(String saturday){
        this.saturday = saturday ;
        this.modify("saturday",saturday);
    }
    /**
     * 设置 [介于]
     */
    public void setStartHour(Double startHour){
        this.startHour = startHour ;
        this.modify("start_hour",startHour);
    }
    /**
     * 设置 [周一]
     */
    public void setMonday(String monday){
        this.monday = monday ;
        this.modify("monday",monday);
    }
    /**
     * 设置 [周五]
     */
    public void setFriday(String friday){
        this.friday = friday ;
        this.modify("friday",friday);
    }
    /**
     * 设置 [和]
     */
    public void setEndHour(Double endHour){
        this.endHour = endHour ;
        this.modify("end_hour",endHour);
    }
    /**
     * 设置 [周日]
     */
    public void setSunday(String sunday){
        this.sunday = sunday ;
        this.modify("sunday",sunday);
    }
    /**
     * 设置 [供应商]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

}


