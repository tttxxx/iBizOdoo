package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_language_import;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_language_import] 服务对象接口
 */
public interface Ibase_language_importClientService{

    public Ibase_language_import createModel() ;

    public Page<Ibase_language_import> search(SearchContext context);

    public void removeBatch(List<Ibase_language_import> base_language_imports);

    public void create(Ibase_language_import base_language_import);

    public void update(Ibase_language_import base_language_import);

    public void remove(Ibase_language_import base_language_import);

    public void get(Ibase_language_import base_language_import);

    public void createBatch(List<Ibase_language_import> base_language_imports);

    public void updateBatch(List<Ibase_language_import> base_language_imports);

    public Page<Ibase_language_import> select(SearchContext context);

}
