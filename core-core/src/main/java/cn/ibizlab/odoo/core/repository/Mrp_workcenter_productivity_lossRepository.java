package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;

/**
 * 实体 [工作中心生产力损失] 存储对象
 */
public interface Mrp_workcenter_productivity_lossRepository extends Repository<Mrp_workcenter_productivity_loss> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context);

    Mrp_workcenter_productivity_loss convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss domain , Mrp_workcenter_productivity_loss po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss convert2Domain( Mrp_workcenter_productivity_loss po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss domain) ;

}
