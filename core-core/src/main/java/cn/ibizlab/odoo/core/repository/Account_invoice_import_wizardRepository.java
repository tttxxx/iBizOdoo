package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;

/**
 * 实体 [从文件中导入你的供应商账单。] 存储对象
 */
public interface Account_invoice_import_wizardRepository extends Repository<Account_invoice_import_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_invoice_import_wizard> searchDefault(Account_invoice_import_wizardSearchContext context);

    Account_invoice_import_wizard convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard domain , Account_invoice_import_wizard po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard convert2Domain( Account_invoice_import_wizard po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard domain) ;

}
