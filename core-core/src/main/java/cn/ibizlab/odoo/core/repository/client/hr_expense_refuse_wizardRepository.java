package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_expense_refuse_wizard;

/**
 * 实体[hr_expense_refuse_wizard] 服务对象接口
 */
public interface hr_expense_refuse_wizardRepository{


    public hr_expense_refuse_wizard createPO() ;
        public void remove(String id);

        public void createBatch(hr_expense_refuse_wizard hr_expense_refuse_wizard);

        public List<hr_expense_refuse_wizard> search();

        public void get(String id);

        public void updateBatch(hr_expense_refuse_wizard hr_expense_refuse_wizard);

        public void removeBatch(String id);

        public void update(hr_expense_refuse_wizard hr_expense_refuse_wizard);

        public void create(hr_expense_refuse_wizard hr_expense_refuse_wizard);


}
