package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;

/**
 * 实体 [调查问题] 存储模型
 */
public interface Survey_question{

    /**
     * 表格类型
     */
    String getMatrix_subtype();

    void setMatrix_subtype(String matrix_subtype);

    /**
     * 获取 [表格类型]脏标记
     */
    boolean getMatrix_subtypeDirtyFlag();

    /**
     * 信息：验证错误
     */
    String getValidation_error_msg();

    void setValidation_error_msg(String validation_error_msg);

    /**
     * 获取 [信息：验证错误]脏标记
     */
    boolean getValidation_error_msgDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 问题类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [问题类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 显示评论字段
     */
    String getComments_allowed();

    void setComments_allowed(String comments_allowed);

    /**
     * 获取 [显示评论字段]脏标记
     */
    boolean getComments_allowedDirtyFlag();

    /**
     * 最大日期
     */
    Timestamp getValidation_max_date();

    void setValidation_max_date(Timestamp validation_max_date);

    /**
     * 获取 [最大日期]脏标记
     */
    boolean getValidation_max_dateDirtyFlag();

    /**
     * 表格行数
     */
    String getLabels_ids_2();

    void setLabels_ids_2(String labels_ids_2);

    /**
     * 获取 [表格行数]脏标记
     */
    boolean getLabels_ids_2DirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最大值
     */
    Double getValidation_max_float_value();

    void setValidation_max_float_value(Double validation_max_float_value);

    /**
     * 获取 [最大值]脏标记
     */
    boolean getValidation_max_float_valueDirtyFlag();

    /**
     * 问题名称
     */
    String getQuestion();

    void setQuestion(String question);

    /**
     * 获取 [问题名称]脏标记
     */
    boolean getQuestionDirtyFlag();

    /**
     * 答案类型
     */
    String getLabels_ids();

    void setLabels_ids(String labels_ids);

    /**
     * 获取 [答案类型]脏标记
     */
    boolean getLabels_idsDirtyFlag();

    /**
     * 最小值
     */
    Double getValidation_min_float_value();

    void setValidation_min_float_value(Double validation_min_float_value);

    /**
     * 获取 [最小值]脏标记
     */
    boolean getValidation_min_float_valueDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 必答问题
     */
    String getConstr_mandatory();

    void setConstr_mandatory(String constr_mandatory);

    /**
     * 获取 [必答问题]脏标记
     */
    boolean getConstr_mandatoryDirtyFlag();

    /**
     * 答案
     */
    String getUser_input_line_ids();

    void setUser_input_line_ids(String user_input_line_ids);

    /**
     * 获取 [答案]脏标记
     */
    boolean getUser_input_line_idsDirtyFlag();

    /**
     * 评论消息
     */
    String getComments_message();

    void setComments_message(String comments_message);

    /**
     * 获取 [评论消息]脏标记
     */
    boolean getComments_messageDirtyFlag();

    /**
     * 评论字段是答案选项
     */
    String getComment_count_as_answer();

    void setComment_count_as_answer(String comment_count_as_answer);

    /**
     * 获取 [评论字段是答案选项]脏标记
     */
    boolean getComment_count_as_answerDirtyFlag();

    /**
     * 验证文本
     */
    String getValidation_required();

    void setValidation_required(String validation_required);

    /**
     * 获取 [验证文本]脏标记
     */
    boolean getValidation_requiredDirtyFlag();

    /**
     * 输入必须是EMail
     */
    String getValidation_email();

    void setValidation_email(String validation_email);

    /**
     * 获取 [输入必须是EMail]脏标记
     */
    boolean getValidation_emailDirtyFlag();

    /**
     * 最小日期
     */
    Timestamp getValidation_min_date();

    void setValidation_min_date(Timestamp validation_min_date);

    /**
     * 获取 [最小日期]脏标记
     */
    boolean getValidation_min_dateDirtyFlag();

    /**
     * 错误消息
     */
    String getConstr_error_msg();

    void setConstr_error_msg(String constr_error_msg);

    /**
     * 获取 [错误消息]脏标记
     */
    boolean getConstr_error_msgDirtyFlag();

    /**
     * 最大文本长度
     */
    Integer getValidation_length_max();

    void setValidation_length_max(Integer validation_length_max);

    /**
     * 获取 [最大文本长度]脏标记
     */
    boolean getValidation_length_maxDirtyFlag();

    /**
     * 栏位数
     */
    String getColumn_nb();

    void setColumn_nb(String column_nb);

    /**
     * 获取 [栏位数]脏标记
     */
    boolean getColumn_nbDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 显示模式
     */
    String getDisplay_mode();

    void setDisplay_mode(String display_mode);

    /**
     * 获取 [显示模式]脏标记
     */
    boolean getDisplay_modeDirtyFlag();

    /**
     * 最小文本长度
     */
    Integer getValidation_length_min();

    void setValidation_length_min(Integer validation_length_min);

    /**
     * 获取 [最小文本长度]脏标记
     */
    boolean getValidation_length_minDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 问卷
     */
    Integer getSurvey_id();

    void setSurvey_id(Integer survey_id);

    /**
     * 获取 [问卷]脏标记
     */
    boolean getSurvey_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 调查页面
     */
    Integer getPage_id();

    void setPage_id(Integer page_id);

    /**
     * 获取 [调查页面]脏标记
     */
    boolean getPage_idDirtyFlag();

}
