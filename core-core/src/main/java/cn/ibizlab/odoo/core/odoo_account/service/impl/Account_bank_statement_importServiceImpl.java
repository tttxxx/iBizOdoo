package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_importService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_importOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_bank_statement_importClientModel;

/**
 * 实体[导入银行对账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_importServiceImpl implements IAccount_bank_statement_importService {

    @Autowired
    account_bank_statement_importOdooClient account_bank_statement_importOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_bank_statement_importClientModel clientModel = new account_bank_statement_importClientModel();
        clientModel.setId(id);
		account_bank_statement_importOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_bank_statement_import get(Integer id) {
        account_bank_statement_importClientModel clientModel = new account_bank_statement_importClientModel();
        clientModel.setId(id);
		account_bank_statement_importOdooClient.get(clientModel);
        Account_bank_statement_import et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_bank_statement_import();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_bank_statement_import et) {
        account_bank_statement_importClientModel clientModel = convert2Model(et,null);
		account_bank_statement_importOdooClient.create(clientModel);
        Account_bank_statement_import rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_import> list){
    }

    @Override
    public boolean update(Account_bank_statement_import et) {
        account_bank_statement_importClientModel clientModel = convert2Model(et,null);
		account_bank_statement_importOdooClient.update(clientModel);
        Account_bank_statement_import rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_bank_statement_import> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_import> searchDefault(Account_bank_statement_importSearchContext context) {
        List<Account_bank_statement_import> list = new ArrayList<Account_bank_statement_import>();
        Page<account_bank_statement_importClientModel> clientModelList = account_bank_statement_importOdooClient.search(context);
        for(account_bank_statement_importClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_bank_statement_import>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_bank_statement_importClientModel convert2Model(Account_bank_statement_import domain , account_bank_statement_importClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_bank_statement_importClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("filenamedirtyflag"))
                model.setFilename(domain.getFilename());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("data_filedirtyflag"))
                model.setData_file(domain.getDataFile());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_bank_statement_import convert2Domain( account_bank_statement_importClientModel model ,Account_bank_statement_import domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_bank_statement_import();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getFilenameDirtyFlag())
            domain.setFilename(model.getFilename());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getData_fileDirtyFlag())
            domain.setDataFile(model.getData_file());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



