package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_automation_line_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_automation_line_test] 服务对象接口
 */
public interface Ibase_automation_line_testClientService{

    public Ibase_automation_line_test createModel() ;

    public void removeBatch(List<Ibase_automation_line_test> base_automation_line_tests);

    public void updateBatch(List<Ibase_automation_line_test> base_automation_line_tests);

    public void remove(Ibase_automation_line_test base_automation_line_test);

    public void createBatch(List<Ibase_automation_line_test> base_automation_line_tests);

    public void get(Ibase_automation_line_test base_automation_line_test);

    public void create(Ibase_automation_line_test base_automation_line_test);

    public void update(Ibase_automation_line_test base_automation_line_test);

    public Page<Ibase_automation_line_test> search(SearchContext context);

    public Page<Ibase_automation_line_test> select(SearchContext context);

}
