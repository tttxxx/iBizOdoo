package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;

/**
 * 实体 [活动提醒] 存储对象
 */
public interface Calendar_alarmRepository extends Repository<Calendar_alarm> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context);

    Calendar_alarm convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm domain , Calendar_alarm po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm convert2Domain( Calendar_alarm po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm domain) ;

}
