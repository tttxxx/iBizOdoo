package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;

/**
 * 实体 [语言] 存储模型
 */
public interface Res_lang{

    /**
     * 可翻译
     */
    String getTranslatable();

    void setTranslatable(String translatable);

    /**
     * 获取 [可翻译]脏标记
     */
    boolean getTranslatableDirtyFlag();

    /**
     * 千位分隔符
     */
    String getThousands_sep();

    void setThousands_sep(String thousands_sep);

    /**
     * 获取 [千位分隔符]脏标记
     */
    boolean getThousands_sepDirtyFlag();

    /**
     * ISO代码
     */
    String getIso_code();

    void setIso_code(String iso_code);

    /**
     * 获取 [ISO代码]脏标记
     */
    boolean getIso_codeDirtyFlag();

    /**
     * 小数分割符
     */
    String getDecimal_point();

    void setDecimal_point(String decimal_point);

    /**
     * 获取 [小数分割符]脏标记
     */
    boolean getDecimal_pointDirtyFlag();

    /**
     * 分割符格式
     */
    String getGrouping();

    void setGrouping(String grouping);

    /**
     * 获取 [分割符格式]脏标记
     */
    boolean getGroupingDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 方向
     */
    String getDirection();

    void setDirection(String direction);

    /**
     * 获取 [方向]脏标记
     */
    boolean getDirectionDirtyFlag();

    /**
     * 日期格式
     */
    String getDate_format();

    void setDate_format(String date_format);

    /**
     * 获取 [日期格式]脏标记
     */
    boolean getDate_formatDirtyFlag();

    /**
     * 时间格式
     */
    String getTime_format();

    void setTime_format(String time_format);

    /**
     * 获取 [时间格式]脏标记
     */
    boolean getTime_formatDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 地区代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [地区代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 一个星期的第一天是
     */
    String getWeek_start();

    void setWeek_start(String week_start);

    /**
     * 获取 [一个星期的第一天是]脏标记
     */
    boolean getWeek_startDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
