package cn.ibizlab.odoo.core.odoo_survey.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[survey_user_input] 对象
 */
public class survey_user_inputClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_create;

    @JsonIgnore
    public boolean date_createDirtyFlag;
    
    /**
     * 截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp deadline;

    @JsonIgnore
    public boolean deadlineDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * EMail
     */
    public String email;

    @JsonIgnore
    public boolean emailDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最后显示页面
     */
    public Integer last_displayed_page_id;

    @JsonIgnore
    public boolean last_displayed_page_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 空白调查的公开链接
     */
    public String print_url;

    @JsonIgnore
    public boolean print_urlDirtyFlag;
    
    /**
     * 测试成绩
     */
    public Double quizz_score;

    @JsonIgnore
    public boolean quizz_scoreDirtyFlag;
    
    /**
     * 调查结果的公开链接
     */
    public String result_url;

    @JsonIgnore
    public boolean result_urlDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 问卷
     */
    public Integer survey_id;

    @JsonIgnore
    public boolean survey_idDirtyFlag;
    
    /**
     * 测试问卷
     */
    public String test_entry;

    @JsonIgnore
    public boolean test_entryDirtyFlag;
    
    /**
     * 标识令牌
     */
    public String token;

    @JsonIgnore
    public boolean tokenDirtyFlag;
    
    /**
     * 回复类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 答案
     */
    public String user_input_line_ids;

    @JsonIgnore
    public boolean user_input_line_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("date_create")
    public Timestamp getDate_create(){
        return this.date_create ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("date_create")
    public void setDate_create(Timestamp  date_create){
        this.date_create = date_create ;
        this.date_createDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_createDirtyFlag(){
        return this.date_createDirtyFlag ;
    }   

    /**
     * 获取 [截止日期]
     */
    @JsonProperty("deadline")
    public Timestamp getDeadline(){
        return this.deadline ;
    }

    /**
     * 设置 [截止日期]
     */
    @JsonProperty("deadline")
    public void setDeadline(Timestamp  deadline){
        this.deadline = deadline ;
        this.deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [截止日期]脏标记
     */
    @JsonIgnore
    public boolean getDeadlineDirtyFlag(){
        return this.deadlineDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最后显示页面]
     */
    @JsonProperty("last_displayed_page_id")
    public Integer getLast_displayed_page_id(){
        return this.last_displayed_page_id ;
    }

    /**
     * 设置 [最后显示页面]
     */
    @JsonProperty("last_displayed_page_id")
    public void setLast_displayed_page_id(Integer  last_displayed_page_id){
        this.last_displayed_page_id = last_displayed_page_id ;
        this.last_displayed_page_idDirtyFlag = true ;
    }

     /**
     * 获取 [最后显示页面]脏标记
     */
    @JsonIgnore
    public boolean getLast_displayed_page_idDirtyFlag(){
        return this.last_displayed_page_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [空白调查的公开链接]
     */
    @JsonProperty("print_url")
    public String getPrint_url(){
        return this.print_url ;
    }

    /**
     * 设置 [空白调查的公开链接]
     */
    @JsonProperty("print_url")
    public void setPrint_url(String  print_url){
        this.print_url = print_url ;
        this.print_urlDirtyFlag = true ;
    }

     /**
     * 获取 [空白调查的公开链接]脏标记
     */
    @JsonIgnore
    public boolean getPrint_urlDirtyFlag(){
        return this.print_urlDirtyFlag ;
    }   

    /**
     * 获取 [测试成绩]
     */
    @JsonProperty("quizz_score")
    public Double getQuizz_score(){
        return this.quizz_score ;
    }

    /**
     * 设置 [测试成绩]
     */
    @JsonProperty("quizz_score")
    public void setQuizz_score(Double  quizz_score){
        this.quizz_score = quizz_score ;
        this.quizz_scoreDirtyFlag = true ;
    }

     /**
     * 获取 [测试成绩]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_scoreDirtyFlag(){
        return this.quizz_scoreDirtyFlag ;
    }   

    /**
     * 获取 [调查结果的公开链接]
     */
    @JsonProperty("result_url")
    public String getResult_url(){
        return this.result_url ;
    }

    /**
     * 设置 [调查结果的公开链接]
     */
    @JsonProperty("result_url")
    public void setResult_url(String  result_url){
        this.result_url = result_url ;
        this.result_urlDirtyFlag = true ;
    }

     /**
     * 获取 [调查结果的公开链接]脏标记
     */
    @JsonIgnore
    public boolean getResult_urlDirtyFlag(){
        return this.result_urlDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [问卷]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return this.survey_id ;
    }

    /**
     * 设置 [问卷]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

     /**
     * 获取 [问卷]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return this.survey_idDirtyFlag ;
    }   

    /**
     * 获取 [测试问卷]
     */
    @JsonProperty("test_entry")
    public String getTest_entry(){
        return this.test_entry ;
    }

    /**
     * 设置 [测试问卷]
     */
    @JsonProperty("test_entry")
    public void setTest_entry(String  test_entry){
        this.test_entry = test_entry ;
        this.test_entryDirtyFlag = true ;
    }

     /**
     * 获取 [测试问卷]脏标记
     */
    @JsonIgnore
    public boolean getTest_entryDirtyFlag(){
        return this.test_entryDirtyFlag ;
    }   

    /**
     * 获取 [标识令牌]
     */
    @JsonProperty("token")
    public String getToken(){
        return this.token ;
    }

    /**
     * 设置 [标识令牌]
     */
    @JsonProperty("token")
    public void setToken(String  token){
        this.token = token ;
        this.tokenDirtyFlag = true ;
    }

     /**
     * 获取 [标识令牌]脏标记
     */
    @JsonIgnore
    public boolean getTokenDirtyFlag(){
        return this.tokenDirtyFlag ;
    }   

    /**
     * 获取 [回复类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [回复类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [回复类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [答案]
     */
    @JsonProperty("user_input_line_ids")
    public String getUser_input_line_ids(){
        return this.user_input_line_ids ;
    }

    /**
     * 设置 [答案]
     */
    @JsonProperty("user_input_line_ids")
    public void setUser_input_line_ids(String  user_input_line_ids){
        this.user_input_line_ids = user_input_line_ids ;
        this.user_input_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [答案]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_line_idsDirtyFlag(){
        return this.user_input_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date_create") instanceof Boolean)&& map.get("date_create")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_create"));
   			this.setDate_create(new Timestamp(parse.getTime()));
		}
		if(!(map.get("deadline") instanceof Boolean)&& map.get("deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("deadline"));
   			this.setDeadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email") instanceof Boolean)&& map.get("email")!=null){
			this.setEmail((String)map.get("email"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("last_displayed_page_id") instanceof Boolean)&& map.get("last_displayed_page_id")!=null){
			Object[] objs = (Object[])map.get("last_displayed_page_id");
			if(objs.length > 0){
				this.setLast_displayed_page_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("print_url") instanceof Boolean)&& map.get("print_url")!=null){
			this.setPrint_url((String)map.get("print_url"));
		}
		if(!(map.get("quizz_score") instanceof Boolean)&& map.get("quizz_score")!=null){
			this.setQuizz_score((Double)map.get("quizz_score"));
		}
		if(!(map.get("result_url") instanceof Boolean)&& map.get("result_url")!=null){
			this.setResult_url((String)map.get("result_url"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("survey_id") instanceof Boolean)&& map.get("survey_id")!=null){
			Object[] objs = (Object[])map.get("survey_id");
			if(objs.length > 0){
				this.setSurvey_id((Integer)objs[0]);
			}
		}
		if(map.get("test_entry") instanceof Boolean){
			this.setTest_entry(((Boolean)map.get("test_entry"))? "true" : "false");
		}
		if(!(map.get("token") instanceof Boolean)&& map.get("token")!=null){
			this.setToken((String)map.get("token"));
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("user_input_line_ids") instanceof Boolean)&& map.get("user_input_line_ids")!=null){
			Object[] objs = (Object[])map.get("user_input_line_ids");
			if(objs.length > 0){
				Integer[] user_input_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setUser_input_line_ids(Arrays.toString(user_input_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate_create()!=null&&this.getDate_createDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_create());
			map.put("date_create",datetimeStr);
		}else if(this.getDate_createDirtyFlag()){
			map.put("date_create",false);
		}
		if(this.getDeadline()!=null&&this.getDeadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDeadline());
			map.put("deadline",datetimeStr);
		}else if(this.getDeadlineDirtyFlag()){
			map.put("deadline",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail()!=null&&this.getEmailDirtyFlag()){
			map.put("email",this.getEmail());
		}else if(this.getEmailDirtyFlag()){
			map.put("email",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLast_displayed_page_id()!=null&&this.getLast_displayed_page_idDirtyFlag()){
			map.put("last_displayed_page_id",this.getLast_displayed_page_id());
		}else if(this.getLast_displayed_page_idDirtyFlag()){
			map.put("last_displayed_page_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPrint_url()!=null&&this.getPrint_urlDirtyFlag()){
			map.put("print_url",this.getPrint_url());
		}else if(this.getPrint_urlDirtyFlag()){
			map.put("print_url",false);
		}
		if(this.getQuizz_score()!=null&&this.getQuizz_scoreDirtyFlag()){
			map.put("quizz_score",this.getQuizz_score());
		}else if(this.getQuizz_scoreDirtyFlag()){
			map.put("quizz_score",false);
		}
		if(this.getResult_url()!=null&&this.getResult_urlDirtyFlag()){
			map.put("result_url",this.getResult_url());
		}else if(this.getResult_urlDirtyFlag()){
			map.put("result_url",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getSurvey_id()!=null&&this.getSurvey_idDirtyFlag()){
			map.put("survey_id",this.getSurvey_id());
		}else if(this.getSurvey_idDirtyFlag()){
			map.put("survey_id",false);
		}
		if(this.getTest_entry()!=null&&this.getTest_entryDirtyFlag()){
			map.put("test_entry",Boolean.parseBoolean(this.getTest_entry()));		
		}		if(this.getToken()!=null&&this.getTokenDirtyFlag()){
			map.put("token",this.getToken());
		}else if(this.getTokenDirtyFlag()){
			map.put("token",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUser_input_line_ids()!=null&&this.getUser_input_line_idsDirtyFlag()){
			map.put("user_input_line_ids",this.getUser_input_line_ids());
		}else if(this.getUser_input_line_idsDirtyFlag()){
			map.put("user_input_line_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
