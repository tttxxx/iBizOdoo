package cn.ibizlab.odoo.core.odoo_sms.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_send_smsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sms.client.sms_send_smsOdooClient;
import cn.ibizlab.odoo.core.odoo_sms.clientmodel.sms_send_smsClientModel;

/**
 * 实体[发送短信] 服务对象接口实现
 */
@Slf4j
@Service
public class Sms_send_smsServiceImpl implements ISms_send_smsService {

    @Autowired
    sms_send_smsOdooClient sms_send_smsOdooClient;


    @Override
    public boolean update(Sms_send_sms et) {
        sms_send_smsClientModel clientModel = convert2Model(et,null);
		sms_send_smsOdooClient.update(clientModel);
        Sms_send_sms rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sms_send_sms> list){
    }

    @Override
    public boolean create(Sms_send_sms et) {
        sms_send_smsClientModel clientModel = convert2Model(et,null);
		sms_send_smsOdooClient.create(clientModel);
        Sms_send_sms rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sms_send_sms> list){
    }

    @Override
    public boolean remove(Integer id) {
        sms_send_smsClientModel clientModel = new sms_send_smsClientModel();
        clientModel.setId(id);
		sms_send_smsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Sms_send_sms get(Integer id) {
        sms_send_smsClientModel clientModel = new sms_send_smsClientModel();
        clientModel.setId(id);
		sms_send_smsOdooClient.get(clientModel);
        Sms_send_sms et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sms_send_sms();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context) {
        List<Sms_send_sms> list = new ArrayList<Sms_send_sms>();
        Page<sms_send_smsClientModel> clientModelList = sms_send_smsOdooClient.search(context);
        for(sms_send_smsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sms_send_sms>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sms_send_smsClientModel convert2Model(Sms_send_sms domain , sms_send_smsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sms_send_smsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("recipientsdirtyflag"))
                model.setRecipients(domain.getRecipients());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("messagedirtyflag"))
                model.setMessage(domain.getMessage());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sms_send_sms convert2Domain( sms_send_smsClientModel model ,Sms_send_sms domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sms_send_sms();
        }

        if(model.getRecipientsDirtyFlag())
            domain.setRecipients(model.getRecipients());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessageDirtyFlag())
            domain.setMessage(model.getMessage());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



