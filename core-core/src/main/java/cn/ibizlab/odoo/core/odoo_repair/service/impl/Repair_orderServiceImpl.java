package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_repair.client.repair_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_repair.clientmodel.repair_orderClientModel;

/**
 * 实体[维修单] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_orderServiceImpl implements IRepair_orderService {

    @Autowired
    repair_orderOdooClient repair_orderOdooClient;


    @Override
    public Repair_order get(Integer id) {
        repair_orderClientModel clientModel = new repair_orderClientModel();
        clientModel.setId(id);
		repair_orderOdooClient.get(clientModel);
        Repair_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Repair_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Repair_order et) {
        repair_orderClientModel clientModel = convert2Model(et,null);
		repair_orderOdooClient.update(clientModel);
        Repair_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Repair_order> list){
    }

    @Override
    public boolean remove(Integer id) {
        repair_orderClientModel clientModel = new repair_orderClientModel();
        clientModel.setId(id);
		repair_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Repair_order et) {
        repair_orderClientModel clientModel = convert2Model(et,null);
		repair_orderOdooClient.create(clientModel);
        Repair_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_order> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_order> searchDefault(Repair_orderSearchContext context) {
        List<Repair_order> list = new ArrayList<Repair_order>();
        Page<repair_orderClientModel> clientModelList = repair_orderOdooClient.search(context);
        for(repair_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Repair_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public repair_orderClientModel convert2Model(Repair_order domain , repair_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new repair_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("internal_notesdirtyflag"))
                model.setInternal_notes(domain.getInternalNotes());
            if((Boolean) domain.getExtensionparams().get("default_address_iddirtyflag"))
                model.setDefault_address_id(domain.getDefaultAddressId());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("invoice_methoddirtyflag"))
                model.setInvoice_method(domain.getInvoiceMethod());
            if((Boolean) domain.getExtensionparams().get("invoiceddirtyflag"))
                model.setInvoiced(domain.getInvoiced());
            if((Boolean) domain.getExtensionparams().get("quotation_notesdirtyflag"))
                model.setQuotation_notes(domain.getQuotationNotes());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("operationsdirtyflag"))
                model.setOperations(domain.getOperations());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("fees_linesdirtyflag"))
                model.setFees_lines(domain.getFeesLines());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("repaireddirtyflag"))
                model.setRepaired(domain.getRepaired());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("amount_untaxeddirtyflag"))
                model.setAmount_untaxed(domain.getAmountUntaxed());
            if((Boolean) domain.getExtensionparams().get("guarantee_limitdirtyflag"))
                model.setGuarantee_limit(domain.getGuaranteeLimit());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("amount_taxdirtyflag"))
                model.setAmount_tax(domain.getAmountTax());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("pricelist_id_textdirtyflag"))
                model.setPricelist_id_text(domain.getPricelistIdText());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_id_textdirtyflag"))
                model.setInvoice_id_text(domain.getInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_invoice_id_textdirtyflag"))
                model.setPartner_invoice_id_text(domain.getPartnerInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("address_id_textdirtyflag"))
                model.setAddress_id_text(domain.getAddressIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("trackingdirtyflag"))
                model.setTracking(domain.getTracking());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("partner_invoice_iddirtyflag"))
                model.setPartner_invoice_id(domain.getPartnerInvoiceId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("address_iddirtyflag"))
                model.setAddress_id(domain.getAddressId());
            if((Boolean) domain.getExtensionparams().get("invoice_iddirtyflag"))
                model.setInvoice_id(domain.getInvoiceId());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Repair_order convert2Domain( repair_orderClientModel model ,Repair_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Repair_order();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getInternal_notesDirtyFlag())
            domain.setInternalNotes(model.getInternal_notes());
        if(model.getDefault_address_idDirtyFlag())
            domain.setDefaultAddressId(model.getDefault_address_id());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getInvoice_methodDirtyFlag())
            domain.setInvoiceMethod(model.getInvoice_method());
        if(model.getInvoicedDirtyFlag())
            domain.setInvoiced(model.getInvoiced());
        if(model.getQuotation_notesDirtyFlag())
            domain.setQuotationNotes(model.getQuotation_notes());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getOperationsDirtyFlag())
            domain.setOperations(model.getOperations());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getFees_linesDirtyFlag())
            domain.setFeesLines(model.getFees_lines());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getRepairedDirtyFlag())
            domain.setRepaired(model.getRepaired());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getAmount_untaxedDirtyFlag())
            domain.setAmountUntaxed(model.getAmount_untaxed());
        if(model.getGuarantee_limitDirtyFlag())
            domain.setGuaranteeLimit(model.getGuarantee_limit());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAmount_taxDirtyFlag())
            domain.setAmountTax(model.getAmount_tax());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getPricelist_id_textDirtyFlag())
            domain.setPricelistIdText(model.getPricelist_id_text());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getInvoice_id_textDirtyFlag())
            domain.setInvoiceIdText(model.getInvoice_id_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_invoice_id_textDirtyFlag())
            domain.setPartnerInvoiceIdText(model.getPartner_invoice_id_text());
        if(model.getAddress_id_textDirtyFlag())
            domain.setAddressIdText(model.getAddress_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getTrackingDirtyFlag())
            domain.setTracking(model.getTracking());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getPartner_invoice_idDirtyFlag())
            domain.setPartnerInvoiceId(model.getPartner_invoice_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAddress_idDirtyFlag())
            domain.setAddressId(model.getAddress_id());
        if(model.getInvoice_idDirtyFlag())
            domain.setInvoiceId(model.getInvoice_id());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        return domain ;
    }

}

    



