package cn.ibizlab.odoo.core.odoo_project.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [任务阶段] 对象
 */
@Data
public class Project_task_type extends EntityClient implements Serializable {

    /**
     * 阶段名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 自动看板状态
     */
    @DEField(name = "auto_validation_kanban_state")
    @JSONField(name = "auto_validation_kanban_state")
    @JsonProperty("auto_validation_kanban_state")
    private String autoValidationKanbanState;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 项目
     */
    @JSONField(name = "project_ids")
    @JsonProperty("project_ids")
    private String projectIds;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 灰色看板标签
     */
    @DEField(name = "legend_normal")
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    private String legendNormal;

    /**
     * 集中到看板中
     */
    @JSONField(name = "fold")
    @JsonProperty("fold")
    private String fold;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 红色的看板标签
     */
    @DEField(name = "legend_blocked")
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    private String legendBlocked;

    /**
     * 星标解释
     */
    @DEField(name = "legend_priority")
    @JSONField(name = "legend_priority")
    @JsonProperty("legend_priority")
    private String legendPriority;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 绿色看板标签
     */
    @DEField(name = "legend_done")
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    private String legendDone;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 点评邮件模板
     */
    @JSONField(name = "rating_template_id_text")
    @JsonProperty("rating_template_id_text")
    private String ratingTemplateIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * EMail模板
     */
    @JSONField(name = "mail_template_id_text")
    @JsonProperty("mail_template_id_text")
    private String mailTemplateIdText;

    /**
     * EMail模板
     */
    @DEField(name = "mail_template_id")
    @JSONField(name = "mail_template_id")
    @JsonProperty("mail_template_id")
    private Integer mailTemplateId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 点评邮件模板
     */
    @DEField(name = "rating_template_id")
    @JSONField(name = "rating_template_id")
    @JsonProperty("rating_template_id")
    private Integer ratingTemplateId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoomailtemplate")
    @JsonProperty("odoomailtemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooMailTemplate;

    /**
     * 
     */
    @JSONField(name = "odooratingtemplate")
    @JsonProperty("odooratingtemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooRatingTemplate;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [阶段名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [自动看板状态]
     */
    public void setAutoValidationKanbanState(String autoValidationKanbanState){
        this.autoValidationKanbanState = autoValidationKanbanState ;
        this.modify("auto_validation_kanban_state",autoValidationKanbanState);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [灰色看板标签]
     */
    public void setLegendNormal(String legendNormal){
        this.legendNormal = legendNormal ;
        this.modify("legend_normal",legendNormal);
    }
    /**
     * 设置 [集中到看板中]
     */
    public void setFold(String fold){
        this.fold = fold ;
        this.modify("fold",fold);
    }
    /**
     * 设置 [红色的看板标签]
     */
    public void setLegendBlocked(String legendBlocked){
        this.legendBlocked = legendBlocked ;
        this.modify("legend_blocked",legendBlocked);
    }
    /**
     * 设置 [星标解释]
     */
    public void setLegendPriority(String legendPriority){
        this.legendPriority = legendPriority ;
        this.modify("legend_priority",legendPriority);
    }
    /**
     * 设置 [绿色看板标签]
     */
    public void setLegendDone(String legendDone){
        this.legendDone = legendDone ;
        this.modify("legend_done",legendDone);
    }
    /**
     * 设置 [EMail模板]
     */
    public void setMailTemplateId(Integer mailTemplateId){
        this.mailTemplateId = mailTemplateId ;
        this.modify("mail_template_id",mailTemplateId);
    }
    /**
     * 设置 [点评邮件模板]
     */
    public void setRatingTemplateId(Integer ratingTemplateId){
        this.ratingTemplateId = ratingTemplateId ;
        this.modify("rating_template_id",ratingTemplateId);
    }

}


