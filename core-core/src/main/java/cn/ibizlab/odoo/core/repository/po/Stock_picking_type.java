package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;

/**
 * 实体 [拣货类型] 存储模型
 */
public interface Stock_picking_type{

    /**
     * 显示详细作业
     */
    String getShow_operations();

    void setShow_operations(String show_operations);

    /**
     * 获取 [显示详细作业]脏标记
     */
    boolean getShow_operationsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 参考序列
     */
    Integer getSequence_id();

    void setSequence_id(Integer sequence_id);

    /**
     * 获取 [参考序列]脏标记
     */
    boolean getSequence_idDirtyFlag();

    /**
     * 创建新批次/序列号码
     */
    String getUse_create_lots();

    void setUse_create_lots(String use_create_lots);

    /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    boolean getUse_create_lotsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 条码
     */
    String getBarcode();

    void setBarcode(String barcode);

    /**
     * 获取 [条码]脏标记
     */
    boolean getBarcodeDirtyFlag();

    /**
     * 显示预留
     */
    String getShow_reserved();

    void setShow_reserved(String show_reserved);

    /**
     * 获取 [显示预留]脏标记
     */
    boolean getShow_reservedDirtyFlag();

    /**
     * 欠单比率
     */
    Integer getRate_picking_backorders();

    void setRate_picking_backorders(Integer rate_picking_backorders);

    /**
     * 获取 [欠单比率]脏标记
     */
    boolean getRate_picking_backordersDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 拣货欠单个数
     */
    Integer getCount_picking_backorders();

    void setCount_picking_backorders(Integer count_picking_backorders);

    /**
     * 获取 [拣货欠单个数]脏标记
     */
    boolean getCount_picking_backordersDirtyFlag();

    /**
     * 生产单的数量
     */
    Integer getCount_mo_todo();

    void setCount_mo_todo(Integer count_mo_todo);

    /**
     * 获取 [生产单的数量]脏标记
     */
    boolean getCount_mo_todoDirtyFlag();

    /**
     * 颜色
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 移动整个包裹
     */
    String getShow_entire_packs();

    void setShow_entire_packs(String show_entire_packs);

    /**
     * 获取 [移动整个包裹]脏标记
     */
    boolean getShow_entire_packsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 生产单等待的数量
     */
    Integer getCount_mo_waiting();

    void setCount_mo_waiting(Integer count_mo_waiting);

    /**
     * 获取 [生产单等待的数量]脏标记
     */
    boolean getCount_mo_waitingDirtyFlag();

    /**
     * 草稿拣货个数
     */
    Integer getCount_picking_draft();

    void setCount_picking_draft(Integer count_picking_draft);

    /**
     * 获取 [草稿拣货个数]脏标记
     */
    boolean getCount_picking_draftDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 迟到拣货个数
     */
    Integer getCount_picking_late();

    void setCount_picking_late(Integer count_picking_late);

    /**
     * 获取 [迟到拣货个数]脏标记
     */
    boolean getCount_picking_lateDirtyFlag();

    /**
     * 拣货个数
     */
    Integer getCount_picking();

    void setCount_picking(Integer count_picking);

    /**
     * 获取 [拣货个数]脏标记
     */
    boolean getCount_pickingDirtyFlag();

    /**
     * 拣货个数准备好
     */
    Integer getCount_picking_ready();

    void setCount_picking_ready(Integer count_picking_ready);

    /**
     * 获取 [拣货个数准备好]脏标记
     */
    boolean getCount_picking_readyDirtyFlag();

    /**
     * 最近10笔完成的拣货
     */
    String getLast_done_picking();

    void setLast_done_picking(String last_done_picking);

    /**
     * 获取 [最近10笔完成的拣货]脏标记
     */
    boolean getLast_done_pickingDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 等待拣货个数
     */
    Integer getCount_picking_waiting();

    void setCount_picking_waiting(Integer count_picking_waiting);

    /**
     * 获取 [等待拣货个数]脏标记
     */
    boolean getCount_picking_waitingDirtyFlag();

    /**
     * 作业的类型
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [作业的类型]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 延迟比率
     */
    Integer getRate_picking_late();

    void setRate_picking_late(Integer rate_picking_late);

    /**
     * 获取 [延迟比率]脏标记
     */
    boolean getRate_picking_lateDirtyFlag();

    /**
     * 作业类型
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 生产单数量延迟
     */
    Integer getCount_mo_late();

    void setCount_mo_late(Integer count_mo_late);

    /**
     * 获取 [生产单数量延迟]脏标记
     */
    boolean getCount_mo_lateDirtyFlag();

    /**
     * 使用已有批次/序列号码
     */
    String getUse_existing_lots();

    void setUse_existing_lots(String use_existing_lots);

    /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    boolean getUse_existing_lotsDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 默认源位置
     */
    String getDefault_location_src_id_text();

    void setDefault_location_src_id_text(String default_location_src_id_text);

    /**
     * 获取 [默认源位置]脏标记
     */
    boolean getDefault_location_src_id_textDirtyFlag();

    /**
     * 退回的作业类型
     */
    String getReturn_picking_type_id_text();

    void setReturn_picking_type_id_text(String return_picking_type_id_text);

    /**
     * 获取 [退回的作业类型]脏标记
     */
    boolean getReturn_picking_type_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 默认目的位置
     */
    String getDefault_location_dest_id_text();

    void setDefault_location_dest_id_text(String default_location_dest_id_text);

    /**
     * 获取 [默认目的位置]脏标记
     */
    boolean getDefault_location_dest_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 默认目的位置
     */
    Integer getDefault_location_dest_id();

    void setDefault_location_dest_id(Integer default_location_dest_id);

    /**
     * 获取 [默认目的位置]脏标记
     */
    boolean getDefault_location_dest_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

    /**
     * 默认源位置
     */
    Integer getDefault_location_src_id();

    void setDefault_location_src_id(Integer default_location_src_id);

    /**
     * 获取 [默认源位置]脏标记
     */
    boolean getDefault_location_src_idDirtyFlag();

    /**
     * 退回的作业类型
     */
    Integer getReturn_picking_type_id();

    void setReturn_picking_type_id(Integer return_picking_type_id);

    /**
     * 获取 [退回的作业类型]脏标记
     */
    boolean getReturn_picking_type_idDirtyFlag();

}
