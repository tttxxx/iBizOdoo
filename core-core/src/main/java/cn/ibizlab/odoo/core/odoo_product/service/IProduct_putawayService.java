package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;


/**
 * 实体[Product_putaway] 服务对象接口
 */
public interface IProduct_putawayService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Product_putaway et) ;
    void createBatch(List<Product_putaway> list) ;
    Product_putaway get(Integer key) ;
    boolean update(Product_putaway et) ;
    void updateBatch(List<Product_putaway> list) ;
    Page<Product_putaway> searchDefault(Product_putawaySearchContext context) ;

}



