package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [fleet_vehicle_log_contract] 对象
 */
public interface fleet_vehicle_log_contract {

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public Double getAmount();

    public void setAmount(Double amount);

    public String getAuto_generated();

    public void setAuto_generated(String auto_generated);

    public Integer getContract_id();

    public void setContract_id(Integer contract_id);

    public String getContract_id_text();

    public void setContract_id_text(String contract_id_text);

    public Double getCost_amount();

    public void setCost_amount(Double cost_amount);

    public String getCost_frequency();

    public void setCost_frequency(String cost_frequency);

    public Double getCost_generated();

    public void setCost_generated(Double cost_generated);

    public Integer getCost_id();

    public void setCost_id(Integer cost_id);

    public String getCost_ids();

    public void setCost_ids(String cost_ids);

    public String getCost_id_text();

    public void setCost_id_text(String cost_id_text);

    public Integer getCost_subtype_id();

    public void setCost_subtype_id(Integer cost_subtype_id);

    public String getCost_subtype_id_text();

    public void setCost_subtype_id_text(String cost_subtype_id_text);

    public String getCost_type();

    public void setCost_type(String cost_type);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public Integer getDays_left();

    public void setDays_left(Integer days_left);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Timestamp getExpiration_date();

    public void setExpiration_date(Timestamp expiration_date);

    public String getGenerated_cost_ids();

    public void setGenerated_cost_ids(String generated_cost_ids);

    public Integer getId();

    public void setId(Integer id);

    public Integer getInsurer_id();

    public void setInsurer_id(Integer insurer_id);

    public String getInsurer_id_text();

    public void setInsurer_id_text(String insurer_id_text);

    public String getIns_ref();

    public void setIns_ref(String ins_ref);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getNotes();

    public void setNotes(String notes);

    public Double getOdometer();

    public void setOdometer(Double odometer);

    public Integer getOdometer_id();

    public void setOdometer_id(Integer odometer_id);

    public String getOdometer_id_text();

    public void setOdometer_id_text(String odometer_id_text);

    public String getOdometer_unit();

    public void setOdometer_unit(String odometer_unit);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getPurchaser_id();

    public void setPurchaser_id(Integer purchaser_id);

    public String getPurchaser_id_text();

    public void setPurchaser_id_text(String purchaser_id_text);

    public Timestamp getStart_date();

    public void setStart_date(Timestamp start_date);

    public String getState();

    public void setState(String state);

    public Double getSum_cost();

    public void setSum_cost(Double sum_cost);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Integer getVehicle_id();

    public void setVehicle_id(Integer vehicle_id);

    public String getVehicle_id_text();

    public void setVehicle_id_text(String vehicle_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
