package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;

/**
 * 实体 [保养请求] 存储对象
 */
public interface Maintenance_requestRepository extends Repository<Maintenance_request> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context);

    Maintenance_request convert2PO(cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request domain , Maintenance_request po) ;

    cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request convert2Domain( Maintenance_request po ,cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request domain) ;

}
