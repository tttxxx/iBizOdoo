package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;

/**
 * 实体 [Preventive Maintenance Rule] 存储对象
 */
public interface Mro_pm_ruleRepository extends Repository<Mro_pm_rule> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_rule> searchDefault(Mro_pm_ruleSearchContext context);

    Mro_pm_rule convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule domain , Mro_pm_rule po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule convert2Domain( Mro_pm_rule po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule domain) ;

}
