package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;

/**
 * 实体 [休假] 存储对象
 */
public interface Hr_leaveRepository extends Repository<Hr_leave> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_leave> searchDefault(Hr_leaveSearchContext context);

    Hr_leave convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave domain , Hr_leave po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave convert2Domain( Hr_leave po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave domain) ;

}
