package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;

/**
 * 实体 [邮件列表] 存储对象
 */
public interface Mail_mass_mailing_listRepository extends Repository<Mail_mass_mailing_list> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context);

    Mail_mass_mailing_list convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list domain , Mail_mass_mailing_list po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list convert2Domain( Mail_mass_mailing_list po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list domain) ;

}
