package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;


/**
 * 实体[Website_redirect] 服务对象接口
 */
public interface IWebsite_redirectService{

    boolean update(Website_redirect et) ;
    void updateBatch(List<Website_redirect> list) ;
    Website_redirect get(Integer key) ;
    boolean create(Website_redirect et) ;
    void createBatch(List<Website_redirect> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Website_redirect> searchDefault(Website_redirectSearchContext context) ;

}



