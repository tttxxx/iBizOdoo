package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;

/**
 * 实体 [Web编辑器转换器测试] 存储模型
 */
public interface Web_editor_converter_test{

    /**
     * Text
     */
    String getText();

    void setText(String text);

    /**
     * 获取 [Text]脏标记
     */
    boolean getTextDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * Lorsqu'un pancake prend l'avion à destination de Toronto et
     */
    String getSelection_str();

    void setSelection_str(String selection_str);

    /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et]脏标记
     */
    boolean getSelection_strDirtyFlag();

    /**
     * Numeric
     */
    Double getNumeric();

    void setNumeric(Double numeric);

    /**
     * 获取 [Numeric]脏标记
     */
    boolean getNumericDirtyFlag();

    /**
     * Integer
     */
    Integer getInteger();

    void setInteger(Integer integer);

    /**
     * 获取 [Integer]脏标记
     */
    boolean getIntegerDirtyFlag();

    /**
     * Binary
     */
    byte[] getBinary();

    void setBinary(byte[] binary);

    /**
     * 获取 [Binary]脏标记
     */
    boolean getBinaryDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 浮点数
     */
    Double getIbizfloat();

    void setIbizfloat(Double ibizfloat);

    /**
     * 获取 [浮点数]脏标记
     */
    boolean getIbizfloatDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * Datetime
     */
    Timestamp getDatetime();

    void setDatetime(Timestamp datetime);

    /**
     * 获取 [Datetime]脏标记
     */
    boolean getDatetimeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * Html
     */
    String getHtml();

    void setHtml(String html);

    /**
     * 获取 [Html]脏标记
     */
    boolean getHtmlDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * Char
     */
    String getIbizchar();

    void setIbizchar(String ibizchar);

    /**
     * 获取 [Char]脏标记
     */
    boolean getIbizcharDirtyFlag();

    /**
     * Selection
     */
    String getSelection();

    void setSelection(String selection);

    /**
     * 获取 [Selection]脏标记
     */
    boolean getSelectionDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * Many2One
     */
    String getMany2one_text();

    void setMany2one_text(String many2one_text);

    /**
     * 获取 [Many2One]脏标记
     */
    boolean getMany2one_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * Many2One
     */
    Integer getMany2one();

    void setMany2one(Integer many2one);

    /**
     * 获取 [Many2One]脏标记
     */
    boolean getMany2oneDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
