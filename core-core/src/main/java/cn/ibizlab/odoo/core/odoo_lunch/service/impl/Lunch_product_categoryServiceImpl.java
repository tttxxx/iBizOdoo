package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_product_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_product_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_product_categoryClientModel;

/**
 * 实体[工作餐产品类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_product_categoryServiceImpl implements ILunch_product_categoryService {

    @Autowired
    lunch_product_categoryOdooClient lunch_product_categoryOdooClient;


    @Override
    public Lunch_product_category get(Integer id) {
        lunch_product_categoryClientModel clientModel = new lunch_product_categoryClientModel();
        clientModel.setId(id);
		lunch_product_categoryOdooClient.get(clientModel);
        Lunch_product_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_product_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Lunch_product_category et) {
        lunch_product_categoryClientModel clientModel = convert2Model(et,null);
		lunch_product_categoryOdooClient.update(clientModel);
        Lunch_product_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_product_category> list){
    }

    @Override
    public boolean remove(Integer id) {
        lunch_product_categoryClientModel clientModel = new lunch_product_categoryClientModel();
        clientModel.setId(id);
		lunch_product_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Lunch_product_category et) {
        lunch_product_categoryClientModel clientModel = convert2Model(et,null);
		lunch_product_categoryOdooClient.create(clientModel);
        Lunch_product_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_product_category> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context) {
        List<Lunch_product_category> list = new ArrayList<Lunch_product_category>();
        Page<lunch_product_categoryClientModel> clientModelList = lunch_product_categoryOdooClient.search(context);
        for(lunch_product_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_product_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_product_categoryClientModel convert2Model(Lunch_product_category domain , lunch_product_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_product_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_product_category convert2Domain( lunch_product_categoryClientModel model ,Lunch_product_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_product_category();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



