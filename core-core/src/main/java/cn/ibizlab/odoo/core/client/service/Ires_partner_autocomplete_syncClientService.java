package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner_autocomplete_sync;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
public interface Ires_partner_autocomplete_syncClientService{

    public Ires_partner_autocomplete_sync createModel() ;

    public void update(Ires_partner_autocomplete_sync res_partner_autocomplete_sync);

    public void updateBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs);

    public void get(Ires_partner_autocomplete_sync res_partner_autocomplete_sync);

    public Page<Ires_partner_autocomplete_sync> search(SearchContext context);

    public void create(Ires_partner_autocomplete_sync res_partner_autocomplete_sync);

    public void createBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs);

    public void remove(Ires_partner_autocomplete_sync res_partner_autocomplete_sync);

    public void removeBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs);

    public Page<Ires_partner_autocomplete_sync> select(SearchContext context);

}
