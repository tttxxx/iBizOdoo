package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailingOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailingClientModel;

/**
 * 实体[群发邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailingServiceImpl implements IMail_mass_mailingService {

    @Autowired
    mail_mass_mailingOdooClient mail_mass_mailingOdooClient;


    @Override
    public boolean update(Mail_mass_mailing et) {
        mail_mass_mailingClientModel clientModel = convert2Model(et,null);
		mail_mass_mailingOdooClient.update(clientModel);
        Mail_mass_mailing rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing> list){
    }

    @Override
    public boolean create(Mail_mass_mailing et) {
        mail_mass_mailingClientModel clientModel = convert2Model(et,null);
		mail_mass_mailingOdooClient.create(clientModel);
        Mail_mass_mailing rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_mass_mailingClientModel clientModel = new mail_mass_mailingClientModel();
        clientModel.setId(id);
		mail_mass_mailingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mail_mass_mailing get(Integer id) {
        mail_mass_mailingClientModel clientModel = new mail_mass_mailingClientModel();
        clientModel.setId(id);
		mail_mass_mailingOdooClient.get(clientModel);
        Mail_mass_mailing et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context) {
        List<Mail_mass_mailing> list = new ArrayList<Mail_mass_mailing>();
        Page<mail_mass_mailingClientModel> clientModelList = mail_mass_mailingOdooClient.search(context);
        for(mail_mass_mailingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailingClientModel convert2Model(Mail_mass_mailing domain , mail_mass_mailingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sent_datedirtyflag"))
                model.setSent_date(domain.getSentDate());
            if((Boolean) domain.getExtensionparams().get("sale_quotation_countdirtyflag"))
                model.setSale_quotation_count(domain.getSaleQuotationCount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("mailing_domaindirtyflag"))
                model.setMailing_domain(domain.getMailingDomain());
            if((Boolean) domain.getExtensionparams().get("openeddirtyflag"))
                model.setOpened(domain.getOpened());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("replieddirtyflag"))
                model.setReplied(domain.getReplied());
            if((Boolean) domain.getExtensionparams().get("crm_lead_countdirtyflag"))
                model.setCrm_lead_count(domain.getCrmLeadCount());
            if((Boolean) domain.getExtensionparams().get("mailing_model_realdirtyflag"))
                model.setMailing_model_real(domain.getMailingModelReal());
            if((Boolean) domain.getExtensionparams().get("clicks_ratiodirtyflag"))
                model.setClicks_ratio(domain.getClicksRatio());
            if((Boolean) domain.getExtensionparams().get("delivereddirtyflag"))
                model.setDelivered(domain.getDelivered());
            if((Boolean) domain.getExtensionparams().get("mailing_model_iddirtyflag"))
                model.setMailing_model_id(domain.getMailingModelId());
            if((Boolean) domain.getExtensionparams().get("reply_to_modedirtyflag"))
                model.setReply_to_mode(domain.getReplyToMode());
            if((Boolean) domain.getExtensionparams().get("schedule_datedirtyflag"))
                model.setSchedule_date(domain.getScheduleDate());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sale_invoiced_amountdirtyflag"))
                model.setSale_invoiced_amount(domain.getSaleInvoicedAmount());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("totaldirtyflag"))
                model.setTotal(domain.getTotal());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("bounced_ratiodirtyflag"))
                model.setBounced_ratio(domain.getBouncedRatio());
            if((Boolean) domain.getExtensionparams().get("next_departuredirtyflag"))
                model.setNext_departure(domain.getNextDeparture());
            if((Boolean) domain.getExtensionparams().get("mail_server_iddirtyflag"))
                model.setMail_server_id(domain.getMailServerId());
            if((Boolean) domain.getExtensionparams().get("ignoreddirtyflag"))
                model.setIgnored(domain.getIgnored());
            if((Boolean) domain.getExtensionparams().get("scheduleddirtyflag"))
                model.setScheduled(domain.getScheduled());
            if((Boolean) domain.getExtensionparams().get("crm_lead_activateddirtyflag"))
                model.setCrm_lead_activated(domain.getCrmLeadActivated());
            if((Boolean) domain.getExtensionparams().get("clickeddirtyflag"))
                model.setClicked(domain.getClicked());
            if((Boolean) domain.getExtensionparams().get("replied_ratiodirtyflag"))
                model.setReplied_ratio(domain.getRepliedRatio());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("statistics_idsdirtyflag"))
                model.setStatistics_ids(domain.getStatisticsIds());
            if((Boolean) domain.getExtensionparams().get("opened_ratiodirtyflag"))
                model.setOpened_ratio(domain.getOpenedRatio());
            if((Boolean) domain.getExtensionparams().get("body_htmldirtyflag"))
                model.setBody_html(domain.getBodyHtml());
            if((Boolean) domain.getExtensionparams().get("bounceddirtyflag"))
                model.setBounced(domain.getBounced());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("mailing_model_namedirtyflag"))
                model.setMailing_model_name(domain.getMailingModelName());
            if((Boolean) domain.getExtensionparams().get("contact_list_idsdirtyflag"))
                model.setContact_list_ids(domain.getContactListIds());
            if((Boolean) domain.getExtensionparams().get("expecteddirtyflag"))
                model.setExpected(domain.getExpected());
            if((Boolean) domain.getExtensionparams().get("faileddirtyflag"))
                model.setFailed(domain.getFailed());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("received_ratiodirtyflag"))
                model.setReceived_ratio(domain.getReceivedRatio());
            if((Boolean) domain.getExtensionparams().get("crm_opportunities_countdirtyflag"))
                model.setCrm_opportunities_count(domain.getCrmOpportunitiesCount());
            if((Boolean) domain.getExtensionparams().get("keep_archivesdirtyflag"))
                model.setKeep_archives(domain.getKeepArchives());
            if((Boolean) domain.getExtensionparams().get("contact_ab_pcdirtyflag"))
                model.setContact_ab_pc(domain.getContactAbPc());
            if((Boolean) domain.getExtensionparams().get("sentdirtyflag"))
                model.setSent(domain.getSent());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_campaign_id_textdirtyflag"))
                model.setMass_mailing_campaign_id_text(domain.getMassMailingCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_campaign_iddirtyflag"))
                model.setMass_mailing_campaign_id(domain.getMassMailingCampaignId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing convert2Domain( mail_mass_mailingClientModel model ,Mail_mass_mailing domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing();
        }

        if(model.getSent_dateDirtyFlag())
            domain.setSentDate(model.getSent_date());
        if(model.getSale_quotation_countDirtyFlag())
            domain.setSaleQuotationCount(model.getSale_quotation_count());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMailing_domainDirtyFlag())
            domain.setMailingDomain(model.getMailing_domain());
        if(model.getOpenedDirtyFlag())
            domain.setOpened(model.getOpened());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getRepliedDirtyFlag())
            domain.setReplied(model.getReplied());
        if(model.getCrm_lead_countDirtyFlag())
            domain.setCrmLeadCount(model.getCrm_lead_count());
        if(model.getMailing_model_realDirtyFlag())
            domain.setMailingModelReal(model.getMailing_model_real());
        if(model.getClicks_ratioDirtyFlag())
            domain.setClicksRatio(model.getClicks_ratio());
        if(model.getDeliveredDirtyFlag())
            domain.setDelivered(model.getDelivered());
        if(model.getMailing_model_idDirtyFlag())
            domain.setMailingModelId(model.getMailing_model_id());
        if(model.getReply_to_modeDirtyFlag())
            domain.setReplyToMode(model.getReply_to_mode());
        if(model.getSchedule_dateDirtyFlag())
            domain.setScheduleDate(model.getSchedule_date());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSale_invoiced_amountDirtyFlag())
            domain.setSaleInvoicedAmount(model.getSale_invoiced_amount());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getTotalDirtyFlag())
            domain.setTotal(model.getTotal());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getBounced_ratioDirtyFlag())
            domain.setBouncedRatio(model.getBounced_ratio());
        if(model.getNext_departureDirtyFlag())
            domain.setNextDeparture(model.getNext_departure());
        if(model.getMail_server_idDirtyFlag())
            domain.setMailServerId(model.getMail_server_id());
        if(model.getIgnoredDirtyFlag())
            domain.setIgnored(model.getIgnored());
        if(model.getScheduledDirtyFlag())
            domain.setScheduled(model.getScheduled());
        if(model.getCrm_lead_activatedDirtyFlag())
            domain.setCrmLeadActivated(model.getCrm_lead_activated());
        if(model.getClickedDirtyFlag())
            domain.setClicked(model.getClicked());
        if(model.getReplied_ratioDirtyFlag())
            domain.setRepliedRatio(model.getReplied_ratio());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getStatistics_idsDirtyFlag())
            domain.setStatisticsIds(model.getStatistics_ids());
        if(model.getOpened_ratioDirtyFlag())
            domain.setOpenedRatio(model.getOpened_ratio());
        if(model.getBody_htmlDirtyFlag())
            domain.setBodyHtml(model.getBody_html());
        if(model.getBouncedDirtyFlag())
            domain.setBounced(model.getBounced());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMailing_model_nameDirtyFlag())
            domain.setMailingModelName(model.getMailing_model_name());
        if(model.getContact_list_idsDirtyFlag())
            domain.setContactListIds(model.getContact_list_ids());
        if(model.getExpectedDirtyFlag())
            domain.setExpected(model.getExpected());
        if(model.getFailedDirtyFlag())
            domain.setFailed(model.getFailed());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getReceived_ratioDirtyFlag())
            domain.setReceivedRatio(model.getReceived_ratio());
        if(model.getCrm_opportunities_countDirtyFlag())
            domain.setCrmOpportunitiesCount(model.getCrm_opportunities_count());
        if(model.getKeep_archivesDirtyFlag())
            domain.setKeepArchives(model.getKeep_archives());
        if(model.getContact_ab_pcDirtyFlag())
            domain.setContactAbPc(model.getContact_ab_pc());
        if(model.getSentDirtyFlag())
            domain.setSent(model.getSent());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMass_mailing_campaign_id_textDirtyFlag())
            domain.setMassMailingCampaignIdText(model.getMass_mailing_campaign_id_text());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getMass_mailing_campaign_idDirtyFlag())
            domain.setMassMailingCampaignId(model.getMass_mailing_campaign_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        return domain ;
    }

}

    



