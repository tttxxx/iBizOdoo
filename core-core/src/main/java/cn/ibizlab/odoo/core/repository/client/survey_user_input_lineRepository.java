package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_user_input_line;

/**
 * 实体[survey_user_input_line] 服务对象接口
 */
public interface survey_user_input_lineRepository{


    public survey_user_input_line createPO() ;
        public void remove(String id);

        public void updateBatch(survey_user_input_line survey_user_input_line);

        public void get(String id);

        public void update(survey_user_input_line survey_user_input_line);

        public void createBatch(survey_user_input_line survey_user_input_line);

        public void create(survey_user_input_line survey_user_input_line);

        public List<survey_user_input_line> search();

        public void removeBatch(String id);


}
