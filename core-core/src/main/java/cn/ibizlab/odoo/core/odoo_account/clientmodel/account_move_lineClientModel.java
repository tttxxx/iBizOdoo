package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_move_line] 对象
 */
public class account_move_lineClientModel implements Serializable{

    /**
     * 科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 货币金额
     */
    public Double amount_currency;

    @JsonIgnore
    public boolean amount_currencyDirtyFlag;
    
    /**
     * 残值金额
     */
    public Double amount_residual;

    @JsonIgnore
    public boolean amount_residualDirtyFlag;
    
    /**
     * 外币残余金额
     */
    public Double amount_residual_currency;

    @JsonIgnore
    public boolean amount_residual_currencyDirtyFlag;
    
    /**
     * 分析账户
     */
    public Integer analytic_account_id;

    @JsonIgnore
    public boolean analytic_account_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String analytic_account_id_text;

    @JsonIgnore
    public boolean analytic_account_id_textDirtyFlag;
    
    /**
     * 分析明细行
     */
    public String analytic_line_ids;

    @JsonIgnore
    public boolean analytic_line_idsDirtyFlag;
    
    /**
     * 分析标签
     */
    public String analytic_tag_ids;

    @JsonIgnore
    public boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 余额
     */
    public Double balance;

    @JsonIgnore
    public boolean balanceDirtyFlag;
    
    /**
     * 现金余额
     */
    public Double balance_cash_basis;

    @JsonIgnore
    public boolean balance_cash_basisDirtyFlag;
    
    /**
     * 无催款
     */
    public String blocked;

    @JsonIgnore
    public boolean blockedDirtyFlag;
    
    /**
     * 公司货币
     */
    public Integer company_currency_id;

    @JsonIgnore
    public boolean company_currency_idDirtyFlag;
    
    /**
     * 公司货币
     */
    public String company_currency_id_text;

    @JsonIgnore
    public boolean company_currency_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 对方
     */
    public String counterpart;

    @JsonIgnore
    public boolean counterpartDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 贷方
     */
    public Double credit;

    @JsonIgnore
    public boolean creditDirtyFlag;
    
    /**
     * 贷方现金基础
     */
    public Double credit_cash_basis;

    @JsonIgnore
    public boolean credit_cash_basisDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 到期日期
     */
    public Timestamp date_maturity;

    @JsonIgnore
    public boolean date_maturityDirtyFlag;
    
    /**
     * 借方
     */
    public Double debit;

    @JsonIgnore
    public boolean debitDirtyFlag;
    
    /**
     * 借记现金基础
     */
    public Double debit_cash_basis;

    @JsonIgnore
    public boolean debit_cash_basisDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 费用
     */
    public Integer expense_id;

    @JsonIgnore
    public boolean expense_idDirtyFlag;
    
    /**
     * 费用
     */
    public String expense_id_text;

    @JsonIgnore
    public boolean expense_id_textDirtyFlag;
    
    /**
     * 匹配号码
     */
    public Integer full_reconcile_id;

    @JsonIgnore
    public boolean full_reconcile_idDirtyFlag;
    
    /**
     * 匹配号码
     */
    public String full_reconcile_id_text;

    @JsonIgnore
    public boolean full_reconcile_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public Integer invoice_id;

    @JsonIgnore
    public boolean invoice_idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_id_text;

    @JsonIgnore
    public boolean invoice_id_textDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 匹配的贷方
     */
    public String matched_credit_ids;

    @JsonIgnore
    public boolean matched_credit_idsDirtyFlag;
    
    /**
     * 匹配的借记卡
     */
    public String matched_debit_ids;

    @JsonIgnore
    public boolean matched_debit_idsDirtyFlag;
    
    /**
     * 日记账分录
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 日记账分录
     */
    public String move_id_text;

    @JsonIgnore
    public boolean move_id_textDirtyFlag;
    
    /**
     * 标签
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 记叙
     */
    public String narration;

    @JsonIgnore
    public boolean narrationDirtyFlag;
    
    /**
     * 上级状态
     */
    public String parent_state;

    @JsonIgnore
    public boolean parent_stateDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 发起人付款
     */
    public Integer payment_id;

    @JsonIgnore
    public boolean payment_idDirtyFlag;
    
    /**
     * 发起人付款
     */
    public String payment_id_text;

    @JsonIgnore
    public boolean payment_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double quantity;

    @JsonIgnore
    public boolean quantityDirtyFlag;
    
    /**
     * 重新计算税额
     */
    public String recompute_tax_line;

    @JsonIgnore
    public boolean recompute_tax_lineDirtyFlag;
    
    /**
     * 已核销
     */
    public String reconciled;

    @JsonIgnore
    public boolean reconciledDirtyFlag;
    
    /**
     * 参考
     */
    public String ref;

    @JsonIgnore
    public boolean refDirtyFlag;
    
    /**
     * 报告
     */
    public Integer statement_id;

    @JsonIgnore
    public boolean statement_idDirtyFlag;
    
    /**
     * 报告
     */
    public String statement_id_text;

    @JsonIgnore
    public boolean statement_id_textDirtyFlag;
    
    /**
     * 用该分录核销的银行核销单明细
     */
    public Integer statement_line_id;

    @JsonIgnore
    public boolean statement_line_idDirtyFlag;
    
    /**
     * 用该分录核销的银行核销单明细
     */
    public String statement_line_id_text;

    @JsonIgnore
    public boolean statement_line_id_textDirtyFlag;
    
    /**
     * 基本金额
     */
    public Double tax_base_amount;

    @JsonIgnore
    public boolean tax_base_amountDirtyFlag;
    
    /**
     * 出现在VAT报告
     */
    public String tax_exigible;

    @JsonIgnore
    public boolean tax_exigibleDirtyFlag;
    
    /**
     * 采用的税
     */
    public String tax_ids;

    @JsonIgnore
    public boolean tax_idsDirtyFlag;
    
    /**
     * 旧税额
     */
    public String tax_line_grouping_key;

    @JsonIgnore
    public boolean tax_line_grouping_keyDirtyFlag;
    
    /**
     * 发起人税
     */
    public Integer tax_line_id;

    @JsonIgnore
    public boolean tax_line_idDirtyFlag;
    
    /**
     * 发起人税
     */
    public String tax_line_id_text;

    @JsonIgnore
    public boolean tax_line_id_textDirtyFlag;
    
    /**
     * 类型
     */
    public Integer user_type_id;

    @JsonIgnore
    public boolean user_type_idDirtyFlag;
    
    /**
     * 类型
     */
    public String user_type_id_text;

    @JsonIgnore
    public boolean user_type_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [货币金额]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return this.amount_currency ;
    }

    /**
     * 设置 [货币金额]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [货币金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return this.amount_currencyDirtyFlag ;
    }   

    /**
     * 获取 [残值金额]
     */
    @JsonProperty("amount_residual")
    public Double getAmount_residual(){
        return this.amount_residual ;
    }

    /**
     * 设置 [残值金额]
     */
    @JsonProperty("amount_residual")
    public void setAmount_residual(Double  amount_residual){
        this.amount_residual = amount_residual ;
        this.amount_residualDirtyFlag = true ;
    }

     /**
     * 获取 [残值金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_residualDirtyFlag(){
        return this.amount_residualDirtyFlag ;
    }   

    /**
     * 获取 [外币残余金额]
     */
    @JsonProperty("amount_residual_currency")
    public Double getAmount_residual_currency(){
        return this.amount_residual_currency ;
    }

    /**
     * 设置 [外币残余金额]
     */
    @JsonProperty("amount_residual_currency")
    public void setAmount_residual_currency(Double  amount_residual_currency){
        this.amount_residual_currency = amount_residual_currency ;
        this.amount_residual_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [外币残余金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_residual_currencyDirtyFlag(){
        return this.amount_residual_currencyDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return this.analytic_account_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return this.analytic_account_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return this.analytic_account_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return this.analytic_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分析明细行]
     */
    @JsonProperty("analytic_line_ids")
    public String getAnalytic_line_ids(){
        return this.analytic_line_ids ;
    }

    /**
     * 设置 [分析明细行]
     */
    @JsonProperty("analytic_line_ids")
    public void setAnalytic_line_ids(String  analytic_line_ids){
        this.analytic_line_ids = analytic_line_ids ;
        this.analytic_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析明细行]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_line_idsDirtyFlag(){
        return this.analytic_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [余额]
     */
    @JsonProperty("balance")
    public Double getBalance(){
        return this.balance ;
    }

    /**
     * 设置 [余额]
     */
    @JsonProperty("balance")
    public void setBalance(Double  balance){
        this.balance = balance ;
        this.balanceDirtyFlag = true ;
    }

     /**
     * 获取 [余额]脏标记
     */
    @JsonIgnore
    public boolean getBalanceDirtyFlag(){
        return this.balanceDirtyFlag ;
    }   

    /**
     * 获取 [现金余额]
     */
    @JsonProperty("balance_cash_basis")
    public Double getBalance_cash_basis(){
        return this.balance_cash_basis ;
    }

    /**
     * 设置 [现金余额]
     */
    @JsonProperty("balance_cash_basis")
    public void setBalance_cash_basis(Double  balance_cash_basis){
        this.balance_cash_basis = balance_cash_basis ;
        this.balance_cash_basisDirtyFlag = true ;
    }

     /**
     * 获取 [现金余额]脏标记
     */
    @JsonIgnore
    public boolean getBalance_cash_basisDirtyFlag(){
        return this.balance_cash_basisDirtyFlag ;
    }   

    /**
     * 获取 [无催款]
     */
    @JsonProperty("blocked")
    public String getBlocked(){
        return this.blocked ;
    }

    /**
     * 设置 [无催款]
     */
    @JsonProperty("blocked")
    public void setBlocked(String  blocked){
        this.blocked = blocked ;
        this.blockedDirtyFlag = true ;
    }

     /**
     * 获取 [无催款]脏标记
     */
    @JsonIgnore
    public boolean getBlockedDirtyFlag(){
        return this.blockedDirtyFlag ;
    }   

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id_text")
    public String getCompany_currency_id_text(){
        return this.company_currency_id_text ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id_text")
    public void setCompany_currency_id_text(String  company_currency_id_text){
        this.company_currency_id_text = company_currency_id_text ;
        this.company_currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_id_textDirtyFlag(){
        return this.company_currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [对方]
     */
    @JsonProperty("counterpart")
    public String getCounterpart(){
        return this.counterpart ;
    }

    /**
     * 设置 [对方]
     */
    @JsonProperty("counterpart")
    public void setCounterpart(String  counterpart){
        this.counterpart = counterpart ;
        this.counterpartDirtyFlag = true ;
    }

     /**
     * 获取 [对方]脏标记
     */
    @JsonIgnore
    public boolean getCounterpartDirtyFlag(){
        return this.counterpartDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [贷方]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return this.credit ;
    }

    /**
     * 设置 [贷方]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

     /**
     * 获取 [贷方]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return this.creditDirtyFlag ;
    }   

    /**
     * 获取 [贷方现金基础]
     */
    @JsonProperty("credit_cash_basis")
    public Double getCredit_cash_basis(){
        return this.credit_cash_basis ;
    }

    /**
     * 设置 [贷方现金基础]
     */
    @JsonProperty("credit_cash_basis")
    public void setCredit_cash_basis(Double  credit_cash_basis){
        this.credit_cash_basis = credit_cash_basis ;
        this.credit_cash_basisDirtyFlag = true ;
    }

     /**
     * 获取 [贷方现金基础]脏标记
     */
    @JsonIgnore
    public boolean getCredit_cash_basisDirtyFlag(){
        return this.credit_cash_basisDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [到期日期]
     */
    @JsonProperty("date_maturity")
    public Timestamp getDate_maturity(){
        return this.date_maturity ;
    }

    /**
     * 设置 [到期日期]
     */
    @JsonProperty("date_maturity")
    public void setDate_maturity(Timestamp  date_maturity){
        this.date_maturity = date_maturity ;
        this.date_maturityDirtyFlag = true ;
    }

     /**
     * 获取 [到期日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_maturityDirtyFlag(){
        return this.date_maturityDirtyFlag ;
    }   

    /**
     * 获取 [借方]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return this.debit ;
    }

    /**
     * 设置 [借方]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

     /**
     * 获取 [借方]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return this.debitDirtyFlag ;
    }   

    /**
     * 获取 [借记现金基础]
     */
    @JsonProperty("debit_cash_basis")
    public Double getDebit_cash_basis(){
        return this.debit_cash_basis ;
    }

    /**
     * 设置 [借记现金基础]
     */
    @JsonProperty("debit_cash_basis")
    public void setDebit_cash_basis(Double  debit_cash_basis){
        this.debit_cash_basis = debit_cash_basis ;
        this.debit_cash_basisDirtyFlag = true ;
    }

     /**
     * 获取 [借记现金基础]脏标记
     */
    @JsonIgnore
    public boolean getDebit_cash_basisDirtyFlag(){
        return this.debit_cash_basisDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [费用]
     */
    @JsonProperty("expense_id")
    public Integer getExpense_id(){
        return this.expense_id ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("expense_id")
    public void setExpense_id(Integer  expense_id){
        this.expense_id = expense_id ;
        this.expense_idDirtyFlag = true ;
    }

     /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_idDirtyFlag(){
        return this.expense_idDirtyFlag ;
    }   

    /**
     * 获取 [费用]
     */
    @JsonProperty("expense_id_text")
    public String getExpense_id_text(){
        return this.expense_id_text ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("expense_id_text")
    public void setExpense_id_text(String  expense_id_text){
        this.expense_id_text = expense_id_text ;
        this.expense_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_id_textDirtyFlag(){
        return this.expense_id_textDirtyFlag ;
    }   

    /**
     * 获取 [匹配号码]
     */
    @JsonProperty("full_reconcile_id")
    public Integer getFull_reconcile_id(){
        return this.full_reconcile_id ;
    }

    /**
     * 设置 [匹配号码]
     */
    @JsonProperty("full_reconcile_id")
    public void setFull_reconcile_id(Integer  full_reconcile_id){
        this.full_reconcile_id = full_reconcile_id ;
        this.full_reconcile_idDirtyFlag = true ;
    }

     /**
     * 获取 [匹配号码]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_idDirtyFlag(){
        return this.full_reconcile_idDirtyFlag ;
    }   

    /**
     * 获取 [匹配号码]
     */
    @JsonProperty("full_reconcile_id_text")
    public String getFull_reconcile_id_text(){
        return this.full_reconcile_id_text ;
    }

    /**
     * 设置 [匹配号码]
     */
    @JsonProperty("full_reconcile_id_text")
    public void setFull_reconcile_id_text(String  full_reconcile_id_text){
        this.full_reconcile_id_text = full_reconcile_id_text ;
        this.full_reconcile_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [匹配号码]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_id_textDirtyFlag(){
        return this.full_reconcile_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return this.invoice_id ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return this.invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return this.invoice_id_text ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return this.invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [匹配的贷方]
     */
    @JsonProperty("matched_credit_ids")
    public String getMatched_credit_ids(){
        return this.matched_credit_ids ;
    }

    /**
     * 设置 [匹配的贷方]
     */
    @JsonProperty("matched_credit_ids")
    public void setMatched_credit_ids(String  matched_credit_ids){
        this.matched_credit_ids = matched_credit_ids ;
        this.matched_credit_idsDirtyFlag = true ;
    }

     /**
     * 获取 [匹配的贷方]脏标记
     */
    @JsonIgnore
    public boolean getMatched_credit_idsDirtyFlag(){
        return this.matched_credit_idsDirtyFlag ;
    }   

    /**
     * 获取 [匹配的借记卡]
     */
    @JsonProperty("matched_debit_ids")
    public String getMatched_debit_ids(){
        return this.matched_debit_ids ;
    }

    /**
     * 设置 [匹配的借记卡]
     */
    @JsonProperty("matched_debit_ids")
    public void setMatched_debit_ids(String  matched_debit_ids){
        this.matched_debit_ids = matched_debit_ids ;
        this.matched_debit_idsDirtyFlag = true ;
    }

     /**
     * 获取 [匹配的借记卡]脏标记
     */
    @JsonIgnore
    public boolean getMatched_debit_idsDirtyFlag(){
        return this.matched_debit_idsDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [日记账分录]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return this.move_id_text ;
    }

    /**
     * 设置 [日记账分录]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return this.move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [记叙]
     */
    @JsonProperty("narration")
    public String getNarration(){
        return this.narration ;
    }

    /**
     * 设置 [记叙]
     */
    @JsonProperty("narration")
    public void setNarration(String  narration){
        this.narration = narration ;
        this.narrationDirtyFlag = true ;
    }

     /**
     * 获取 [记叙]脏标记
     */
    @JsonIgnore
    public boolean getNarrationDirtyFlag(){
        return this.narrationDirtyFlag ;
    }   

    /**
     * 获取 [上级状态]
     */
    @JsonProperty("parent_state")
    public String getParent_state(){
        return this.parent_state ;
    }

    /**
     * 设置 [上级状态]
     */
    @JsonProperty("parent_state")
    public void setParent_state(String  parent_state){
        this.parent_state = parent_state ;
        this.parent_stateDirtyFlag = true ;
    }

     /**
     * 获取 [上级状态]脏标记
     */
    @JsonIgnore
    public boolean getParent_stateDirtyFlag(){
        return this.parent_stateDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [发起人付款]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return this.payment_id ;
    }

    /**
     * 设置 [发起人付款]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

     /**
     * 获取 [发起人付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return this.payment_idDirtyFlag ;
    }   

    /**
     * 获取 [发起人付款]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return this.payment_id_text ;
    }

    /**
     * 设置 [发起人付款]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发起人付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return this.payment_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return this.quantity ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return this.quantityDirtyFlag ;
    }   

    /**
     * 获取 [重新计算税额]
     */
    @JsonProperty("recompute_tax_line")
    public String getRecompute_tax_line(){
        return this.recompute_tax_line ;
    }

    /**
     * 设置 [重新计算税额]
     */
    @JsonProperty("recompute_tax_line")
    public void setRecompute_tax_line(String  recompute_tax_line){
        this.recompute_tax_line = recompute_tax_line ;
        this.recompute_tax_lineDirtyFlag = true ;
    }

     /**
     * 获取 [重新计算税额]脏标记
     */
    @JsonIgnore
    public boolean getRecompute_tax_lineDirtyFlag(){
        return this.recompute_tax_lineDirtyFlag ;
    }   

    /**
     * 获取 [已核销]
     */
    @JsonProperty("reconciled")
    public String getReconciled(){
        return this.reconciled ;
    }

    /**
     * 设置 [已核销]
     */
    @JsonProperty("reconciled")
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.reconciledDirtyFlag = true ;
    }

     /**
     * 获取 [已核销]脏标记
     */
    @JsonIgnore
    public boolean getReconciledDirtyFlag(){
        return this.reconciledDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("ref")
    public String getRef(){
        return this.ref ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return this.refDirtyFlag ;
    }   

    /**
     * 获取 [报告]
     */
    @JsonProperty("statement_id")
    public Integer getStatement_id(){
        return this.statement_id ;
    }

    /**
     * 设置 [报告]
     */
    @JsonProperty("statement_id")
    public void setStatement_id(Integer  statement_id){
        this.statement_id = statement_id ;
        this.statement_idDirtyFlag = true ;
    }

     /**
     * 获取 [报告]脏标记
     */
    @JsonIgnore
    public boolean getStatement_idDirtyFlag(){
        return this.statement_idDirtyFlag ;
    }   

    /**
     * 获取 [报告]
     */
    @JsonProperty("statement_id_text")
    public String getStatement_id_text(){
        return this.statement_id_text ;
    }

    /**
     * 设置 [报告]
     */
    @JsonProperty("statement_id_text")
    public void setStatement_id_text(String  statement_id_text){
        this.statement_id_text = statement_id_text ;
        this.statement_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [报告]脏标记
     */
    @JsonIgnore
    public boolean getStatement_id_textDirtyFlag(){
        return this.statement_id_textDirtyFlag ;
    }   

    /**
     * 获取 [用该分录核销的银行核销单明细]
     */
    @JsonProperty("statement_line_id")
    public Integer getStatement_line_id(){
        return this.statement_line_id ;
    }

    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    @JsonProperty("statement_line_id")
    public void setStatement_line_id(Integer  statement_line_id){
        this.statement_line_id = statement_line_id ;
        this.statement_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    @JsonIgnore
    public boolean getStatement_line_idDirtyFlag(){
        return this.statement_line_idDirtyFlag ;
    }   

    /**
     * 获取 [用该分录核销的银行核销单明细]
     */
    @JsonProperty("statement_line_id_text")
    public String getStatement_line_id_text(){
        return this.statement_line_id_text ;
    }

    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    @JsonProperty("statement_line_id_text")
    public void setStatement_line_id_text(String  statement_line_id_text){
        this.statement_line_id_text = statement_line_id_text ;
        this.statement_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    @JsonIgnore
    public boolean getStatement_line_id_textDirtyFlag(){
        return this.statement_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [基本金额]
     */
    @JsonProperty("tax_base_amount")
    public Double getTax_base_amount(){
        return this.tax_base_amount ;
    }

    /**
     * 设置 [基本金额]
     */
    @JsonProperty("tax_base_amount")
    public void setTax_base_amount(Double  tax_base_amount){
        this.tax_base_amount = tax_base_amount ;
        this.tax_base_amountDirtyFlag = true ;
    }

     /**
     * 获取 [基本金额]脏标记
     */
    @JsonIgnore
    public boolean getTax_base_amountDirtyFlag(){
        return this.tax_base_amountDirtyFlag ;
    }   

    /**
     * 获取 [出现在VAT报告]
     */
    @JsonProperty("tax_exigible")
    public String getTax_exigible(){
        return this.tax_exigible ;
    }

    /**
     * 设置 [出现在VAT报告]
     */
    @JsonProperty("tax_exigible")
    public void setTax_exigible(String  tax_exigible){
        this.tax_exigible = tax_exigible ;
        this.tax_exigibleDirtyFlag = true ;
    }

     /**
     * 获取 [出现在VAT报告]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibleDirtyFlag(){
        return this.tax_exigibleDirtyFlag ;
    }   

    /**
     * 获取 [采用的税]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return this.tax_ids ;
    }

    /**
     * 设置 [采用的税]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [采用的税]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return this.tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [旧税额]
     */
    @JsonProperty("tax_line_grouping_key")
    public String getTax_line_grouping_key(){
        return this.tax_line_grouping_key ;
    }

    /**
     * 设置 [旧税额]
     */
    @JsonProperty("tax_line_grouping_key")
    public void setTax_line_grouping_key(String  tax_line_grouping_key){
        this.tax_line_grouping_key = tax_line_grouping_key ;
        this.tax_line_grouping_keyDirtyFlag = true ;
    }

     /**
     * 获取 [旧税额]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_grouping_keyDirtyFlag(){
        return this.tax_line_grouping_keyDirtyFlag ;
    }   

    /**
     * 获取 [发起人税]
     */
    @JsonProperty("tax_line_id")
    public Integer getTax_line_id(){
        return this.tax_line_id ;
    }

    /**
     * 设置 [发起人税]
     */
    @JsonProperty("tax_line_id")
    public void setTax_line_id(Integer  tax_line_id){
        this.tax_line_id = tax_line_id ;
        this.tax_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [发起人税]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_idDirtyFlag(){
        return this.tax_line_idDirtyFlag ;
    }   

    /**
     * 获取 [发起人税]
     */
    @JsonProperty("tax_line_id_text")
    public String getTax_line_id_text(){
        return this.tax_line_id_text ;
    }

    /**
     * 设置 [发起人税]
     */
    @JsonProperty("tax_line_id_text")
    public void setTax_line_id_text(String  tax_line_id_text){
        this.tax_line_id_text = tax_line_id_text ;
        this.tax_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发起人税]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_id_textDirtyFlag(){
        return this.tax_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("user_type_id")
    public Integer getUser_type_id(){
        return this.user_type_id ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("user_type_id")
    public void setUser_type_id(Integer  user_type_id){
        this.user_type_id = user_type_id ;
        this.user_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_idDirtyFlag(){
        return this.user_type_idDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("user_type_id_text")
    public String getUser_type_id_text(){
        return this.user_type_id_text ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("user_type_id_text")
    public void setUser_type_id_text(String  user_type_id_text){
        this.user_type_id_text = user_type_id_text ;
        this.user_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_id_textDirtyFlag(){
        return this.user_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 0){
				this.setAccount_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 1){
				this.setAccount_id_text((String)objs[1]);
			}
		}
		if(!(map.get("amount_currency") instanceof Boolean)&& map.get("amount_currency")!=null){
			this.setAmount_currency((Double)map.get("amount_currency"));
		}
		if(!(map.get("amount_residual") instanceof Boolean)&& map.get("amount_residual")!=null){
			this.setAmount_residual((Double)map.get("amount_residual"));
		}
		if(!(map.get("amount_residual_currency") instanceof Boolean)&& map.get("amount_residual_currency")!=null){
			this.setAmount_residual_currency((Double)map.get("amount_residual_currency"));
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 0){
				this.setAnalytic_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 1){
				this.setAnalytic_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("analytic_line_ids") instanceof Boolean)&& map.get("analytic_line_ids")!=null){
			Object[] objs = (Object[])map.get("analytic_line_ids");
			if(objs.length > 0){
				Integer[] analytic_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAnalytic_line_ids(Arrays.toString(analytic_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("analytic_tag_ids") instanceof Boolean)&& map.get("analytic_tag_ids")!=null){
			Object[] objs = (Object[])map.get("analytic_tag_ids");
			if(objs.length > 0){
				Integer[] analytic_tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAnalytic_tag_ids(Arrays.toString(analytic_tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("balance") instanceof Boolean)&& map.get("balance")!=null){
			this.setBalance((Double)map.get("balance"));
		}
		if(!(map.get("balance_cash_basis") instanceof Boolean)&& map.get("balance_cash_basis")!=null){
			this.setBalance_cash_basis((Double)map.get("balance_cash_basis"));
		}
		if(map.get("blocked") instanceof Boolean){
			this.setBlocked(((Boolean)map.get("blocked"))? "true" : "false");
		}
		if(!(map.get("company_currency_id") instanceof Boolean)&& map.get("company_currency_id")!=null){
			Object[] objs = (Object[])map.get("company_currency_id");
			if(objs.length > 0){
				this.setCompany_currency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_currency_id") instanceof Boolean)&& map.get("company_currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_currency_id");
			if(objs.length > 1){
				this.setCompany_currency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("counterpart") instanceof Boolean)&& map.get("counterpart")!=null){
			this.setCounterpart((String)map.get("counterpart"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("credit") instanceof Boolean)&& map.get("credit")!=null){
			this.setCredit((Double)map.get("credit"));
		}
		if(!(map.get("credit_cash_basis") instanceof Boolean)&& map.get("credit_cash_basis")!=null){
			this.setCredit_cash_basis((Double)map.get("credit_cash_basis"));
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_maturity") instanceof Boolean)&& map.get("date_maturity")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_maturity"));
   			this.setDate_maturity(new Timestamp(parse.getTime()));
		}
		if(!(map.get("debit") instanceof Boolean)&& map.get("debit")!=null){
			this.setDebit((Double)map.get("debit"));
		}
		if(!(map.get("debit_cash_basis") instanceof Boolean)&& map.get("debit_cash_basis")!=null){
			this.setDebit_cash_basis((Double)map.get("debit_cash_basis"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("expense_id") instanceof Boolean)&& map.get("expense_id")!=null){
			Object[] objs = (Object[])map.get("expense_id");
			if(objs.length > 0){
				this.setExpense_id((Integer)objs[0]);
			}
		}
		if(!(map.get("expense_id") instanceof Boolean)&& map.get("expense_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("expense_id");
			if(objs.length > 1){
				this.setExpense_id_text((String)objs[1]);
			}
		}
		if(!(map.get("full_reconcile_id") instanceof Boolean)&& map.get("full_reconcile_id")!=null){
			Object[] objs = (Object[])map.get("full_reconcile_id");
			if(objs.length > 0){
				this.setFull_reconcile_id((Integer)objs[0]);
			}
		}
		if(!(map.get("full_reconcile_id") instanceof Boolean)&& map.get("full_reconcile_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("full_reconcile_id");
			if(objs.length > 1){
				this.setFull_reconcile_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_id") instanceof Boolean)&& map.get("invoice_id")!=null){
			Object[] objs = (Object[])map.get("invoice_id");
			if(objs.length > 0){
				this.setInvoice_id((Integer)objs[0]);
			}
		}
		if(!(map.get("invoice_id") instanceof Boolean)&& map.get("invoice_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("invoice_id");
			if(objs.length > 1){
				this.setInvoice_id_text((String)objs[1]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 1){
				this.setJournal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("matched_credit_ids") instanceof Boolean)&& map.get("matched_credit_ids")!=null){
			Object[] objs = (Object[])map.get("matched_credit_ids");
			if(objs.length > 0){
				Integer[] matched_credit_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMatched_credit_ids(Arrays.toString(matched_credit_ids).replace(" ",""));
			}
		}
		if(!(map.get("matched_debit_ids") instanceof Boolean)&& map.get("matched_debit_ids")!=null){
			Object[] objs = (Object[])map.get("matched_debit_ids");
			if(objs.length > 0){
				Integer[] matched_debit_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMatched_debit_ids(Arrays.toString(matched_debit_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 0){
				this.setMove_id((Integer)objs[0]);
			}
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 1){
				this.setMove_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("narration") instanceof Boolean)&& map.get("narration")!=null){
			this.setNarration((String)map.get("narration"));
		}
		if(!(map.get("parent_state") instanceof Boolean)&& map.get("parent_state")!=null){
			this.setParent_state((String)map.get("parent_state"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payment_id") instanceof Boolean)&& map.get("payment_id")!=null){
			Object[] objs = (Object[])map.get("payment_id");
			if(objs.length > 0){
				this.setPayment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_id") instanceof Boolean)&& map.get("payment_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_id");
			if(objs.length > 1){
				this.setPayment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 1){
				this.setProduct_uom_id_text((String)objs[1]);
			}
		}
		if(!(map.get("quantity") instanceof Boolean)&& map.get("quantity")!=null){
			this.setQuantity((Double)map.get("quantity"));
		}
		if(map.get("recompute_tax_line") instanceof Boolean){
			this.setRecompute_tax_line(((Boolean)map.get("recompute_tax_line"))? "true" : "false");
		}
		if(map.get("reconciled") instanceof Boolean){
			this.setReconciled(((Boolean)map.get("reconciled"))? "true" : "false");
		}
		if(!(map.get("ref") instanceof Boolean)&& map.get("ref")!=null){
			this.setRef((String)map.get("ref"));
		}
		if(!(map.get("statement_id") instanceof Boolean)&& map.get("statement_id")!=null){
			Object[] objs = (Object[])map.get("statement_id");
			if(objs.length > 0){
				this.setStatement_id((Integer)objs[0]);
			}
		}
		if(!(map.get("statement_id") instanceof Boolean)&& map.get("statement_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("statement_id");
			if(objs.length > 1){
				this.setStatement_id_text((String)objs[1]);
			}
		}
		if(!(map.get("statement_line_id") instanceof Boolean)&& map.get("statement_line_id")!=null){
			Object[] objs = (Object[])map.get("statement_line_id");
			if(objs.length > 0){
				this.setStatement_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("statement_line_id") instanceof Boolean)&& map.get("statement_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("statement_line_id");
			if(objs.length > 1){
				this.setStatement_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("tax_base_amount") instanceof Boolean)&& map.get("tax_base_amount")!=null){
			this.setTax_base_amount((Double)map.get("tax_base_amount"));
		}
		if(map.get("tax_exigible") instanceof Boolean){
			this.setTax_exigible(((Boolean)map.get("tax_exigible"))? "true" : "false");
		}
		if(!(map.get("tax_ids") instanceof Boolean)&& map.get("tax_ids")!=null){
			Object[] objs = (Object[])map.get("tax_ids");
			if(objs.length > 0){
				Integer[] tax_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTax_ids(Arrays.toString(tax_ids).replace(" ",""));
			}
		}
		if(!(map.get("tax_line_grouping_key") instanceof Boolean)&& map.get("tax_line_grouping_key")!=null){
			this.setTax_line_grouping_key((String)map.get("tax_line_grouping_key"));
		}
		if(!(map.get("tax_line_id") instanceof Boolean)&& map.get("tax_line_id")!=null){
			Object[] objs = (Object[])map.get("tax_line_id");
			if(objs.length > 0){
				this.setTax_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("tax_line_id") instanceof Boolean)&& map.get("tax_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("tax_line_id");
			if(objs.length > 1){
				this.setTax_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("user_type_id") instanceof Boolean)&& map.get("user_type_id")!=null){
			Object[] objs = (Object[])map.get("user_type_id");
			if(objs.length > 0){
				this.setUser_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_type_id") instanceof Boolean)&& map.get("user_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_type_id");
			if(objs.length > 1){
				this.setUser_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_id()!=null&&this.getAccount_idDirtyFlag()){
			map.put("account_id",this.getAccount_id());
		}else if(this.getAccount_idDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAccount_id_text()!=null&&this.getAccount_id_textDirtyFlag()){
			//忽略文本外键account_id_text
		}else if(this.getAccount_id_textDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAmount_currency()!=null&&this.getAmount_currencyDirtyFlag()){
			map.put("amount_currency",this.getAmount_currency());
		}else if(this.getAmount_currencyDirtyFlag()){
			map.put("amount_currency",false);
		}
		if(this.getAmount_residual()!=null&&this.getAmount_residualDirtyFlag()){
			map.put("amount_residual",this.getAmount_residual());
		}else if(this.getAmount_residualDirtyFlag()){
			map.put("amount_residual",false);
		}
		if(this.getAmount_residual_currency()!=null&&this.getAmount_residual_currencyDirtyFlag()){
			map.put("amount_residual_currency",this.getAmount_residual_currency());
		}else if(this.getAmount_residual_currencyDirtyFlag()){
			map.put("amount_residual_currency",false);
		}
		if(this.getAnalytic_account_id()!=null&&this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",this.getAnalytic_account_id());
		}else if(this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_account_id_text()!=null&&this.getAnalytic_account_id_textDirtyFlag()){
			//忽略文本外键analytic_account_id_text
		}else if(this.getAnalytic_account_id_textDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_line_ids()!=null&&this.getAnalytic_line_idsDirtyFlag()){
			map.put("analytic_line_ids",this.getAnalytic_line_ids());
		}else if(this.getAnalytic_line_idsDirtyFlag()){
			map.put("analytic_line_ids",false);
		}
		if(this.getAnalytic_tag_ids()!=null&&this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",this.getAnalytic_tag_ids());
		}else if(this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",false);
		}
		if(this.getBalance()!=null&&this.getBalanceDirtyFlag()){
			map.put("balance",this.getBalance());
		}else if(this.getBalanceDirtyFlag()){
			map.put("balance",false);
		}
		if(this.getBalance_cash_basis()!=null&&this.getBalance_cash_basisDirtyFlag()){
			map.put("balance_cash_basis",this.getBalance_cash_basis());
		}else if(this.getBalance_cash_basisDirtyFlag()){
			map.put("balance_cash_basis",false);
		}
		if(this.getBlocked()!=null&&this.getBlockedDirtyFlag()){
			map.put("blocked",Boolean.parseBoolean(this.getBlocked()));		
		}		if(this.getCompany_currency_id()!=null&&this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",this.getCompany_currency_id());
		}else if(this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",false);
		}
		if(this.getCompany_currency_id_text()!=null&&this.getCompany_currency_id_textDirtyFlag()){
			//忽略文本外键company_currency_id_text
		}else if(this.getCompany_currency_id_textDirtyFlag()){
			map.put("company_currency_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCounterpart()!=null&&this.getCounterpartDirtyFlag()){
			map.put("counterpart",this.getCounterpart());
		}else if(this.getCounterpartDirtyFlag()){
			map.put("counterpart",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCredit()!=null&&this.getCreditDirtyFlag()){
			map.put("credit",this.getCredit());
		}else if(this.getCreditDirtyFlag()){
			map.put("credit",false);
		}
		if(this.getCredit_cash_basis()!=null&&this.getCredit_cash_basisDirtyFlag()){
			map.put("credit_cash_basis",this.getCredit_cash_basis());
		}else if(this.getCredit_cash_basisDirtyFlag()){
			map.put("credit_cash_basis",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDate_maturity()!=null&&this.getDate_maturityDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_maturity());
			map.put("date_maturity",datetimeStr);
		}else if(this.getDate_maturityDirtyFlag()){
			map.put("date_maturity",false);
		}
		if(this.getDebit()!=null&&this.getDebitDirtyFlag()){
			map.put("debit",this.getDebit());
		}else if(this.getDebitDirtyFlag()){
			map.put("debit",false);
		}
		if(this.getDebit_cash_basis()!=null&&this.getDebit_cash_basisDirtyFlag()){
			map.put("debit_cash_basis",this.getDebit_cash_basis());
		}else if(this.getDebit_cash_basisDirtyFlag()){
			map.put("debit_cash_basis",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getExpense_id()!=null&&this.getExpense_idDirtyFlag()){
			map.put("expense_id",this.getExpense_id());
		}else if(this.getExpense_idDirtyFlag()){
			map.put("expense_id",false);
		}
		if(this.getExpense_id_text()!=null&&this.getExpense_id_textDirtyFlag()){
			//忽略文本外键expense_id_text
		}else if(this.getExpense_id_textDirtyFlag()){
			map.put("expense_id",false);
		}
		if(this.getFull_reconcile_id()!=null&&this.getFull_reconcile_idDirtyFlag()){
			map.put("full_reconcile_id",this.getFull_reconcile_id());
		}else if(this.getFull_reconcile_idDirtyFlag()){
			map.put("full_reconcile_id",false);
		}
		if(this.getFull_reconcile_id_text()!=null&&this.getFull_reconcile_id_textDirtyFlag()){
			//忽略文本外键full_reconcile_id_text
		}else if(this.getFull_reconcile_id_textDirtyFlag()){
			map.put("full_reconcile_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_id()!=null&&this.getInvoice_idDirtyFlag()){
			map.put("invoice_id",this.getInvoice_id());
		}else if(this.getInvoice_idDirtyFlag()){
			map.put("invoice_id",false);
		}
		if(this.getInvoice_id_text()!=null&&this.getInvoice_id_textDirtyFlag()){
			//忽略文本外键invoice_id_text
		}else if(this.getInvoice_id_textDirtyFlag()){
			map.put("invoice_id",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getJournal_id_text()!=null&&this.getJournal_id_textDirtyFlag()){
			//忽略文本外键journal_id_text
		}else if(this.getJournal_id_textDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getMatched_credit_ids()!=null&&this.getMatched_credit_idsDirtyFlag()){
			map.put("matched_credit_ids",this.getMatched_credit_ids());
		}else if(this.getMatched_credit_idsDirtyFlag()){
			map.put("matched_credit_ids",false);
		}
		if(this.getMatched_debit_ids()!=null&&this.getMatched_debit_idsDirtyFlag()){
			map.put("matched_debit_ids",this.getMatched_debit_ids());
		}else if(this.getMatched_debit_idsDirtyFlag()){
			map.put("matched_debit_ids",false);
		}
		if(this.getMove_id()!=null&&this.getMove_idDirtyFlag()){
			map.put("move_id",this.getMove_id());
		}else if(this.getMove_idDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getMove_id_text()!=null&&this.getMove_id_textDirtyFlag()){
			//忽略文本外键move_id_text
		}else if(this.getMove_id_textDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNarration()!=null&&this.getNarrationDirtyFlag()){
			map.put("narration",this.getNarration());
		}else if(this.getNarrationDirtyFlag()){
			map.put("narration",false);
		}
		if(this.getParent_state()!=null&&this.getParent_stateDirtyFlag()){
			map.put("parent_state",this.getParent_state());
		}else if(this.getParent_stateDirtyFlag()){
			map.put("parent_state",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPayment_id()!=null&&this.getPayment_idDirtyFlag()){
			map.put("payment_id",this.getPayment_id());
		}else if(this.getPayment_idDirtyFlag()){
			map.put("payment_id",false);
		}
		if(this.getPayment_id_text()!=null&&this.getPayment_id_textDirtyFlag()){
			//忽略文本外键payment_id_text
		}else if(this.getPayment_id_textDirtyFlag()){
			map.put("payment_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_id_text()!=null&&this.getProduct_uom_id_textDirtyFlag()){
			//忽略文本外键product_uom_id_text
		}else if(this.getProduct_uom_id_textDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getQuantity()!=null&&this.getQuantityDirtyFlag()){
			map.put("quantity",this.getQuantity());
		}else if(this.getQuantityDirtyFlag()){
			map.put("quantity",false);
		}
		if(this.getRecompute_tax_line()!=null&&this.getRecompute_tax_lineDirtyFlag()){
			map.put("recompute_tax_line",Boolean.parseBoolean(this.getRecompute_tax_line()));		
		}		if(this.getReconciled()!=null&&this.getReconciledDirtyFlag()){
			map.put("reconciled",Boolean.parseBoolean(this.getReconciled()));		
		}		if(this.getRef()!=null&&this.getRefDirtyFlag()){
			map.put("ref",this.getRef());
		}else if(this.getRefDirtyFlag()){
			map.put("ref",false);
		}
		if(this.getStatement_id()!=null&&this.getStatement_idDirtyFlag()){
			map.put("statement_id",this.getStatement_id());
		}else if(this.getStatement_idDirtyFlag()){
			map.put("statement_id",false);
		}
		if(this.getStatement_id_text()!=null&&this.getStatement_id_textDirtyFlag()){
			//忽略文本外键statement_id_text
		}else if(this.getStatement_id_textDirtyFlag()){
			map.put("statement_id",false);
		}
		if(this.getStatement_line_id()!=null&&this.getStatement_line_idDirtyFlag()){
			map.put("statement_line_id",this.getStatement_line_id());
		}else if(this.getStatement_line_idDirtyFlag()){
			map.put("statement_line_id",false);
		}
		if(this.getStatement_line_id_text()!=null&&this.getStatement_line_id_textDirtyFlag()){
			//忽略文本外键statement_line_id_text
		}else if(this.getStatement_line_id_textDirtyFlag()){
			map.put("statement_line_id",false);
		}
		if(this.getTax_base_amount()!=null&&this.getTax_base_amountDirtyFlag()){
			map.put("tax_base_amount",this.getTax_base_amount());
		}else if(this.getTax_base_amountDirtyFlag()){
			map.put("tax_base_amount",false);
		}
		if(this.getTax_exigible()!=null&&this.getTax_exigibleDirtyFlag()){
			map.put("tax_exigible",Boolean.parseBoolean(this.getTax_exigible()));		
		}		if(this.getTax_ids()!=null&&this.getTax_idsDirtyFlag()){
			map.put("tax_ids",this.getTax_ids());
		}else if(this.getTax_idsDirtyFlag()){
			map.put("tax_ids",false);
		}
		if(this.getTax_line_grouping_key()!=null&&this.getTax_line_grouping_keyDirtyFlag()){
			map.put("tax_line_grouping_key",this.getTax_line_grouping_key());
		}else if(this.getTax_line_grouping_keyDirtyFlag()){
			map.put("tax_line_grouping_key",false);
		}
		if(this.getTax_line_id()!=null&&this.getTax_line_idDirtyFlag()){
			map.put("tax_line_id",this.getTax_line_id());
		}else if(this.getTax_line_idDirtyFlag()){
			map.put("tax_line_id",false);
		}
		if(this.getTax_line_id_text()!=null&&this.getTax_line_id_textDirtyFlag()){
			//忽略文本外键tax_line_id_text
		}else if(this.getTax_line_id_textDirtyFlag()){
			map.put("tax_line_id",false);
		}
		if(this.getUser_type_id()!=null&&this.getUser_type_idDirtyFlag()){
			map.put("user_type_id",this.getUser_type_id());
		}else if(this.getUser_type_idDirtyFlag()){
			map.put("user_type_id",false);
		}
		if(this.getUser_type_id_text()!=null&&this.getUser_type_id_textDirtyFlag()){
			//忽略文本外键user_type_id_text
		}else if(this.getUser_type_id_textDirtyFlag()){
			map.put("user_type_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
