package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;

/**
 * 实体 [调查标签] 存储模型
 */
public interface Survey_label{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 标签序列顺序
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [标签序列顺序]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 这个选项的分数
     */
    Double getQuizz_mark();

    void setQuizz_mark(Double quizz_mark);

    /**
     * 获取 [这个选项的分数]脏标记
     */
    boolean getQuizz_markDirtyFlag();

    /**
     * 建议值
     */
    String getValue();

    void setValue(String value);

    /**
     * 获取 [建议值]脏标记
     */
    boolean getValueDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 第二个问题
     */
    Integer getQuestion_id_2();

    void setQuestion_id_2(Integer question_id_2);

    /**
     * 获取 [第二个问题]脏标记
     */
    boolean getQuestion_id_2DirtyFlag();

    /**
     * 疑问
     */
    Integer getQuestion_id();

    void setQuestion_id(Integer question_id);

    /**
     * 获取 [疑问]脏标记
     */
    boolean getQuestion_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
