package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;

/**
 * 实体 [创建批量发票 (修理)] 存储对象
 */
public interface Repair_order_make_invoiceRepository extends Repository<Repair_order_make_invoice> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Repair_order_make_invoice> searchDefault(Repair_order_make_invoiceSearchContext context);

    Repair_order_make_invoice convert2PO(cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice domain , Repair_order_make_invoice po) ;

    cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice convert2Domain( Repair_order_make_invoice po ,cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice domain) ;

}
