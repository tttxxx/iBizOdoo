package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;

/**
 * 实体 [活动类别] 存储模型
 */
public interface Event_type{

    /**
     * 最大注册数
     */
    Integer getDefault_registration_max();

    void setDefault_registration_max(Integer default_registration_max);

    /**
     * 获取 [最大注册数]脏标记
     */
    boolean getDefault_registration_maxDirtyFlag();

    /**
     * 邮件排程
     */
    String getEvent_type_mail_ids();

    void setEvent_type_mail_ids(String event_type_mail_ids);

    /**
     * 获取 [邮件排程]脏标记
     */
    boolean getEvent_type_mail_idsDirtyFlag();

    /**
     * Twitter主题标签
     */
    String getDefault_hashtag();

    void setDefault_hashtag(String default_hashtag);

    /**
     * 获取 [Twitter主题标签]脏标记
     */
    boolean getDefault_hashtagDirtyFlag();

    /**
     * 时区
     */
    String getDefault_timezone();

    void setDefault_timezone(String default_timezone);

    /**
     * 获取 [时区]脏标记
     */
    boolean getDefault_timezoneDirtyFlag();

    /**
     * 最小注册数
     */
    Integer getDefault_registration_min();

    void setDefault_registration_min(Integer default_registration_min);

    /**
     * 获取 [最小注册数]脏标记
     */
    boolean getDefault_registration_minDirtyFlag();

    /**
     * 入场券
     */
    String getEvent_ticket_ids();

    void setEvent_ticket_ids(String event_ticket_ids);

    /**
     * 获取 [入场券]脏标记
     */
    boolean getEvent_ticket_idsDirtyFlag();

    /**
     * 出票
     */
    String getUse_ticketing();

    void setUse_ticketing(String use_ticketing);

    /**
     * 获取 [出票]脏标记
     */
    boolean getUse_ticketingDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 在线活动
     */
    String getIs_online();

    void setIs_online(String is_online);

    /**
     * 获取 [在线活动]脏标记
     */
    boolean getIs_onlineDirtyFlag();

    /**
     * 活动类别
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [活动类别]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 自动确认注册
     */
    String getAuto_confirm();

    void setAuto_confirm(String auto_confirm);

    /**
     * 获取 [自动确认注册]脏标记
     */
    boolean getAuto_confirmDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 在网站上显示专用菜单
     */
    String getWebsite_menu();

    void setWebsite_menu(String website_menu);

    /**
     * 获取 [在网站上显示专用菜单]脏标记
     */
    boolean getWebsite_menuDirtyFlag();

    /**
     * 使用默认主题标签
     */
    String getUse_hashtag();

    void setUse_hashtag(String use_hashtag);

    /**
     * 获取 [使用默认主题标签]脏标记
     */
    boolean getUse_hashtagDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有限名额
     */
    String getHas_seats_limitation();

    void setHas_seats_limitation(String has_seats_limitation);

    /**
     * 获取 [有限名额]脏标记
     */
    boolean getHas_seats_limitationDirtyFlag();

    /**
     * 自动发送EMail
     */
    String getUse_mail_schedule();

    void setUse_mail_schedule(String use_mail_schedule);

    /**
     * 获取 [自动发送EMail]脏标记
     */
    boolean getUse_mail_scheduleDirtyFlag();

    /**
     * 使用默认时区
     */
    String getUse_timezone();

    void setUse_timezone(String use_timezone);

    /**
     * 获取 [使用默认时区]脏标记
     */
    boolean getUse_timezoneDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
