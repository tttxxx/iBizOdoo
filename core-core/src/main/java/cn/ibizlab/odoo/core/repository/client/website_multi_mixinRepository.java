package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_multi_mixin;

/**
 * 实体[website_multi_mixin] 服务对象接口
 */
public interface website_multi_mixinRepository{


    public website_multi_mixin createPO() ;
        public void get(String id);

        public void create(website_multi_mixin website_multi_mixin);

        public void remove(String id);

        public void createBatch(website_multi_mixin website_multi_mixin);

        public List<website_multi_mixin> search();

        public void updateBatch(website_multi_mixin website_multi_mixin);

        public void update(website_multi_mixin website_multi_mixin);

        public void removeBatch(String id);


}
