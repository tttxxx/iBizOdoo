package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_label;

/**
 * 实体[survey_label] 服务对象接口
 */
public interface survey_labelRepository{


    public survey_label createPO() ;
        public void remove(String id);

        public void updateBatch(survey_label survey_label);

        public void update(survey_label survey_label);

        public void get(String id);

        public void removeBatch(String id);

        public void createBatch(survey_label survey_label);

        public List<survey_label> search();

        public void create(survey_label survey_label);


}
