package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;

/**
 * 实体 [网站重定向] 存储对象
 */
public interface Website_redirectRepository extends Repository<Website_redirect> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_redirect> searchDefault(Website_redirectSearchContext context);

    Website_redirect convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect domain , Website_redirect po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect convert2Domain( Website_redirect po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect domain) ;

}
