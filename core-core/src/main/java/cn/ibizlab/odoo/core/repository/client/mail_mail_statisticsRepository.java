package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mail_statistics;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
public interface mail_mail_statisticsRepository{


    public mail_mail_statistics createPO() ;
        public void remove(String id);

        public List<mail_mail_statistics> search();

        public void updateBatch(mail_mail_statistics mail_mail_statistics);

        public void create(mail_mail_statistics mail_mail_statistics);

        public void removeBatch(String id);

        public void createBatch(mail_mail_statistics mail_mail_statistics);

        public void update(mail_mail_statistics mail_mail_statistics);

        public void get(String id);


}
