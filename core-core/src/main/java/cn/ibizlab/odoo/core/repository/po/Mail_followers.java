package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;

/**
 * 实体 [文档关注者] 存储模型
 */
public interface Mail_followers{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 子类型
     */
    String getSubtype_ids();

    void setSubtype_ids(String subtype_ids);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idsDirtyFlag();

    /**
     * 相关的文档模型名称
     */
    String getRes_model();

    void setRes_model(String res_model);

    /**
     * 获取 [相关的文档模型名称]脏标记
     */
    boolean getRes_modelDirtyFlag();

    /**
     * 相关的业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 监听器
     */
    String getChannel_id_text();

    void setChannel_id_text(String channel_id_text);

    /**
     * 获取 [监听器]脏标记
     */
    boolean getChannel_id_textDirtyFlag();

    /**
     * 监听器
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [监听器]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 相关的业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
