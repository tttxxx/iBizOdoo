package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_return_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;

/**
 * 实体 [退回拣货] 存储对象
 */
public interface Stock_return_pickingRepository extends Repository<Stock_return_picking> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context);

    Stock_return_picking convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking domain , Stock_return_picking po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking convert2Domain( Stock_return_picking po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking domain) ;

}
