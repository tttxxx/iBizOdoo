package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;

/**
 * 实体 [Production Order] 存储对象
 */
public interface Mrp_productionRepository extends Repository<Mrp_production> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_production> searchDefault(Mrp_productionSearchContext context);

    Mrp_production convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production domain , Mrp_production po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production convert2Domain( Mrp_production po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production domain) ;

}
