package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_challenge_line;

/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
public interface gamification_challenge_lineRepository{


    public gamification_challenge_line createPO() ;
        public void create(gamification_challenge_line gamification_challenge_line);

        public void update(gamification_challenge_line gamification_challenge_line);

        public List<gamification_challenge_line> search();

        public void updateBatch(gamification_challenge_line gamification_challenge_line);

        public void removeBatch(String id);

        public void get(String id);

        public void createBatch(gamification_challenge_line gamification_challenge_line);

        public void remove(String id);


}
