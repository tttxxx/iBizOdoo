package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_surveyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_surveyOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_surveyClientModel;

/**
 * 实体[问卷] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_surveyServiceImpl implements ISurvey_surveyService {

    @Autowired
    survey_surveyOdooClient survey_surveyOdooClient;


    @Override
    public Survey_survey get(Integer id) {
        survey_surveyClientModel clientModel = new survey_surveyClientModel();
        clientModel.setId(id);
		survey_surveyOdooClient.get(clientModel);
        Survey_survey et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_survey();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Survey_survey et) {
        survey_surveyClientModel clientModel = convert2Model(et,null);
		survey_surveyOdooClient.create(clientModel);
        Survey_survey rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_survey> list){
    }

    @Override
    public boolean update(Survey_survey et) {
        survey_surveyClientModel clientModel = convert2Model(et,null);
		survey_surveyOdooClient.update(clientModel);
        Survey_survey rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_survey> list){
    }

    @Override
    public boolean remove(Integer id) {
        survey_surveyClientModel clientModel = new survey_surveyClientModel();
        clientModel.setId(id);
		survey_surveyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_survey> searchDefault(Survey_surveySearchContext context) {
        List<Survey_survey> list = new ArrayList<Survey_survey>();
        Page<survey_surveyClientModel> clientModelList = survey_surveyOdooClient.search(context);
        for(survey_surveyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_survey>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_surveyClientModel convert2Model(Survey_survey domain , survey_surveyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_surveyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("users_can_go_backdirtyflag"))
                model.setUsers_can_go_back(domain.getUsersCanGoBack());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("tot_start_surveydirtyflag"))
                model.setTot_start_survey(domain.getTotStartSurvey());
            if((Boolean) domain.getExtensionparams().get("result_urldirtyflag"))
                model.setResult_url(domain.getResultUrl());
            if((Boolean) domain.getExtensionparams().get("print_urldirtyflag"))
                model.setPrint_url(domain.getPrintUrl());
            if((Boolean) domain.getExtensionparams().get("quizz_modedirtyflag"))
                model.setQuizz_mode(domain.getQuizzMode());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("tot_comp_surveydirtyflag"))
                model.setTot_comp_survey(domain.getTotCompSurvey());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("public_url_htmldirtyflag"))
                model.setPublic_url_html(domain.getPublicUrlHtml());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("user_input_idsdirtyflag"))
                model.setUser_input_ids(domain.getUserInputIds());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("page_idsdirtyflag"))
                model.setPage_ids(domain.getPageIds());
            if((Boolean) domain.getExtensionparams().get("auth_requireddirtyflag"))
                model.setAuth_required(domain.getAuthRequired());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("tot_sent_surveydirtyflag"))
                model.setTot_sent_survey(domain.getTotSentSurvey());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("designeddirtyflag"))
                model.setDesigned(domain.getDesigned());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("titledirtyflag"))
                model.setTitle(domain.getTitle());
            if((Boolean) domain.getExtensionparams().get("thank_you_messagedirtyflag"))
                model.setThank_you_message(domain.getThankYouMessage());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("public_urldirtyflag"))
                model.setPublic_url(domain.getPublicUrl());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("is_closeddirtyflag"))
                model.setIs_closed(domain.getIsClosed());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("email_template_id_textdirtyflag"))
                model.setEmail_template_id_text(domain.getEmailTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("email_template_iddirtyflag"))
                model.setEmail_template_id(domain.getEmailTemplateId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_survey convert2Domain( survey_surveyClientModel model ,Survey_survey domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_survey();
        }

        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getUsers_can_go_backDirtyFlag())
            domain.setUsersCanGoBack(model.getUsers_can_go_back());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTot_start_surveyDirtyFlag())
            domain.setTotStartSurvey(model.getTot_start_survey());
        if(model.getResult_urlDirtyFlag())
            domain.setResultUrl(model.getResult_url());
        if(model.getPrint_urlDirtyFlag())
            domain.setPrintUrl(model.getPrint_url());
        if(model.getQuizz_modeDirtyFlag())
            domain.setQuizzMode(model.getQuizz_mode());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getTot_comp_surveyDirtyFlag())
            domain.setTotCompSurvey(model.getTot_comp_survey());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getPublic_url_htmlDirtyFlag())
            domain.setPublicUrlHtml(model.getPublic_url_html());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getUser_input_idsDirtyFlag())
            domain.setUserInputIds(model.getUser_input_ids());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getPage_idsDirtyFlag())
            domain.setPageIds(model.getPage_ids());
        if(model.getAuth_requiredDirtyFlag())
            domain.setAuthRequired(model.getAuth_required());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getTot_sent_surveyDirtyFlag())
            domain.setTotSentSurvey(model.getTot_sent_survey());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getDesignedDirtyFlag())
            domain.setDesigned(model.getDesigned());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getTitleDirtyFlag())
            domain.setTitle(model.getTitle());
        if(model.getThank_you_messageDirtyFlag())
            domain.setThankYouMessage(model.getThank_you_message());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getPublic_urlDirtyFlag())
            domain.setPublicUrl(model.getPublic_url());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getIs_closedDirtyFlag())
            domain.setIsClosed(model.getIs_closed());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getEmail_template_id_textDirtyFlag())
            domain.setEmailTemplateIdText(model.getEmail_template_id_text());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getEmail_template_idDirtyFlag())
            domain.setEmailTemplateId(model.getEmail_template_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



