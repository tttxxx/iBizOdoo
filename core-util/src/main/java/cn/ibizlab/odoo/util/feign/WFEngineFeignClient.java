package cn.ibizlab.odoo.util.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 工作流引擎服务
 */
public interface WFEngineFeignClient<T> extends FeignClientBase {

    /**
     * 工作流启动
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfstart")
    public T wfStart(@RequestBody T et);

    /**
     * 工作流提交处理
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfsubmit")
    public T wfSubmit(T et);

    /**
     * 工作流关闭
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfclose")
    public T wfClose(T et);

    /**
     * 工作流跳转
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfgoto")
    public T wfGoto(T et);

    /**
     * 工作流重新启动
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfrestart")
    public T wfRestart(T et);

    /**
     * 工作流撤回
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfrollback")
    public T wfRollback(T et);

    /**
     * 工作流退回
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfsendback")
    public T wfSendBack(T et);

    /**
     * 工作流重新分配（处理人）
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfreassign")
    public T wfReassign(T et);

    /**
     * 工作流标记为已读
     */
	@RequestMapping(method = RequestMethod.POST, value = "/wfmarkread")
    public T wfMarkRead(T et);

}
