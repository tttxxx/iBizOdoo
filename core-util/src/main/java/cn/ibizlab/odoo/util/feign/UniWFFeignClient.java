package cn.ibizlab.odoo.util.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.ibizlab.odoo.util.domain.WFInstance;

/**
 * 统一工作流服务
 */
public interface UniWFFeignClient extends FeignClientBase {

   /**
     * 创建工作流实例
     */
   @RequestMapping(method = RequestMethod.POST, value = "/wfinstances/")
   public WFInstance createWFInstance();

   /**
    * 权限检查
    */
   @RequestMapping(method = RequestMethod.POST, value = "/checkPermisson/")
   public boolean checkPermission();
}
