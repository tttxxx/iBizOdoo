package cn.ibizlab.odoo.util.filter;

import lombok.Data;
import cn.ibizlab.odoo.util.enums.SearchFieldType;

@Data
public class SearchFieldFilter extends SearchFilter{

	private String param ;//参数名称
	private SearchFieldType condition ;//操作符号
	private Object value ;//参数值
	
}
