package cn.ibizlab.odoo.util.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(SysInfoConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(SysInfoProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class SysInfoAutoConfiguration {

}
