package cn.ibizlab.odoo.util.security.userdetail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.security.core.GrantedAuthority;

import lombok.Data;

@Data
public class LoginUser extends org.springframework.security.core.userdetails.User {

    private String personId ;
    private String userIconPath ;
    private String userMode ;
    private Locale locale ;
    private TimeZone timeZone ;
    private String orgUserId ;
    private String orgUserName ;
    private String orgId ;
    private String orgName ;
    private String orgDeptId ;
    private String orgDeptName ;
    private List<Map> orgDepts = new ArrayList<Map>() ;
    private boolean isSuperUser ;

    public LoginUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

}

