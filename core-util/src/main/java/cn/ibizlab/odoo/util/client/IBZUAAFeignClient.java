package cn.ibizlab.odoo.util.client;

import cn.ibizlab.odoo.util.security.AuthenticationUser;
import cn.ibizlab.odoo.util.security.AuthorizationLogin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import java.util.Map;

@FeignClient(value = "ibzuaa-api",fallback = IBZUAAFallback.class)
public interface IBZUAAFeignClient
{
	/**
	 * 推送系统权限数据到uaa
	 * @param systemPermissionData
	 * @param systemId
	 * @return
	 */
	@PostMapping("/uaa/permission/save")
	JSONObject pushSystemPermissionData(@RequestBody Map<String, Object> systemPermissionData,  @RequestParam("systemid") String systemId);

	/**
	 * 用户登录
	 * @param authorizationLogin 登录信息
	 * @return
	 */
	@PostMapping(value = "/uaa/login")
	AuthenticationUser login(@RequestBody AuthorizationLogin authorizationLogin);


	@PostMapping(value = "/uaa/loginbyusername")
	AuthenticationUser loginByUsername(@RequestBody String username);
}
