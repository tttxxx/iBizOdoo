package cn.ibizlab.odoo.util.enums;

public enum SearchFieldType {
    /**
     * 包含
     */
    LIKE("like"),
    /**
     * 左包含
     */
    LEFTLIKE("leftlike"),
    /**
     * 右包含
     */
    RIGHTLIKE("rightlike"),
    /**
     * 等于
     */
    EQ("eq"),
    /**
     * 不等于
     */
    NOTEQ("noteq"),
    /**
     * 大于
     */
    GT("gt"),
    /**
     * 大于等于
     */
    GTANDEQ("gtandeq"),
    /**
     * 小于
     */
    LT("lt"),
    /**
     * 小于等于
     */
    LTANDEQ("ltandeq"),
    /**
     * 不为空
     */
    ISNOTNULL("isnotnull"),
    /**
     * 为空
     */
    ISNULL("isnull"),
    /**
     * 在范围内
     */
    IN("in"),
    /**
     * 不在范围内
     */
    NOTIN("notin") ;
    
    
    private String type;

	SearchFieldType(String type) {
		this.type = type;
	}
}