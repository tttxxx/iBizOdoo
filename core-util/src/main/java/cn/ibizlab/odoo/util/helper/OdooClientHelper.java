package cn.ibizlab.odoo.util.helper;

import lombok.Data;
import org.apache.commons.codec.digest.DigestUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OdooClientHelper {

    public static final int CMD_CREATE = 0 ;
    public static final int CMD_UPDATE = 1 ;
    public static final int CMD_REMOVE = 2 ;
    public static final int CMD_REMOVE2 = 3 ;
    public static final int CMD_ADD = 4 ;
    public static final int CMD_REMOVEALL = 5 ;
    public static final int CMD_REPLACE = 6 ;

    /**
     * 生成动态密码
     * @param curUserName
     * @param sercret
     * @return
     */
    public static String getCurUserdynaPass(String curUserName,String sercret){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH");
        String curTime = sdf.format(new Date());
        String dyncPassWord = DigestUtils.md5Hex(curUserName + curTime + sercret );
        return dyncPassWord;
    }

    public static CurOdooUser newCurOdooUser(){
        return new CurOdooUser();
    }

    @Data
    public static final class CurOdooUser
    {
        private int userId;

        private String pass;
    }
}