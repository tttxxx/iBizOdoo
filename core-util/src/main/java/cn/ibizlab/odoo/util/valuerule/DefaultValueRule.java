package cn.ibizlab.odoo.util.valuerule;

import cn.ibizlab.odoo.util.valuerule.condition.VRGroupCondition;
import cn.ibizlab.odoo.util.valuerule.condition.VRStringLengthCondition;
import cn.ibizlab.odoo.util.valuerule.condition.VRSysValueRuleCondition;
import cn.ibizlab.odoo.util.valuerule.condition.VRValueRecursionCondition;
import lombok.extern.slf4j.Slf4j;

/**
 * 默认值规则
 *
 * @param <T>
 */
@Slf4j
public class DefaultValueRule<T> extends ValueRule<T> {

    public DefaultValueRule(String name, String ruleInfo, String field, T value) {
        super(name, ruleInfo, field, value);

    }

    public DefaultValueRule<T> init(Integer strLength, String dupCheckMode, String dupCheckValues, String baseRule, Boolean isCheckRecursion) {
        initStrLength(strLength);
        initBaseRule(baseRule);
        initRecursion(isCheckRecursion);
        initDupCheck(dupCheckMode, dupCheckValues);
        return this;
    }

    /**
     * 字符串长度
     *
     * @param length 限制长度
     */
    public void initStrLength(Integer length) {
        if (length != null && length != -1) {
            VRStringLengthCondition<T> stringLengthCondition = new VRStringLengthCondition<>("字符长度", false, String.format("内容长度必须小于等于[%s]", length), value)
                    .init(null, length, false, true);
            super.groupCondition.add(stringLengthCondition);

        }
    }

    /**i
     * 基础值规则校验。
     *
     * @param ruleName 值规则名
     */
    public void initBaseRule(String ruleName) {
        if (ruleName != null) {
            VRSysValueRuleCondition<T> sysValueRuleCondition = new VRSysValueRuleCondition<>(ruleName, false, ruleInfo, value);
            super.groupCondition.add(sysValueRuleCondition);
        }
    }

    /**
     * 递归检查
     *
     * @param isCheckRecursion 是否递归检查
     */
    public void initRecursion(boolean isCheckRecursion) {
        if (isCheckRecursion) {
            VRValueRecursionCondition<T> vrValueRecursionCondition = new VRValueRecursionCondition<>(name, false, ruleInfo, value);
            super.groupCondition.add(vrValueRecursionCondition);
        }
    }

    /**
     * 重复值检查
     *
     * @param dupCheckMode
     * @param dupCheckValues
     */
    public void initDupCheck(String dupCheckMode, String dupCheckValues) {
//        private String dupCheckMode;//重复值检查 getDupCheckMode()
//        //重复检查值范围 getDupCheckValues()
//        private String dupCheckValues;
//        //重复检查范围属性   getDupCheckPSDEField()
    }
}
