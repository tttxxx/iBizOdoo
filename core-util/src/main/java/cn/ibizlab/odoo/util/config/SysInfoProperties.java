package cn.ibizlab.odoo.util.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.beans.factory.annotation.Value;
import lombok.Data;

@Data
@ConfigurationProperties
public class SysInfoProperties {

	/**
     * 部署系统标识
     */
    @Value("${ibiz.systemid:#{null}}")
    String systemid;

	/**
     * 部署域标识
     */
    @Value("${ibiz.domainid:#{null}}")
    String domainid;

    /**
     * 部署域名称
     */
    @Value("${ibiz.domainname:#{null}}")
    String domainname;

    /**
	 * 服务标识
	 */
	@Value("${spring.application.name:#{null}}")
	String serviceid;

	/**
	 * 服务路径
	 */
	@Value("${spring.cloud.nacos.discovery.server-addr:#{null}}")
	String serviceurl;

	/*
	 * 系统逻辑名
	 */
    @Value("${ibiz.systemlogicname:#{null}}")
	String systemlogicname;

	/**
     * 部署系统名称
     */
    @Value("${ibiz.systemname:#{null}}")
    String systemname;

    /**
     * 部署系统名称
     */
    @Value("${ibiz.sysorgid:#{null}}")
    String sysorgid;

    /**
     * 部署系统名称
     */
    @Value("${ibiz.sysorgdeptid:#{null}}")
    String sysorgdeptid;

    /**
     * 部署系统名称
     */
    @Value("${ibiz.systype:#{null}}")
    String systype;

    /**
	 * 应用标识
	 */
	@Value("${ibiz.appid:#{null}}")
	String appid;

	/*
	 * 应用逻辑名
	 */
	@Value("${ibiz.applogicname:#{null}}")
	String applogicname;

	/*
	 * 应用url
	 */
	@Value("${app.web.url:#{null}}")
	String appurl;

	/*
	 * 工作流开始地址
	 */
	@Value("${app.web.wfstarturl:#{null}}")
	String wfstarturl;

	/*
	 * 工作流查看地址
	 */
	@Value("${app.web.wfviewurl:#{null}}")
	String wfviewurl;

	/*
	 * 业务查看地址
	 */
	@Value("${app.web.proxyviewurl:#{null}}")
	String proxyviewurl;

	/**
     * 应用名称
     */
    @Value("${ibiz.appname:#{null}}")
    String appname;

    /*
	 * 逻辑名
	 */
    @Value("${ibiz.domainlogicname:#{null}}")
	String domainlogicname;

	/*
	 * 工作流服务路径
	 */
    @Value("${ibiz.wfrootpath:#{null}}")
	String wfrootpath;

	public String getSysorgid() {
	    if (sysorgid != null && sysorgid.isEmpty()) {
	        sysorgid = null;
	    }
		return sysorgid;
	}

	public String getSysorgdeptid() {
	    if (sysorgdeptid != null && sysorgdeptid.isEmpty()) {
	        sysorgdeptid = null;
	    }
		return sysorgdeptid;
	}

	public String getAppurl() {
	    if (appurl == null) {
	        return null;
	    }
	    if (appurl.endsWith("/")) {
        	appurl = appurl.substring(0, appurl.lastIndexOf("/"));
        }
        return appurl;
	}
	public String getWfstarturl() {
	    if (wfstarturl == null) {
	        return null;
	    }
	    if (!wfstarturl.startsWith("/")) {
        	wfstarturl = "/" + wfstarturl;
        }
        return wfstarturl;
	}

	public String getWfviewurl() {
	    if (wfviewurl == null) {
	        return null;
	    }
	    if (!wfviewurl.startsWith("/")) {
        	wfviewurl = "/" + wfviewurl;
        }
        return wfviewurl;
	}

	public String getProxyviewurl() {
	    if (proxyviewurl == null) {
	        return null;
	    }
	    if (!proxyviewurl.startsWith("/")) {
        	proxyviewurl = "/" + proxyviewurl;
        }
        return proxyviewurl;
	}

}
