package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.valuerule.utils.SysValueRule;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 系统值规则条件
 * @param <T>
 */
@Slf4j
@Data
@IBIZLog
public class VRSysValueRuleCondition<T> extends VRSingleCondition<T> {

    public VRSysValueRuleCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }
    public VRSingleCondition<T> init(){
        return this;
    }

    @Override
    public boolean validate() {
        try {
            String targetMethod = this.name.replace("[系统值规则]", "").replace("[!]", "");
            boolean isValid = SysValueRule.validateByName(targetMethod, String.valueOf(value));
            return isValid;
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("没有找到对应的系统值规则实现，规则名:【" + name + "】");
        }
        return false;
    }

}
