package cn.ibizlab.odoo.util;

import java.util.ArrayList;
import java.util.List;

import cn.ibizlab.odoo.util.enums.SearchGroupType;

import lombok.Data;

@Data
public class SearchGroupFilter extends SearchFilter{

    private SearchGroupType searchGroupType ;
	
	/**
	 * 条件
	 */
	private List<SearchFilter> condition = new ArrayList<SearchFilter>() ;
	
}
