package cn.ibizlab.odoo.util.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(UniWFClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(UniWFClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class UniWFClientAutoConfiguration {

}
